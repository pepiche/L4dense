% plot spines distribution along dendrites
outDir = '/tmpscratch/sahilloo/L4/data/';
n=load('/tmpscratch/sahilloo/L4/data/dendritesPostQueryStateLengths.mat');
dendriteLengths = n.aggloLens./1E3; % um
m=load('/tmpscratch/amotta/l4/spine-attachment/20170725T200449/spine-heads-and-attachment.mat');
dendritesExt = m.dendAgglos;
sh = arrayfun(@(x) m.shAgglos(m.attached==x),[1:length(dendritesExt)]','uni',0);
shCount = cellfun(@length,sh);

% spine density
shDensity = shCount./dendriteLengths; %per um
% replace NaN with zero spine density
shDensity(isnan(shDensity))=0; %zero spines
% shDensity(shDensity == Inf)

% make a nice table
myTable = table(dendritesExt,dendriteLengths,sh,shCount,shDensity);
save(fullfile(outDir,'spineDistribution.mat'),'spineTable');

%% plot per agglo sh numbers
[~,idxSort] = sort(dendriteLengths);
subplot(3,1,1)
% plot(log(dendriteLengths(idxSort)));
plot(dendriteLengths(idxSort)./1E3); % mm
ylabel('Path lengths sorted (log)')
subplot(3,1,2)
plot(shCount(idxSort));
ylabel('# spineheads per dendrite')
subplot(3,1,3)
plot(log(shCount(idxSort)));
ylabel('# spineheads per dendrite(log)')
xlabel('Dendrite agglos sorted by path lengths')
hold off;

%% plot spine density per um
figure;
histogram(shDensity);
xlabel('Spine density: # spines per um');
ylabel('# dendrite agglos');
hold on
% plot spine density for >10 um agglos
toPlot = myTable{:,2}>10;
histogram(shDensity(toPlot))
legend({'With all agglos','With agglos > 10um'});

