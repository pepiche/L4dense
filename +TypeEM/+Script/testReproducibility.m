% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear

%% Configuration
rootDir = '/gaba/u/mberning/results/pipeline/20170217_ROI';

info = Util.runInfo();
Util.showRunInfo(info);

%% Loading data
param = load(fullfile(rootDir, 'allParameter.mat'));
param = param.p;

segClassifiers = TypeEM.Classifier.train(param, 'segment');
aggloClassifiers = TypeEM.Classifier.train(param, 'segmentAgglo');

%% Calculate and plot precision / recall curve
[segPrecRec, segFig] = ...
    TypeEM.Classifier.test(param, segClassifiers);

[aggloPrecRec, aggloFig] = ...
    TypeEM.Classifier.test(param, aggloClassifiers);

precRec = aggloPrecRec;
precRec.spinehead = segPrecRec.spinehead;

%% Calculate key numbers
% Precision and recall at point of maximal area-under-curve
optimPrecRec = structfun( ...
    @(pr) pr(Util.nthOutput(2, @() max(prod(pr, 2))), :), ...
    precRec, 'UniformOutput', false);

fprintf('Precision / recall at max. AUC\n');
fprintf('\n'); disp(optimPrecRec);
