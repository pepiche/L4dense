% Tests for L4.Synapse package
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

function tests = testSynapsePackage()
tests = functiontests(localfunctions);
end

function testSynapseAggloMapping(test)

% 4 agglos (2 pre and 2 post)
presynAgglo = {(1:5)'; (6:10)'};
postsynAgglo = {(11:15)'; (16:20)'};

% 9 synapses (partially overlapping with multiple agglos
synapses.presynId = {1; 2; [1; 1]; [1; 2]; 6; 7; 3; [5; 6]; 9};
synapses.postsynId = {11; 12; 11; 12; 16; 17; 18; 11; [15; 16]};
synapses = struct2table(synapses);

[ syn2Agglo, pre2syn, post2syn ] = ...
    L4.Synapses.synapsesAggloMapping(synapses, presynAgglo, postsynAgglo);

verifyEqual(test, syn2Agglo.syn2pre, {1; 1; 1; 1; 2; 2; 1; [1; 2]; 2});
verifyEqual(test, syn2Agglo.syn2post, {1; 1; 1; 1; 2; 2; 2; 1; [1; 2]});
verifyEqual(test, pre2syn, {[1; 2; 3; 4; 7; 8]; [5; 6; 8; 9]});
verifyEqual(test, post2syn, {[1; 2; 3; 4; 8; 9]; [5; 6; 7; 9]});

end

