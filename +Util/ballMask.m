function [ h ] = ballMask( r, voxelSize )
%BALLMASK Returns the logical mask of a 3d ball of a given radius.
% INPUT r: Double ball radius.
%       voxelSize: (Optional) [3x1] double specifying the voxel size in each
%           dimension.
%           (Default: [1, 1, 1])
% OUTPUT h: 3d logical mask for the ball structing element.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('voxelSize','var') || isempty(voxelSize)
    voxelSize = ones(3,1);
end

%construct ball
siz = ceil(r./voxelSize);
[x,y,z] = meshgrid(-siz(1):siz(1),-siz(2):siz(2),-siz(3):siz(3));
h = (voxelSize(1).*x).^2 + (voxelSize(2).*y).^2 + (voxelSize(3).*z).^2 ...
    <= r^2;

end

