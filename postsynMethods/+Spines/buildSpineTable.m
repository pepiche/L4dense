function spineTable = buildSpineTable(config,plotFlag)
% Description:
%   Collect all dendrite agglos, their corresponding lengths, spines. spine density
if ~exist('plotFlag','var') || isempty(plotFlag)
    plotFlag = true;
end

outDir = config.outDir;
outfile = fullfile(outDir,'spineTable.mat');

m = load(config.param);
p = m.p;

% load dendrite agglos
Util.log('Loading dendrite agglos');
m=load(config.dendriteFile);
dendAgglosAll =  Superagglos.getSegIds(m.dendrites);
%aggloSizes = Agglo.calcVolumes(p,dendAgglosAll);
%idxCand = aggloSizes(:) > (10 ^ 5.5);
%dendAgglosWoSh = dendAgglosAll(idxCand);

% spine attachment
Util.log('Collecting spine attachments for all dendrites')
m=load(config.spineFile);
dendAgglos = cellfun(@(x) double(x),m.dendAgglos,'uni',0);
shAgglos = cellfun(@(x) double(x),m.shAgglos,'uni',0);
attached = m.attached;
indBigDends = m.indBigDends;

sh = arrayfun(@(x) shAgglos(attached==x),(1:length(dendAgglos))','uni',0);
shCount = cellfun(@length,sh);

sh = sh(indBigDends);
shCount = shCount(indBigDends);
dendritesExt = dendAgglos(indBigDends);

% calculate dendrite lengths
Util.log('Calculating path lengths w/o spines');
idxForPl = Agglo.findAggloAcrossVersions(p,dendritesExt,dendAgglosAll);

inputfile = fullfile(outDir,'dendriteLengths.mat');
if exist(inputfile,'file')
    m = load(inputfile);
    dendriteLengths = m.dendriteLengths;
else
    dendriteLengths = Util.calcPathLengths(p,...
        dendAgglosAll(idxForPl))./1E3;
    save(inputfile,'dendriteLengths');
end

shDensity = shCount./dendriteLengths;
shDensity(isnan(shDensity))=0; %zero spines
spineTable = table(dendritesExt,dendriteLengths,sh,shCount,shDensity);

Util.log('Plotting...');
if plotFlag
    %% plot per agglo sh numbers
    if ~exist([outDir 'figures'],'dir')
        mkdir([outDir 'figures']);
    end 
    [~,idxSort] = sort(dendriteLengths);
    fig = figure;
    subplot(3,1,1)
    plot(log(dendriteLengths(idxSort)));
    ylabel('path lengths (log)')
    subplot(3,1,2)
    plot(shCount(idxSort));
    ylabel('# spineheads')
    subplot(3,1,3)
    plot(log(shCount(idxSort)));
    ylabel('# spineheads (log)')
    xlabel('Dendrite agglos sorted by path lengths')
    saveas(fig,fullfile(outDir,'figures/spineDistribution.png'))
    close all
    %% plot spine density per um
    fig = figure;
    histogram(shDensity);
    xlabel('Spine density: # spines per um');
    ylabel('# dendrite agglos');
    hold on
    % plot spine density for >10 um agglos
    toPlot = spineTable{:,2}>10;
    histogram(shDensity(toPlot))
    legend({'With all agglos','With agglos > 10um'});
    saveas(fig,fullfile(outDir,'figures/spineDensity.png'))
    close all
end
Util.log('Saving results to spineTable');                                                                                               
info = Util.runInfo(false);
save(outfile,'spineTable','info');

end
