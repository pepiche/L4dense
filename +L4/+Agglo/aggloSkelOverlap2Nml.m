function [ skelsOV ] = aggloSkelOverlap2Nml( skels, p, dendrites, axons, segCom )
%AGGLOSKELOVERLAP2NML Write the GT skeletons with all overlapping agglos to
% nml for inspection.
% INPUT skels: [Nx1] cell of skeleton objects
%           The ground truth skeletons.
%       p: struct or [Nx1] cell
%           Segmentation parameter struct to load the segmentation ids or
%           diretly the segment ids for skels (i.e. the output of
%           Skeleton.getSegmentIdsOfSkelCellArray(p, skels))
%       dendrites, axons: [Nx1] struct
%           The agglos used for overlap calculation in the superagglo
%           format.
%       segCom: [Nx3] int
%           Coordinates of single segment coms for segments not contained
%           in the dendrite or axon agglos.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


if isstruct(p)
    [skelToDendrites, segIds] = L4.Agglo.aggloSkelOverlap(skels, p, ...
        dendrites);
else
    segIds = p;
    [skelToDendrites, ~] = L4.Agglo.aggloSkelOverlap(segIds, [], ...
        dendrites);
end

[skelToAxons, ~] = L4.Agglo.aggloSkelOverlap(segIds, [], axons);

% determine ids not in dendrites or axons
maxId = max(cellfun(@(x)max(x{1}), segIds));
restIds = setdiff(1:maxId, cell2mat(cat(1, ...
    Superagglos.getSegIds(dendrites), ...
    Superagglos.getSegIds(axons))));
restIdLUT = false(maxId, 1);
restIdLUT(restIds) = true;

% write to skeletons
skelsOV = cell(length(skels), 1);
for i = 1:length(skels)
    skel = skels{i};
    skel.verbose = false;
    skel = L4.Agglo.superAgglo2Skel(dendrites(skelToDendrites{i}(:, 1)), skel);
    skel.names(2:end) = arrayfun(@(x)sprintf('DenAgglo_%d', x), ...
        skelToDendrites{i}(:,1), 'uni', 0);
    l = skel.numTrees();
    skel = L4.Agglo.superAgglo2Skel(axons(skelToAxons{i}(:,1)), skel);
    skel.names(l+1:end) = arrayfun(@(x)sprintf('AxonAgglo_%d', x), ...
        skelToAxons{i}(:,1), 'uni', 0);
    ids = segIds{i}{1}(segIds{i}{1} > 0);
    singleSegs = ids(restIdLUT(ids));
    l = skel.numTrees();
    skel = skel.addNodesAsTrees(segCom(singleSegs, :));
    skel.names(l+1:end) = arrayfun(@(x)sprintf('Seg_%d', x), ...
        singleSegs, 'uni', 0);
    skelsOV{i} = skel;
end

end

