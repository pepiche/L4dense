function str = toCamelCase(str)
    % str = toCamelCase(str)
    %   Converts the given string to camel-case.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % return if string is empty
    if isempty(str); return; end
    
    % capitalize first letter and all
    % other letters following a whitespace
    capMask = isletter(str) & [true, isspace(str(1:(end - 1)))];
    str(capMask) = upper(str(capMask));
end
