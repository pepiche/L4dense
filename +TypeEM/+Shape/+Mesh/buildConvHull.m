function outMesh = buildConvHull(inMesh, simplify)
    % BUILDCONVHULL Builds the convex hull aroung the
    % given surface triangulation
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    verts = inMesh.vertices;
    
    % calculate convex hull
    convHull = convhull( ...
        verts(:, 1), verts(:, 2), verts(:, 3), ...
        'simplify', simplify);
    
    % build new mesh
    outMesh = struct;
    outMesh.vertices = verts;
    outMesh.faces = convHull;
    
    % clean up mesh
    outMesh = TypeEM.Shape.Mesh.clean(outMesh);
end
