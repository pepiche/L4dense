% run config using the svm predictions except sj and all paper features
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% data and save folder
dataFolder = ['/gaba/u/bstaffle/data/SynEM/' ...
    'SynapseDetectionTrainingData_2_typeLabels'];
saveFolder = ['/tmpscratch/bstaffle/data/SynEM/Classifier/' ...
    'runShaft/cnn17_2_synem_noSJ_trShaft'];

% feature map
m = load('/gaba/u/bstaffle/data/SyConn/cnn17_2_enn.mat', 'cnet');
cnet = m.cnet;
fm = SynEM.getFeatureMap('PaperAndCNN', cnet, [], {3:4});

% training options
options.useGPU = true;
options.shaftSynapseTraining = true;

% cluster
cluster = getCluster('gpu');

% start training
SynEM.Training.trainingPipeline(dataFolder, saveFolder, fm, [], [], ...
    options, cluster);
