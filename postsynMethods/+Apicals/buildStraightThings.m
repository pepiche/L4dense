setConfig;
config.outDir = '/tmpscratch/sahilloo/L4/forIso/';
outDir = config.outDir;
config.dendriteFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_03.mat';
config.spineFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/spine_heads_and_attachment_03.mat';

Util.log('Loading stuff...');
config
m=load(config.param);
p = m.p;

m=load(config.dendriteFile);
indBigDends = m.indBigDends;
dendritesSA = m.dendrites(indBigDends);
dendrites = Superagglos.getSegIds(m.dendrites(indBigDends));
 
tol = 500;
bounds = p.bbox;

Util.log('filtering touch to up and down...')
tic;
% extract boundaries (contact sites)
xbounds = bounds(1,:); ybounds = bounds(2,:); zbounds = bounds(3,:);

m = load(config.meta);
points = cellfun(@(x) m.point(:,x)', dendrites,'uni',0);


% filter out agglos that touch the border in x direction
mask = zeros(length(dendrites), 1);

for i=1:length(dendrites)
    for j=1:size(points{i},1)
        point = points{i}(j);   % only consider x
        % if border is touched at both places within an agglo
        if point <= (xbounds(1) + tol)
            p1 = points{i}(j,:);
            for k=1:size(points{i},1)
                point = points{i}(k);
                p2 = points{i}(k,:);
                if point >= (xbounds(2) - tol)
%                    if ~Apicals.moreContactPoints(points{i}, vertcat(p1,p2), tol, 0, tol, xbounds, ybounds, zbounds)
                        mask(i) = 1;
%                    end
                end
            end
        end
    end
end
idxST = find(mask==1);
straightThings = dendrites(idxST);
straightThingsSA = dendritesSA(idxST);
save(fullfile(config.outDir,'straightThingsState.mat'),'straightThings','idxST','straightThingsSA');

