function nml = loadNml(param, nmlPath)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % load dense NML tracing
    nml = NML.Dense.load(param, nmlPath);
    
    % remove mergers
    nml(nml.label == 'merger', :) = [];
    nml(nml.comment == 'merger', :) = [];
    
    % reduce to known classes
    keepClasses = { ...
        'astrocyte'; 'axon'; 'dendrite';
        'microglia'; 'myelin'; 'oligodendrocyte'};
    nml = nml(ismember(nml.label, keepClasses), :);
end

