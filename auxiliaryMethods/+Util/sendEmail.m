function [status, cmdout] = sendEmail(email,subjectText)
% Description:
%   This function allows you to send email via matlab
% Input:
%   email: string, with destination email id eg. 'sahil.loomba@brain.mpg.de'
%   subjectText: string, text for your email subject eg. 'hello world, I am a computer.'
% Output:
%   status: delivery status of the email
if ~exist('email','var') || isempty(email)
    email = extractUserEmail();
end
command = ['mail -s ', '"', subjectText, '"', ' ', email, ' ', '< /dev/null'];
[status,cmdout] = system(command);

end

function email = extractUserEmail()
    % only works for rzg users
    [~,out]= system('whoami');
    email = [out(1:end-1), '@rzg.mpg.de']; % ignore end character
end
