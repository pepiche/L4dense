function all_fns=KMBmakeFileList(C,prefix,suffix)



osc = sum(C.allslice_arr(:,3)-C.allslice_arr(:,2)+1); % # of slices

P=ceil(osc/C.slPth)*C.slPth+1;

all_fns = cell(C.M,C.N,P);
Pidx = 0;
for count = 1:size(C.allslice_arr,1)
    thisst=C.allslice_arr(count,1);
    thisimg=(C.allslice_arr(count,2)-1)*...
        (C.allslice_arr(count,5)-C.allslice_arr(count,4)+1)...
        *(C.allslice_arr(count,7)-C.allslice_arr(count,6)+1);
    for count2 = C.allslice_arr(count,2):C.allslice_arr(count,3)
        Pidx = Pidx + 1;
        for Nidx = C.allslice_arr(count,6):C.allslice_arr(count,7)
            for Midx = C.allslice_arr(count,4):C.allslice_arr(count,5)
                Naa=(1-C.allslice_arr(count,6)+C.allslice_arr(count,7))*(1-C.allslice_arr(count,4)+C.allslice_arr(count,5));
                thisst_str=sprintf('st%0.3d',thisst);
                thisimg_str=sprintf('%0.7d',thisimg);
                thisdiv_str=sprintf('%0.3d',floor(thisimg/Naa));
                
                stackdir =  fullfile(prefix, [thisst_str 'Charm'], thisst_str);
                all_fns{Nidx,Midx,Pidx} = fullfile(stackdir,strcat(C.imfn,'_',thisst_str,'_',thisimg_str,suffix));
                thisimg=thisimg+1;
            end
            if mod(Nidx,2)==0
                all_fns(Nidx,:,Pidx)=fliplr(all_fns(Nidx,:,Pidx));
            end
        end
    end
end

