function showScores(param, scoreStruct, varargin)
    % showScores(param, scoreStruct, varargin)
    %   Show a movie of the raw data and the corresponding segment
    %   probabilities. This is useful for debugging purposes.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    conf = struct;
    conf.nColors = 100;
    conf.center = transpose(mean(param.bbox, 2));
    conf.fovSize = reshape(param.tileSize, 1, []);
    conf = Util.modifyStruct(conf, varargin{:});
    
    box = bsxfun(@times, conf.fovSize(:), [-0.5, +0.5]);
    box = bsxfun(@plus, conf.center(:), box);
    box = round(box);
    
    maxSegId = Seg.Global.getMaxSegId(param);
    scoreLUT = zeros(maxSegId, 1);
    scoreLUT(scoreStruct.segIds) = scoreStruct.scores;
    
    seg = loadSegDataGlobal(param.seg, box);
    segMask = logical(seg);
    seg = double(seg);
    
    seg(segMask) = scoreLUT(seg(segMask));
    scoreEdges = linspace(0, 1, conf.nColors + 1);
    seg(segMask) = discretize(seg(segMask), scoreEdges);
    
    colors = parula(conf.nColors);
    colors = cat(1, zeros(1, 3), colors);
    colors = transpose(colors);
    
    segSize = size(seg);
    seg = colors(:, 1 + seg(:));
    seg = reshape(seg, cat(2, 3, segSize));
    seg = permute(seg, [2, 3, 1, 4]);
    
    raw = loadRawData(param.raw, box);
    raw = double(raw) / double(intmax(class(raw)));
    raw = repmat(raw, 1, 1, 1, 3);
    raw = permute(raw, [1, 2, 4, 3]);
    
    mov = cat(2, raw, seg);
    implay(mov);
end