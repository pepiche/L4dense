function CRC = calculateCRC( skels, p, edges, borderCom, eClasses, noMergerDetection )
%CALCULATECRC Calculate the correctly reconstructed components.
% INPUT skels: [Nx1] cell
%           Cell array of ground truth skeletons.
%       p: struct
%           Segmentation parameter struct.
%           An [Nx1] cell array of segment ids can be supplied instead
%           (i.e. the result of
%            Skeleton.getSegmentIdsOfSkelCellArray(p, skels)).
%       edges: [Nx2] int
%           The global edge list.
%       borderCom: [Nx3] int
%           The global border coms used for merge identification (in voxel
%           space). Only required if merger detection is done.
%       eClasses: (Optional) [Nx1] cell
%           Equivalence classes of segment ids to combine segments into
%           agglomerates.
%           (Default: direct usage of p.seg)
%       noMergerDetection: (Optional) logical
%           Flag to disable merger detection.
%           (Default: false)
% OUTPUT CRC: [Nx1] cell
%           Cell array of struct containing the CRCs for each skeleton.
%           CRCs are stored in a struct representing the set CRC(S, L).
%           'segId': Segmentation id of the corresponding CRC (L).
%           'skelL': Total skeleton path length (in units of skel.scale)
%           'L': The length of the CRC (in units of skel.scale)
%           'skel': The skeleton containing the CRC components.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('noMergerDetection', 'var') || isempty(noMergerDetection)
    noMergerDetection = false;
end

cellfun(@(x)assert(x.numTrees() == 1), skels);

% skeleton path lenghs
L = cellfun(@(x)x.pathLength(), skels);

if isstruct(p)
    segIds = Skeleton.getSegmentIdsOfSkelCellArray(p, skels);
else
    segIds = p;
end
if iscell(segIds{1})
    segIds = cellfun(@(x)x{1}, segIds, 'uni', 0);
end

% delete nodes with zero seg id using close gaps from deleteNodes
Util.log('Deleting nodes at boundary voxels.');
skels = arrayfun(@(idx) ...
    skels{idx}.deleteNodes(1, segIds{idx} == 0, true), ...
    1:length(skels), 'uni', 0);
segIds = cellfun(@(x)x(x ~= 0), segIds, 'uni', 0);

% delete out of seg bbox as well for now
Util.log('Deleting nodes outside segmentation bbox.');
skels = arrayfun(@(idx) ...
    skels{idx}.deleteNodes(1, segIds{idx} == -1, false), ...
    1:length(skels), 'uni', 0);
segIds = cellfun(@(x)x(x ~= -1), segIds, 'uni', 0);

% map to agglomeration ids and update edge list
if exist('eClasses', 'var') && ~isempty(eClasses)
    Util.log('Applying equivalence class mapping.');
    mapping = Seg.Global.eClassMapping(eClasses, edges);
    for i = 1:length(segIds)
        segIds{i} = mapping(segIds{i});
    end
    edges = mapping(edges);
    toDelEdges = edges(:,1) == edges(:,2);
    edges(toDelEdges, :) = [];
    if ~noMergerDetection
        borderCom(toDelEdges, :) = [];
    end
end


% find merger nodes
if ~noMergerDetection
    Util.log('Identifying merger segments.');
    [mergerIds, mBIdx] = identifyMergerNodes(skels, segIds, edges, borderCom);
else
    mergerIds = [];
    mBIdx = repmat({[]}, length(skels), 1);
end

CRC = cell(length(skels), 1);
for i = 1:length(skels)
    skel = skels{i};
    
    % skeleton path length
    CRC{i}.L = L(i);
    
    % delete nodes in merger segments
    mergeIdx = ismember(segIds{i}, mergerIds);
    mergeNodes = skel.nodes{1}(mergeIdx, 1:3);
    skel = skel.deleteNodes(1, mergeIdx);
    
    % delete edges that are between nodes with different segIds
    R = segIds{i}(skel.edges{1});
    toDel = R(:,1) ~= R(:,2);
    skel.edges{1}(toDel, :) = [];
    % add artificial edge for splitCC with last node
    skel.edges{1}(end+1,:) = [size(skel.nodes{1}, 1), size(skel.nodes{1}, 1)];
    
    % split skeleton into connected components (corresponding to the CRC
    % components)
    [skel, ~, nodeIdx] = skel.splitCC();
    CRC{i}.skel = skel;
    CRC{i}.mergeNodes = mergeNodes;
    CRC{i}.mergeBorderIdx = mBIdx{i};
    CRC{i}.segId = segIds{i}(cellfun(@(x)x(1), nodeIdx));
    
    % paths lengths of each components
    CRC{i}.skelL = skel.pathLength();
    
end


end

function [mergerIds, mergerBorderIdx] = identifyMergerNodes(skels, ...
    segIds, edges, borderCom)
% Identify all merger segments. For the 2.2 um criterion all borderComs of
% that segment are used as a proxy.

edgeLUT = Seg.Global.getEdgeLookupTable(edges);

% find segments that occur in several skeletons
lut = L4.Agglo.buildLUT(cellfun(@unique, segIds, 'uni', 0), [], true);
mergerIds = find(cellfun(@length, lut) > 1);

% find segments that are more then 2 um away from any skeleton (see p. 7
% bottom)
borderComNM = bsxfun(@times, double(borderCom), skels{1}.scale);
mergerBorderIdx = cell(length(skels), 1);
for i = 1:length(skels)
    mergerBorderIdx{i} = zeros(0, 1);
    
    % find segments that contain two skeleton nodes
    mulHitSegIds = tabulate(segIds{i});
    mulHitSegIds = mulHitSegIds(mulHitSegIds(:,2) > 1,1);
    mulHitSegIds = setdiff(mulHitSegIds, mergerIds);
    
    nodesNM = bsxfun(@times, skels{i}.nodes{1}(:,1:3), skels{i}.scale);
    
    % check if any border com is farther than 2.2 um away from skeleton
    distT = 2200; % in nm
    for j = 1:length(mulHitSegIds)
        thisId = mulHitSegIds(j);
        thisBordersNM = borderComNM(edgeLUT{thisId}, :);
        
        % identify borders that are within the distance threshold
        d = any(pdist2(thisBordersNM, ...
                       nodesNM(segIds{i} == thisId, :)) <= distT, 2);
                   
        % if any exceeds the distance threshold then the segment is a
        % merger
        if ~all(d)
            mergerIds = cat(1, mergerIds, thisId);
            mergerBorderIdx{i} = cat(1, mergerBorderIdx{i}, ...
                                        edgeLUT{thisId}(~d));
            continue;
        end
    end
    
end
end