function x = bbinornd(num, n, a, b)
%BBINORND Sample from beta-binomial distribution.
% INPUT num: int
%           Total number of points to sample.
%       n: int
%           Distribution parameter: Number of trials.
%       a, b: double
%           Distributions parameters (a, b > 0).
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

p = Math.Prob.bbinopdf(0:n, n, a, b);
x = randsample(n + 1, num, true, p) - 1;

end

