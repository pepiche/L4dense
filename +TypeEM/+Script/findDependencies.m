% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
info = Util.runInfo();

typeEmDir = fullfile(info.git_repos{1}.local, 'matlab', '+TypeEM');
auxMethDir = fullfile(info.git_repos{1}.local, 'matlab', 'libs', 'aux');

typeEmFiles = dir(fullfile(typeEmDir, '**', '*.m'));
typeEmFiles(cat(1, typeEmFiles.isdir)) = [];

typeEmFiles = arrayfun( ...
    @(f) fullfile(f.folder, f.name), ...
    typeEmFiles, 'UniformOutput', false);

typeEmDeps = Util.findDependencies( ...
    typeEmFiles, 'keepAuxiliaryMethods', false);
typeEmDeps(startsWith(typeEmDeps, typeEmFiles)) = [];

disp(typeEmDeps(:))
