function [ d, X ] = maxDiff( A, B, relA )
%MAXDIFF Calculate the maximal difference per location between two equally
% sized array.
% INPUT A: N-dimensional array.
%       B: N-dimensional array of same size as A.
%       relA: (Optional) logical
%           Calculate difference relative to A.
%           (Default: false)
% OUTPUT d: Maximal of the absolute value of pointwise differences between
%           two array.
%        X: [Nx2] int
%           Coordinates sorted by decreasing difference.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if exist('relA', 'var') && relA
    D = abs(A - B)./max(abs(A), abs(B));
else
    D = abs(A - B);
end

d = max(D(:));
if nargout > 1
    [~,idx] = sort(D(:), 'descend');
    [x, y] = ind2sub(size(D), idx);
    X = [x, y];
end

end

