function edgeIdx = removeNCInterfaces(edgeIdx,ncProbThr)
outDir = '/tmpscratch/sahilloo/L4/data/';
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
voxelSize = p.raw.voxelSize;
m=load([p.saveFolder 'globalGPProbList.mat']);
globalGPProbList = m.prob;

load('/tmpscratch/sahilloo/L4/data/dendritesPostQueryWithShafts.mat')
load('/tmpscratch/sahilloo/L4/data/shaftSegmentsOrdered.mat');

% remove interfaces with neurite continuity prob>50%
%ncProbThr = 0.5;
toIgn = globalGPProbList(edgeIdx)>ncProbThr;

edgeIdx(toIgn) = false;

end

