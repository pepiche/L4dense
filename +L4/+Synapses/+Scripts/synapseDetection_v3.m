% synapse detection pipeline starting from SynEM predictions with different
% classifiers for shaft and spine synapses
%
%   classifiers used:
%   - spine heads: SynEM
%   - all other interfaces: cnn17_2_synem_trShaft
%                           (see
%                           L4.Synapses.predScripts.cnn17_2_synem_trShaft)
%
% NOTE Indices in synapses are w.r.t. to the edges that were used during
%      synaptic interface clustering. This is done with the edges from the
%      graph, i.e. including correspondences.
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


info = Util.runInfo();
p = Gaba.getSegParameters('ex145_ROI2017');
ver = 'v8';

outputFile = fullfile(p.connectome.saveFolder, ...
    sprintf('SynapseAgglos_%s.mat', ver));

% output file with soma heuristic executed
outputFileSH = fullfile(p.connectome.saveFolder, ...
    sprintf('SynapseAgglos_%s_somaH.mat', ver));


%% load/set agglo files for heuristics (for reproducability)

p.agglo.axonAggloFile = fullfile(p.saveFolder, ...
    '/aggloState/axons_19_a.mat');
axons = L4.Axons.getLargeAxons(p, true, true);

p.agglo.somaFile = fullfile(p.saveFolder, 'soma_BS', 'somaAgglo_07.mat');
p.agglo.spineHeadFile = fullfile(p.saveFolder, 'aggloState', ...
    'spine_heads_and_attachment_03.mat');



%% interface agglomeration parameters

params_agglo.synT = [];

params.params_agglo = params_agglo;
% params.synT_spine = -1.67; % SynEM optimal threshold from dense test set and all synapses
params.synT_spine = -1.2292; % SynEM optimal threshold from dense test
                             % for spine synapses only
                             % @ 89% recall, 94% precision
                             % determined via Paper.SynEM.Figures.figure2 
                             % section plot e: Synapse detection test set
params.synT_shaft = -1.28317; % cnn17_2_synem_trShaft optimal threshold for
                             % inhibitory test set (shaft only)
                             % @ 69% recall, 91% precision
                             % see L4.Synapses.Shaft.getPredictionThreshold

                             
%% determine interfaces onto spines

m = load(p.svg.edgeFile, 'edges');
edges = m.edges;
maxEdgeIdx = size(edges, 1);
m = load(p.agglo.spineHeadFile, 'shAgglos');
isSHInt = any(ismember(edges, cell2mat(m.shAgglos)), 2);


%% use separate scores for spine/other interfaces

p.svg.synScoreFile = fullfile(p.saveFolder, 'globalSynScores.mat'); % SynEM
scores_sh = SynEM.Util.loadGlobalSynScores(p.svg.synScoreFile, maxEdgeIdx);

p.svg.synScoreFile = fullfile(p.saveFolder, ...
    'globalSynScores_cnn17_2_synem_trShaft.mat');
scores_other = SynEM.Util.loadGlobalSynScores(p.svg.synScoreFile, ...
    maxEdgeIdx);

scores = nan(size(scores_other));
scores(isSHInt, :) = scores_sh(isSHInt, :);
scores(~isSHInt, :) = scores_other(~isSHInt, :);
clear scores_sh scores_other

% determine initial synaptic indices (separately for spine/other)
synIdx = false(size(scores, 1), 1);
synIdx(isSHInt) = any(scores(isSHInt, :) > params.synT_spine, 2);
synIdx(~isSHInt) = any(scores(~isSHInt, :) > params.synT_shaft, 2);

% convert scores to graph (i.e. including correspondences)
m = load(p.svg.graphFile, 'borderIdx');
borderIdx = m.borderIdx;
tmp = nan(size(borderIdx, 1), 2);
tmp(~isnan(borderIdx), :) = scores;
scores = tmp;

% convert synIdx to graph
tmp = false(size(borderIdx, 1), 1);
tmp(~isnan(borderIdx)) = synIdx;
synIdx = tmp;


%% interface agglomeration

[synapses, isSpineSyn, heuristics, debug] = ...
    L4.Synapses.createSynapseAgglomerates(p, params_agglo, axons, [], ...
    scores, synIdx);


%% save result

Util.ssave(outputFile, synapses, isSpineSyn, heuristics, debug, params, ...
    info);


%% execute soma FP exclusion

[synapses, isSpineSyn, heuristics] = ...
    L4.Synapses.executeSomaFPExclusionHeuristic( ...
        synapses, isSpineSyn, heuristics);

Util.ssave(outputFileSH, synapses, isSpineSyn, heuristics, params, info);
