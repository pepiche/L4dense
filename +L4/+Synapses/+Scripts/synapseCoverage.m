% script to determine the number of synapses picked up by pre- and
% postsynaptic processes
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo(false);
plotFlag = false; %#ok<*UNRCH>


%% load data

p = Gaba.getSegParameters('ex145_ROI2017');
% synFile = p.connectome.synFile;
synFile = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v3.mat');
axFile = p.agglo.axonAggloFile;
% deFile = p.agglo.dendriteAggloFile;
deFile = ['/gaba/u/mberning/results/pipeline/20170217_ROI/' ...
    'aggloState/dendrites_wholeCells_01_spine_attachment_rename.mat'];
Util.log('Using synapses from %s.', synFile);
mSy = load(synFile);
Util.log('Using axons from %s.', axFile);
mAx = load(axFile);
Util.log('Using dendrites from %s.', deFile);
mDe = load(deFile);


%% get flight path overlaps

tic
Util.log('Calculating flight path overlaps.');

% use wkwrap segmentation
p.seg.backend = 'wkwrap';
p.seg.root = ['/gabaghi/wKcubes_archive/2012-09-28_ex145_07x2_ROI2017/' ...
    'segmentation/1/'];

axonsSmall = Superagglos.origFields(mAx.axons(~mAx.indBigAxons));

% some segments were discarded during chiasma splitting and are not part of
% any axon agglomerate anymore. to make it possible that those are picked
% up by flight paths they are added as single segment agglos
mAxOld = load(['/gaba/u/mberning/results/pipeline/20170217_ROI/' ...
    'aggloState/axons_03.mat']);
toAddAxonSegs = setdiff(cell2mat(Superagglos.getSegIds(mAxOld.axons)), ...
    cell2mat(Superagglos.getSegIds(mAx.axons)));
lostAgglos = struct('nodes', ...
    arrayfun(@(x)zeros(1,4), (1:length(toAddAxonSegs))', 'uni', 0));
lostAgglos(1).edges = [];
for i = 1:length(lostAgglos)
    lostAgglos(i).nodes(1,4) = toAddAxonSegs(i);
end
axonsSmall = cat(1, axonsSmall, lostAgglos);

axonsBig = mAx.axons(mAx.indBigAxons);
fp_axons_large = Superagglos.getFlightPath(axonsBig, 'nodes');
ov = Superagglos.flightPathAggloOverlap(p, fp_axons_large, axonsSmall, ...
    [], 'max');

% merge the flight path ovs into the axons (only seg ids are actually
% required; nodes are ignored for now)
axons_fp_pickups = axonsBig;
for i = 1:length(axonsBig)
    if ~isempty(ov{i})
        axons_fp_pickups(i).nodes = cat(1, axons_fp_pickups(i).nodes, ...
            cell2mat({axonsSmall(ov{i}).nodes}'));
    end
end

Util.log('Finished flight path overlap calculation in %f seconds', toc);


%% get coverage

% delete flight path only agglos
toDel = Superagglos.getPureFlightPathAgglos(axonsBig);
axonsBig(toDel) = [];
toDel = Superagglos.getPureFlightPathAgglos(axons_fp_pickups);
axons_fp_pickups(toDel) = [];
axons = Superagglos.origFields(mAx.axons);
toDel = Superagglos.getPureFlightPathAgglos(axons);
axons(toDel) = [];
axons = cat(1, axons, lostAgglos); % add lost agglos here as well

% coverage stats (all axons including chiasma resolution lost segments)
[stats_all, syn2agglo_all] = L4.Synapses.synapseCoverage( ...
    mSy.synapses, axons, mDe.dendrites);
stats_all.dendrites = deFile;
stats_all.axons = axFile;
stats_all.synapses = synFile;

% large axons only
[stats_large, syn2agglo_large] = L4.Synapses.synapseCoverage( ...
    mSy.synapses, axonsBig, mDe.dendrites(mDe.indBigDends));
stats_large.dendrites = deFile;
stats_large.axons = axFile;
stats_large.synapses = synFile;

% large axons including flight-path segments
[stats_fp, syn2agglo_fp] = L4.Synapses.synapseCoverage(mSy.synapses, ...
   axons_fp_pickups, mDe.dendrites(mDe.indBigDends));
stats_fp.dendrites = deFile;
stats_fp.axons = axons_fp_pickups;
stats_fp.synapses = synFile;
stats_fp.fp_overlap = ov;

% axons between 3 and 5 um
axons_3To5 = Superagglos.origFields(mAx.axons(~mAx.indBigAxons));
toDel = Superagglos.getPureFlightPathAgglos(axons_3To5);
axons_3To5(toDel) = [];
toKeep = Agglo.isMaxBorderToBorderDistAbove(p, 3000, ...
    Superagglos.getSegIds(axons_3To5));
axons_3To5 = axons_3To5(toKeep);

[stats_3To5, syn2agglo_3To5] = L4.Synapses.synapseCoverage(mSy.synapses, ...
   axons_3To5, mDe.dendrites(mDe.indBigDends));
stats_3To5.dendrites = deFile;
stats_3To5.axons = axons_3To5;
stats_3To5.synapses = synFile;

% num axons 3-5 that contain a synapse
idxPre = cell2mat(syn2agglo_3To5.syn2pre( ...
    ~cellfun(@isempty, syn2agglo_3To5.syn2pre)));
numPre3To5 = length(unique(idxPre));


%% save results

[~,tmp] = fileparts(synFile);
[~,tmp2] = fileparts(axFile);
outFile = fullfile(p.connectome.saveFolder, ...
    [tmp '_' tmp2 '_coverageStats.mat']);
if exist(outFile, 'file')
    Util.log(['Output file %s already exists. The data needs to be ' ...
        'saved manually.'], outFile);
else
    Util.log('Storing output at %s.', outFile);
    save(outFile, 'stats_all', 'stats_large', 'stats_fp', 'info');
end


%% completely missed synapse examples

if plotFlag
    n = 50;
    idx = find(cellfun(@isempty, syn2agglo_all.syn2pre));
    [~, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
    skel = L4.Synapses.synapse2Skel(...
        mSy.synapses(idx(randperm(length(idx), n)),:), ...
        borderMeta.borderCoM, segmentMeta.point);
    skel.write('SynapsesNotCovered.nml');
end


%% synapses not picked up by large agglos but by small agglos

if plotFlag
    n = 50;
    
    % synapses to skeleton
    idx = find(~cellfun(@isempty, syn2agglo_all.syn2pre) & ...
               cellfun(@isempty, syn2agglo_fp.syn2pre));
    [~, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
    skel = L4.Synapses.synapse2Skel(...
        mSy.synapses(idx(randperm(length(idx), n)),:), ...
        borderMeta.borderCoM, segmentMeta.point);
    skel.write('SynapsesNotCoveredByLargeAgglos.nml');
    
    % mapping for aid in the inspection of the above skeleton
    segs_ax_fp = cell2mat(Superagglos.getSegIds(axons_fp_pickups));
    seg_ax_all_wo_fp = setdiff(cell2mat(Superagglos.getSegIds(axons)), ...
        segs_ax_fp);
    segs_to_zero = setdiff((0:15030572)', [segs_ax_fp; seg_ax_all_wo_fp]);
%     seg_dend = cell2mat(Superagglos.getSegIds(mDe.dendrites(mDe.indBigDends)));
%     segs_to_zero = setdiff((0:15030572)', [segs_ax_fp; seg_ax_all_wo_fp; seg_dend]);
    components = cat(1, {segs_to_zero}, {segs_ax_fp}, {seg_ax_all_wo_fp});
    WK.makeWKMapping(components, 'axons_11_a');
end


%% plot histogram over number of synapses

if plotFlag
    % plot stats_fp only
    N = stats_fp.synPerAxon(2:end,1);
    bin = stats_fp.synPerAxon(2:end,2);
    histogram(repelem(N, bin), 'BinMethod', 'integers')
    a = gca;
    a.YScale = 'log';
    a.YLim(1) = 9e-1;
    a.XLim(1) = 0.5;
    a.XTick = cat(2, 1, a.XTick);
    Visualization.Figure.plotDefaultSettings()
    title('Synapses per axon histogram');
    xlabel('Syn/Axon')
    ylabel('Count')
end