function dat = getRawDataset()
%GETRAWDATASET Get a dataset object for the L4 raw data.
% OUTPUT dat: Datasets.WkDataset object
%           Dataset object with path to a wkw dataset for the L4 raw data.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

datPath = '/wKcubes_live/2012-09-28_ex145_07x2_ROI2017/color/1';
dat = Datasets.WkDataset(datPath);

end

