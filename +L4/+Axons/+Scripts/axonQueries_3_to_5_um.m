% script to generate axon queries for the 3-5 um axons
%
% see also https://mhlablog.net/2018/01/22/l4-3-5-um-axon-queries-round-1/
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% note if ever redoing this:
% for each query store: startPos, startSegmentId, direction, aggloIdx

info = Util.runInfo(false);
queryRound = 'round_1'; % output subfolder (see below)
createDynamicQuerySeg = true;


%% get the axon agglos

p = Gaba.getSegParameters('ex145_ROI2017');
synFile = p.connectome.synFile;
% axFile = p.agglo.axonAggloFile;
axFile = fullfile(p.agglo.saveFolder, 'axons_16_a.mat');
Util.log('Using synapses from %s.', synFile);
mSy = load(synFile);
Util.log('Using axons from %s.', axFile);
mAx = load(axFile);

% get axons between 3 and 5 um
axons_3To5 = Superagglos.origFields(mAx.axons(~mAx.indBigAxons));
toDel = Superagglos.getPureFlightPathAgglos(axons_3To5);
axons_3To5(toDel) = [];
toKeep = Agglo.isMaxBorderToBorderDistAbove(p, 3000, ...
    Superagglos.getSegIds(axons_3To5));
axons_3To5 = axons_3To5(toKeep);

% synapses onto these axons
[stats_3To5, syn2agglo_3To5] = L4.Synapses.synapseCoverage( ...
    mSy.synapses, axons_3To5);

% num axons 3-5 that contain a synapse
idxPre = cell2mat(syn2agglo_3To5.syn2pre( ...
   ~cellfun(@isempty, syn2agglo_3To5.syn2pre)));
numPre3To5 = length(unique(idxPre));

axons = axons_3To5(unique(idxPre));


%% get directionality for these agglomerates

Util.log('Running axon border directionality calculation.');
agglos = Superagglos.getSegIds(axons);
options.bboxDist = 1000;
options.voxelSize = [11.24, 11.24, 28];
[graph, segmentMeta, borderMeta, globalSegmentPCA] = ...
       connectEM.loadAllSegmentationData(p);
directionality = connectEM.calculateDirectionalityOfAgglomerates( ...
    agglos, graph, segmentMeta, borderMeta, globalSegmentPCA, options);

% hack to get the agglo seg ids for each border (why is this not part of
% directionality???)
m = load(p.svg.edgeFile); % directionality.borderIdx are not wrt to graph edges
                          % (but why???????)
directionality.startSegId = cell(length(directionality.borderIdx), 1);
for i = 1:length(directionality.borderIdx)
    tmp = m.edges(directionality.borderIdx{i}, :);
    tmp2 = ismember(tmp, agglos{i});
    idx = zeros(size(tmp2, 1), 1);
    idx(tmp2(:,1)) = find(tmp2(:,1));
    idx(tmp2(:,2)) = find(tmp2(:,2)) + size(tmp2, 1);
    directionality.startSegId{i} = tmp(idx);
end


%% select endings

Util.log('Running ending detection.');
% choose ending based on max f1 score (does this make sense?)

% first direction (positive score)
idxPos = cellfun(@(x)x > 0, directionality.scores, 'uni', 0);

tmpPos = cellfun(@(x, y, i) [x(i,1), abs(y(i))], directionality.latent, ...
    directionality.scores, idxPos, 'uni', 0);
tmpNeg = cellfun(@(x, y, i) [x(~i,1), abs(y(~i))], directionality.latent, ...
    directionality.scores, idxPos, 'uni', 0);

% for each score direction get query position with maximal f1 score of
% latent and scores (tmpPos, tmpNeg)
idx = zeros(length(directionality.scores), 2);
bidx = zeros(length(directionality.scores), 2);
scores = zeros(length(directionality.scores), 2);
startSegId = zeros(length(directionality.scores), 2);
for i = 1:size(idx, 1)
    
    lidxPos = find(idxPos{i});
    lidxNeg = find(~idxPos{i});
    
    % positive direction
     [~, tmp] = max(harmmean(tmpPos{i}, 2));
     idx(i, 1) = lidxPos(tmp);
     bidx(i, 1) = directionality.borderIdx{i}(idx(i, 1));
     scores(i, 1) = directionality.scores{i}(idx(i, 1));
     startSegId(i, 1) = directionality.startSegId{i}(idx(i, 1));
     
     % negative direction
     [~, tmp] = max(harmmean(tmpNeg{i}, 2));
     idx(i, 2) = lidxNeg(tmp);
     bidx(i, 2) = directionality.borderIdx{i}(idx(i, 2));
     scores(i, 2) = directionality.scores{i}(idx(i, 2));
     startSegId(i, 2) = directionality.startSegId{i}(idx(i, 2));
end


%% save results

outputFolder = fullfile(p.agglo.saveFolder, 'axons_3_to_5_um', queryRound);
if ~exist(outputFolder, 'dir')
    mkdir(outputFolder)
end

outFile = fullfile(outputFolder, 'data.mat');
if ~exist(outFile, 'file')
    Util.log('Storing axon and directionality results at %s.', outFile);
    save(outFile, 'info', 'agglos', 'directionality');
else
    % completely stop to avoid data loss
    error('Output file %s already exists. Aborting ...');
end


%% create queries
% (based on connectEM.generateAxonQueries)

Util.log('Starting query generation on cluster.');

% border positions
borderPositionsPos = double(borderMeta.borderCoM(bidx(:, 1),:));
borderPositionsNeg = double(borderMeta.borderCoM(bidx(:, 2),:));

% directions (first pca of border)
directionsPos = zeros(size(borderPositionsPos));
directionsNeg = zeros(size(borderPositionsPos));
for i = 1:length(directionsPos)
    directionsPos(i,:) = directionality.pca{i}(:,1,idx(i, 1))';
    directionsNeg(i,:) = -directionality.pca{i}(:,1,idx(i, 2))';
end

% use query location that is more towards dataset center (determined by
% scalar product of query direction 'directionsPos/Neg' and the vector from
% borderPosition to dataset center)
cen = (p.bbox(:,2) - p.bbox(:,1))'./2;
centerDirPos = L4.Axons.getDirection(borderPositionsPos, cen);
centerDirScorePos = dot(centerDirPos, directionsPos, 2);
centerDirNeg = L4.Axons.getDirection(borderPositionsNeg, cen);
centerDirScoreNeg = dot(centerDirNeg, directionsNeg, 2);
useDirPos = centerDirScorePos > centerDirScoreNeg;

% final border positions and directions for queries
borderPositions = cell(length(borderPositionsPos), 1);
directions = cell(length(borderPositionsPos), 1);
startSegId2 = zeros(length(borderPositionsPos), 1);
for i = 1:length(borderPositionsPos)
    if useDirPos(i)
        borderPositions{i}{1} = borderPositionsPos(i, :);
        directions{i}{1} = directionsPos(i,:);
        startSegId2(i) = startSegId(i, 1);
    else
        borderPositions{i}{1} = borderPositionsNeg(i, :);
        directions{i}{1} = directionsNeg(i,:);
        startSegId2(i) = startSegId(i, 2);
    end
end
startSegId = startSegId2;
clear startSegId2
% save(fullfile(outputFolder, 'startSegIds.mat'), 'startSegId');

% query generation on cluster
batchBoundaries = 1:1000:(length(agglos) + 1);
if batchBoundaries(end) < (length(agglos) + 1)
    batchBoundaries(end+1) = (length(agglos) + 1);
end
inputList = num2cell(1:length(batchBoundaries));
sharedInputs = {batchBoundaries, agglos, borderPositions, directions, ...
    p, outputFolder};
sharedInputsLocation = 2:7;
cluster = Cluster.getCluster('-p 0', '-l h_vmem=12G', ...
    '-l s_rt=5:28:00', '-l h_rt=5:29:00');
job = Cluster.startJob(@connectEM.processQueryTasks, inputList, ...
    'name', 'queryGeneration', 'cluster', cluster, ...
    'sharedInputs', sharedInputs, ...
    'sharedInputsLocation', sharedInputsLocation);
wait(job);


%% create tasks

% get position and rotations from batch files
files = dir(fullfile(outputFolder, 'batch*.mat'));
pos = cell(length(files), 1);
rot = cell(length(files), 1);
for i = 1:length(files)
    m = load(fullfile(outputFolder, files(i).name));
    pos{i} = m.q.pos(:);
    rot{i} = m.q.angles(:);
end
pos = vertcat(pos{:});
pos = cell2mat(vertcat(pos{:}));
rot = vertcat(rot{:});
rot = cell2mat(vertcat(rot{:}));

% save pos and rot of queries again for convenience
save(fullfile(outputFolder, 'queries.mat'), 'pos', 'rot');

% write task definitions
taskDef = table;
taskDef.position = pos;
taskDef.rotation = rot;
taskDef.bbox = zeros(size(pos, 1), 6);

% task params
taskParam = struct;
taskParam.dataSet = '2012-09-28_ex145_07x2_ROI2017';
taskParam.taskTypeId = '?';
taskParam.expDomain = '?';
taskParam.expMinVal = 1;
taskParam.instances = 1;
taskParam.team = 'Connectomics department';
taskParam.project = 'axons_3_to_5_endings';


task_file = fullfile(outputFolder, 'axons_3_to_5_um_ending_tasks');

Util.log('Writing query tasks to %s.', task_file);
fullTaskDef = connectEM.exportTaskDefinitions(taskParam, taskDef, task_file);


%% create segmentation

if createDynamicQuerySeg
    segOutput = fullfile(outputFolder, 'seg', '1'); %#ok<UNRCH>
    wkwInit('new', segOutput, 32, 32, 'uint32', 1);
    seg.root = segOutput;
    seg.backend = 'wkwrap';
    Util.log('Creating segmentation for dynamic stopping at %s.', ...
        seg.root);
    connectEM.generateSegmentationForNewQueries(p, seg, axFile);
end


%% debug queries

if false % debug
    skelOut = outputFolder; %#ok<UNRCH>
    m = load(fullfile(outputFolder, files(1).name));
    q = structfun(@(y)cellfun(@(x)x{1},y,'uni', 0), m.q, 'uni', 0);
    connectEM.debugNewQueries(segmentMeta, m.theseAxons, q, skelOut);
end