% plot spines distribution along dendrites
outDir = '/tmpscratch/sahilloo/L4/data/';

n=load('/tmpscratch/sahilloo/L4/data/dendritesPostQueryStateLengths.mat')
dendriteLengths = n.aggloLens./1E3; % um

m=load('/home/loombas/mnt/gaba/tmpscratch/L4/data/dendritesSmoothState.mat');
idx_smooth = m.idx_smooth;
dendritesSmooth = m.dendritesSmooth;
dendritesSmoothLengths = dendriteLengths(idx_smooth);
dendritesExt = m.dendritesExt;

%% plot all smooth dendrites
[~,idxSort] = sort(dendritesSmoothLengths);
subplot(3,1,1)
plot(dendritesSmoothLengths(idxSort));
ylabel('Path lengths (um)')
subplot(3,1,2)
plot(log(dendritesSmoothLengths(idxSort)));
ylabel('Path lengths (log)')
xlabel('Smooth Dendrite agglos sorted by path lengths')
subplot(3,1,3)
histogram(dendritesSmoothLengths)
xlabel('Path lengths (um)')
ylabel('# of smooth dendrites')

%% path lengths of dendrites with < 0.25 spine density
m=load(fullfile(outDir,'spineDistribution.mat'));
spineTable = m.myTable;
toPlot = spineTable{:,5}<0.25;
dendriteLengthsToPlot = spineTable{toPlot,2};

[~,idxSort] = sort(dendriteLengthsToPlot);
subplot(3,1,1)
plot(dendriteLengthsToPlot(idxSort));
ylabel('Path lengths (um)')
subplot(3,1,2)
plot(log(dendriteLengthsToPlot(idxSort)));
ylabel('Path lengths (log)')
xlabel('Agglos < 0.25 spine density sorted by path lengths')
subplot(3,1,3)
histogram(dendriteLengthsToPlot)
xlabel('Path lengths (um)')
ylabel('# of Agglos < 0.25 spine density')
xlim([0,500])