% set configuration parameters for L4 analysis state
config = struct;

config.param = '/gaba/u/mberning/results/pipeline/20170217_ROI/allParameterWithSynapses.mat';
%config.axonFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/chiasmataSplitting/20171009T193744-kmb-on-axons-6c/outputs/20171023T115300_results.mat';
config.axonFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/connectomeState/axons.mat';
config.dendriteFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_16.mat';
config.spineFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_wholeCells_01_spine_attachment.mat'
config.spineFileOriginal = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_wholeCells_01.mat'
config.synapseFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/connectomeState/SynapseAgglos_v2.mat';
config.outDir = '/tmpscratch/sahilloo/L4/dataPostSyn/';
config.somaFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/somas_with_merged_somas.mat';
config.meta = '/gaba/u/mberning/results/pipeline/20170217_ROI/segmentMeta.mat';
if ~exist(config.outDir,'dir')
    mkdir(config.outDir);
end
