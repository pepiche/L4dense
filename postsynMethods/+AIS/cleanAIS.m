% replace the ais with branching axons with a cleaner ais whose branching axon part was manually cut

setConfig;

load(config.param);
info = Util.runInfo();
aggloDir = fullfile(p.saveFolder, 'aggloState');

oldDendFile = fullfile(aggloDir, 'dendrites_17_e.mat');
outFile = fullfile(aggloDir, 'dendrites_18_b.mat');

voxelSize = p.raw.voxelSize;

Util.log('Loading segment data');
maxSegId = Seg.Global.getMaxSegId(p);
points = Seg.Global.getSegToPointMap(p);

Util.log('Loading new dendrites from %s', oldDendFile);
oldDend = load(oldDendFile);

% post hoc clean ais in oldDend to typecast nodes as double
typeStr = arrayfun(@(x) class(x.nodes), oldDend.dendrites(oldDend.indAIS),'uni',false);
if any(typeStr == "uint32" | typeStr == "int32")    
    for i = find(oldDend.indAIS)'
        [oldDend.dendrites(i).nodes] = double(oldDend.dendrites(i).nodes);
    end 
end

m = load(fullfile(config.outDir,'aisFromWCState.mat'));
ais = m.ais;
idxAISToWC = m.idxAISToWC;

% after manual inspection, remove the axons that are not ais
aisToClean = [5,9,12,23,29,36,40];
nmlFile = fullfile(config.outDir,'aisCutFromAxons_v2.nml');

Util.log('Convert ais with axons cut skeletons to superagglos');
[aisAxonsCut,axons,orderAisAxonsCut,orderAxons] = extractAisFromNmlFile(nmlFile,p,ais(aisToClean),true);

Util.log('save axon superagglos')
axonsFromWCIds = aisToClean(orderAxons);
save(fullfile(config.outDir,'axonsFromWCState.mat'),'axons','axonsFromWCIds');

if isrow(aisAxonsCut)
    aisAxonsCut = aisAxonsCut';
end

Util.log('Reordering aisAxonsCut to match inital ais order...');
aisClean = ais;
aisClean(aisToClean) = aisAxonsCut(orderAisAxonsCut); 
aisCount = numel(aisClean);

Util.log(['Adding the ' num2str(aisCount) ' found ais...']);
if isrow(aisClean)
    aisClean = aisClean';
end 

%% Complete output
Util.log('Cleaning ais');
aisClean = SuperAgglo.clean(aisClean);

% Prepare output                                            
out = struct;
out.dendrites = cat(1,oldDend.dendrites,aisClean);

% Build masks
%indBigAIS = Agglo.isMaxBorderToBorderDistAbove(p, 5000, Superagglos.transformAggloNewOldRepr(aisClean)); % only big ais found from wholecells
indBigAIS = true(aisCount,1); % all ais as indBig
out.indBigDends = cat(1,oldDend.indBigDends,indBigAIS); % ais as post-syn target
out.indAIS = cat(1,oldDend.indAIS,true(aisCount,1));

out.idxAIS = cat(1,zeros(size(oldDend.indAIS)),idxAISToWC);

% additional flags
out.flags.indAISnonWholeCell = cat(1,oldDend.indAIS,false(aisCount,1));
out.flags.indAISWholeCell = cat(1,false(size(oldDend.indAIS)),true(aisCount,1));

% Sanity check
assert(all(out.indBigDends(out.indAIS)));

% Save result
out.info = info;

% sanity check
SuperAgglo.check(out.dendrites) % amotta repo

%% Save result
Util.log('Writing results to %s', outFile);
Util.saveStruct(outFile, out);

function [ais, axon, order,orderAxons] = extractAisFromNmlFile(nmlFile,p,aisToClean,preserveSegCount);
    ais = struct();
    axon = struct();
    
    skel = skeleton(nmlFile);
    idxAis = arrayfun(@(x) any(skel.getNodesWithComment('ais',x)),1:skel.numTrees);     
    skelAis = skel.deleteTrees(~idxAis);
    
    for i = 1:skelAis.numTrees
        ais(i).edges = skelAis.edges{i};
        nodes = skelAis.nodes{i}(:,1:3);
        [~,i2] = arrayfun(@(x) ismember(nodes,x.nodes(:,1:3),'rows'),aisToClean,'uni',0);
        treePos = cellfun(@(x) any(x),i2);
        order(i) = find(treePos);
        if preserveSegCount
            nodesToAdd = aisToClean(treePos).nodes(i2{treePos},:);
            ais(i).nodes = nodesToAdd;
        else   
            segToAdd = Seg.Global.getSegIds(p,nodes);
            segToAdd(segToAdd==0) = NaN;
            ais(i).nodes = horzcat(nodes,segToAdd);
        end
    end
    
    skelAxon = skel.deleteTrees(idxAis);
    idxMerger = arrayfun(@(x) any(skelAxon.getNodesWithComment('merger',x)),1:skelAxon.numTrees);
    skelAxon = skelAxon.deleteTrees(idxMerger);
       for i = 1:skelAxon.numTrees
        axon(i).edges = skelAxon.edges{i};
        nodes = skelAxon.nodes{i}(:,1:3);
        [~,i2] = arrayfun(@(x) ismember(nodes,x.nodes(:,1:3),'rows'),aisToClean,'uni',0);
        treePos = cellfun(@(x) any(x),i2);
        orderAxons(i) = find(treePos);
        if preserveSegCount
            nodesToAdd = aisToClean(treePos).nodes(i2{treePos},:);
            axon(i).nodes = nodesToAdd;
        else   
            segToAdd = Seg.Global.getSegIds(p,nodes);
            segToAdd(segToAdd==0) = NaN;
            axon(i).nodes = horzcat(nodes,segToAdd);
        end 
    end
end


