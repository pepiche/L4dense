% determine the optimal prediction threshold for shaft synapse classifiers
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


%% load inhibitory test set

p = Gaba.getSegParameters('ex145_ROI2017');
if ~ispc
    inhTestFolder = ['/u/bstaffle/data/L4/Synapses/PerformanceAssessment/' ...
        'InhTestSetROI2017'];
else
    inhTestFolder = ['E:\workspace\data\backed\L4\Synapses\' ...
        'PerformanceAssessment\InhTestSetROI2017\'];
    p.saveFolder = 'E:\workspace\data\backed\20170217_ROI\';
    p.svg.synScoreFile = ['E:\workspace\data\backed\20170217_ROI\' ...
        'globalSynScores.mat'];
    p.svg.borderMetaFile = ['E:\workspace\data\backed\20170217_ROI\' ...
        'globalBorder.mat'];
end

% synem scores
m = load(p.svg.borderMetaFile, 'borderSize');
borderSize = double(m.borderSize);

% ground truth data
m = load(fullfile(inhTestFolder, 'GroundTruth.mat'));

% get prediction
borderSize = borderSize(cell2mat(m.edgeIdx));
borderSize(borderSize < 151) = nan;
[~, label, edgeDir, edgeIdx] = Util.deleteRowsWithNaNs(borderSize, ...
    m.labelAll, cell2mat(m.edgeDir), cell2mat(m.edgeIdx));

% shaft only
m2 = load(fullfile(inhTestFolder, 'PostsynNodesAndLabels.mat'));

skel = skeleton(fullfile(inhTestFolder, 'Tracings', ...
    'InhTestSet_ShaftSynAnnotations.nml'));
names = skel.names();
names = sort(names);
names(cellfun(@(x)contains(x, 'concensus'), names)) = [];
isShaft = cellfun(@(x)contains(x, 'shaft'), names);
shaftIdx = find(isShaft);
shaftLabel = m2.psLabel(shaftIdx);
toExcl = ismember(label, setdiff(1:max(label), shaftLabel));


%% evaluate and calculate optimal threshold

p.saveFolder = '/media/benedikt/DATA2/workspace/data/backed/20170217_ROI/';
file = 'globalSynScores_cnn17_2_synem_trShaft.mat';

% get classifier scores
p.svg.synScoreFile = fullfile(p.saveFolder, file);
scores = SynEM.Util.loadGlobalSynScores(p.svg.synScoreFile);
scores = scores(cell2mat(m.edgeIdx),:);
scores = scores(~any(isnan(scores), 2), :);
scores = SynapseDetection.scoreForDir(scores, edgeDir);
[rp, thr] = SynEM.Eval.interfaceRP(label, scores);
[rp_sh, thr_sh] = SynEM.Eval.interfaceRP(label(~toExcl), scores(~toExcl));
[f1, idx_sh, opt_t] = SynEM.Eval.optimalF1(rp_sh, thr_sh);
idx = find(thr > opt_t, 1, 'first');
Util.log('Optimal threshold: %.5f', opt_t); % -1.28317
Util.log('Resulting RP (Inh test set; shaft only): %s', ...
    mat2str(rp_sh(idx_sh,:), 3));
Util.log('Resulting RP (Inh test set): %s', ...
    mat2str(rp(idx,:), 3));
