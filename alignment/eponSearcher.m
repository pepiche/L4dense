montage=cell(30,1);
border=32;
i=0;
for p=250:500:7250
    i=i+1
    p
    montage{i}=zeros(1768*13,2048*9,'uint8');
    for m=1:9
        for n=1:13
            [m n p];
            if size(all_fns{m,n,p},1)>0
                a=imread(all_fns{m,n,p});
                a(1:border,:)=0;
                a(:,1:border)=0;
                a(end-border:end,:)=0;
                a(:,end-border:end)=0;
                
                montage{i}((n-1)*1768+1:(n)*1768,(m-1)*2048+1:(m)*2048)=a;
            end
        end
    end
end
%% Show
montage2=zeros([fliplr(size(montage{1}))/8 i],'uint8');
for j=1:i
    montage2(:,:,j)=montage{j}(1:8:end,1:8:end)';
end
ij.stack(montage2)