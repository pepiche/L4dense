function problems_save = findMaximumProblem(problem, surround, N)
problems_save.problems = problem;
problems_save.todo = [];
for i = 1 : N
    [maxval, dim_idx] = max(cellfun(@(x)max(x(:)), problem));
    [~, in_dim_idx] = max(problem{dim_idx}(:));
    [X_idx, Y_idx, Z_idx] = ind2sub(size(problem{dim_idx}), in_dim_idx);
    disp(round([maxval, dim_idx, X_idx, Y_idx, Z_idx]))
    problems_save.todo(end + 1, :) = [maxval, dim_idx, X_idx, Y_idx, Z_idx];
    
    for dimIt = 1 : 3
        problem{dimIt}(:,:,max(1,Z_idx-surround):Z_idx+surround) = 0;
    end
    
end