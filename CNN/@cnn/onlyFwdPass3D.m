function result = onlyFwdPass3D(cnet, input)
% Load data with right border for cnet
cnet = cnet.forWeights(cnet.run.actvtClass);
activity = cell(cnet.numLayer, max(cnet.numFeature));
activity{1,1} = cnet.run.actvtClass(input);
clear input; 
% DO the THING
layer = 2;
while size(activity,1) > 1
	for fm=1:cnet.layer{layer}.numFeature
		activity{2,fm} = zeros(size(activity{1,1}) - cnet.filterSize + [1 1 1], class(activity{1,1}));
		for oldFm=1:cnet.layer{layer-1}.numFeature
			activity{2, fm} = activity{2, fm} + convn(activity{1, oldFm}, cnet.layer{layer}.W{oldFm,fm}, 'valid');
		end
		activity{2, fm} = cnet.nonLinearity(activity{2, fm} + cnet.layer{layer}.B(fm));
	end
	activity(1,:) = [];
	layer = layer + 1;
end
% Pass on result
result = gather(activity{1,1});
end
