% findSmoothDendrites without myelin
outDir = '/tmpscratch/sahilloo/L4/data/';
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

% load smooth dendrites results
m=load(fullfile(outDir,'dendritesSmoothState.mat'));
dendrites = m.dendrites;
idx_smooth = m.idx_smooth';

% load myelin fraction scores
n=load(fullfile(outDir,'fracMyelinAggloBorder_Dend.mat'));
myFrac = n.fracMyelinAggloBorder_Dend;
idx_non_myelin = myFrac<0.1;

m=load(fullfile(outDir,'spineDistribution.mat'));
spineTable = m.spineTable;

% add myFrac to spineTable
spineTable = [spineTable table(myFrac)];

%% agglos smooth non nyelin with path lengths > 10um
rng(0);
idx_longPathLength = spineTable{:,2}>10;
idx_out = idx_smooth & idx_non_myelin & idx_longPathLength;
table_out  = spineTable(idx_out,:);
scores = Util.typeEMScoresForAgglos(table_out{:,1},'smoothNonMyelinLarge_typeEM_scores.png');
save(fullfile(outDir,'smoothNonMyelinLarge_typeEM_scores.mat'),'scores');

idxToPlot = randperm(size(table_out,1),100);
for i=1:length(idxToPlot)
    nodes = table_out{idxToPlot(i),1};
    if numel(table_out{idxToPlot(i),1}{1})==1
        fprintf('DendritesAgglo %d is single segId \n',idxToPlot(i));
        continue;
    end
    skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
    skel.names(1) = {['- sd:' num2str(table_out{idxToPlot(i),5}) ' - '...
        num2str(table_out{idxToPlot(i),2}) 'um ' '- my:' ...
        num2str(table_out{idxToPlot(i),6}) ' - ' ...
        num2str(scores(idxToPlot(i)).astrocyte,'%.2f') '/' ...
        num2str(scores(idxToPlot(i)).axon,'%.2f') '/' ...
        num2str(scores(idxToPlot(i)).dendrite,'%.2f')]};
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDir, ['nmls/dendritesSmoothNonMyLarge_' num2str(i) '.nml']));
end

%% agglos with low spine density and long path lengths > 10um
rng(0);
idx_lowSpineDensity = spineTable{:,5}<0.25;
idx_longPathLength = spineTable{:,2}>10;
idx_out = idx_lowSpineDensity & idx_non_myelin & idx_longPathLength;
table_out  = spineTable(idx_out,:);
scores = Util.typeEMScoresForAgglos(table_out{:,1},'lowSpineDensityNonMyelinLarge_typeEM_scores.png');
save(fullfile(outDir,'lowSpineDensityNonMyelinLarge_typeEM_scores.mat'),'scores');
idxToPlot = randperm(size(table_out,1),100);
for i=1:length(idxToPlot)
    nodes = table_out{idxToPlot(i),1};
    if numel(table_out{idxToPlot(i),1}{1})==1
        fprintf('DendritesAgglo %d is single segId \n',idxToPlot(i));
        continue;
    end
    skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
    skel.names(1) = {['- sd:' num2str(table_out{idxToPlot(i),5}) ' - '...
        num2str(table_out{idxToPlot(i),2}) 'um ' '- my:' ...
        num2str(table_out{idxToPlot(i),6}) ' - ' ...
        num2str(scores(idxToPlot(i)).astrocyte,'%.2f') '/' ...
        num2str(scores(idxToPlot(i)).axon,'%.2f') '/' ...
        num2str(scores(idxToPlot(i)).dendrite,'%.2f')]};
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDir, ['nmls/dendritesLowSpineDensityNonMyLarge_' num2str(i) '.nml']));
end

