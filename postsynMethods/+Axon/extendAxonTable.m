function axonTable = extendAxonTable(config,axonTable,sfThr)
% use specified sfThr to relabel synapses

outDir = config.outDir;
outfile = fullfile(outDir,'axonTableExt.mat');

Util.log('Extending axon table by relabeling the synapses with sfThr specified')
oldLabels = axonTable{:,3};
aboveThr = num2cell(axonTable{:,4}>sfThr);
newLabels = cellfun(@(x,y) relabelSynapses(x,y),oldLabels,aboveThr,'uni',0);
axonTable = [axonTable table(newLabels)];
save(outfile,'axonTable');

% store synapse relabels: exc 1, inh 0, notpicked -1
Util.log('Saving relabels')
m=load(fullfile(outDir,'synapseLabels.mat'));
synapseLabels = m.synapseLabels;
t = table(vertcat(axonTable{:,2}{:}), vertcat(axonTable{:,5}{:}));
synapseRelabels = -1*ones(size(synapseLabels,1),1);
synapseRelabels(t{:,1}) = t{:,2} ;
save(fullfile(outDir,'synapseRelabels.mat'),'synapseRelabels');                                                                     

end

function newLabels = relabelSynapses(labels,aboveThr)
    if aboveThr
        newLabels = true(size(labels,1),1);
    else
         newLabels = false(size(labels,1),1);
    end
end
