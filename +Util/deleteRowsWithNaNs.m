function varargout = deleteRowsWithNaNs(varargin)
%DELETEROWSWITHNANS Delete nans from a set of inputs w.r.t. the first input.
% Usage
%   [X, ...] = deleteNans(X, ..) uses the first input to calculate the rows
%       in X that contains NaNs and deletes them. The corresponding rows
%       for all aditional inputs are deleted as well.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

toDel = any(isnan(varargin{1}), 2);

for i = 1:length(varargin)
    varargout{i} = varargin{i}(~toDel,:);
end
end
