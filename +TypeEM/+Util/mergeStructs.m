function outStruct = mergeStructs(baseStruct, appendStruct, prefix)
    % MERGESTRUCTS
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % start with base struct
    outStruct = baseStruct;
    
    if exist('prefix', 'var') && ~isempty(prefix)
        % to camel case
        buildNewName = @(oldName) [ ...
            prefix, upper(oldName(1)), oldName(2:end)];
    else
        % identitity
        buildNewName = @(oldName) oldName;
    end
    
    % find fields in append struct
    fields = fieldnames(appendStruct);
    fieldsCount = numel(fields);
    
    for fieldIdx = 1:fieldsCount
        fieldName = fields{fieldIdx};
        fieldVal = getfield(appendStruct, fieldName);
        
        % build new field name
        fieldNewName = buildNewName(fieldName);
        
        % check if field name alreay exists
        if isfield(outStruct, fieldNewName)
            errorMessage = [ ...
                'Field "', fieldNewName, ...
                '" is being overwritten!'];
            error(errorMessage);
        end
        
        % write new field
        outStruct = setfield( ...
            outStruct, fieldNewName, fieldVal);
    end
    
    % order names
    outStruct = orderfields(outStruct);
end

