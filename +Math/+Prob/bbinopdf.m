function p = bbinopdf(x, n, a, b)
%BBINOPDF Probability density function of the beta-binomial distribution.
% INPUT x: [Nx1] int
%           Locations where the pdf is evaluated.
%       n: int
%           Number of trials.
%       a, b: double
%           Distributions parameters (a, b > 0).
% OUTPUT p: [Nx1] double
%           The corresponding probabilities for the values in x.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% % direct
% C = beta(a, b);
% p = arrayfun(@(x)nchoosek(n, x) * beta(a + x, b + n - x) / C, x);

% log space (good for large n)
log_p = gammaln(n + 1) + gammaln(x + a) + gammaln(n - x + b) + ...
    gammaln(a + b) - ...
	(gammaln(x + 1) + gammaln(n - x + 1) + gammaln(n + a + b) + ...
    gammaln(a) + gammaln(b));
p = exp(log_p);

end

