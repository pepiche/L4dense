function shId = shEdges( shAgglos, edges )
%SHEDGES Get all edges onto spine heads.
% INPUT shAgglos: [Nx1] int
%           Spine head agglomerates.
%       edges: [Nx2] int
%           Global edges list.
% OUTPUT shId: [Nx1] int
%           Contains the spine head agglo idx for each edge if the edge has
%           exactly one spine head agglo segment.
%           Interfaces between two spine heads are not considered.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


lut = L4.Agglo.buildLUT(shAgglos, max(edges(:)));
shId = lut(edges);
shId(all(shId > 0, 2),:) = 0;
shId = max(shId, [], 2);

end

