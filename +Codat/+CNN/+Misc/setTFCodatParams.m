function cnet = setTFCodatParams( cnet, w, b )
%SETTFCODATPARAMS Set the parameters from a network trained with the codat
%python implementation.
% INPUT cnet: Codat.cnn object
%           A codat cnn object with the correct architectur.
%       w: [Nx1] cell
%           The weights of the python codat cnet. Should have have
%           length(cnet.layer) - 1, i.e. only hidden layer.
%           (see python.synem.syconn_com.get_params)
%       b: [Nx1] cell
%           The biases of the python codat cnet. Should have have
%           length(cnet.layer) - 1, i.e. only hidden layer.
%           (see python.synem.syconn_com.get_params)
% OUTPUT cnet: Codat.cnn object
%           The cnn object with updated parameters.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% weights in spatial dimensions need to be first sorted backwards and then
% the previous fms need to be inverted

w = cellfun(@invertSpatialDim, w, 'uni', 0);
w = cellfun(@(x)x(:,:,:,end:-1:1,:), w, 'uni', 0);
cnet.W(2:end) = w;

% bias for the hidden layers
cnet.b(2:end) = cellfun(@(x)x(:), b, 'uni', 0);


end

function w = invertSpatialDim(w)
sizeW = size(w);
sizeW(end+1:5) = 1;
for i = 1:sizeW(4)
    for j = 1:sizeW(5)
        tmp = w(:,:,:,i,j);
        sz = size(tmp);
        tmp = tmp(end:-1:1);
        tmp = reshape(tmp, sz);
        w(:,:,:,i,j) = tmp;
    end
end
end