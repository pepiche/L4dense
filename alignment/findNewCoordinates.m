function node_coords_new = findNewCoordinates(node_coords, matching_frames_old, shifts_new, shifts_old)
for node_idx = 1 : size(node_coords, 1)
    node_idx
    [X, Y] = ind2sub([3, 3], find(matching_frames_old(:, :, node_idx), 1));
    range_needed = ...
        {node_coords(node_idx, 1) - (X - 1) * 3072 - shifts_old(X, Y, node_coords(node_idx, 3), 1), ...
        node_coords(node_idx, 2) - (Y - 1) * 2048 - shifts_old(X, Y, node_coords(node_idx, 3), 2)};
    [x_move, y_move] = findMatchingFramesNew(shifts_new, 36, 24, 3424, X, Y, node_coords(node_idx, 3), range_needed);
    x_move = x_move + 3072 * (X - 1) + 1;
    y_move = y_move + 2048 * (X - 1) + 1;
    node_coords_new(node_idx, :) = ...
        [y_move, x_move, node_coords(node_idx, 3) - 1];
end
end
