% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear;

%% configuration
rootDir = '/gaba/u/mberning/results/pipeline/20170217_ROI';

dendFile = fullfile( ...
    rootDir, 'aggloState', ...
    'dendrites_wholeCells_02_v3.mat');

attachParam = struct;
attachParam.maxAstroProb = 1;
attachParam.maxAxonProb  = 0.8;
attachParam.minDendProb  = 0;
attachParam.minEdgeProb  = 0.25;
attachParam.maxNumSteps  = 10;

info = Util.runInfo();

%% complete configuration
[outDir, outFile] = fileparts(dendFile);
outFile = fullfile(outDir, sprintf('%s_auto.mat', outFile));
clear outDir;

Util.log('Reading dendrites from "%s"', dendFile);
Util.log('Writing results to "%s"', outFile);

%% actually do some work
param = load(fullfile(rootDir, 'allParameter.mat'));
param = param.p;

Util.log('Building spine heads');
sh = struct;
[sh.agglos, sh.edges] = connectEM.Spine.Head.buildAgglos(param);

Util.log('Loading graph');
graphFile = fullfile(rootDir, 'graphNewNew.mat');
graph = load(graphFile, 'edges', 'prob');
graph = Graph.addNeighbours(graph);

Util.log('Loading dendrites');
dendIn = load(dendFile);
dendIn.dendrites = SuperAgglo.clean(dendIn.dendrites);

Util.log('Running spine head attachment');
[sh.edges, sh.attached, out] = ...
    connectEM.Spine.Head.attach(param, attachParam, graph, sh, dendIn);

% TODO(amotta): Enable check when spine attachment is fixed
out.dendrites = SuperAgglo.clean(out.dendrites, false);

%% Completing and writing output
Util.log('Completing output');

% Translate indices
indFields = sort(fieldnames(dendIn));
indFields = indFields( ...
    startsWith(indFields, 'ind') ...
  | startsWith(indFields, 'idx'));

for curIdx = 1:numel(indFields)
    curName = indFields{curIdx};
    curVal = dendIn.(curName);
    curVal = curVal(out.parentIds);
    
    out.(curName) = curVal;
    clear curName curVal;
end

out.dendAgglos = arrayfun( ...
    @Agglo.fromSuperAgglo, out.dendrites, 'UniformOutput', false);
out.indBigDends = ...
    Agglo.isMaxBorderToBorderDistAbove(param, 5000, out.dendAgglos);
    
out.shAgglos = sh.agglos;
out.shEdges = sh.edges;
out.edges = sh.edges;
out.attached = sh.attached;
out.info = info;

Util.log('Writing output');
Util.saveStruct(outFile, out);
Util.protect(outFile);

Util.log('Done');
