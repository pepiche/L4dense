% evaluation of SynEM on the inhibitory test set on ROI2017
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

p = Gaba.getSegParameters('ex145_ROI2017');
if ~ispc
    inhTestFolder = ['/u/bstaffle/data/L4/Synapses/PerformanceAssessment/' ...
        'InhTestSetROI2017'];
else
    inhTestFolder = ['E:\workspace\data\backed\L4\Synapses\' ...
        'PerformanceAssessment\InhTestSetROI2017\'];
    p.saveFolder = 'E:\workspace\data\backed\20170217_ROI\';
    p.svg.synScoreFile = ['E:\workspace\data\backed\20170217_ROI\' ...
        'globalSynScores.mat'];
    p.svg.borderMetaFile = ['E:\workspace\data\backed\20170217_ROI\' ...
        'globalBorder.mat'];
end

% synem scores
scores = SynEM.Util.loadGlobalSynScores(p.svg.synScoreFile);

% ground truth data
m = load(fullfile(inhTestFolder, 'GroundTruth.mat'));

% get prediction
scores = scores(cell2mat(m.edgeIdx),:);
[scores, label, edgeDir, edgeIdx] = Util.deleteRowsWithNaNs(scores, ...
    m.labelAll, cell2mat(m.edgeDir), cell2mat(m.edgeIdx));
scores = SynapseDetection.scoreForDir(scores, edgeDir);
[rp, thresholds] = SynEM.Eval.interfaceRP(label, scores);

% shaft only
m2 = load(fullfile(inhTestFolder, 'PostsynNodesAndLabels.mat'));

skel = skeleton(fullfile(inhTestFolder, 'Tracings', ...
    'InhTestSet_ShaftSynAnnotations.nml'));
names = skel.names();
names = sort(names);
names(cellfun(@(x)~isempty(strfind(x, 'concensus')), names)) = [];
isShaft = cellfun(@(x)~isempty(strfind(x, 'shaft')), names);
shaftIdx = find(isShaft);
shaftLabel = m2.psLabel(shaftIdx);
toExcl = ismember(label, setdiff(1:max(label), shaftLabel));

% this requires the first section to have run
[rp_sh, thresholds_sh] = SynEM.Eval.interfaceRP(label(~toExcl), ...
    scores(~toExcl));


%% rp curve

Visualization.plotPRCurve({rp, rp_sh}, [], ...
    {'all inh synapses', 'shaft synapses'});


%% look at errors

m = load(p.svg.borderMetaFile, 'borderCoM');
borderCom = m.borderCoM;

% fns
[~,~,~,~,~,~, CI] = SynEM.Util.confusionMatrix( ...
    label, scores > -4 );
fns = label(CI{1, 2});
skel = L4.Util.getSkel();
for i = 1:length(fns)
    thisEdges = edgeIdx(label == fns(i));
    skel = skel.addTree(sprintf('FN_%d', i), borderCom(thisEdges, :));
end
skel.write('InhTestSetROI2017_FNs.nml');

% fps
[~,~,~,~,~,~, CI] = SynEM.Util.confusionMatrix( ...
    label, scores > -4 );
fps = CI{2, 1};
skel = L4.Util.getSkel();
thisEdges = edgeIdx(fps);
skel = skel.addNodesAsTrees(borderCom(thisEdges, :), 'FP');
skel.write('InhTestSetROI2017_FPs.nml');


%%  cnn/shaft-trained classifier
% run first section of script first

% p.saveFolder = '/media/benedikt/DATA2/workspace/data/backed/20170217_ROI/';
files = {'globalSynScores_cnn17_synem_tr2.mat', ...
    'globalSynScores_cnn17_synem_trShaft.mat', ...
    'globalSynScores_cnn17_4_large_synem.mat', ...
    'globalSynScores_synem_preOnly_tr2.mat', ...
    'globalSynScores_synem_preOnly_trShaft.mat', ...
    'globalSynScores_synem_trShaft.mat', ...
    'globalSynScores_cnn17_2_synem_preOnly_trShaft.mat', ...
    'globalSynScores_cnn17_2_synem_trShaft.mat'};

N = length(files) + 1;
tmp = cell(N, 1);
tmp{1} = rp;
rp = tmp;
tmp = cell(N, 1);
tmp{1} = rp_sh;
rp_sh = tmp;
tmp = cell(N, 1);
tmp{1} = scores;
scoresC = tmp;
clear tmp
thr = cell(N, 1);
thr{1} = thresholds;
thr_sh = cell(N, 1);
thr_sh{1} = thresholds_sh;

for i = 1:(N-1)
    
    %ground truth data
    m = load(fullfile(inhTestFolder, 'GroundTruth.mat'));
    
    %synem scores
    p.svg.synScoreFile = fullfile(p.saveFolder, files{i});
    scores = SynEM.Util.loadGlobalSynScores(p.svg.synScoreFile);
    scores = scores(cell2mat(m.edgeIdx),:);

    [scores, label, edgeDir, edgeIdx] = Util.deleteRowsWithNaNs(scores, ...
        m.labelAll, cell2mat(m.edgeDir), cell2mat(m.edgeIdx));
    scores = SynapseDetection.scoreForDir(scores, edgeDir);
    [rp{i+1}, thr{i+1}] = SynEM.Eval.interfaceRP(label, scores);
    [rp_sh{i+1}, thr_sh{i+1}] = SynEM.Eval.interfaceRP( ...
        label(~toExcl), scores(~toExcl));
    scoresC{i+1} = scores;
end

% combine multiple classifiers
scores = (scoresC{7} + scoresC{8}) / 2;
[rp_comb, thr_comb] = SynEM.Eval.interfaceRP(label, scores);
[rp_sh_comb, thr_sh_comb] = SynEM.Eval.interfaceRP( ...
    label(~toExcl), scores(~toExcl));
scores_comb = scores;

names = {'synem', ...
    'cnn17 synem tr2', ...
    'cnn17 synem trShaft', ...
    'cnn17\_4 synem tr2', ...
    'synem preOnly tr2', ...
    'synem preOnly trShaft', ...
    'synem trShaft', ...
    'cnn17\_2\_synem preOnly trShaft', ...
    'cnn17\_2\_synem trShaft'}';
names_sh = cellfun(@(x) [x ' (shaft only)'], names, 'uni', 0);

% shaft only
figure;
plot(rp_sh{1}(:,1), rp_sh{1}(:,2), '--', 'Color', [0, 0, 0]);
hold on
Visualization.plotPRCurve(rp_sh(2:end), [], [], 0, 0, gcf);
legend(names_sh, 'Location', 'SouthWest');
title('Inhibitory test set (shaft only)');

% whole test set
figure;
plot(rp{1}(:,1), rp{1}(:,2), '--', 'Color', [0, 0, 0]);
hold on
Visualization.plotPRCurve(rp(2:end), [], [], 0, 0, gcf);
legend(names, 'Location', 'SouthWest');
title('Inhibitory test set');

% whole test set & shaft
Visualization.plotPRCurve(rp, [], []);
hold on
a = gca;
col = arrayfun(@(x)x.Color, a.Children, 'uni', 0);
col = col(end:-1:1);
for i = 1:N
    plot(rp_sh{i}(:,1), rp_sh{i}(:,2), '--', 'Color', col{i});
end
legend(cat(1, names, names_sh), 'Location', 'SouthWest')
title('Inhibitory test set');


%% look at errors

% m = load(p.svg.borderFile, 'borderCoM');
% borderCom = m.borderCoM;
    
% select classifier
idx = 8;

% % optimal f1
% [~, t_idx] = max(harmmean(rp_sh{idx}, 2));
% t = thr_sh{idx}(t_idx);

% minimal rec
t_idx = find(rp_sh{idx}(:,1) >= 0.8, 1, 'last');
t = thr_sh{idx}(t_idx);

t_idx_2 = find(thr{idx} > t, 1, 'first');
Util.log('Test set performance of %s.', names{idx});
Util.log('All inh syn: %.2f recall, %.2f precision.', rp{idx}(t_idx_2,:));
Util.log('Shaft syn: %.2f recall, %.2f precision.', rp_sh{idx}(t_idx,:));

scores = scoresC{idx};
[~,~,~,~,~,~, CI] = SynEM.Util.confusionMatrix( ...
    label, scores > t, scores );

% tps
tps = label(CI{1, 1});
skel = L4.Util.getSkel();
for i = 1:length(tps)
    thisEdges = edgeIdx(label == tps(i));
    skel = skel.addTree(sprintf('TP_%d_score_%.2f', i, max(scores(CI{1,1}(i)))), ...
        borderCom(thisEdges, :));
    trIdx = skel.numTrees();
    skel = skel.setComment(trIdx, 1:skel.numNodes(trIdx), ...
        arrayfun(@(x)sprintf('%.2f', x), scores(label == tps(i)), 'uni', 0));
    skel.colors{i} = [0 1 1 1];
end

% fns
fns = label(CI{1, 2});
for i = 1:length(fns)
    thisEdges = edgeIdx(label == fns(i));
    skel = skel.addTree(sprintf('FN_%d_score_%.2f', i, max(scores(CI{1,2}(i)))), ...
        borderCom(thisEdges, :));
    trIdx = skel.numTrees();
    skel = skel.setComments(trIdx, 1:skel.numNodes(trIdx), ...
        arrayfun(@(x)sprintf('%.2f', x), scores(label == fns(i)), 'uni', 0));
    skel.colors{end} = [1 1 0 1];
end

% fps
fps = CI{2, 1};
thisEdges = edgeIdx(fps);
skel = skel.addNodesAsTrees(borderCom(thisEdges, :), ...
    arrayfun(@(i, x)sprintf('FP_%d_score_%.2f', i, x), 1:length(fps), ...
        scores(fps)', 'uni', 0));
skel = skel.setDescription(sprintf(['Inhibitory test set shaft ' ...
    'synapses confusion matrix errors at threshold %.3f with ' ...
    '%.2f recall and %.2f precision (shaft syn only).'], t, ...
    rp_sh{idx}(t_idx,1), ...
    rp_sh{idx}(t_idx,2)));
skel.write(sprintf('InhTestSetROI2017_%s_errors.nml', ...
    strrep(names{idx}, '\', '')));
