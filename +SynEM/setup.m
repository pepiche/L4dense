function setup(force)
%SETUP Setup operations for SynEM.
% INPUT force: logical
%           If true, MEX files are rebuilt even if they already exist.
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('force', 'var') || isempty(force)
    force = false;
end

% go to directory of this file
prevDir = pwd();
thisDir = fileparts(mfilename('fullpath'));
cd(thisDir);

% compile mex files
if force || exist(which('SynEM.Aux.eig3S')) ~= 3 %#ok
    mex CXXFLAGS='$CXXFLAGS -std=c++11' ...
        -outdir +Aux...
        +Aux/eig3S.cpp
end

if force || exist(which('SynEM.Aux.sortAbs')) ~= 3 %#ok
    mex CXXFLAGS='$CXXFLAGS -std=c++11'...
        -outdir +Aux...
        +Aux/sortAbs.cpp
end

% go to original directory
cd(prevDir);

end
