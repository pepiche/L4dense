function outMesh = clean(inMesh)
    % CLEAN The reducepatch function can produce a
    % degenerate surface triangulation. This function
    % tries to find these cases and filters them out.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    verts = inMesh.vertices;
    faces = inMesh.faces;
    
    % calculate normals on faces
    normStruct = TypeEM.Shape.Mesh.surfaceNormals(inMesh);
    normVecs = normStruct.vecs;
    normLens = normStruct.lens;
    
    % remove faces if
    %   a) normal vector contains nan values
    %   b) length of normal vector is zero
    facesRemove = ...
        any(isnan(normVecs), 2) | (normLens == 0);
    
    faces = faces(~facesRemove, :);
    facesCount = size(faces, 1);
    
    % extract remaining vertices
    vertIds = unique(faces(:));
    vertIds = sort(vertIds);
    vertCount = numel(vertIds);
    
    % only keep needed vertices
    verts = verts(vertIds, :);
    
    % build the new faces matrix
    facesNew = zeros(facesCount, 3);
    
    for vertIdx = 1:vertCount
        vertId = vertIds(vertIdx);
        vertMask = (faces == vertId);
        
        facesNew(vertMask) = vertIdx;
    end
    
    outMesh = struct;
    outMesh.vertices = verts;
    outMesh.faces = facesNew;
end

