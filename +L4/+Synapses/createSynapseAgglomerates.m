function [ synapses, isSpineSyn, heuristics, debug ] = ...
    createSynapseAgglomerates( p, params, axons, outFile, synScores, synIdx )
%CREATESYNAPSEAGGLOMERATES Wrapper function to crate synapse agglomerates
% from SynEM interface predictions.
% INPUT p: struct
%           Segmentation parameter struct. p is used to load the graph vis
%           Seg.IO.loadGraph, as well as to load the files
%           p.agglo.somaFile
%           p.agglo.spineHeadFile
%       params: (Optional) struct
%           Parameters for synaptic interface agglomeration:
%           'synT': Threshold for synapse detection (Default: -1.67)
%               (see also L4.Synapses.getSynapsePredictions)
%           'probT: Threshold for local pre- and postsynaptic
%               agglomeration. (Default: 0.98)
%               (see also L4.Synapses.clusterSynapticInterfaces)
%           'dist': Maximal distance along the graph in the local pre- and
%               postsynaptic agglomeration. (Default: 2)
%               (see also L4.Synapses.clusterSynapticInterfaces)
%           'marginNM': Margin to the dataset boundary in units of
%               p.raw.voxelSize. (Default: 3000)
%           'sh_wo_synapse_synT': Synaptic interfaces above this threhold
%               that are onto a spine head without any innervation are
%               added as synaptic before agglomeration.
%               (Specify Nan to not execute this heuristic.)
%               see also L4.Synapses.spineWithoutSynapticInterfaceHeuristic
%               (Default: -0.75)
%           'd_bouton': Distance to cluster synaptic interfaces to bouton
%               in the soma FP exclusion heuristic (Default: 1000)
%               (see also L4.Synapses.singleSHAgglosExclusionHeuristic)
%       axons: (Optional) [Nx1] cell
%           Axons in the agglo format used to merge synapses between the
%           same spine and same axon agglo and for the soma FP exclusion
%           heuristic.
%       outFile: (Optional) string
%           Path to save outputs including info struct.
%       synScores: (Optional) [Nx2] double
%           Synapse scores w.r.t. the graph.
%           (Default: graph.synapses scores return from Seg.IO.loadGraph)
%       synIdx: (Optional) [Nx1] logical
%           Interfaces that are considered synaptic but are still
%           post-processed.
%           (Default: graph.synScores > param.synT
%                     see L4.Synapses.getSynapsePredictions)
%
% OUTPUT synapses: [Nx3] table
%           Synapse agglomerates consisting of multiple interfaces
%           (edgeIdx) and the corresponding pre- and postsynaptic indices.
%        isSpineSyn: [Nx1] logical
%           Logical array specifying whether the correspoding synapse in
%           the synapses output is onto a spine head (meaning that at least
%           one postsynaptic segment is in a spine head agglomerate).
%        heuristics: struct
%           Struct with indices of synapses that should be deleted
%           according to several heuristics.
%           'toDelSingleSH': [Nx1] int
%               Linear indices of the synapses that should be deleted
%               according to the heuristic that a spine head consisting of
%               a singel segment should only have one synapse.
%               (see also L4.Synapses.singleSHAgglosExclusionHeuristic)
%           'toDelSomaFPs': [Nx1] int
%               Linear indices of the synapses that should be deleted
%               according to the heuristic that if a bouton has both a soma
%               synapse and a spine head synapse which is the only synapse
%               onto the corresponding spine, then the soma synapse is
%               deleted.
%               (see also L4.Synapses.somaFPExclusionHeuristic)
%        debug: struct
%           Struct with additional data for visualization/debugging.
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo();

defParams = getDefParams();
if exist('params', 'var') && ~isempty(params)
    params = Util.setUserOptions(defParams, params);
else
    params = defParams;
end
Util.log('Run parameters set to:');
disp(params);

Util.log('Loading graph.');
[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);

if exist('synScores', 'var') && ~isempty(synScores)
    assert(isequal(size(graph.synScores), size(synScores)), ...
        'Size of synScores does not match the graph.');
    graph.synScores = synScores;
    Util.log('Overwriting graph,synScores with input synScores');
end

Util.log('Loading soma agglos from %s.', p.agglo.somaFile);
m = load(p.agglo.somaFile);
somaAgglos = m.somaAgglos(:,1);

Util.log('Postprocessing SynEM predictions.');
if ~exist('synIdx', 'var') || isempty(synIdx)
    synIdx = [];
else
    Util.log('Using input synIdx instead of applying param.synT.');
end
synIdx = L4.Synapses.getSynapsePredictions(graph.synScores, ...
    params.synT, true, graph.prob, [], graph.edges, ...
    segmentMeta.myelinScore, [], somaAgglos, synIdx);
debug.synIdxPostProc = synIdx;

Util.log('Discarding interfaces at margin of dataset.');
margin = round(params.marginNM./p.raw.voxelSize(:));
bboxM = bsxfun(@plus, p.bbox, [margin, -margin]);
isAtBorder = ~Util.isInBBox(borderMeta.borderCoM, bboxM);
synIdx(isAtBorder) = false;
debug.synIdxCenter = synIdx;

if ~isnan(params.sh_wo_synapse_synT)
    Util.log(['Adding most likely interface (with threshold ' ...
        'above %.3f) for attached spine heads ' ...
        'without innervation (for spines from %s).'], ...
        params.sh_wo_synapse_synT, p.agglo.spineHeadFile);
    m = load(p.agglo.spineHeadFile);
    shAgglos = m.shAgglos;
    isAttached = m.attached > 0;
    shAgglos = shAgglos(isAttached);
    toDel = ~cellfun( ...
        @(x)all(Util.isInBBox(segmentMeta.point(x, :), bboxM)), shAgglos);
    shAgglos(toDel) = [];
    synIdx = L4.Synapses.spineWithoutSynapticInterfaceHeuristic( ...
        shAgglos, synIdx, graph.edges, graph.synScores, ...
        params.sh_wo_synapse_synT);
    debug.synIdxShWoSynHeuristic = synIdx;
end

Util.log('Create synapse agglomerates.');
synapses = L4.Synapses.clusterSynapticInterfaces(graph.edges, ...
    graph.synScores, synIdx, graph.prob, params.probT, params.dist);

Util.log('Labelling spine head synapses for spines from %s.', ...
    p.agglo.spineHeadFile);
m = load(p.agglo.spineHeadFile);
shAgglos = m.shAgglos;
isSH = false(length(segmentMeta.voxelCount), 1);
isSH(cell2mat(shAgglos)) = true;
isSpineSyn = cellfun(@(x)any(isSH(x)), synapses.postsynId); 

if exist('axons', 'var') && ~isempty(axons)
    Util.log('Clustering synapses between same axon and spine head.');
    [synapses, ~, toDel] = L4.Synapses.clusterSameSpineSynapses(axons, ...
        shAgglos, synapses);
    isSpineSyn(toDel) = [];
end

Util.log('Running single segment spine head exclusion heuristic.')
toDelSingleSH = L4.Synapses.singleSHAgglosExclusionHeuristic(synapses, ...
    graph.synScores, shAgglos);
heuristics.toDelSingleSH = toDelSingleSH;

if exist('axons', 'var') && ~isempty(axons)
    Util.log('Running soma FP exclusion heuristic.');
    toDelSomaFPs = L4.Synapses.somaFPExclusionHeuristic(p, synapses, ...
        axons, somaAgglos, shAgglos, borderMeta.borderCoM, ...
        params.d_bouton);
    heuristics.toDelSomaFPs = toDelSomaFPs;
end

if exist('outFile', 'var') && ~isempty(outFile)
    Util.log('Saving output to %s.', outFile);
    Util.save(outFile, synapses, isSpineSyn, info);
end

end

function params = getDefParams()
    params.synT = -1.67;
    params.probT = 0.98;
    params.dist = 2;
    params.marginNM = 3000;
    params.d_bouton = 1e3; % nm
    params.sh_wo_synapse_synT = -0.75;
end
