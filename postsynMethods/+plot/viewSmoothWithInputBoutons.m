%viewSmoothWithInputBoutons
outDir = '/tmpscratch/sahilloo/L4/data/';
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

% load dendrites all info
m=load(fullfile(outDir,'spineDistribution.mat'));                                                                                       
spineTable = m.spineTable;
m=load(fullfile(outDir,'dendritesPostQueryWithShafts.mat'));
dendritesTable = [spineTable table(m.dendritesShaftSegs)]; % add shaft segs to dendrites

% load smooth dendrites with high prob
m=load(fullfile(outDir,'smoothPreBoutons.mat'));
smoothInBoutonsIdx = m.inputBoutonsIdx;
smoothInBoutonLabels = m.inputBoutonLabels;
if exist(fullfile(outDir,'idxSmooth.mat'),'file')
    m=load(fullfile(outDir,'idxSmooth.mat'));
    idxSmooth = m.idxSmooth; 
else
    idxSmooth = Smooth.viewSmoothDendritesAllInRange({[],5},{100,300});   
end
smoothTable = dendritesTable(idxSmooth,:);
disp('Finished loading smooth and their input boutons');

% load all axonBoutons and find smooth
m=load(fullfile(outDir,'boutonTable.mat'));
allBoutons = vertcat(m.boutonTable{:,2}{:});   
assert(size(allBoutons,1)==size(smoothInBoutonsIdx{1},1))
smoothInBoutons = cellfun(@(x) allBoutons(x),smoothInBoutonsIdx,'uni',0);

% view in wK smooth dendrites with input axon boutons only
idxToPlot = randperm(size(smoothTable,1),10);
for i=1:length(idxToPlot)
    nodes = smoothTable{idxToPlot(i),1}; % dendrite agglo
    skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
    skel.names(1) = {'smooth'}; 
    curB = smoothInBoutons{idxToPlot(i)};
    curBLabels = smoothInBoutonLabels{idxToPlot(i)};
        for j=1:size(curB,1)
          skelBouton =  Skeleton.fromMST({points(curB{j},:)},voxelSize);
          skelBouton.names(1) = {['b' num2str(j) '_' num2str(curBLabels(j))]};
          skel = skel.addTreeFromSkel(skelBouton);
          clear skelBouton
        end
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDir, ['nmls/smoothWInBoutons_' num2str(i) '_' ...
        datestr(now,'yyyymmdd') '.nml']));
end
