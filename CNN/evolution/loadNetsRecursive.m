function nets = loadNetsRecursive(path)

nets = struct;
files = dir(path);

for file=1:length(files)
	if not(strcmp(files(file).name, '.') || strcmp(files(file).name,'..'))
		if files(file).isdir
			nets.(files(file).name) = loadNetsRecursive([path '/' files(file).name]);
		else
			if strcmp(files(file).name(end-3:end), '.mat') && file == length(files)
				a = load([path '/' files(file).name]);
				nets.(files(file).name(1:end-4)) = a.cnet;
			end
		end
	end
end

end

