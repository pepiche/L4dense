NOTE
====

TypeEM contains its own version of the SynEM filter bank, because it was
build on top of an older version than was used for synapse detection in
the L4 project. This separate copy is only here for the purpose of
reproducibility. If you're thinking about applying TypeEM to a new
dataset, please consider using the latest SynEM version instead.
