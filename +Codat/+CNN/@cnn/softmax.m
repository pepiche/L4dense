function x = softmax( x )
%SOFTMAX Softmax activation function.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% shift to make numerically stable (avoid Inf after exp)
x = bsxfun(@minus, x, max(x, [], 4));
exps = exp(x);
x = bsxfun(@rdivide, exps, max(sum(exps, 4), 1e-8));

end

