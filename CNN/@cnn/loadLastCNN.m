function cnet = loadLastCNN( cnet )
if exist(cnet.run.savingPath, 'dir')
	files = dir([cnet.run.savingPath '*.mat']);
	if length(files) == 0
		result = struct();
		warning(['No CNN found: ' cnet.run.savingPath]);
	else
		idx = length(files);
		while true
			try
				a = load([cnet.run.savingPath files(idx).name]);
				cnet = a.cnet;
				break;
			catch
				warning(['Corrupt file: ' cnet.run.savingPath files(idx).name]);
				idx = idx - 1;
				if idx == 0
					result = struct();
					break;
				end
			end
		end
	end
else
	result = struct();
	warning(['Directory does not exist: ' cnet.run.savingPath]);
end

end

