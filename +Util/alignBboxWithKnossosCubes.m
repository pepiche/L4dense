function [ bbox ] = alignBboxWithKnossosCubes( bbox, cubeSize )
%ALIGNBBOXWITHKNOSSOSCUBES Align the lower left end of a bbox with knossos
%cubes.
% INPUT bbox: [3x2] int
%           Bounding box.
%       cubeSize: (Optional)[1x3] int
%           Size of the knossos cubes.
%           (Default: [128 128 128])
% OUTPUT bbox: [3x2] int
%           The aligned bbox.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('cubeSize', 'var') || isempty(cubeSize)
    cubeSize = [128 128 128];
end

bbox(:,1) = bbox(:,1) - mod(bbox(:,1), cubeSize(:)) + 1;

end

