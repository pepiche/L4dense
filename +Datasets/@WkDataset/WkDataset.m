classdef WkDataset < handle
    %WKDATASET Wrapper class for Wk datasets.
    % Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
    
    properties
        root                    % root folder of the dataset
        backend                 % the backed ('wkwrap' or 'wkcube')
        p                       % additional parameters needed for the
                                % backend
        dtype                   % data type of the dataset
        numChannels             % number of channels of the dataset
    end
    
    methods
        function obj = WkDataset(varargin)
            % Class construct
            % wkD = WkDataset() default constructor not pointing to any
            %   dataset.
            %
            % wkD = WkDatset(p) construct a dataset from a parameter
            %   struct. p must contain the field root. For a wkcube dataset
            %   it can furthermore contain the fields
            %   'prefix': string
            %       Prefix of cube filenames (Default: determined
            %       from files).
            %   'dtype': string
            %       Data type of the dataset.
            %       To change the dtype for wkcube datasets, it needs to be
            %       changed in obj.p.dtype.
            %       Datatype (Default: 'uint8')
            %   'suffix': string
            %       Suffix of cube filenames. (Default: '')
            %   'cubesize': [1x3] or [1x4] int
            %       Size of the cubes.
            %       (Default: [128 128 128 1])
            %   'ending': string
            %       File extension of the cube files.
            %       (Default: 'raw')
            % wkD = WkDataset(root) constructor for a 'wkwraw' dataset.
            % wkD = WkDataset(root, prefix, dtype, cubesize)
            %       constructor for a 'wkcube' dataset. All but the
            %       first argument are optional with the default values
            %       specified as above.
            
            if isempty(varargin) % empty dataset
                return;
            end
            
            if length(varargin) == 1
                if isstruct(varargin{1})
                    p = varargin{1};
                    if ~isfield(p, 'root')
                        error(['The parameter struct most contain the ' ...
                            'field ''root''']);
                    end
                    obj.root = p.root;
                    if obj.iswkwDataset(obj.root)
                        obj.backend = 'wkwrap';
                        obj.p.xyzcOrder = true;
                        obj.p.numChannels = obj.getChannels();
                    else
                        obj.backend = 'wkcube';
                        obj.p = p;
                        if ~isfield(p, 'suffix')
                            obj.p.suffix = '';
                        end
                        if ~isfield(p, 'ending')
                            obj.p.ending = 'raw';
                        end
                        if ~isfield(p, 'prefix')
                            obj.p.prefix = obj.getPrefix();
                        end
                        if ~isfield(p, 'cubesize')
                            obj.p.cubesize = [128 128 128 1];
                        end
                        if ~isfield(p, 'dtype')
                            obj.p.dtype = 'uint8';
                        end
                    end
                elseif ischar(varargin{1})
                    obj.root = varargin{1};
                    if obj.iswkwDataset(obj.root)
                        obj.backend = 'wkwrap';
                        % TODO determine from header or similar
                        obj.p.numChannels = obj.getChannels();
                        obj.p.xyzcOrder = true;
                    else
                        obj.backend = 'wkcube';
                        obj.p.suffix = '';
                        obj.p.ending = 'raw';
                        obj.p.prefix = obj.getPrefix();
                        obj.p.cubesize = [128 128 128 1];
                        obj.p.dtype = 'uint8';
                    end
                end
            else
                obj.root = varargin{1};
                obj.backend = 'wkcube';
                obj.p.suffix = ''; % too lazy to make specific input for that
                obj.p.ending = 'raw';% too lazy to make specific input for that
                if length(varargin) > 1 && ~isempty(varargin{2})
                    obj.p.prefix = varargin{2};
                else
                    obj.p.prefix = obj.getPrefix();
                end
                if length(varargin) > 2
                    obj.p.dtype = varargin{3};
                else
                    obj.p.dtype = 'uint8';
                end
                if length(varargin) > 3
                    obj.p.cubesize = varargin{4};
                else
                    obj.p.cubesize = [128 128 128 1];
                end
            end
            obj.root = Util.addFilesep(obj.root);
            
            obj.dtype = obj.p.dtype;
            switch obj.backend
                case 'wkwrap'
                    obj.numChannels = obj.p.numChannels;
                case 'wkcube'
                    obj.numChannels = obj.p.cubesize(4);
            end
        end
    end
    
    methods (Access = private)
        function iswkw = iswkwDataset(~, root)
            % check if a .wkw file exists
            iswkw = logical(exist(fullfile(root, 'header.wkw'), 'file'));
        end
        
        function noChannels = getChannels(obj)
            fid = fopen(fullfile(obj.root, 'header.wkw'));
            header = fread(fid, 'uint8=>uint8');
            voxel_type = header(7);
            
            % determine voxel datatype in bytes
            switch voxel_type
                case 1
                    d_size = 1;
                    dtype = 'uint8';
                case 2
                    d_size = 2;
                    dtype = 'uint16';
                case 3
                    d_size = 4;
                    dtype = 'uint32';
                case 4
                    d_size = 8;
                    dtype = 'uint64';
                case 5
                    d_size = 4;
                    dtype = 'single';
                case 6
                    d_size = 8;
                    dtype = 'double';
            end
            obj.p.dtype = dtype;
            voxel_size = double(header(8));
            noChannels = voxel_size / d_size;
        end
    end
    
    methods (Static)
        coords = transformCoordsToMag(coords, magFrom, magTo);
    end
    
end

