function dat = getSegDataset()
%GETSEGDATASET Get a dataset object for the L4 segmentation data.
% OUTPUT dat: Datasets.WkDataset object
%           Dataset object with path to a wkw dataset for the L4 default
%           segmentation data.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

datPath = '/wKcubes_live/2012-09-28_ex145_07x2_ROI2017/segmentation/1';
dat = Datasets.WkDataset(datPath);

end

