function [x_move, y_move] = findMatchingFramesNew(shifts_unclean, columns, rows, slices, x_it, y_it, myslice, range_needed, ttemp)
shifts = cleanupShifts(shifts_unclean, ttemp)
old_columns = 3;
old_rows = 3;
lateral_shift = 64;

width = 3072;
height = 2048;
size_small_images = 256;

padding = 1000;

xx = 0.2*(squeeze(mean(mean(shifts(:,:,1:end-5,1))))+...
    squeeze(mean(mean(shifts(:,:,2:end-4,1))))+...
    squeeze(mean(mean(shifts(:,:,3:end-3,1))))+...
    squeeze(mean(mean(shifts(:,:,4:end-2,1))))+...
    squeeze(mean(mean(shifts(:,:,5:end-1,1)))));

yy = 0.2*(squeeze(mean(mean(shifts(:,:,1:end-5,2))))+...
    squeeze(mean(mean(shifts(:,:,2:end-4,2))))+...
    squeeze(mean(mean(shifts(:,:,3:end-3,2))))+...
    squeeze(mean(mean(shifts(:,:,4:end-2,2))))+...
    squeeze(mean(mean(shifts(:,:,5:end-1,2)))));

naive_x = repmat(([1:12]*size_small_images)',...
    [3, rows, slices]) - size_small_images / 2 + padding;
naive_y = repmat([1:8]*size_small_images,...
    [columns, 3, slices]) - size_small_images / 2 + padding;

naive = ...
    cat(4, naive_x, naive_y);
xx= [xx(1); xx(1); xx(1); xx(1);xx(1); xx];
yy= [yy(1); yy(1); yy(1); yy(1);yy(1); yy];

all_shifts = shifts + naive - repmat(permute(cat(4, xx, yy),[3 2 1 4]),[36,24,1,1]);
for dimIt = 1:2
    x_range = [(x_it - 1) * (columns / old_columns) + 1, x_it * (columns / old_columns)];
    y_range = [(y_it - 1) * (rows / old_rows) + 1, y_it * (rows / old_rows)];
    
    ssv_xvsy_origvsnew{dimIt,1} = ...
        naive(x_range(1) : x_range(2), y_range(1) : y_range(2), myslice, dimIt);
    ssv_xvsy_origvsnew{dimIt,2} = ...
        all_shifts(x_range(1) : x_range(2), y_range(1) : y_range(2), myslice, dimIt);
end
for dimIt=1:2
    for typeIt=1:2
        ssv_xvsy_origvsnew{dimIt,typeIt} = ssv_xvsy_origvsnew{dimIt,typeIt}(:);
    end
end
% plot([ssv_xvsy_origvsnew_norm{1, 1}, ssv_xvsy_origvsnew_norm{1, 2}]', [ssv_xvsy_origvsnew_norm{2, 1}, ssv_xvsy_origvsnew_norm{2, 2}]')
% plot([ssv_xvsy_origvsnew_norm{2, 1}, ssv_xvsy_origvsnew_norm{2, 2}]', [ssv_xvsy_origvsnew_norm{1, 1}, ssv_xvsy_origvsnew_norm{1, 2}]')
moving_points = [ssv_xvsy_origvsnew{2, 1}, ssv_xvsy_origvsnew{1, 1}];
fixed_points = [ssv_xvsy_origvsnew{2, 2} ssv_xvsy_origvsnew{1, 2}];
[Xq,Yq] = meshgrid(1000 + range_needed{1},1000 + range_needed{2});
[X,Y] = meshgrid(1128-256:256:3944+256,1128-256:256:2920+256);
prelim = reshape(ssv_xvsy_origvsnew{1, 2},12,8)';
prelim = [prelim(1,:); prelim; prelim(end,:)];
prelim = [prelim(:,1)-256 prelim prelim(:,end)+256];

x_move = round(interp2(X, Y, prelim ,Xq,Yq))'+64*y_it;

prelim = reshape(ssv_xvsy_origvsnew{2, 2},12,8)';
prelim = [prelim(:,1) prelim prelim(:,end)];
prelim = [prelim(1,:)-256; prelim; prelim(end,:)+256];

y_move = round(interp2(X, Y, prelim ,Xq,Yq))'-64*x_it;
