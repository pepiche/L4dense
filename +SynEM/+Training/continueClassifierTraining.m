function continueClassifierTraining( runFolder, outputFile, options )
%CONTINUECLASSIFIERTRAINING Continue training for a classifier from a
% trainingPipeline.
% INPUT runFolder: string
%           Path to trainingPipeline run save folder.
%       outputFile: string
%           Name of the output file (will be saved in the
%           classifier/outputFile). There will be two versions of the
%           classifier stored. One with the name outputFile_Full
%           containing the full classifier and one only name outputFile
%           with the compact classifier. At last the classifier is
%           evaluated on the validation and test data and the results are
%           stored in result_outputFile.
%       options: struct
%           Struct containing the training options
%           (see function defaultOptions() below).
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo();

defOpts = defaultOptions();

% load run options
m = load(fullfile(runFolder, 'TrainingPipelineInputs.mat'), 'options');
options.discardUndecided = m.options.discardUndecided;
options.labelType = m.options.labelType;

% user options
options = Util.setUserOptions(defOpts, options);

% check output files
classFolder = fullfile(runFolder, 'classifier');
[~, outputFile] = fileparts(outputFile);
outFileFull = fullfile(classFolder, [outputFile '_Full.mat']);
outFileCompact = fullfile(classFolder, [outputFile '.mat']);
resultFile = fullfile(classFolder, ['result_' outputFile '.mat']);

if exist(outFileFull, 'file') && ~options.forceOverwrite
    error('The file %s already exists. Aborting ...', outFileFull);
end
if exist(outFileCompact, 'file') && ~options.forceOverwrite
    error('The file %s already exists. Aborting ...', outFileCompact);
end

Util.log('Loading classifier %s.', options.classifierFile);
m = load(fullfile(runFolder, 'classifier', options.classifierFile));
classifier = m.classifier;

Util.log('Continuing classifier training.');
classifier.ens = classifier.ens.resume(options.nlearn, 'nprint', 100);
classifier.options.nlearn = classifier.options.nlearn + options.nlearn;
L = classifier.resubLoss();
try
    classifier = classifier.calculatePredVar();
catch err
    warning('Could not calculate pred var due to %s.', err.message);
end

% save classifier
Util.log('Saving full classifier to %s.', outFileFull);
Util.save(outFileFull, classifier, info);
classifier = classifier.compact();
Util.log('Saving compact classifier to %s.', outFileCompact);
Util.save(outFileCompact, classifier, info);

[~, outName] = fileparts(outFileCompact);
result = SynEM.Training.evalPipelineClassifier_v2(runFolder, ...
    outName, false);
result.L = L;

% save result
Util.log('Saving result to %s.', resultFile);
Util.save(resultFile, result);

end

function defOpts = defaultOptions()
defOpts.nlearn = 1500;
defOpts.classifierFile = 'LogitBoostFull.mat';
defOpts.testSetPath = '/u/bstaffle/data/SynEM/data/';
defOpts.testSetVer = [];
defOpts.forceOverwrite = false;
defOpts.labelType = 'direction';
defOpts.discardUndecided = false;
end
