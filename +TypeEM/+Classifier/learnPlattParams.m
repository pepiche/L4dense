function [a, b] = learnPlattParams(scores, labels)
    % [a, b] = learnPlattParams(scores, labels)
    %   This function learns the Platt (1999) scaling.
    %
    %   It assumes that classification scores can be trans-
    %   formed into probability estimates by passing them
    %   through a sigmoid function:
    %
    %   prob(score) = 1 / (1 + exp(score * a + b))
    %
    %   This works remarkably well for the output of SVMs
    %   and boosted ensembles of decision trees.
    %
    %   The parameters `a` and `b` are determined by mini-
    %   mization of the negative log-likelihood. Here, the
    %   optimization toolbox is used for this purpose.
    %
    % scores
    %   Vector with classification scores
    %
    % labels
    %   True labels from the set {0, 1}
    %
    % a and b
    %   The Platt scaling parameters
    %
    % References
    %   Platt (1999). Probabilistic Outputs for Support
    %   Vector Machines and Comparisons to Regularized
    %   Likelihood Methods. Adv. in Large Margin Classifiers.
    %
    %   Niculescu-Mizil and Caruana (2005). Predicting Good
    %   Probabilities with Supervised Learning. Proc ICML.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % sanity check
    assert(isequal(size(scores), size(labels)));
    assert(isequal(unique(labels(:)), [0; 1]));
    
    % sigmoid
    sigExp = @(s, a, b) exp(s .* a + b);
    sig = @(s, a, b) 1 ./ (1 + sigExp(s, a, b));
    
    % its derivatives
    sigDb = @(s, a, b) -sigExp(s, a, b) .* sig(s, a, b) .^ 2;
    sigDa = @(s, a, b)  s .* sigDb(s, a, b);
      
    % log-likelihood
    logLik = @(s, l, a, b) -sum( ...
             l  .* log(    sig(s, a, b)) ...
      + (1 - l) .* log(1 - sig(s, a, b)));
  
    % and its derivatives
    logLikDa = @(s, l, a, b) -sum( ...
             l  .* sigDa(s, a, b) ./      sig(s, a, b) ...
      - (1 - l) .* sigDa(s, a, b) ./ (1 - sig(s, a, b)));
    logLikDb = @(s, l, a, b) -sum( ...
             l  .* sigDb(s, a, b) ./      sig(s, a, b) ...
      - (1 - l) .* sigDb(s, a, b) ./ (1 - sig(s, a, b)));
  
    % setting up the problem
    opts = optimoptions( ...
        'fminunc', ...
        'Algorithm', 'trust-region', ...
        'Display', 'notify-detailed', ...
        'SpecifyObjectiveGradient', true);
    
    func = @(in) deal( ...
        logLik(  scores, labels, in(1), in(2)), ...
       [logLikDa(scores, labels, in(1), in(2)); ...
        logLikDb(scores, labels, in(1), in(2))]);
    
    % find optimal a and b
    ab = fminunc(func, [0, 0], opts);
    
    % define platt scaling
    a = ab(1);
    b = ab(2);
end
