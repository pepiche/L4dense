function fmap = buildFeatureMap(param, fmapName, varargin)
    % fmap = buildFeatureMap(param, fmapName, varargin)
    %   Builds the named feature map.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    areaT = 0;
    subvolsSize = [];
    quantiles = [0.25 0.5 0.75 0 1];
    moments = true(4, 1);

    % voxel size
    voxelSize = reshape(param.raw.voxelSize, 1, []);
    sigma = 12 ./ voxelSize;

    switch fmapName
        case 'texture'
            fmap = buildTextureFeatMap(varargin{:});
        case 'shapeBasic'
            fmap = buildShapeBasicFeatMap(varargin{:});
        otherwise
            error('Unknown feature map');
    end
    
    % select all features
    fmap.setSelectedFeat();

    function fmap = buildTextureFeatMap(inputType)
        import TypeEM.SynEM.FeatureMap;
        import TypeEM.SynEM.Feature.*;
        
        switch inputType
            case 'raw'
                % zero mean and unit standard deviation
                normFunc = @(x) (single(x) - 122) ./ 22;
            case 'membrane'
                % identity
                normFunc = @(x) x;
            otherwise
                error('Invalid input type');
        end

        % construct feature map
        fmap = FeatureMap(subvolsSize, areaT, ...
            quantiles, moments, 'single', voxelSize, normFunc);

        % based on Benedikt's "paperNew" feature map
        fmap.addFeature(Identity());
        fmap.addFeature(EigenvaluesStructureTensor(sigma, sigma, 2, 2));
        fmap.addFeature(EigenvaluesStructureTensor(sigma, 2 .* sigma, 2, 2));
        fmap.addFeature(EigenvaluesStructureTensor(2 .* sigma, sigma, 2, 2));
        fmap.addFeature(EigenvaluesStructureTensor(2 .* sigma, 2 .* sigma, 2, 2));
        fmap.addFeature(EigenvaluesStructureTensor(3 .* sigma, 3 .* sigma, 2, 2));
        fmap.addFeature(EigenvaluesHessian(sigma, 2));
        fmap.addFeature(EigenvaluesHessian(2 .* sigma, 2));
        fmap.addFeature(EigenvaluesHessian(3 .* sigma, 2));
        fmap.addFeature(EigenvaluesHessian(4 .* sigma, 2));
        fmap.addFeature(GaussFilter(sigma, 2));
        fmap.addFeature(GaussFilter(2 .* sigma, 2));
        fmap.addFeature(GaussFilter(3 .* sigma, 2));
        fmap.addFeature(DoG(sigma, 1.5, 2));
        fmap.addFeature(DoG(sigma, 2, 2));
        fmap.addFeature(DoG(2 .* sigma, 1.5, 2));
        fmap.addFeature(DoG(2 .* sigma, 2, 2));
        fmap.addFeature(DoG(3 .* sigma, 1.5, 2));
        fmap.addFeature(LoG(sigma, 2));
        fmap.addFeature(LoG(2 .* sigma, 2));
        fmap.addFeature(LoG(3 .* sigma, 2));
        fmap.addFeature(LoG(4 .* sigma, 2));
        fmap.addFeature(GaussGradientMagnitude(sigma, 2));
        fmap.addFeature(GaussGradientMagnitude(2 .* sigma, 2));
        fmap.addFeature(GaussGradientMagnitude(3 .* sigma, 2));
        fmap.addFeature(GaussGradientMagnitude(4 .* sigma, 2));
        fmap.addFeature(GaussGradientMagnitude(5 .* sigma, 2));
        fmap.addFeature(StdFilter([5, 5, 3]));
        fmap.addFeature(EntropyFilter([5, 5, 3], [], true));
        fmap.addFeature(IntVar([3, 3, 1]));
        fmap.addFeature(IntVar([5, 5, 3]));
        fmap.addFeature(AverageFilter('ball', 3, voxelSize ./ min(voxelSize)));
        fmap.addFeature(AverageFilter('ball', 6, voxelSize ./ min(voxelSize)));
    end

    function fmap = buildShapeBasicFeatMap()
        import TypeEM.SynEM.FeatureMap;
        import TypeEM.SynEM.Feature.*;
        
        % construct feature map
        fmap = FeatureMap(subvolsSize, areaT, ...
            quantiles, moments, 'single', voxelSize);
        
        fmap.addFeature(Volume([], 1), 1);
        fmap.addFeature(Volume( ...
            '@(x)single(nthroot(6*x*(11.24*11.24*28)*pi,3))', 1), 1);
        fmap.addFeature(PrincipalAxis('length'), 1);
        fmap.addFeature(ConvexHull(1), 1);
    end
end