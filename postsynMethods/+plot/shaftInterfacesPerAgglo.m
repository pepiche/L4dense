% plot shaft interfaces per dendrite agglomerate

load('/tmpscratch/sahilloo/L4/data/dendritesPostQueryWithShafts.mat');
load('/tmpscratch/sahilloo/L4/data/dendritesPostQueryStateLengths.mat');

%% plot unique segments and actual interfaces

subplot(3,1,1)
hold on
numInterfaces = cellfun(@numel,dendritesShaftSegs);
% countU = cellfun(@(x) numel(unique(x)),dendritesShaftSegs);
aggloSegsNum = cellfun(@numel,dendritesPostQuery);

plot(log(aggloLens))
plot(log(aggloSegsNum))
plot(log(numInterfaces))
hold on;

xlabel('DendriteAgglos id');
ylabel('log scale');
legend({'path lengths','# agglo segments','# interfaces'});
hold off

subplot(3,1,2)
hold on
[~,sortIdx]=sort(numInterfaces);

plot(log(aggloLens(sortIdx)))
plot(log(aggloSegsNum(sortIdx))) 
plot(log(numInterfaces(sortIdx)))

xlabel('dendriteAgglos sorted by # interfaces');
ylabel('log scale');
legend({'path lengths','# agglo segments','# interfaces'});
% ylim([0 10])
hold off

% plot sorted as per dendrite agglo length
subplot(3,1,3)
hold on
[~,sortIdx] = sort(aggloLens);
plot(log(aggloLens(sortIdx)))
plot(log(aggloSegsNum(sortIdx)))
plot(log(numInterfaces(sortIdx)))
xlabel('dendriteAgglos sorted by path length');
ylabel('log scale');
legend({'path lengths','# agglo segments','# interfaces'});
hold off