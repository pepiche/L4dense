% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear

%% Configuration
rootDir = '/gaba/u/mberning/results/pipeline/20170217_ROI';

% Single-segment classifier for spine heads
segClassifiers = 'segmentClassifier.mat';

% Segment-agglomerate classifier for astrocytes, axons, and dendrites
aggloClassifiers = 'segmentAggloClassifier.mat.20170331';

info = Util.runInfo();
Util.showRunInfo(info);

%% Loading data
param = load(fullfile(rootDir, 'allParameter.mat'));
param = param.p;

segClassifiers = fullfile(param.saveFolder, segClassifiers);
segClassifiers = load(segClassifiers, '-mat');

aggloClassifiers = fullfile(param.saveFolder, aggloClassifiers);
aggloClassifiers = load(aggloClassifiers, '-mat');

%% Calculate and plot precision / recall curve
[segPrecRec, segFig] = ...
    TypeEM.Classifier.test(param, segClassifiers);

[aggloPrecRec, aggloFig] = ...
    TypeEM.Classifier.test(param, aggloClassifiers);

precRec = aggloPrecRec;
precRec.spinehead = segPrecRec.spinehead;

%% Customize plot
clear cur*;
curLimits = [50, 100];

aggloFig.Color = 'white';
aggloFig.Position(3:4) = [480, 340];

ax = findobj(aggloFig.Children, 'Type', 'Axes');
leg = findobj(aggloFig.Children, 'Type', 'Legend');

% Join plots
spineHeadPlot = segFig.findobj( ...
    '-depth', 2, 'DisplayName', 'Spinehead');
copyobj(spineHeadPlot, ax);

close(segFig);
clear segFig;

% Sort classes alphabetically
[~, sortIds] = sort(leg.String);
ax.Children = ax.Children(sortIds);

axis(ax, 'square');
ax.TickDir = 'out';
xlim(ax, curLimits);
ylim(ax, curLimits);

leg.Box = 'off';
leg.Location = 'EastOutside';

title(ax, ...
    {info.filename; info.git_repos{1}.hash}, ...
    'FontWeight', 'normal', 'FontSize', 10);

%% Calculate key numbers
% Precision and recall at point of maximal area-under-curve
optimPrecRec = structfun( ...
    @(pr) pr(Util.nthOutput(2, @() max(prod(pr, 2))), :), ...
    precRec, 'UniformOutput', false);

fprintf('Precision / recall at max. AUC\n');
fprintf('\n'); disp(optimPrecRec);
