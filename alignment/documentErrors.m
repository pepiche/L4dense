function documentErrors(problems, C, all_fns, generous_z)
for problem_it = 1 : size(problems.todo, 1)
    C.dimsTODO = problems.todo(problem_it, 2);
    C.document_ccs = 'full';
    C.joblist = 1 : C.procs;
    x_ind = problems.todo(problem_it, 3);
    y_ind = problems.todo(problem_it, 4);
    z_ind = mod(problems.todo(problem_it, 5) - 1, C.slPth) + 1;
    if generous_z
        C.subslicesTODO = [x_ind, x_ind; y_ind, y_ind; 1, C.slPth + 1];
    else
        C.subslicesTODO = [x_ind, x_ind; y_ind, y_ind; z_ind, z_ind + 1];
    end
    if C.dimsTODO < 3
        C.subslicesTODO(C.dimsTODO, 2) = C.subslicesTODO(C.dimsTODO, 2) + 1;
    end
    proc_it = ceil(problems.todo(problem_it, 5) / C.slPth);
    slice = all_fns(:, :, (proc_it - 1) * C.slPth + 1 : proc_it * C.slPth + 1);
    KMBmeasureShifts(C, proc_it, slice);
end