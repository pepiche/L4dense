function [ rcnn ] = setParamsToActvtClass( rcnn, varargin )
%SETPARAMSTOACTVTCLASS Call setParamsToActvtClass for each cnet.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~isempty(varargin)
    rcnn.cnets = cellfun(@(x)x.setParamsToActvtClass(varargin{:}), ...
        rcnn.cnets, 'UniformOutput', false);
else
    rcnn.cnets = cellfun(@(x)x.setParamsToActvtClass(), ...
        rcnn.cnets, 'UniformOutput', false);
end


end

