function synScores = loadGlobalSynScores( p, maxEdgeIdx, undir )
%LOADGLOBALSYNSCORES Auxiliary function to load the global synapse scores.
% INPUT p: struct or string
%           Segmentation parameter struct or full path to global synapse
%           score file. If p is a struct it is assumed that the synapse
%           scores are saved at [p.saveFolder 'globalSynScores.mat']
%       maxEdgeIdx: (Optional) int
%           Number of rows of the edges list
%           (Default: the maximum edge index in the synapse score file).
%       undir: (Optional) logical
%           Flag to return only the maximal synapse score over the two
%           interface directions for each interface.
%           (Default: false)
% OUTPUT synScores: [Nx2] double or [Nx1] double
%           The synapse scores for each interface.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if isstruct(p)
    m = load(fullfile(p.saveFolder, 'globalSynScores.mat'));
else
    m = load(p);
end

if ~exist('maxEdgeIdx', 'var') || isempty(maxEdgeIdx)
    maxEdgeIdx = max(m.edgeIdx);
end

synScores = nan(maxEdgeIdx, 2);
synScores(m.edgeIdx, :) = m.synScores;

if exist('undir', 'var') && undir
    synScores = max(synScores, [], 2);
end

end

