function [shAgglos, shEdges] = buildAgglos(param)
    % [shAgglos, shEdges] = buildAgglos(param)
    %   Builds spine head agglomerates.
    %
    % Output arguments
    %   shAgglos
    %     Nx1 cell array. Each cell defines the segment equivalence
    %     class of a single spine head.
    %   shEdges
    %     Nx1 cell array. Each cell contains the (undirected) edges that
    %     make up a single spine head. Rows correspond to `shAgglos`.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % configuration
    minSpineHeadProb = 0.5;
    maxVesselScore = 0.5;
    minEdgeProb = 0.98;
    
    % load spine head probabilities
    predsFile = fullfile(param.saveFolder, 'segmentPredictions.mat');
    preds = load(predsFile);

    % find spine head segments
    shProbs = preds.probs(:, preds.class == 'spinehead');
    shSegIds = preds.segId(shProbs > minSpineHeadProb);
    
    % find vessel segments
    heuristicsFile = fullfile(param.saveFolder, 'heuristicResult.mat');
    heuristics = load(heuristicsFile);
    
    % remove "spine heads" in blood vessel segments
    vesselMask = heuristics.vesselScore > maxVesselScore;
    vesselSegIds = heuristics.segIds(vesselMask);
    shSegIds = setdiff(shSegIds, vesselSegIds);

    % prepare graph for grouping
    graph = Graph.load(param.saveFolder);
    graph(graph.prob < minEdgeProb, :) = [];
    graph(~all(ismember(graph.edges, shSegIds), 2), :) = [];

    % group segments into spine heads
    maxSegId = Seg.Global.getMaxSegId(param);
    segToShIdLUT = Graph.buildConnectedComponents(maxSegId, graph.edges);

    % convert zeros into their own label
    shLabel = segToShIdLUT(shSegIds);
    shLabel(~shLabel) = max(shLabel) + (1:sum(~shLabel));

    % build agglomerates
    shAgglos = accumarray(shLabel, shSegIds, [], @(ids) {ids});
    
    % build edges
    graph.shId = ...
        segToShIdLUT(graph.edges(:, 1));
    shEdges = accumarray( ...
        graph.shId, reshape(1:size(graph, 1), [], 1), ...
        [], @(rows) {graph.edges(rows, :)});
    
    % empty edge list for single segments
    shEdges((end + 1):numel(shAgglos)) = {zeros(0, 2)};
end