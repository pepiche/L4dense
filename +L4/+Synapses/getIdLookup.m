function [ lutPre, lutPost ] = getIdLookup( synapses, maxId )
%GETIDLOOKUP Get pre- and postsynaptic segment id lookup tables.
% INPUT synapses: [Nx3] table
%           Table with synapses.
%           (see also L4.Synapses.clusterSynapticInterfaces).
% OUTPUT lutPre: [Nx1] cell
%           Each cell contains the linear indices of all synapses with
%           containing the corresponding presynaptic id.
%        lutPost: [Nx1] cell
%           Same as lutPre but for the postsynaptic ids.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('maxId', 'var') || isempty(maxId)
    maxId = max([max(cellfun(@max, synapses.presynId)); ...
                 max(cellfun(@max, synapses.postsynId))]);
end

lutPre = L4.Agglo.buildLUT(synapses.presynId, maxId, true);
lutPost = L4.Agglo.buildLUT(synapses.postsynId, maxId, true);
end

