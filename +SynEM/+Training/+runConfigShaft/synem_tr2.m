% run config for synem using only the shaft synapses of the training data
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% data and save folder
dataFolder = ['/gaba/u/bstaffle/data/SynEM/' ...
    'SynapseDetectionTrainingData_2_typeLabels'];
saveFolder = ['/tmpscratch/bstaffle/data/SynEM/Classifier/' ...
    'runShaft/synem_tr2'];

% feature map
fm = SynEM.getFeatureMap('paper_opt');

% training options
options.shaftSynapseTraining = true;

% cluster
cluster = getCluster('cpu_prio');

% start training
SynEM.Training.trainingPipeline(dataFolder, saveFolder, fm, [], [], ...
    options, cluster),
