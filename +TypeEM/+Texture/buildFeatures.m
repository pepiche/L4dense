function feat = buildFeatures(param, box, segProps)
    % feat = buildFeatures(param, box, segProps)
    %   Calculate the texture features for all segments contained in a
    %   given bounding box. This function returns a structure with the
    %   following fields:
    %
    %   featMat
    %     N x M single matrix. The texture features.
    %   featNames
    %     1 x M cell vector with character arrays. The feature names. 
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    %% calculate texture features
    featRaw = calcTextureFeats(param, box, segProps, 'raw');
    featMembrane = calcTextureFeats(param, box, segProps, 'membrane');
    featShape = calcShapeFeats(param, box, segProps);
    
    %% build output
    feat = Util.concatStructs(2, featRaw, featMembrane, featShape);
end

function out = calcTextureFeats(param, box, segProps, inputName)
    % prepare feature map
    featMap = TypeEM.Texture.buildFeatureMap(param, 'texture', inputName);
    featureBoxPad = bsxfun(@times, featMap.border(:) ./ 2, [-1, +1]); 
    featureBox = bsxfun(@plus, box, featureBoxPad);
    
    % load data
    switch inputName
        case 'raw'
            data = loadRawData(param.raw, featureBox);
        case 'membrane'
            data = loadClassData(param.class, featureBox);
        otherwise
            error('Invalid input type');
    end
    
    % build output
    out = struct;
    out.featMat = featMap.calculate(buildInterfaces(segProps), data);
    out.featNames = reshape(featMap.createNameStrings(), 1, []);
    out.featNames = strcat(TypeEM.Util.toCamelCase(inputName), '_', out.featNames);
end

function out = calcShapeFeats(param, box, segProps)
    featMap = TypeEM.Texture.buildFeatureMap(param, 'shapeBasic');
    
    % build output
    boxSize = 1 + reshape(diff(box, 1, 2), 1, []);
    
    calcForSegment = @(segProps) ...
        featMap.calculate(buildInterfaces(segProps), boxSize);
    
    out = struct;
    out.featMat = arrayfun(@(curIdx) ...
        calcForSegment(segProps(curIdx, :)), ...
        1:size(segProps, 1), 'UniformOutput', false);
    out.featMat = cell2mat(reshape(out.featMat, [], 1));
    out.featNames = reshape(featMap.createNameStrings(), 1, []);
end

function interfaces = buildInterfaces(segProps)
    interfaces = struct;
    interfaces.surface = segProps.voxelIds;
    interfaces.subseg = {};
end
