function typeLabels = parseTypeAnnotations( skel )
%PARSETYPEANNOTATIONS Parse the SynEM training cube type annotations nml
% files.
% Annotation nml were created using SynEM.TrainingData.trainingData2Nml
% script
% Annotations are contained in the tree names and are
%   she - postsyn is spine head
%   sha - postsyn is shaft
%   som - postsyn is soma
%   ax - postsyn is axon
%   glia - postsyn is glia
%   spn - postsyn is spine neck
%   ign - ignore because most likely not synaptic
%
% INPUT skel: string or skeleton object
%           Path to type annotation nml file or the corresponding skeleton
%           object.
% OUTPUT typeLabels: struct
%           Struct containing the fields
%           'idx': [Nx1] double
%               Linear indices of the corresponding interfaces in the
%               training cube.
%           'type': categorical array with type for the corresponding
%               interface
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ischar(skel)
    skel = skeleton(skel);
end

names = skel.names;
names = cellfun(@(x)strsplit(x, '_'), names, 'uni', 0);
names = cellfun(@(x)x{4}, names, 'uni', 0);
idx = cellfun(@(x)str2double(regexp(x, '[\d.]+', 'match')), names);
ann = cellfun(@(x)regexp(x, '[^0-9]+', 'match'), names);
annC = toCategorical(ann);
isWrong = find(isundefined(annC));
if any(isWrong)
    error('Wrong annotation ''%s'' found for file %s.', ann{isWrong(1)}, ...
        skel.filename);
end
typeLabels.idx = idx;
typeLabels.type = annC;
    
end

function ann = toCategorical(ann)
cats = {'she', 'sha', 'spn', 'som', 'ax', 'glia', 'ign'};
cats_new = {'spine_head', 'shaft', 'spine_neck', 'soma', 'axon', ...
    'glia', 'ignore'};
ann = categorical(ann, cats);
ann = renamecats(ann, cats, cats_new);
end
