% analyze the dendrites for which the ff was overlapped with axons. Are there any AIS?
setConfig;
outDir = '/tmpscratch/sahilloo/L4/forIso/';
dendriteFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_flight_01.mat';
overlapFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/axonDendriteQueryOverlaps_1.0.mat';
outDirIso = '/tmpscratch/sahilloo/L4/dataPostSyn/isoForAIS_v2/';

isoFlag = false;
info = Util.runInfo();

Util.log('Loading stuff...');
m=load(config.param);
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

m = load(dendriteFile);
dendritesSmall = m.dendrites(~m.indBigDends);
if isfield(dendritesSmall,'nodes')
    dendritesSmall = Superagglos.getSegIds(dendritesSmall);
end
dendrites = m.dendrites(m.indBigDends);
if isfield(dendrites,'nodes')
    dendrites = Superagglos.getSegIds(dendrites);
end

% find indices of AIS manually annotated from STs in ff-dend-axon processing
m = load(overlapFile);
startDendrite = m.results.startDendrite;
endAxon = m.results.endAxon;

toDel = cellfun(@isempty,endAxon);
startDendrite(toDel) = [];

m=load(fullfile(outDir,'STfromDendAxOL.mat'));
idxST = m.rInd_1;
m = load(fullfile(outDir,'idxAISinST.mat'));
idxAISinST = m.idxAISinST;
idxAISinff = idxST(idxAISinST);

dendAIS = dendrites([startDendrite{idxAISinff}]);

% transfer to new version
m = load(fullfile(config.outDir,'spineTable.mat'));
spineTable = m.spineTable;

idxNewAIS = Agglo.findAggloAcrossVersions(p,dendAIS,spineTable.dendritesExt);
newAISTable = spineTable(idxNewAIS,:);

Util.saveStruct( ...
    fullfile(config.outDir, 'dendritesAISState.mat'), ...
    struct('info', info, 'idxAIS', idxNewAIS));

if isoFlag
    agglos = newAISTable.dendritesExt;
    Util.log('Now running main viz')
    p.seg.root = ['/gabaghi/wKcubes_archive/2012-09-28_ex145_07x2_ROI2017/segmentation/1/'];
    p.seg.backend = 'wkwrap';

    if ~exist(outDirIso,'dir')
        mkdir(outDirIso)
    end

    %Visualization.exportAggloToAmira(p,agglos,outDir);
    Visualization.exportAggloToAmira(p,agglos,outDirIso,'reduce',0.5,'smoothSizeHalf',4,'smoothWidth',8);
    save(fullfile(outDir,'info.mat'),'info');
end
