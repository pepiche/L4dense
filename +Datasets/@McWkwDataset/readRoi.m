function data = readRoi( obj, bbox, channels )
%READROI Read data.
% INPUT bbox: [3x2] of [1x6] or [6x1] int
%           Bounding box in the form
%           [x_min x_max; y_min y_max; z_min z_max]
%           If bbox is a vector that it is assumed to be the linearized
%           version of a bbox.
%       channels: (Optional) [Nx1] int
%           Linear indices of the channels to read.
%           (Default: all channels)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('channels', 'var') || isempty(channels)
    channels = 1:obj.numChannels;
end

dtype = obj.datChannels{1}.p.dtype;

bbox = reshape(bbox, 3, 2);
siz = diff(bbox, [], 2) + 1;
data = zeros([siz(:)', length(channels)], dtype);
for i = 1:length(channels)
    data(:,:,:,i) = obj.datChannels{channels(i)}.readRoi(bbox, false);
end


end

