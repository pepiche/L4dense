# Densely annotated bounding boxes (v3)
These are the bounding boxes selected by Manuel to generate new training
data for the edge probability classifier.

More details can be found in this blog post by Manuel:  
https://mhlablog.net/2017/03/06/training-data-for-new-pipeline-run/

I've then labeled these trees as either astrocytic, axonal, or
dendritic. Furthermore, the nodes corresponding to spine head segments
were labeled as such using a comment.

More details in this blog post:  
https://mhlablog.net/2017/03/27/segment-classification-with-new-training-data/

## Bounding boxes
* Region #1: 4133, 3963, 2253, 446, 446, 180
* Region #2: 4438, 1320,  893, 446, 446, 180
* Region #3: 1824, 6673, 1239, 446, 446, 180

In webKNOSSOS format (i.e., `x_min, y_min, z_min, x_len, y_len, z_len`)
