
outDir = '/tmpscratch/sahilloo/L4/data/';
load(fullfile(outDir,'dendritesPrePostInterfaceScores.mat'))
plot([scores(:).pre],[scores(:).post],'.','color','b')
axis('equal')
hold on;

load(fullfile(outDir,'/dendritesPrePostSegScores.mat'))
plot([scores(:).pre],[scores(:).post],'.','color','r')
xlabel('pre')
ylabel('post')
legend({'Border interface based','Segment based'})
