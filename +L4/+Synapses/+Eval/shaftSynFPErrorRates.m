% script that writes out a random set of shaft synapses to assess FP error
% rates
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo();
outFolder = '/u/bstaffle/data/L4/Synapses/PerformanceAssessment/';


%% get synapses

p = Gaba.getSegParameters('ex145_ROI2017');
[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
m = load(['/gaba/u/mberning/results/pipeline/20170217_ROI/' ...
    'connectomeState/SynapseAgglos_v3_ax_spine_clustered.mat']);
synapses = m.synapses(~m.isSpineSyn, :); % shaft synapses


%% write random set of synapses to WK

N = 200;
rng('shuffle');
idx = randperm(size(synapses, 1), N);
skel = L4.Synapses.synapse2Skel(synapses(idx, :), borderMeta.borderCoM, ...
    [], [], 'all', idx);
skel = skel.setDescription(sprintf(['Random shaft synapses from ' ...
    'SynapseAgglos_v3_ax_spine_clustered. Git-hash:%s'], ...
    info.git_repos{2}.hash));
skel.write(fullfile(outFolder, ...
    'ShaftSyns_SynapseAgglos_v3_ax_spine_clustered.nml'));
save(fullfile(outFolder, 'ShaftSynFPAssessment_v1.mat'), 'synapses', 'idx');


%% eval tracing

skel = skeleton(['E:\workspace\data\backed\L4\Synapses\' ...
    'PerformanceAssessment\' ...
    'ShaftSyns_SynapseAgglos_v3_ax_spine_clustered.nml']);

names = skel.names();

% annotations were:
% (Yes): is synapse
% (Yes) spine: is spine synapse -> discard
% (No): no synapse
% (No) spine bias: no synapse, spine bias could work to exclude this synpase
% (Ign): unsure if synapse

% spine synapses
idx_spine_syns = cellfun(@(x)~isempty(strfind(x, '(Yes) spine')), names);
noSH = sum(idx_spine_syns);

% shaft synapses
idx_shaft = cellfun(@(x)~isempty(strfind(x, '(Yes)')), names) & ...
    ~idx_spine_syns;
idx_sec_spine = cellfun(@(x)~isempty(strfind(x, ...
    '(Yes) secondary spine')), names) & ~idx_spine_syns;
n2SH = sum(idx_sec_spine);

% no shafts
idx_FP = cellfun(@(x)~isempty(strfind(x, '(No)')), names);
idx_sh_bias = cellfun(@(x)~isempty(strfind(x, '(No) spine bias')), names);
idx_no_sh_bias = cellfun(@(x)~isempty(strfind(x, '(Yes) despite')), names);
nSHB = sum(idx_sh_bias);
nNoSHB = sum(idx_no_sh_bias);

% ignored ones
idx_ign = cellfun(@(x)~isempty(strfind(x, '(Ign)')), names);
nIgn = sum(idx_ign);

% sanity check
assert(all(idx_shaft | idx_spine_syns | idx_FP | idx_ign));

% performance
TP = sum(idx_shaft);
FP = sum(idx_FP);
p = TP / (TP + FP);

Util.log('Num spine syns: %d', noSH);
Util.log('Num ign: %d', nIgn);
Util.log('Num secondary spine: %d', n2SH);
Util.log('Shaft syn precision %.3f (%d of %d)', p, TP, TP + FP);
Util.log('Potentially solvable by SH bias: %.3f (%d)', nSHB/FP, nSHB);
Util.log('Potentially wrong due to SH bias: %d', nNoSHB);
