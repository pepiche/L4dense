%% script to generate the connectome for a segmentation v3
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo(false);


%% seg parameters & output file

p = Gaba.getSegParameters('ex145_ROI2017');
axon_ver = 'axons_18_b_linearized.mat';
[~, ax_name] = fileparts(axon_ver);
outFile = fullfile(p.connectome.saveFolder, ...
    sprintf('connectome_%s_ax_spine_syn_clust.mat', ax_name));
outFileSmall = fullfile(p.connectome.saveFolder, ...
    sprintf('connectome_%s_ax_spine_syn_clust_small.mat', ax_name));


%% load agglos & synapses

% axons
Util.log('Loading axons.');
p.agglo.axonAggloFile = sprintf(['/gaba/u/mberning/results/pipeline/' ...
    '20170217_ROI/aggloState/%s'], axon_ver);
axons = L4.Axons.getLargeAxons(p, true, true);
axons(cellfun(@isempty, axons)) = [];

% dendrites
Util.log('Loading dendrites and target classes.');
[dendFile, targetClass, targetLabels] = L4.Connectome.buildTargetClasses();
m = load(dendFile);
dendrites = m.dendAgglos(targetClass ~= 'Ignore');
targetClass(targetClass == 'Ignore') = [];

% add somata to dendrites
mSom = load(p.agglo.somaFile);
somata = mSom.somaAgglos(:,1);
[dendrites, toDel] = L4.Specificity.addSomasToDendrites(dendrites, ...
    somata, 'separate');
targetClass(toDel) = [];
targetClass(end+1:end+length(somata)) = 'Somata';

% target classes
idxSoma = targetClass == 'Somata';
idxWC = targetClass == 'WholeCell';
idxAD = targetClass == 'ApicalDendrite';
idxSD = targetClass == 'SmoothDendrite';
idxAIS = targetClass == 'AxonInitialSegment';
idxOther = targetClass == 'OtherDendrite';

% synapses
Util.log('Loading synapses.');
% mSyn = load(p.connectome.synFile);
synFile = ['/gaba/u/mberning/results/pipeline/20170217_ROI/' ...
    'connectomeState/SynapseAgglos_v3_ax_spine_clustered.mat'];
mSyn = load(synFile);
synapses = mSyn.synapses;


%% calculate full connectome

[ syn2Agglo, pre2syn, post2syn, connectome ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, axons, dendrites );
connectomeFull = L4.Connectome.pairwise2Full(connectome, ...
    [length(axons), length(dendrites)]);


%% connectome synapse information

Util.log('Adding synapse meta information.');

% load border meta (must be wrt to edges in graph)
[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
m = load(p.svg.borderMetaFile, 'borderArea');
borderMeta.borderArea = nan(size(borderMeta.borderSize));
borderMeta.borderArea(~isnan(borderMeta.borderSize)) = m.borderArea;

connectomeMeta = L4.Connectome.connectomeBorderMeta(connectome, ...
    synapses, borderMeta);


%% generate contactome

Util.log('Calculating contactome.')
contactome = L4.Agglo.findEdgesBetweenAgglos2(axons, dendrites, ...
    graph.edges);
contactomeMeta = L4.Connectome.contactomeBorderMeta(contactome, ...
    borderMeta);


%% target class connectome

Util.log('Target class mapping.');
denClasses = {'soma', 'whole cells', 'apical dendrites', ...
    'smooth dendrite', 'AIS', 'other'};
classConnectome = zeros(length(axons), length(denClasses));
classConnectome(:,1) = sum(connectomeFull(:, idxSoma), 2);
classConnectome(:,2) = sum(connectomeFull(:, idxWC), 2);
classConnectome(:,3) = sum(connectomeFull(:, idxAD), 2);
classConnectome(:,4) = sum(connectomeFull(:, idxSD), 2);
classConnectome(:,5) = sum(connectomeFull(:, idxAIS), 2);
classConnectome(:,6) = sum(connectomeFull(:, idxOther), 2);


%% axon statistics
% taken from connectEM.Connectome.augmentForMoritz

Util.log('Adding axon meta information.');
axonMeta = table;
axonMeta.id = reshape(1:numel(axons), [], 1);
axonMeta.synCount = accumarray( ...
    connectome.edges(:, 1), ...
    cellfun(@numel, connectome.synIdx), ...
   [numel(axons), 1], @sum, 0);
axonMeta.spineSynCount = accumarray( ...
    connectome.edges(:, 1), cellfun(@(ids) ...
    sum(mSyn.isSpineSyn(ids)), connectome.synIdx), ...
   [numel(axons), 1], @sum, 0);


%% dendrite statistics
% added by amotta

Util.log('Adding dendrite meta information.');
denMeta = table;
denMeta.id = reshape(1:numel(dendrites), [], 1);
denMeta.targetClass = reshape(targetClass, [], 1);
denMeta.synCount = accumarray( ...
    connectome.edges(:, 2), ...
    cellfun(@numel, connectome.synIdx), ...
   [numel(dendrites), 1], @sum, 0);
denMeta.spineSynCount = accumarray( ...
    connectome.edges(:, 2), cellfun(@(ids) ...
    sum(mSyn.isSpineSyn(ids)), connectome.synIdx), ...
   [numel(dendrites), 1], @sum, 0);


%% save results

if ~exist(outFile, 'file')
    save(outFile, 'connectome', 'contactome', 'connectomeMeta', ...
        'contactomeMeta', 'axons', 'dendrites', 'info', ...
        'classConnectome', 'denClasses', 'axonMeta', 'denMeta', ...
        'axon_ver');
    save(outFileSmall, 'connectome', 'connectomeMeta', ...
        'classConnectome', 'denClasses', 'axonMeta', 'denMeta', ...
        'axon_ver', 'info');
end
