classdef CNNFeat < TypeEM.SynEM.Feature.TextureFeature
    %CNNFEAT Features generated by a Codat.CNN
    % PROPERTIES
    %  cnn: Codat.CNN.cnn object.
    %       CNN used for feature calculation.
    %  options: struct
    %       see Codat.CNN.Misc.predictCube
    % Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
    
    properties
        cnn
        options = struct();
        fRawNorm = [];
    end
    
    methods
        function obj = CNNFeat(cnn, options, fRawNorm)
            obj.name = 'CNN';
            obj.cnn = cnn;
            obj.numChannels = cnn.featureMaps(end);
            obj.border = cnn.border;
            
            if exist('options', 'var') && ~isempty(options)
                obj.options = options;
            end
            
            if exist('fRawNorm', 'var') && ~isempty(fRawNorm)
                if ~ischar(fRawNorm)
                    fRawNorm = func2str(fRawNorm);
                end
                obj.fRawNorm = fRawNorm;
            end
        end
        
        function fm = calc(obj, raw)
            if ~isempty(obj.fRawNorm)
                f = str2func(obj.fRawNorm);
                raw = f(raw);
            end
            fm = Codat.CNN.Misc.predictCube(obj.cnn, raw, obj.options);
            if obj.numChannels > 1
                fm = squeeze(num2cell(fm, 4));
            end
        end
    end
    
end

