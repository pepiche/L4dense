function trainingSetFwdPassForSegTest(iter, net)
% Make a fwdPass through the network of region aligned with denseL4 tracing from Heiko

% Load CNN
dateStrings = '20131012T234219';
load(['/zdata/manuel/results/parameterSearch/' dateStrings '/iter' num2str(iter, '%.2i') '/gpu' num2str(net, '%.2i') '/' 'saveNet0000000001.mat'], 'cnet');
cnet = cnet.loadLastCNN;

% Load raw data settings
pathRaw(1).root = '/zdata/manuel/data/cortex/2012-09-28_ex145_07x2_corrected/color/1/';
pathRaw(1).prefix = '2012-09-28_ex145_07x2_corrected_mag1';
pathRaw(2).root = '/zdata/manuel/data/cortex/2012-09-28_ex145_07x2_corrected/color/1/';
pathRaw(2).prefix = '2012-09-28_ex145_07x2_corrected_mag1';
% BBoxes
pathRaw(1).bbox = [4097 4736; 4481 5248; 2250 2450];
pathRaw(2).bbox = [1417 1717; 4739 5039; 890 1190];
% Expand input according to CNN
pathRaw(1).bbox = pathRaw(1).bbox + [-cnet.randOfConvn/2; cnet.randOfConvn/2]';
pathRaw(2).bbox = pathRaw(2).bbox + [-cnet.randOfConvn/2; cnet.randOfConvn/2]';

for i=1:2
	raw = readKnossosRoi(pathRaw(i).root, pathRaw(i).prefix, pathRaw(i).bbox);
	% Normalization
	if cnet.normalize
	        raw = normalizeStack(single(raw));
	else
	        raw = single(raw);
	end
	% Run fwdPass
	classification = cnet.fwdPass3DonCPU(raw);
	save(['/zdata/manuel/sync/parameterSearch/seg' dateStrings num2str(iter, '%.2i') num2str(net, '%.2i') '_' num2str(i, '%.1i') '.mat'], 'classification', 'raw');
end

end
