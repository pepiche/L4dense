% cluster the found shaft synapses into dendrite agglos
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
load('/gaba/u/mberning/results/pipeline/20170217_ROI/globalBorder.mat')
p = m.p;
rng(0);
points = Seg.Global.getSegToPointMap(p);
voxelSize = p.raw.voxelSize;

outDir = '/tmpscratch/sahilloo/L4/data/';

m=load('/tmpscratch/sahilloo/L4/data/dendritesPostQueryWithShafts.mat');
numInterfaces = cellfun(@numel,m.dendritesShaftSegs); 
idxNoInterfaces = find(numInterfaces==0);
dendrites = m.dendritesPostQuery;
dendWoShafts = dendrites(idxNoInterfaces);
m = load('/tmpscratch/sahilloo/L4/data/dendritesPostQueryStateLengths.mat');
dendriteLengths = m.aggloLens./1E3; % in um
dendWoShaftsLengths = dendriteLengths(idxNoInterfaces);

% view in wK 
idxToPlot = randi(length(dendWoShafts),1,50);
for i=1:length(idxToPlot)
    nodes = dendWoShafts(idxToPlot(i));
    if numel(dendWoShafts{idxToPlot(i)})==1
        fprintf('Dendrites %d is single segId \n',idxToPlot(i));
        continue;
    end
    skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
    skel.names(1) = {['agglo-pl-' num2str(dendWoShaftsLengths(idxToPlot(i))) ' um']};
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDir, ['nmls/dendritesWithoutShafts_' num2str(idxToPlot(i)) '.nml']));
end

