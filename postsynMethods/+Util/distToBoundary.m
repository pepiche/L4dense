function d = distToBoundary( points, bbox, voxelSize )
%DISTTOBOUNDARY Determine the distance of points from a bbox boundary.
% INPUT points: [Nx3] float
%           3d coordinates of points.
%       bbox: [3x2] int
%           Bounding box.
%       voxelSize: (Optional) [1x3] float
%           Size of a voxel.
%           (Default: [1 1 1])
% OUTPUT d: [Nx1] float
%           Minimal distance to the border of the bounding box for each
%           point.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~all(Util.isInBBox(points, bbox))
    warning('Not all points are within the bounding box.');
end

if isinteger(points), points = single(points); end
if exist('voxelSize', 'var') && ~isempty(voxelSize)
    points = bsxfun(@times, points, voxelSize(:)');
    bbox = bsxfun(@times, bbox, voxelSize(:));
end

d_lower = min(abs(bsxfun(@minus, points, bbox(:,1)')), [], 2);
d_upper = min(abs(bsxfun(@minus, bbox(:,2)', points)), [], 2);
d = min([d_lower, d_upper], [], 2);

end


