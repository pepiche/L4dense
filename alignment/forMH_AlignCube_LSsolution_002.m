function LSshifts = forMH_AlignCube_LSsolution_002(C,alldeltas,...
    allweights)
P=C.procs*C.slPth;
F=cleverKLBstuff(C.M,C.N,P);
allweights(allweights==4)=1;
W_vec = allweights';
W_vec(C.M*C.N*(P-1)+C.M*(C.N-1)*P+(C.M-1)*C.N*P+1)=1;
W_temp = speye(length(W_vec));
W = spdiags((W_vec)',0,W_temp);

% FIX offset
ff=zeros(1,C.M*C.N*P);
floor(C.M/2)+C.M*floor(C.N/2);
ff(1,ceil(C.M/2)+C.M*ceil(C.N/2))=1;
F=cat(1,F,ff);
alldeltas=cat(1,alldeltas,zeros(1,2));

shifts=zeros(C.M*C.N*P,2);
shifts(:,2) = (W*F)\(W*alldeltas(:,2));
shifts(:,1) = (W*F)\(W*alldeltas(:,1));


alldeltasErr=(W*F)*shifts;
ee=alldeltasErr-W*alldeltas;
ee(end,:)=0;
LSshifts.shifts = shifts;

W_vec(C.M*C.N*(P-1)+C.M*(C.N-1)*P+(C.M-1)*C.N*P+1)=0;
LSshifts.weightsum=abs(transpose(abs(F)))*transpose(W_vec);

alldeltasErr2=F*shifts;
ee2=alldeltasErr2-alldeltas;
ee2(end,:)=0;
LSshifts.weightsum=LSshifts.weightsum./(sum((abs(F')*(ee2.^2)),2).^0.5);




positions=zeros(C.M*C.N*P,5);
for count = 1:P
    
    for imgcount = 1:C.M*C.N
        thisimg = (count-1)*C.M*C.N+imgcount;
        positions(thisimg,1) = C.dims(1)*mod(imgcount-1,C.M)+fix(shifts(thisimg,1))+1;
        positions(thisimg,2) = C.dims(2)*floor((imgcount-1)/C.M)+fix(shifts(thisimg,2))+1;
        positions(thisimg,3)=count;
        xIt = mod(imgcount-1,C.M)+1;
        yIt = floor((imgcount-1)/C.M)+1;
        positions(thisimg,4)=xIt;
        positions(thisimg,5)=yIt;
    end
end
LSshifts.R=[sum((abs(F')*(ee.^2)),2).^0.5,positions];
LSshifts.R=[sum((abs(F')*(ee.^2)),2).^0.5,positions];
LSshifts.total.F = F;
LSshifts.total.W = W;
LSshifts.total.alldeltas = alldeltas;

LSshifts.errors = ee;
