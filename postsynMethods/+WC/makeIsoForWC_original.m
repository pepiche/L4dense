
setConfig;
load(config.param);
config.outDir = '/tmpscratch/sahilloo/L4/dataPostSyn/';
aggloStateFolder = fullfile(p.saveFolder,'aggloState');
config.dendriteFile = fullfile(aggloStateFolder,'dendrites_wholeCells_01.mat');

outDir = fullfile(config.outDir,'isoForWC_original');

if ~exist(outDir,'dir')
    mkdir(outDir);
end
m = load(config.param);
p = m.p;
points = Seg.Global.getSegToPointMap(p);
voxelSize = p.raw.voxelSize;

Util.log('Loading stuff..')
m=load(config.dendriteFile);
dendAgglos = Superagglos.getSegIds(m.dendrites);
superagglos = m.dendrites(m.indWholeCells);
%agglos = agglos(24);

Util.log('Writing skeletons...')
% name output nmls to shCount and wc index
tic;
endStep = length(superagglos);
for i=1:endStep
    nodes = superagglos(i);
    skel = Superagglos.toSkel(nodes);
    skel.write(fullfile(outDir, ['wc_' num2str(i) '.nml']));
    Util.progressBar(i,endStep);
end
Util.log('Finished creating nmls to view');
return

agglos = dendAgglos(m.indWholeCells);   
Util.log('Now running main viz')
%p.seg.root='/tmpscratch/amotta/l4/2012-09-28_ex145_07x2_ROI2017/segmentation/1/';
p.seg.root = ['/gabaghi/wKcubes_archive/2012-09-28_ex145_07x2_ROI2017/segmentation/1/'];
p.seg.backend = 'wkwrap';
Visualization.exportAggloToAmira(p,agglos,outDir,'reduce',0.05,'smoothSizeHalf',4,'smoothWidth',8);
info=Util.runInfo(false);
save(fullfile(outDir,'info.mat'),'info');
