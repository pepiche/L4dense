function subGraph = buildSubgraphSegments(graph, segIds)
    % subGraph = buildSubgraphSegments(graph, segIds)
    %   Builds a segment-induced subgraph of graph.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    mask = all(ismember(graph.edges, segIds), 2);
    subGraph = graph(mask, :);
end
