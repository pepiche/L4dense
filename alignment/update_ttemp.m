function ttemp = update_ttemp (ttemp_prelim,C, all_fns)
ttemp = ttemp_prelim;
C.minis=[Inf Inf];
C.maxis=[-Inf -Inf];
%ttemp.shifts=ttemp.shifts+[(1:C.procs*C.slPth)'*1/2 (1:C.procs*C.slPth)'*1/2]
for count = 1:C.P
    this_slicecount = count;
    for imgcount = 1:C.M*C.N
        thisimg = (this_slicecount-1)*C.M*C.N+imgcount;
        xyIts = [ mod(imgcount-1,C.M) floor((imgcount-1)/C.M)]+1;
        thisshifts = ttemp.shifts(thisimg,:);
        xycoords = C.dims.*(xyIts-1)+fix(thisshifts)+1;
        if size(at(all_fns,[xyIts';count]),1)>0
            C.minis=min([C.minis;xycoords]);
            C.maxis=max([C.maxis;xycoords+C.dims-1]);
        end
        
    end
end

ttemp.shiftsN=[ttemp.shifts(:,1)-C.minis(1) ttemp.shifts(:,2)-C.minis(2)]+1;
%overtemp=zeros(C.M,C.N,size(ttemp.R,1),2);
for i=ttemp.R'
    overtemp(i(4),i(5),i(6),:)=i(2:3);
end
overtempC=[];
overfake=[];
colfake=cell(3,1);
for di=1:3
    sd=size(ttemp.delta_col{di});
    colfake{di}=cell([sd(1:2),1,2]);
    for m=1:sd(1)
        for n=1:sd(2)
            for xy=1:2
                colfake{di}{m,n,1,xy}=sprintf('dim%u m%uof%u n%uof%u comp%u',di,m,sd(1),n,sd(2),xy);
            end
        end
    end
end

for i=1:3
    mean(ttemp.delta_col{i},3)
    if size(ttemp.delta_col{i},3)<C.P
        ttemp.delta_col{i}(:,:,C.P,:)=0;
    end
    for j=1:2
        overtempC=cat(2,overtempC,reshape(permute(ttemp.delta_col{i}(:,:,:,j),[3 1 2]),C.P,[]));
        overfake=cat(2,overfake,reshape(permute(colfake{i}(:,:,:,j),[3 1 2]),1,[]));
    end
end
%         overtempC=overtempC-repmat(mean(overtempC),[C.P,1]);
overtempA=reshape(overtemp(:,:,:,1),C.P,[]);
overtempA=overtempA-repmat(mean(overtempA),[C.P,1]);
overtempB=reshape(overtemp(:,:,:,2),C.P,[]);
overtempB=overtempB-repmat(mean(overtempB),[C.P,1])+mean(std(overtempA))*4;
