function d = pdist( p1, p2 )
%PDIST Distance of two points.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

voxelSize = [11.24, 11.24, 28];
d = norm(bsxfun(@times, p1 ,voxelSize) - bsxfun(@times, p2, voxelSize));
end

