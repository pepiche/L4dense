function switchMag( obj, mag )
%SWITCHMAG Use a different magnification of the same dataset assuming that
% the root path of the dataset ends with the folder that specifies the
% magnification.
% INPUT mag: int
%           Integer specifying the magnification.

newRoot = [fileparts(fileparts(obj.root)), filesep, num2str(mag), filesep];
switch obj.backend
    case 'wkcube'
        obj.root = newRoot;
        obj.p.prefix = obj.getPrefix();
    case 'wkwrap'
        obj.root = newRoot;
    otherwise
        error('Unknown backend %s.', obj.backend);
end


end

