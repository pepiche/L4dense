% check nearby synapses with the latest agglo states
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

p = Gaba.getSegParameters('ex145_ROI2017');

axFile = fullfile(p.agglo.saveFolder, 'axons_18_a.mat');
denFile = fullfile(p.agglo.saveFolder, 'dendrites_wholeCells_01_spine_attachment.mat');
synFile = fullfile(p.connectome.saveFolder, 'SynapseAgglos_v3.mat');

% axons
mAx = load(axFile);
axons = mAx.axons(mAx.indBigAxons);
idx = Superagglos.getPureFlightPathAgglos(axons);
axons(idx) = [];

% dendrites
mDen = load(denFile);
dendrites = mDen.dendAgglos(mDen.indBigDends);

% synapses
mSyn = load(synFile);
synapses = mSyn.synapses;


%% calculate synapse agglo mappings

[ syn2Agglo, pre2syn, post2syn, connectome ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, axons, dendrites );


%% get connections with multiple synapses

[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
borderCom = borderMeta.borderCoM;

distT = 375; % nm
skel = L4.Util.getSkel();
multiSynConnection = find(cellfun(@length, connectome.synIdx) > 1);
for i = 1:length(multiSynConnection)
    thisSyn = synapses(connectome.synIdx{multiSynConnection(i)}, :);
    coms = L4.Synapses.synapseCom(thisSyn, borderCom);
    d = pdist(bsxfun(@times, coms, p.raw.voxelSize));
    if any(d < distT)
        skel = Skeleton.fromMST(coms, [], skel);
        skel.names{end} = [skel.names{end} '_' num2str(min(d))];
%         skel = skel.addTree([], coms);
    end
end
skel = skel.setDescription(sprintf(['connections with multiple ' ...
    'synapses for which two synapses are closer than %d nm ' ...
    '(comment: closest distance in nm)'], uint32(distT)));
skel.write('MultiSynapseConnections.nml')