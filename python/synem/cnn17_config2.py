# -*- coding: utf-8 -*-
"""
Marius Felix Killinger --- Training a Neural Network

CONFIGURATION FOR CNN-TRAINING
------------------------------

 Als see the notes in ``TrainCNN.py``

- In the comments values in [] denotes preferred/recommended settings
- Parameters that must be set specifically are marked by (*)
- Parameters that are unlikely to need change by the user (in order to get it working and get any results) are
  marked by ($). They are not well documented and users may have read-up the concepts elsewhere or inspect
  the implementation directly
- Paths must have trailing '/'
- For isotropic CNNs (same input and filter size in all directions) the variable desired_input is scalar and
  filters & nof_filters are lists of scalars. For anisotropic CNNs all scalars are replaced by the
  lists of respective shapes.
- Pooling factors cannot be set anisotropic (independent of the anisotropic filters)
- Check your CNN architecture by using ``Net.netutils.CNNCalculator``! To get centered field of views (i.e.
  that labels are aligned with output neurons and not "in between") when using pooling factors of 2, the
  filter size in the first layer must be even.

adapted by: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
"""
# this was run via
# elektronn-train

# import cPickle as pkl
# Paths and General ###
save_path        = "~/data/SynEM/CNN_Training/"         # (*) where to create the CNN directory. In this directory a new folder is created
# save_name        = 'BIRD_ARGUS_v45_1037'               # (*) with the save_name.
save_name        = 'cnn17_SVM_2'
overwrite        = False                 # whether to delete/overwrite existing directory
plot_on          = True                 # [True]/False: whether to create plots of the errors etc.
print_status     = True                 # [True]/False: whether to print Training status to std.out
save_path += save_name+'/'
mode             = 'img-img'

param_file       = None # string/None: optional parameter file to initialise weights from
data_path       = '/home/benedikt/data/SynEM/TrainingCubes/' # Path to data dir
label_path      = '/home/benedikt/data/SynEM/TrainingCubes/' #'/home/sdorkenw/Data/synapse_bird/' # Path to label dir
# d_files         = []   # list of tupels of file name and filed name in h5 dataset
d_files         = [('cube_%d_raw.h5' % ii, 'raw') for ii in range(1, 8)]
l_files         = [('cube_%d_labels.h5' % ii, 'labels') for ii in range(1, 8)]
del ii
cube_prios      = []
# del ii
# z_axis           = -1                  # [-1]: (*) (reverse) indices of axis with low resolution (needed for
                                       # slicing and anisotropic CNNs)
n_lab            = 4                   # (*) <int>/None: (None means auto detect, very slow, don't do this!)

# cube_prios       = [70./24]*6+[20/24.]*16+[10/24.]*2              # List of sampling priorities for cubes or None, then: sampling ~ example_size
valid_cubes      = []                  # List of cube indices (from the file-lists) to use as validation data, may be empty
grey_augment_channels = [0]            # (*) list of <int> channel-indices to apply grey augmentation, use [] to disable.
                                       # It distorts the histogram of the raw images (darker, lighter, more/less contrast)
flip_data        = True                # [True]/False: whether to flip/rotate/mirror data randomly (augmentation)
anisotropic_data = True               # (*) <Bool>, if True 2D slices are only cut in z-direction, otherwise all 3 alignments are used
# new_GA           = True               # <Bool> new grey value augmentation, seems to work not better, maybe the value range is to mall
background_processes = True            # whether to "pre-fetch" batches in separate background process
# sparse_labels    = True               # ($) Special Training with lazy annotations
warp_on          = False                # Warping augmentations (CPU-intense, use background processes!)
# example_ignore_threshold = -0.01         # If the fraction of non-negative labels in an example patch exceeds
                                       # this threshold this example is discarded
### Architecture ###
activation_func = 'tanh'               # [tanh], abs, linear, sig, relu, (maxout for only 3d)
n_dim           = 3                    # (*) 2/3
# desired_input   = [202, 202, 22]                  # (*) <int> or <2/3-tuple> in (x,y)/(z,x,y)-order for anisotropic CNN
desired_input   = [190, 190, 45]                  # (*) <int> or <2/3-tuple> in (x,y)/(z,x,y)-order for anisotropic CNN
# desired_input = [150, 150, 25]
# Note: the last layer is added automatically (with n_lab outputs)
filters         = [[8,8,4], [5,5,3], [5,5,3], [3,3,3], [3,3,3], [3,3,3], [3,3,2], [3,3,2]]
pool            = [[1,1,1], [2,2,1], [1,1,1], [2,2,2], [1,1,1], [2,2,2], [1,1,1], [1,1,1]]
# nof_filters     = [16, 16, 32, 32, 48, 48, 48, 48]     # (*) list of <int>
nof_filters     = [12, 12, 24, 24, 48, 48, 48, 48]
MFP             = [ 0, 0, 0, 0, 0, 0, 0, 0]     # (*) list of <int{0,1}>: whether to apply Max-Fragment-Pooling in this layer
batch_size      = 1                    # (*) [1]/<int>  number of different slices used for one update step

### Optimisation Options ###
n_steps               = 50 * 1000 * 1000  # (*) number of update steps
max_runtime           = 4 * 24 * 3600      # (*)  maximal Training time in seconds (overrides n_steps)
history_freq          = [2000]          # (*) create plots, print status and test model after x steps (must be list to be mutable)
monitor_batch_size    = 10              # (*) number of patches to test model on (valid and Training subset)

# dropout               = False          # [False]/True Activates Dropout in all layers but the last at rat 0.5
weight_decay          = False          # ($) [False]/<float>: L2-norm on weights, False is equal to 0.0
# training_mode         = 'SGD'          # [SGD]/CG/RPORP/LBFGS optimiser method for Training
optimizer            = 'SGD'

LR_decay              = 0.997          # (*) decay of SGD learning rate w.r.t to an interval of 1000 update steps

# SGD
SGD_params      = {'LR': 0.001, 'momentum': 0.9, }  # (*) initial learning rate and momentum

# RPROP ($)
RPROP_params    = {'penalty': 0.35,
                   'gain'   : 0.2,
                   'beta': 0.7,
                   'initial_update_size': 1e-4}
# CG ($)
CG_params       =  {'n_steps':4,       # update steps per same batch 3 <--> 6
                    'alpha':0.35,      # termination criterion of line search, must be <= 0.35
                    'beta':0.75,       # precision of line search,  imprecise 0.5 <--> 0.9 precise
                    'max_step':0.02,   # similar to learning rate in SGD 0.1 <--> 0.001.
                    'min_step':8e-5}

# LBFGS ($)
LBFGS_params    =  {'maxfun': 40,      # function evaluations
                    'maxiter': 4,      # iterations
                    'm': 10,           # maximum number of variable metric corrections
                    'factr': 1e2,      # factor of machine precision as termination criterion (haha!)
                    'pgtol': 1e-9,     # projected gradient tolerance
                    'iprint': -1}      # set to 0 for direct printing of steps