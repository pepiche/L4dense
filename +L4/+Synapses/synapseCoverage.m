function [stats, syn2agglo] = synapseCoverage( synapses, axons, dendrites )
%SYNAPSECOVERAGE Determine how many synapses are picked up by the axon
% and dendrite agglomerates.
% INPUT synapses: [Nx3] table
%       axons: [Nx1] cell or [Nx1] struct
%           The axon agglomerates as cell array of segment ids or as
%           superagglomerates. Can be [].
%       dendrites: [Nx1] cell or [Nx1] struct
%           The dendrite agglomerates as cell array of segment ids or as
%           superagglomerates. Can be [].
% OUTPUT stats: struct
%           Struct with the coverage statistics.
%        syn2agglo: [Nx2] table
%           The synapse to agglo mapping (see also the first output of
%           L4.Synapses.synapsesAggloMapping).
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('axons', 'var')
    axons = [];
end
if ~exist('dendrites', 'var')
    dendrites = [];
end

noSyn = size(synapses, 1);
[syn2agglo, pre2syn] = L4.Synapses.synapsesAggloMapping( ...
    synapses, axons, dendrites);
retrieved_pre = sum(~cellfun(@isempty, syn2agglo.syn2pre));
retrieved_post = sum(~cellfun(@isempty, syn2agglo.syn2post));

stats.no_synapses = noSyn;
stats.total_retrieved_pre = retrieved_pre;
stats.fraction_retrieved_pre = retrieved_pre/noSyn;
stats.total_retrieved_post = retrieved_post;
stats.fraction_retrieved_post = retrieved_post/noSyn;
stats.synPerAxon = tabulate(cellfun(@length, pre2syn));


end

