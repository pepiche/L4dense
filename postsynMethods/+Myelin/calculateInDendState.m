% calculate myelin scores for latest dendrites state

setConfig;
load(config.param)
outputFolder = fullfile(p.saveFolder, 'aggloState');

info = Util.runInfo(false);

Util.log('Loading stuff...')
graph = load(fullfile(p.saveFolder, 'graphNew.mat'),'edges','borderIdx');

if  ~all(isfield(graph,{'neighbours','neighBorderIdx'}))
    [graph.neighbours, neighboursIdx] = Graph.edges2Neighbors(graph.edges);
    graph.neighBorderIdx = cellfun(@(x)graph.borderIdx(x), neighboursIdx, 'uni', 0);
    clear neighboursIdx
end
if ~exist('borderMeta','var')
    borderMeta = load(fullfile(p.saveFolder, 'globalBorder.mat'), 'borderSize', 'borderCoM');
end
if ~exist('heuristics','var')
    heuristics = load(fullfile(p.saveFolder, 'heuristicResult.mat'),'myelinScore');
end

config.dendriteFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_17_a.mat';
m = load(config.dendriteFile);

Util.log('Calculating myelin scores...')
[ fracMyelinAggloBorder,aggloNeighBordersMyelin, aggloNeighBordersNoMyelin,myelinatedNeighbours ] = ...
        connectEM.calculateSurfaceMyelinScore( m.dendrites, graph, borderMeta, heuristics );


m.myelinDend = fracMyelinAggloBorder;

% append myelin scores to dendrite state
Util.saveStruct(config.dendriteFile,m)


