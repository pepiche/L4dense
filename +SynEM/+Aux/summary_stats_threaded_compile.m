function summary_stats_threaded_compile()

% go to directory of this file
prevDir = pwd();
thisDir = fileparts(mfilename('fullpath'));
cd(thisDir);

if ispc
    mex CXXFLAGS='$CXXFLAGS -std=c++11' ...
        OPTIMFLAGS='/openmp' ...
        summary_stats_threaded.cpp
else
    mex CXXFLAGS="\$CXXFLAGS -fopenmp -std=c++11" ...
        LDFLAGS="\$LDFLAGS -fopenmp" ...
        summary_stats_threaded.cpp
end

% go to original directory
cd(prevDir);
end
