classdef McWkwDataset
    %MCWKWDATASET Multi-channel wkw dataset class wrapper.
    % Each channel is saved as a separate wkw dataset.
    % Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
    
    properties
        root
        datChannels = cell(0);
        numChannels = 0;
    end
    
    methods
        function obj = McWkwDataset(root, numChannels, dtype)
            % Usage
            % McWkDatset(root, numChannels, dtype)
            %   Construct a McWkwDataset object whose single channels wkw
            %   datasets are all stored at the root folder with data of
            %   type dtype. Initializes the single channel wkw datasets if
            %   they do not exist yet (it is assumed that the channel wkw
            %   datasets are stored in folders with names "channel%d",
            %   where d is the channel number
            % McWkDataset(root)
            %  Constructor if the channel wkw datasets already exists and
            %  thus the remaining information can be inferred from those
            %  datasets.
            
            obj.root = root;
            list = dir(root);
            list(~[list.isdir]) = [];
            list(cellfun(@(x)any(strcmp(x, {'.', '..'})), {list.name})) = [];
            list(cellfun(@(x)isempty(regexpi(x, 'channel\d*')), ...
                {list.name})) = [];
            
            if isempty(list)
                assert(any(strcmp(dtype, {'uint8', 'uint32', 'single'})));
                % no initialized channel datasets found, create them
                obj.datChannels = cell(numChannels, 1);
                for i = 1:numChannels
                    channelRoot = fullfile(root, sprintf('channel%d', i));
                    wkwInit('new', channelRoot, 32, 32, dtype, 1);
                    obj.datChannels{i} = Datasets.WkDataset(channelRoot);
                end
                obj.numChannels = numChannels;
            else
                obj.numChannels = length(list);
                obj.datChannels = cell(obj.numChannels, 1);
                for i = 1:obj.numChannels
                    channelRoot = fullfile(root, sprintf('channel%d', i));
                    obj.datChannels{i} = Datasets.WkDataset(channelRoot);
                end
                if exist('numChannels', 'var') && ~isempty(numChannels)
                    assert(numChannels == obj.numChannels);
                end
            end
        end
    end
    
end

