classdef KMBarray
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        a;
    end
    
    methods
        function obj=KMBarray(star)
            if nargin>0
                obj.a=star;
            end
        end
        function y=get(obj, i)
            if size(i,2)==1
                i=[i i];
            end
            switch size(i,1)
                case 1
                    y=obj.a(i(1,1):i(1,2));
                case 2
                    y=obj.a(i(1,1):i(1,2),i(2,1):i(2,2));
                case 3
                    y=obj.a(i(1,1):i(1,2),i(2,1):i(2,2),i(3,1):i(3,2));
                case 4
                    y=obj.a(i(1,1):i(1,2),i(2,1):i(2,2),i(3,1):i(3,2),i(4,1):i(4,2));
                case 5
                    y=obj.a(i(1,1):i(1,2),i(2,1):i(2,2),i(3,1):i(3,2),i(4,1):i(4,2),i(5,1):i(5,2));
            end
            
        end
        function obj=set(obj, i, value)
            if size(i,2)==1
                i=[i i];
            end
            switch size(i,1)
                case 1
                    obj.a(i(1,1):i(1,2))=value;
                case 2
                    obj.a(i(1,1):i(1,2),i(2,1):i(2,2))=value;
                case 3
                    obj.a(i(1,1):i(1,2),i(2,1):i(2,2),i(3,1):i(3,2))=value;
                case 4
                    obj.a(i(1,1):i(1,2),i(2,1):i(2,2),i(3,1):i(3,2),i(4,1):i(4,2))=value;
                case 5
                    obj.a(i(1,1):i(1,2),i(2,1):i(2,2),i(3,1):i(3,2),i(4,1):i(4,2),i(5,1):i(5,2))=value;
            end
            
        end
        function y=it(obj,x1,x2)
            
            function yy=aP(c)
                yy=1;
                for ii=1:size(c,2)
                    yy=yy*c(ii);
                end
            end
            sizz=size(obj.a);
            if nargin==1
                x=[ones(size(sizz,2),1),sizz'];
            else
                x=[x1 x2];
            end
            
            
            xdiff=(x(:,2)-x(:,1)+1)';
            n=size(xdiff,2);
            prod=aP(xdiff);
            y=zeros(prod,n);
            for i=1:prod
                for j=1:n
                    p=aP(xdiff(1:j-1));
                    y(i,j)=mod(floor((i-1)/p),xdiff(j))+x(j,1);
                end
            end
            y=y';
        end
    end
end
