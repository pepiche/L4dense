% examples of synapse distributions for agglos
%
% at time of writing corresponding to Fig5a and Fig5c
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo();


%% configuration

rootDir = '/gaba/u/mberning/results/pipeline/20170217_ROI';
connFile = fullfile(rootDir, 'connectomeState', ...
    'connectome_axons-19-a-partiallySplit-v2_dendrites-wholeCells-03-v2-classified_SynapseAgglos-v8-classified.mat');
synFile = fullfile( ...
    rootDir, 'connectomeState', ...
    'SynapseAgglos_v8_classified.mat');
wcFile = fullfile(rootDir, 'aggloState', 'wholeCells_GTAxon_08_v4.mat');

targetClasses = { ...
    'Somata', 'ProximalDendrite', 'ApicalDendrite', ...
    'SmoothDendrite', 'AxonInitialSegment', 'OtherDendrite'};


%% loading data
% unforunately everything is distributed over many files right now with no
% direct correspondences. this means that agglos need to be identified
% based on overlap

p = Gaba.getSegParameters('ex145_ROI2017');

[conn, ~, axonClasses] = ...
    connectEM.Connectome.load(p, connFile);

minSynPre = 1;
[conn, axonClasses] = ...
    connectEM.Connectome.prepareForSpecificityAnalysis( ...
        conn, axonClasses, 'minSynPre', minSynPre);
synapses = load(synFile);

wc = load(wcFile);


[~, segMeta, borderMeta] = Seg.IO.loadGraph(p, false);


%% get soma agglos

p = Gaba.getSegParameters('ex145_ROI2017');
% somata_5a = [283, 7981, 1188]; % original position from HW
somata_5a = [395, 7949, 1188];  % position in segments by BS
somata_5c = [236, 3498, 990];

% get the ids of the segments at the soma locations
dat = Datasets.WkDataset('/wKcubes_live/2012-09-28_ex145_07x2_ROI2017/segmentation/1');
id_5a = dat.readRoi([somata_5a', somata_5a']);  % 5796324
id_5c = dat.readRoi([somata_5c', somata_5c']);  % 4048482

% find agglos in dendrites
idx_5a = cellfun(@(x)ismember(id_5a, x), conn.dendrites);
id = conn.denMeta.cellId(idx_5a);
idx_5a = find(conn.denMeta.cellId == id);
idx_5a(conn.denMeta.targetClass(idx_5a) == 'AxonInitialSegment') = [];
% idx_5a = [11228; 11330]; % result

idx_5c = cellfun(@(x)ismember(id_5c, x), conn.dendrites);
id = conn.denMeta.cellId(idx_5c);
idx_5c = find(conn.denMeta.cellId == id);
idx_5c(conn.denMeta.targetClass(idx_5c) == 'AxonInitialSegment') = [];
% idx_5c = [11268; 11325]; % result

% find agglos in wc
idx_5a_wc = find(arrayfun(@(x) ...
    any(ismember(cell2mat(conn.dendrites(idx_5a)), ...
    x.nodes(:,4))), wc.wholeCells)); % 21
idx_5c_wc = find(arrayfun(@(x) ...
    any(ismember(cell2mat(conn.dendrites(idx_5c)), ...
    x.nodes(:,4))), wc.wholeCells)); % 61


%% input synapses onto 5a

idx_conn = find(ismember(conn.connectome.edges(:,2), idx_5a));
idx_pre = conn.connectome.edges(idx_conn, 1);
idx_syn = conn.connectome.synIdx(idx_conn);
l = cellfun(@length, idx_syn);

type_pre = conn.axonMeta.axonClass(idx_pre);
coms = cellfun(@(x)L4.Synapses.synapseCom(synapses.synapses(x, :), ...
    borderMeta.borderCoM, 'mean'), ...
    idx_syn, 'uni', 0);
coms = round(cell2mat(coms));

type_pre = repelem(type_pre, l);

[type_pre, sI] = sort(type_pre, 'ascend');
coms = coms(sI, :);

save('Fig5a_synapses.mat', 'info', 'type_pre', 'coms');


%% output synapses of 5c

% get axon of soma
axSegs = wc.wholeCells(idx_5c_wc).nodes(wc.wholeCells(idx_5c_wc).axon, 4);
axSegs(isnan(axSegs)) = [];
axLUT = L4.Agglo.buildLUT(conn.axons, max(axSegs));
idx_ax = setdiff(axLUT(axSegs), 0);
ov_1093 = axSegs(axLUT(axSegs) == 1093); % for debugging why this overlaps

% get synapses of axons
[~, pre2syn] = L4.Synapses.synapsesAggloMapping(synapses.synapses, ...
    conn.axons(idx_ax), []);

% get coms
pre2syn = cell2mat(pre2syn);
coms_ax = L4.Synapses.synapseCom(synapses.synapses(pre2syn, :), ...
    borderMeta.borderCoM, 'mean');
isSpine = synapses.isSpineSyn(pre2syn);

save('Fig5c_synapses.mat', 'info', 'coms_ax', 'isSpine');


%% check in WK

% % fig 5a
% skel = L4.Agglo.agglo2Nml(conn.dendrites(idx_5a), segMeta.point');
% skel = skel.addNodesAsTrees(coms, 'Synapse', ...
%     arrayfun(@char, type_pre, 'uni', 0));
% skel.write('Fig5a_skel.nml')

% fig 5c
skel = L4.Agglo.agglo2Nml(conn.axons(idx_ax), segMeta.point');
skel.names = arrayfun(@(x)sprintf('Axon%d', x), idx_ax, 'uni', 0);
comments = repmat({'spine synapse'}, length(isSpine), 1);
comments(~isSpine) = repmat({'shaft synapse'}, sum(~isSpine), 1);
skel = skel.addNodesAsTrees(coms_ax, 'Synapse');
synTrees = (skel.numTrees()-length(isSpine)+1):skel.numTrees();
skel.names(synTrees) = cellfun(@(x, y)strcat(x, ' - ', y), ...
    skel.names(synTrees), comments, 'uni', 0);
skel.colors(length(idx_ax) + 1:end) = {[0, 1, 1, 1]};
skel = skel.addGroup('synapses');
skel = skel.addTreesToGroup(synTrees, 'synapses');
skel.write('Fig5c_skel.nml')

% add whole cell to fig5c skel for debugging
tmp = wc.wholeCells(61).edges;
tmp = unique(tmp, 'rows');
tmp(tmp(:,1) == tmp(:,2),:) = [];
skel = skel.addTree('WC_61', wc.wholeCells(61).nodes(:,1:3), tmp);
skel.colors{end} = [0 1 0 1];
skel = L4.Agglo.agglo2Nml({ov_1093}, segMeta.point', skel);
skel.colors{end} = [0 0 1 1];
skel.names{end} = 'WC61_Axon1093_overlaps';
skel.write('Fig5c_skel_debug.nml')


%% check axon agglo 1093

axonFile = fullfile( ...
    rootDir, 'aggloState', ...
    'axons_19_a_partiallySplit_v2.mat');
p.agglo.axonAggloFile = axonFile;
[axons, indBigAxons, axonParentIds] = L4.Axons.getLargeAxons(p, true, true);
mAx = load(axonFile);
segIds = Superagglos.getSegIds(mAx.axons);
lut = L4.Agglo.buildLUT(segIds);

idx_ax_1093 = 1099;
tmp = Superagglos.toSkel(mAx.axons(idx_ax_1093));
tmp.write('AxonAgglo_all_1099_.nml')

idx_ax_1093 = setdiff(lut(conn.axons{1093}), 0);
tmp = Superagglos.toSkel(mAx.axons(idx_ax_1093));
tmp.write('AxonAggloAll_overlap1093Large.nml');

%% reconstruct overlap calculation for agglo 1093
m = load(['/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/' ...
    'axons_19_a_partiallySplit_v2_large_mergedFlightPaths.mat']);

% need to correct for deleted ones (just updated the function to do this
% by default in newer versions)
indAxonsBig = find(mAx.indBigAxons);
tmp = ismember(indAxonsBig, m.parentIds);
tmp = find(tmp);
ov = m.ov(tmp); 

axonsBig = mAx.axons(mAx.indBigAxons);
axonsBig = axonsBig(tmp);
axonsSmall = mAx.axons(~mAx.indBigAxons);

% to skeletons
skel = Superagglos.toSkel(axonsBig(1093));
skel.names{end} = 'AxonAgglo1093';
skel = Superagglos.toSkel(axonsSmall(ov{1093}), skel);
skel.names(2:end) = arrayfun(@(x)sprintf('AxonSmall%d', x), ov{1093}, ...
    'uni', 0);
skel.colors(2:end) = repmat({[0 0 1 1]}, skel.numTrees() - 1, 1);
skel.write('AxonAgglo1093_overlaps.nml');

% get flight paths
fp = Superagglos.getFlightPath(axonsBig(1093), 'nodes');
fpSegIds = Skeleton.getSegmentIdsOfNodes(p, fp.nodes);
