function [out, mask] = dropoutLayer( cnet, input, lyr )
%DROPOUTLAYER Summary of this function goes here.
% INPUT input: 4d numerical
%           Input cube.
% OUTPUT out: 4d array
%           The modified input with cnet.dropout(lyr) units set to 0.
%        mask: 3d array
%           The mask that is multiplied with the input to get the output.
%           The mask is replicated for each channel (i.e. along the fourth
%           dimension of the input).
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if isa(input, 'gpuArray')
    mask = gpuArray.rand(cnet.mSize(input,1:3)) < (1 - cnet.dropout(lyr));
else
    mask = cnet.actvtClass(binornd(1,1 - cnet.dropout(lyr), ...
            cnet.mSize(input,1:3)));
end
out = bsxfun(@times, input, mask)./(1-cnet.dropout(lyr));

end

