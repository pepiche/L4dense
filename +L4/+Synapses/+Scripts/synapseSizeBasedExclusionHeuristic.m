% check heuristics that use synapse size for FP exclusion
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

p = Gaba.getSegParameters('ex145_ROI2017');


%% load data

% axons
Util.log('Loading axons.');
p.agglo.axonAggloFile = ['/gaba/u/mberning/results/pipeline/' ...
    '20170217_ROI/aggloState/axons_18_a.mat'];
axons = L4.Axons.getLargeAxons(p, true, true);

[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);

synFile = ['/gaba/u/mberning/results/pipeline/20170217_ROI/' ...
    'connectomeState/SynapseAgglos_v3_ax_spine_clustered.mat'];
mSyn = load(synFile);
synapses = mSyn.synapses;
synSize = cellfun(@(x)sum(borderMeta.borderSize(x)), synapses.edgeIdx);
isSpineSyn = mSyn.isSpineSyn;
shaftSynIdx = find(~isSpineSyn);


%% sort shaft synapses by increasing synapse size
% size = total number of  border voxels

[~, idx] = sort(synSize(shaftSynIdx), 'ascend');
shaftSynSorted = synapses(shaftSynIdx(idx),:);


%% 100 smallest shaft syns to WK

skel = L4.Synapses.synapse2Skel(shaftSynSorted(1:100,:), ...
    borderMeta.borderCoM, [], [], 'all', shaftSynIdx(idx(1:100)));
skel = skel.setDescription(['100 smallest shaft synapes from ' ...
    'SynapseAgglos_v3_ax_spine_clustered.']);
skel.write('SmallestShaftSyns.nml');

