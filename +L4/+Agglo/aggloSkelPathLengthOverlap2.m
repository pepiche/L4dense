function [ovL, recL] = aggloSkelPathLengthOverlap2( skels, segIds, ...
    skelToAgglos, agglos, nodeT )
%AGGLOSKELPATHLENGTHOVERLAP2 Calculate the skeleton path length that is
% covered by overlapping agglos. If the agglo results in disconnected
% components along the skeletons those are connected if the distance
% between them is less then a given threshold.
% INPUT skels: [Nx1] cell of skeleton objects
%           The ground truth skeletons.
%       segIds: [Nx1] cell
%           The segment ids for the corresponding skeleton in skels.
%       skelToAgglos: [Nx1] cell
%           The indices of the agglos that contain skeleton nodes.
%           (see output of L4.Agglo.aggloSkelOverlap)
%       agglos: [Nx1] struct or cell
%           The agglomerates in the agglo or superagglo format.
%       compDistT: (Optional) int
%           Threshold on the number of nodes between two components of the
%           same agglo that are not removed and thus contribute to the path
%           length for the agglo.
%           (Default: 3)
% OUTPUT ovL: [Nx1] cell
%           Contains the overlapping length in the skeleton for each
%           overlapping agglo in skelToAgglos in um.
%        recL: double
%           Total recovered path length covered by all overlapping agglos.
%           This can deviate slightly from cellfun(@sum, ovL) due to the
%           zero id handling.
%
% NOTE Due to the closing of stretches between agglos it can happen that
%      the same path length is counted for multiple agglos if nodeT is
%      chosen such that overlapping agglos are accepted with less then
%      nodeT overlap nodes.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('nodeT', 'var') || isempty(nodeT)
    nodeT = 3;
end

ovL = cellfun(@(x)zeros(size(x, 1), 1), skelToAgglos, 'uni', 0);
recL = zeros(length(skels), 1);
if iscell(segIds{1})
    segIds = cellfun(@(x)x{1}, segIds, 'uni', 0);
end

for i = 1:length(skelToAgglos)
    
    % get segment ids in agglos
    if isstruct(agglos)
        aggloIds = Superagglos.getSegIds(agglos(skelToAgglos{i}));
    else
        aggloIds = agglos(skelToAgglos{i});
    end
    
    for j = 1:size(skelToAgglos{i}, 1)
        
        skel = skels{i};
        
        % find the components for the current agglo
        toDelNodes = ~ismember(segIds{i}, aggloIds{j});
        tmp = skel.deleteNodes(1, toDelNodes);
        aggloNodes = find(~toDelNodes);
        compEndNodes = tmp.calculateNodeDegree(1);
        compEndNodes = compEndNodes{1} == 1;
        nodes = aggloNodes(compEndNodes);
        
        for k = 1:length(nodes)
            skel = skels{i}; % required if branches are disconnected
            skel = skel.directedEdgeList(1, nodes(k));
            G = skel.getGraph(1, false);
            [~, pred, ~] = graphtraverse(G.adjacency, nodes(k), ...
                'Depth', nodeT + 1);
            paths = graphpred2path(pred);
            paths = paths(~cellfun(@isempty, paths));
            paths(cellfun(@length, paths) == 1) = [];
            pathNodesInAgglo = cellfun(@(pNode) ...
                ismember(segIds{i}(pNode), aggloIds{j}), paths, 'uni', 0);
            keepPaths = cellfun(@(x)x(1) & x(end), pathNodesInAgglo);
            toDelNodes(cell2mat(paths(keepPaths))) = false;
        end
        skel = skels{i};
        skel = skel.deleteNodes(1, toDelNodes);
        ovL{i}(j) = sum(skel.pathLength())./1000;
    end
    
    % recovered path length by combining all agglos
    aggloIds = cell2mat(aggloIds);
    skel = skels{i};
        
    % find the components for the current agglo
    toDelNodes = ~ismember(segIds{i}, aggloIds);
    tmp = skel.deleteNodes(1, toDelNodes);
    aggloNodes = find(~toDelNodes);
    compEndNodes = tmp.calculateNodeDegree(1);
    compEndNodes = compEndNodes{1} == 1;
    nodes = aggloNodes(compEndNodes);

    for k = 1:length(nodes)
        skel = skels{i}; % required if branches are disconnected
        skel = skel.directedEdgeList(1, nodes(k));
        G = skel.getGraph(1, false);
        [~, pred, ~] = graphtraverse(G.adjacency, nodes(k), ...
            'Depth', nodeT + 1);
        paths = graphpred2path(pred);
        paths = paths(~cellfun(@isempty, paths));
        paths(cellfun(@length, paths) == 1) = [];
        pathNodesInAgglo = cellfun(@(pNode) ...
            ismember(segIds{i}(pNode), aggloIds), paths, 'uni', 0);
        keepPaths = cellfun(@(x)x(1) & x(end), pathNodesInAgglo);
        toDelNodes(cell2mat(paths(keepPaths))) = false;
    end
    skel = skels{i};
    skel = skel.deleteNodes(1, toDelNodes);
    recL(i) = sum(skel.pathLength())./1000;
end


end

