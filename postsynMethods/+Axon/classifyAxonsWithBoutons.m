function [axons,axonBoutons,labels] = classifyAxonsWithBoutons(wkFlag,plotFlag)
if ~exist('wkFlag','var') || isempty(wkFlag)
    wkFlag = false;
end
if ~exist('plotFlag','var') || isempty(plotFlag)
    plotFlag = false;
end

% find for each axon agglo, the fraction of exc vs inh interfaces
outDir = '/tmpscratch/sahilloo/L4/data/';
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

m = load(fullfile(outDir,'spine-heads-and-attachment.mat'));
shIds = vertcat(m.shAgglos{:});

% load all axon agglos and boutons
m = load(fullfile(outDir,'axonsWithBoutons.mat'));
axons = m.axons(2:end);
axonBoutons = m.axonBoutons(2:end);

% load synaptic interfaces
m = load([outDir 'interfacesPrePost.mat']);
preInterfaces = m.preInterfaces;

% find all bouton interfaces;
labels = {};
tic % for progess bar
for i=1:size(axonBoutons,1)
    labels{i} = cell2mat(cellfun(@(x) findBoutonLabel(x,preInterfaces,shIds), axonBoutons{i},'uni',0));
    Util.progressBar(i,size(axonBoutons,1));
end
labels = labels';
spineFraction = cell2mat(cellfun(@(x) sum(x)/length(x),labels,'uni',0));
boutonTable = table(axons,axonBoutons,labels,spineFraction);
save(fullfile(outDir,'boutonTable.mat'),'boutonTable');

if wkFlag
    % view in wK
    excBoutons = cellfun(@(x,y) x(y),axonBoutons,labels,'uni',0);
    excBoutonsAll = vertcat(excBoutons{:});
    inhBoutons = cellfun(@(x,y) x(~y),axonBoutons,labels,'uni',0); 
    inhBoutonsAll = vertcat(inhBoutons{:});

    rng(0);
    idxToPlot = randperm(size(excBoutonsAll,1),50);
    for i=1:length(idxToPlot)
        nodes = excBoutonsAll(idxToPlot(i));
        skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
        skel = Skeleton.setParams4Pipeline(skel, p);
        skel.write(fullfile(outDir, ['nmls/excBoutons_' num2str(i) '_' datestr(now,'yyyymmdd') '.nml']));
    end

    rng(0);
    idxToPlot = randperm(size(inhBoutonsAll,1),50);
    for i=1:length(idxToPlot)
        nodes = inhBoutonsAll(idxToPlot(i));
        skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
        skel = Skeleton.setParams4Pipeline(skel, p);
        skel.write(fullfile(outDir, ['nmls/inhBoutons_' num2str(i) '_' datestr(now,'yyyymmdd') '.nml']));
    end
end
if plotFlag
    fig = figure;
    subplot(2,1,1)
    % plot distribution
    excCount = cellfun(@(x) sum(x),labels);
    inhCount = cellfun(@(x) sum(~x),labels);
    plot(inhCount,excCount,'.'); axis('equal')
    xlabel('non-spine targeting')
    ylabel('spine targeting')
    title('# "spine-targeting" and "non-spine-targeting" boutons per axon')
    % histogram of spine fraction of axons
    subplot(2,1,2)
    histogram(spineFraction,'DisplayStyle','stairs');
    xlabel('Spine-targeting bouton fraction');
    ylabel('# of axons');
    title('Histogram of spine-targetingBoutons/totalBoutons in axons')
    saveas(fig,fullfile(outDir,'figures/spine-non-spineTargetingBoutons.png'));
    close all
end
end

function label = findBoutonLabel(allSeg,preInterfaces,shIds)
    idxSynInterfaces = ismember(preInterfaces(:,1),allSeg);
    % find all interfaces with pre in first column
    allInterfaces = preInterfaces(idxSynInterfaces,:);
    label = any(ismember(allInterfaces(:,2),shIds)); % assign true label if atleast one post syn seg is spine head
end 
