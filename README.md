# Code Repository for
**Dense Connectomic Reconstruction in Layer 4 of the Somatosensory
Cortex**  
Alessandro Motta\*, Manuel Berning\*, Kevin M. Boergens\*, Benedikt
Staffler\*, Marcel Beining, Sahil Loomba, Philipp Hennig, Heiko Wissler,
Moritz Helmstaedter. Science (2019). DOI:
[10.1126/science.aay3134](https://doi.org/10.1126/science.aay3134)

with code contributions from Florian Drawitsch, Ali Karimi, Martin
Schmidt, Christian Schramm, Martin Zauser, and
[more](https://brain.mpg.de/research/helmstaedter-department/people.html).

The electron microscopy volume, neurite reconstructions, and more are
available

* for viewing and annotation at [webknossos.org](https://webknossos.org/datasets/MPI_Brain_Research/2012-09-28_ex145_07x2_ROI2017/view), and
* for download at [https://l4dense2019.brain.mpg.de](https://l4dense2019.brain.mpg.de/).


## Getting Started
The code in this repository was primarily developed for the R2015b and
R2017b releases of [MATLAB](https://www.mathworks.com/products/matlab.html).

To run the code, first download the [ZIP archive](https://l4dense2019.brain.mpg.de)
or clone this repository using [Git](https://git-scm.com/). Then install
the external dependencies listed below. Finally, switch to the code
directory and start MATLAB. This should automatically run the
`startup.m` script.

A high-level commentary on all the processing performed in this work is
available in the [main and supplementary methods](https://doi.org/10.1126/science.aay3134).


## Dependencies
* [JSONLab](/auxiliaryMethods/jsonlab/README.md)
* [JSON-C](/auxiliaryMethods/json/json-c/README.md)
* [MATLAB JSON](/auxiliaryMethods/json/matlab-json/README.md)
* [GPML](/active/gpml/README.md)
* [WKW](/auxiliaryMethods/wkw/README.md)
* [DGof](https://cran.r-project.org/web/packages/dgof/index.html)  
  Taylor B. Arnold, John W. Emerson (2011) The R Journal  
  Nonparametric Goodness-of-Fit Tests for Discrete Null Distributions  
  DOI: [10.32614/RJ-2011-016](https://doi.org/10.32614/RJ-2011-016)


## Acknowledgements
* Dr. Zdravko Botev for
  [kde2d](https://www.mathworks.com/matlabcentral/fileexchange/17204-kernel-density-estimation),
  a function for fast two-dimensional kernel density estimation  
  Z. I. Botev, J. F. Grotowski, D. P. Kroese (2010) Annals of Statistics  
  Kernel Density Estimation via Diffusion  
  DOI: [10.1214/10-AOS799](https://doi.org/10.1214/10-AOS799)
* Kevin L. Briggman for early versions of the 3D image alignment routines that were used in  
  Kevin L. Briggman, Moritz Helmstaedter, Winfried Denk (2011) Nature  
  Wiring Specificity in the Direction-selectivity Circuit of the Retina  
  DOI: [10.1038/nature09818](https://doi.org/10.1038/nature09818)
* Timothy E. Holy for [distinguishable_colors](https://www.mathworks.com/matlabcentral/fileexchange/29702-generate-maximally-perceptually-distinct-colors), a function to "generate
  maximally perceptually-distinct colors"
* Arseny Kapoulkine for pugixml, a XML parser library for C++  
  https://pugixml.org/


## License
This project is licensed under the [MIT license](LICENSE).  
Copyright (c) 2019 Department of Connectomics, Max Planck Institute for
Brain Research, D-60438 Frankfurt am Main, Germany
