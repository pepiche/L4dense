function y = calcPositions(C,all_fns)

for i=it([[1;1;1],[C.M; C.N; 1]])
    if size(at(all_fns,i),1)>0
        idx_width = mod(i(1)-1,12) * 256;
        idx_height = mod(i(2)-1,8) * 256;
        if mod(i(1),12) ~= 1
            idx_width = idx_width - 32;
        end
        if mod(i(1),12) == 0
            idx_width = idx_width - 32;
        end
        if mod(i(2),8) ~= 1
            idx_height = idx_height - 8;
        end
        if mod(i(2),8) == 0
            idx_height = idx_height - 8;
        end
        
        
       y.x_y_z_xvsy_beginvsend(i(1),i(2),i(3),:,:) = ...
           permute([idx_width + 1, idx_width + C.dims(1); idx_height + 1, idx_height + C.dims(2)], ...
           [5 6 3 4 1 2]);
       
    end
end
y.x_y_z_xvsy_beginvsend = repmat (y.x_y_z_xvsy_beginvsend, [1, 1, C.P, 1, 1]);