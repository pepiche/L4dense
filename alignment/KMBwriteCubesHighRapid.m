function KMBwriteCubesHighRapid(currentThread2,C)
thismontage=cell(4,1);
for i=1:8
    currentThread=(currentThread2-1)*8+i;
    fn=[C.cubedir 'rapidFin' sprintf('%u',currentThread) '.mat'];
    if exist(fn,'file')~=0
    load(fn);
    end
    thismontage{4}=cat(3,thismontage{4},thismontage2);
end

xyzCs=[1;1;currentThread2];
relativeV=[1,1;1,1;1 8];
        magIt=8;
        c8d=1/magIt*[C.maxMagMultXY';1].*relativeV(:,2);
        KMBwriteCubeItsMaker(C,xyzCs,magIt,thismontage,0,c8d,false)
     
