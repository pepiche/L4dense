
setConfig;
%outDir = [config.outDir 'filter_9_4/'];
outDir = fullfile(config.outDir,[date 'axon-initial-segment/']);
if ~exist(outDir,'dir')
    mkdir(outDir);
end

load(config.param);
Util.log('Loading stuff..')
aggloFolder = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState';
m=load(fullfile(aggloFolder,'dendrites_18_b.mat'));
agglos = Superagglos.getSegIds(m.dendrites(m.indAIS));

Util.log('Now running main viz')
%p.seg.root='/tmpscratch/amotta/l4/2012-09-28_ex145_07x2_ROI2017/segmentation/1/';
p.seg.root = ['/gabaghi/wKcubes_archive/2012-09-28_ex145_07x2_ROI2017/segmentation/1/'];
p.seg.backend = 'wkwrap';
%Visualization.exportAggloToAmira(p,agglos,outDir);
Visualization.exportAggloToAmira(p,agglos,outDir,'reduce',0.5,'smoothSizeHalf',4,'smoothWidth',8);
info=Util.runInfo();
save(fullfile(outDir,'info.mat'),'info');
