% script to query synapses that are not associated to any axon agglomerate
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo();
p = Gaba.getSegParameters('ex145_ROI2017');
queryRound = 'round_1';
outFolder = fullfile(p.agglo.saveFolder, ...
    sprintf('missed_boutons_%s', queryRound));
createDynamicQuerySeg = true;


%% prepare outputs

Util.log('Queries are stored at %s.', outFolder);
if ~exist(outFolder, 'dir')
    mkdir(outFolder)
end


%% get axons

axon_ver = 'axons_18_a.mat';
p.agglo.axonAggloFile = fullfile(p.agglo.saveFolder, axon_ver);
axons = L4.Axons.getLargeAxons(p, true, true);
idx = cellfun(@isempty, axons);
axons = axons(~idx);


%% get synapses

synFile = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v3_ax_spine_clustered.mat');
m = load(synFile);
synapses = m.synapses;


%% synapses with no presyn agglo

[stats, syn2agglo] = L4.Synapses.synapseCoverage( synapses, axons, []);
idx = cellfun(@isempty, syn2agglo.syn2pre);


%%  get query start pos (nodes of presyn segment for ortho tracings)

m = load(p.svg.segmentMetaFile, 'point');
point = m.point';
queryPoints = point(cellfun(@(x)x(1), synapses.presynId(idx)), :);

queryFile = fullfile(outFolder, 'queries.mat');
if ~exist(queryFile, 'file')
    synIdx = find(idx);
    save(queryFile, 'queryPoints', 'synapses', 'synIdx', 'info');
end


%% create segmentation

if createDynamicQuerySeg
    segOutput = fullfile(outFolder, 'seg', '1');
    wkwInit('new', segOutput, 32, 32, 'uint32', 1);
    seg.root = segOutput;
    seg.backend = 'wkwrap';
    Util.log('Creating segmentation for dynamic stopping at %s.', ...
        seg.root);
    connectEM.generateSegmentationForNewQueries(p, seg, axons);
end
