function l = approxDendriteAggloLength( dendrites, somaAgglos, ...
    keepFlightPaths, scale )
%APPROXDENDRITEAGGLOLENGTH Agglo length calculation based on the minimum
% spanning tree of segments and exclusion of soma.
% INPUT dendrites: struct
%           The dendrite agglomerates in the superagglo format.
%       somaAgglos: [Nx1] cell
%           Soma agglomerations in the agglo format.
%       keepFlightPaths: (Optional) logical
%           Flag whether to discard flight path nodes from MST calculation.
%           (Default: true)
%       scale: (Optional) [1x3] double
%           Voxel size scale.
%           (Default: [1, 1, 1])
% OUTPUT l: [Nx1] double
%           The agglo length of each dendrite in um.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

maxSegId = max([arrayfun(@(x)max(x.nodes(:,4)), dendrites); ...
                cellfun(@max, somaAgglos)]);
somaLUT = false(maxSegId + 1, 1);
somaLUT(cell2mat(somaAgglos)) = true;

if exist('keepFlightPaths', 'var') && ~isempty(keepFlightPaths) ...
        && ~keepFlighPaths
    somaLUT(maxSegId + 1) = true;
end

% flight paths to maxSegIds + 1
nodes = cell(length(dendrites), 1);
for i = 1:length(dendrites)
    nodes{i} = dendrites(i).nodes;
    nodes{i}(isnan(nodes{i})) = maxSegId + 1;
end
    
% discard soma segments
nodes = cellfun(@(x)x(~somaLUT(x(:,4)), :), nodes, 'uni', 0);
for i = 1:length(dendrites)
    dendrites(i).nodes = nodes{i};
end

% get mst length
if exist('scale', 'var') && ~isempty(scale)
    l = Superagglos.mstLength(dendrites, scale); % in um
else
    l = Superagglos.mstLength(dendrites);
end

end

