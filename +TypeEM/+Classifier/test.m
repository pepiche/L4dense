function [precRec, fig] = test(param, classifiers)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    import TypeEM.*;
    
    gt = GroundTruth.loadSetTest(param, classifiers.featureSetName);
   [precRec, fig] = Classifier.evaluate(param, classifiers, gt);
end
