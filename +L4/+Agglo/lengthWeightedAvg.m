function avg = lengthWeightedAvg( l, l_tot )
%LENGTHWEIGHTEDAVG Length weighted average of agglos.
% INPUT l: [Nx1] double
%           The lenght of the single agglos.
%       l_tot: (Optional) double
%           Total length of the process.
%           (Default: sum(l))
% OUTPUT avg: double
%           The length weighted average:
%           avg = sum_i (l_i*(l_i / l_tot)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('l_tot', 'var') || isempty(l_tot)
    l_tot = sum(l);
end

avg = sum(l.*(l./l_tot));


end

