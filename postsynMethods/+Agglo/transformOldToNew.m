function newAgglo = transformOldToNew(agglo,edges,points)
% agglo : cell array with segIds
% edges : segment edge list Nx2 array
% points: comList for segIds
% create new agglo format from old agglo
newAgglo = struct;
newAgglo.nodes = points(agglo{:},:);
newAgglo.nodes(:,4) = agglo{:};

[~,newAgglo.edges(:,1)] = ismember(edges(:,1),agglo{:});
[~,newAgglo.edges(:,2)] = ismember(edges(:,2),agglo{:});


end


