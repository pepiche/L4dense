% analysis of axon queries round 2 with comments
outDir = '/tmpscratch/sahilloo/L4/data/';
m=load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameterWithSynapses.mat');
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

skeletonFolders = {'/tmpscratch/scchr/AxonEndings/axonQueryResults/CS_MB_L4_AxonLeftQueries_nmls/'};

% Lookup segment ids of nodes+neighbours of nmls in all folders defined above
[ff.segIds, ff.neighbours, ff.filenames, ff.nodes, ff.startNode, ff.comments] ...
    = connectEM.lookupNmlMulti(p, skeletonFolders, false);
save('/tmpscratch/sahilloo/L4/data/ff_struct_SL_all.mat', 'ff');

% look at comments
allComments = vertcat(ff.comments);
nonemptyIdx = find(~cellfun(@isempty,allComments));
allNonemptyComments = allComments(nonemptyIdx);

% find which comments match the token
token1 = 'unsolved';
token2 = 'unsure';
isToken = false(size(allComments,1),1);
for i =1:size(allComments,1)
    str = char(allComments{i});
    isToken(i) = any(regexpi(str,token1)) | any(regexpi(str,token2)) ;
end

% find which nmls correspond to those comments
ffOut = structfun(@(x) x(isToken),ff,'uni',0);

% copy these nmls to separate folder
outFolder = '/tmpscratch/sahilloo/L4/axonQueriesWithComments/';
if ~exist(outFolder,'dir')
    mkdir(outFolder);
end
outfiles = ffOut.filenames;
cellfun(@(x) copyfile(x,outFolder), outfiles, 'uni',false);
fprintf('Finished copying nmls with comments \n');

% calculate path lengths and prob
% agglos = ffOut.segIds;
% lens = calcPathLengths(p, agglos);
% prob = arrayfun(@(x) x/sum(lens), lens,'uni',0);

%% random sampling path lengths based
rng(0)
% filesToView = datasample(1:outfiles,20,'Weights',prob);
count = 50;
filesToView = outfiles(randperm(length(outfiles),count));
outFolderSampled = '/tmpscratch/sahilloo/L4/axonQueriesWithCommentsSampled/';
if ~exist(outFolderSampled,'dir')
    mkdir(outFolderSampled);
end
cellfun(@(x,y) copyfile(x,fullfile(outFolderSampled,['Tree ' num2str(y) '.nml'])), filesToView, ...
    num2cell(1:count), 'uni',false);
zip(fullfile(outDir,'axonQueriesWithComments'),filesToView);
fprintf('Finished creating zip with random nmls with unsolved comments \n');


