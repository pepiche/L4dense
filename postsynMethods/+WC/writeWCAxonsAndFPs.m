% Description: write wholecells along with Hiwi axons traced parts
setConfig;
outDirNml = fullfile(config.outDir,'wcNMLsWithAxonsAndFPs');
Util.log('Loading data...')
m = load(config.param);
p = m.p;

aggloFolder = fullfile(p.saveFolder, 'aggloState');
% wholeCells_07 still has axons
% wholeCells_08 has axons removed automated fashion only
% wholeCells_GTAxon_08_v3 has axons removed using hiwi tracings but bugs
% wholeCells_GtAxon_08_v4 
m=load(fullfile(aggloFolder,'wholeCells_GTAxon_08_v4.mat'));
wc = m.wholeCells;
%{
m = load(fullfile(config.outDir,'wholeCellsAxonCutState.mat'));
usedCells = m.usedCells;
filenames = m.filenames;
%}
fpSkels = cellfun(@(x) skeleton(x),filenames);

%assert(isequal(wc,m.wholeCells))

info = Util.runInfo();

%view wc in wK
Util.log('Now writing nmls to view in wK...')
if ~exist(outDirNml,'dir')
    mkdir(outDirNml);
end

% name output nmls to shCount and wc index
tic;
endStep = length(wc);
%idxToPlot = 1:randperm(endStep,5);
idxToPlot = 1:endStep;
for i = idxToPlot
    thisWC = wc(i);
    skel = Superagglos.toSkel(thisWC);
    thisWC.axon(isnan(thisWC.axon)) = 0;% axon mask can be NaN
    nodesToComment = find(thisWC.axon);
    skel = skel.addBranchpoint(nodesToComment);
    skel.names(1) = {'wc'};

    skelToAdd = fpSkels(usedCells(i)); countTrees = skelToAdd.numTrees;
    skel = skel.addTreeFromSkel(skelToAdd);
    skel.names(2:countTrees+1) = repelem({'flightpath'},countTrees);
  
    skel.write(fullfile(outDirNml, ['wcWAxon_' num2str(i) '.nml']));
    Util.progressBar(i,endStep);
end

Util.log('Finished creating nmls to view');



