function gt = loadSet(param, featureSetName, nmlFiles)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % make nmlFiles a cell array
    if ~iscell(nmlFiles)
        nmlFiles = {nmlFiles};
    end
    
    switch featureSetName
        case 'segment'
            gtFunc = @TypeEM.GroundTruth.buildForSegments;
        case 'segmentAgglo'
            gtFunc = @TypeEM.GroundTruth.buildForAgglomerates;
        otherwise
            error('Invalid feature set name');
    end
    
    % load and merge ground truth structures
    gt = cellfun(@(nml) {gtFunc(param, nml)}, nmlFiles);
    gt = Util.concatStructs(1, gt{:});
    gt.class = gt.class(1, :);
end