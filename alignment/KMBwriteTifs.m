function KMBwriteTifs(all_fns,currentThread2,C,ttemp)
ixx=it([C.inA;[currentThread2,currentThread2]]);
for ix2=1:size(ixx,2)
    currentThread=(currentThread2-1)*size(ixx,2)+ix2;
    
    %kmbw=KMBwait(currentThread,fileReaderThreads,C,'montagesMade',true);
    xyzCs=ixx(:,ix2);
    thismontage=cell(2,size(ixx,2));
    writeB=ones([C.maxMagMultXY,1],'uint8');
    
    for count = (xyzCs(3)-1)*C.cubesize+1:min(xyzCs(3)*C.cubesize,C.P)
        LSSweightsum=ttemp.weightsum((count-1)*C.M*C.N+1:count*C.M*C.N);
        for i=1:size(LSSweightsum,1)
            LSSweightsum(i,2)=i;
        end
        LSSweightsum=sortrows(LSSweightsum);
        countT=count-(xyzCs(3)-1)*C.cubesize;
        thismontage{1,countT}=zeros(C.cubesize*[C.maxMagMultXY],'uint8');
    
        for imgcount2 = 1:C.M*C.N
            imgcount=LSSweightsum(end+1-imgcount2,2);
            thisimg = (count-1)*C.M*C.N+imgcount;
            xyIts = [ mod(imgcount-1,C.M) floor((imgcount-1)/C.M)]+1;
            thisshifts = ttemp.shiftsN(thisimg,:);
            xycoords = C.dims.*(xyIts-1)+fix(thisshifts)+1;
            xycoordsT=xycoords+C.dims-1;
            
            
            if size(at(all_fns,[xyIts(1);xyIts(2);count]),1)>0
                
               cache=imread(at(all_fns,[xyIts(1);xyIts(2);count]))';
                 thismontage{1,countT}(xycoords(1):xycoordsT(1), xycoords(2):xycoordsT(2))=cache;
            end
        end
       imwrite(thismontage{1,countT}',[C.tifdir '\' sprintf('%0.5u.tif',count)],'tif');
       thismontage{1,countT}=[];
    end
    
end


end