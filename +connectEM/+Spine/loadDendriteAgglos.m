function [dendAgglos, aggloIds, nonCandAgglos, nonCandAgglosIdx, dendAgglosAll] = ...
        loadDendriteAgglos(param, aggloFile)
    % [dendAgglos, aggloIds, dendAgglosAll] = loadDendriteAgglos(config)
    %   This function load the dendrite reconstructions and returns
    %
    %   1. the dendrite agglomerates with at least 10 ^ 5.5 voxels (in
    %      dendAgglos). The size threshold is used to exclude spine
    %      head-only agglomerates.
    %   2. the agglomerate IDs for all agglomerates above the size limit
    %   3. all dendrite agglomerates (in dendAgglosAll)
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % load all dendrite super-agglomerates
    dendAgglosAll = load(aggloFile);
    dendAgglosAll = dendAgglosAll.dendrites;

    % build regular agglomerates
    dendAgglosAll = arrayfun( ...
        @(sa) {Agglo.fromSuperAgglo(sa)}, dendAgglosAll);

    % discard spine head-only agglos
    aggloSizes = Agglo.calculateVolume(param, dendAgglosAll);
    idxCand = aggloSizes(:) > (10 ^ 5.5);
    aggloIds = find(idxCand);
    dendAgglos = dendAgglosAll(aggloIds);
    nonCandAgglosIdx = find(~idxCand);
    nonCandAgglos = dendAgglosAll(~idxCand);
end
