% Description: write wholecells to nmls and check node precision
setConfig;
Util.log('Loading data...')
m = load(config.param);
p = m.p;

aggloFolder = fullfile(p.saveFolder, 'aggloState');
m=load(fullfile(aggloFolder,'wholeCells_07.mat'));
wc = m.wholeCells;

outDirNml = fullfile(config.outDir,'wcNmls');
if ~exist(outDirNml,'dir')
    mkdir(outDirNml);
end

info = Util.runInfo();

%view wc in wK
Util.log('Now writing nmls to view in wK...')

tic;
endStep = length(wc);
idxToPlot = 1:endStep;
skel = {};
for i = idxToPlot
    thisWC = wc(i);
    thisSkel = Superagglos.toSkel(thisWC);
    thisSkel.write(fullfile(outDirNml, ['wc_' num2str(i) '.nml']));
    skel{i} = thisSkel;
    Util.progressBar(i,endStep);
end

Util.log('Finished creating nmls to view');

% after the axon edges are cut off

% load the wc skel as skelNew

% find which nodes are missing between wc.nodes and skelNew nodes

% update wc with Superagglos.removeNodesFromAgglo(,true)

% sanity check that the wc.edges still form only one CC with Graph.findConnectedComponents




