function [dendrites, toDel] = addSomasToDendrites( dendrites, somas, method )
%ADDSOMASTODENDRITES Add somata to the dendrite agglos.
% INPUT dendrites: [Nx1] struct or [Nx1] cell
%           Dendrite agglos in the agglo or superagglo format.
%       somas: [Nx1] cell
%           The soma agglomerations.
%       method: string
%           Mode of adding the soma
%           'separate': (Default) Somata are added as separate agglos.
%               Soma segment ids are removed from all other dendrites.
%           'merge': Somata are merged into the class with which they
%               overlap most (in terms of total number of segments).
% OUTPUT dendrites: [Nx1] cell
%           The new set of dendrite agglos with incorporated somatain the
%           agglo format.
%        toDel: [Nx1] int
%           Linear indices wrt to the input dendrites that are removed in
%           the 'separate' method.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('method', 'var') || isempty(method)
    method = 'separate';
end

if isstruct(dendrites)
    dendrites = Superagglos.getSegIds(dendrites);
end

maxSegId = max([cellfun(@max, dendrites); cellfun(@max, somas)]);


switch method
    case 'separate'
        isSomaSeg = false(maxSegId, 1);
        isSomaSeg(cell2mat(somas)) = true;
        dendrites = cellfun(@(x)x(~isSomaSeg(x)), dendrites, 'uni', 0);
        toDel = find(cellfun(@isempty, dendrites));
        dendrites = cat(1, dendrites, somas);
        dendrites(toDel) = [];
    case 'merge'
        denLUT = Agglo.buildLUT(maxSegId, dendrites);
        soma2den = cellfun(@(x)denLUT(x), somas, 'uni', 0);
        soma2den = cellfun(@(x)mode(x(x>0)), soma2den);
        dendrites(soma2den) = cellfun(@(x, y) unique([x; y]), ...
            dendrites(soma2den), somas, 'uni', 0);
        toDel = [];
    otherwise
        error('Unknown mode %s.', method);
end


end

