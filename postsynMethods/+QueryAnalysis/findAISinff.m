% filter all trees marked AIS

skel1 = skeleton('/home/loombas/Downloads/2012-09-28_ex145_07x2_ROI2017__explorational__sloomba__859300.nml');

skel2 = skeleton('/home/loombas/Downloads/2012-09-28_ex145_07x2_ROI2017__explorational__sloomba__8592f7.nml');

skel3 = skeleton('/home/loombas/Downloads/2012-09-28_ex145_07x2_ROI2017__explorational__sloomba__8592ef.nml');

skel1Names = sort(skel1.names);
skel2Names = sort(skel2.names);
skel3Names = sort(skel3.names);

token = '\(AIS\)';
skel = skel1;
skel = skel.mergeSkels(skel2);
skel = skel.mergeSkels(skel3);
treeNames = skel.names;
idxAIS = cellfun(@(x) any(regexp(x,token)),treeNames);

%% find the corresponding ff in axon-ff-dend overlap
idxAISinST = cellfun(@(x) str2double(x(1:regexpi(x,'f')-1)),treeNames(idxAIS));
save('/home/loombas/mnt/gaba/tmpscratch/L4/forIso/idxAISinST.mat','idxAISinST');