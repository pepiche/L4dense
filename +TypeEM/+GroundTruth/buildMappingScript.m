function buildMappingScript(param, gt)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % find largest occuring segment ID
    maxSegId = Seg.Global.getMaxSegId(param);
    
    % build segment sets
    classCount = numel(gt.class);
    agglos = arrayfun(@(classIdx) ...
        gt.segId(gt.label(:, classIdx) > 0), ...
        1:classCount, 'UniformOutput', false);
    
    % build mapping
    WK.makeMappingScript(maxSegId, agglos);
end