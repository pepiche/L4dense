% generate connectome for interfaces from an agglo
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo(false);
p = Gaba.getSegParameters('ex145_ROI2017');
aggloFolder = fullfile(p.saveFolder, ...
    '/SVGDB/agglos/ax18a_deWC01_wSp_wSo/');
axon_ver = 'axons_18_a.mat';
p.agglo.axonAggloFile = fullfile(p.agglo.saveFolder, axon_ver);
synFile = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v5_ax18a_deWC01wSp.mat');
outFile = fullfile(p.connectome.saveFolder, ...
    'connectome_ax18a_deWC01wSp_v4.mat');
outFileSmall = fullfile(p.connectome.saveFolder, ...
    'connectome_ax18a_deWC01wSp_v4_small.mat');


%% load agglos & synapses

% axons
Util.log('Loading axons.');
axons = L4.Axons.getLargeAxons(p, true, true);
idx = cellfun(@isempty, axons);
axons = axons(~idx);

% dendrites
Util.log('Loading dendrites and target classes.');
[dendFile, targetClass, targetLabels] = L4.Connectome.buildTargetClasses();
m = load(dendFile);
dendrites = m.dendAgglos(targetClass ~= 'Ignore');
targetClass(targetClass == 'Ignore') = [];

% add somata to dendrites
mSom = load(p.agglo.somaFile);
somata = mSom.somaAgglos(:,1);
[dendrites, toDel] = L4.Specificity.addSomasToDendrites(dendrites, ...
    somata, 'separate');
targetClass(toDel) = [];
targetClass(end+1:end+length(somata)) = 'Somata';

% target classes
idxSoma = targetClass == 'Somata';
idxWC = targetClass == 'WholeCell';
idxAD = targetClass == 'ApicalDendrite';
idxSD = targetClass == 'SmoothDendrite';
idxAIS = targetClass == 'AxonInitialSegment';
idxOther = targetClass == 'OtherDendrite';

% synapses
Util.log('Loading synapses.');
mSyn = load(synFile);
synapses = mSyn.synapses;


%% map agglos to agglo segmentation

m = load(fullfile(aggloFolder, 'eClass.mat'), 'segMapping');
[axons, dendrites] = L4.AggloSeg.mapSegIds(m.segMapping, axons, ...
    dendrites);


%% calculate full connectome

Util.log('Calculating connectome.');
[ syn2Agglo, pre2syn, post2syn, connectome ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, axons, dendrites );
connectomeFull = L4.Connectome.pairwise2Full(connectome, ...
    [length(axons), length(dendrites)]);


%% connectome synapse information

Util.log('Adding synapse meta information.');

% load border meta
borderMeta = load(fullfile(aggloFolder, 'globalBorders.mat'));
connectomeMeta = L4.Connectome.connectomeBorderMeta(connectome, ...
    synapses, borderMeta);


%% generate contactome

Util.log('Calculating contactome.')
m = load(fullfile(aggloFolder, 'edges.mat'), 'edges');
edges = m.edges;
contactome = L4.Agglo.findEdgesBetweenAgglos2(axons, dendrites, edges);
contactomeMeta = L4.Connectome.contactomeBorderMeta(contactome, ...
    borderMeta);


%% target class connectome

Util.log('Target class mapping.');
denClasses = {'soma', 'whole cells', 'apical dendrites', ...
    'smooth dendrite', 'AIS', 'other'};
classConnectome = zeros(length(axons), length(denClasses));
classConnectome(:,1) = sum(connectomeFull(:, idxSoma), 2);
classConnectome(:,2) = sum(connectomeFull(:, idxWC), 2);
classConnectome(:,3) = sum(connectomeFull(:, idxAD), 2);
classConnectome(:,4) = sum(connectomeFull(:, idxSD), 2);
classConnectome(:,5) = sum(connectomeFull(:, idxAIS), 2);
classConnectome(:,6) = sum(connectomeFull(:, idxOther), 2);


%% axon statistics
% taken from connectEM.Connectome.augmentForMoritz

Util.log('Adding axon meta information.');
axonMeta = table;
axonMeta.id = reshape(1:numel(axons), [], 1);
axonMeta.synCount = accumarray( ...
    connectome.edges(:, 1), ...
    cellfun(@numel, connectome.synIdx), ...
   [numel(axons), 1], @sum, 0);
axonMeta.spineSynCount = accumarray( ...
    connectome.edges(:, 1), cellfun(@(ids) ...
    sum(mSyn.isSpineSyn(ids)), connectome.synIdx), ...
   [numel(axons), 1], @sum, 0);


%% dendrite statistics
% added by amotta

Util.log('Adding dendrite meta information.');
denMeta = table;
denMeta.id = reshape(1:numel(dendrites), [], 1);
denMeta.targetClass = reshape(targetClass, [], 1);
denMeta.synCount = accumarray( ...
    connectome.edges(:, 2), ...
    cellfun(@numel, connectome.synIdx), ...
   [numel(dendrites), 1], @sum, 0);
denMeta.spineSynCount = accumarray( ...
    connectome.edges(:, 2), cellfun(@(ids) ...
    sum(mSyn.isSpineSyn(ids)), connectome.synIdx), ...
   [numel(dendrites), 1], @sum, 0);


%% save results

if ~exist(outFile, 'file')
    save(outFile, 'connectome', 'contactome', 'connectomeMeta', ...
        'contactomeMeta', 'axons', 'dendrites', 'info', ...
        'classConnectome', 'denClasses', 'axonMeta', 'denMeta', ...
        'axon_ver');
    save(outFileSmall, 'connectome', 'connectomeMeta', ...
        'classConnectome', 'denClasses', 'axonMeta', 'denMeta', ...
        'axon_ver', 'info');
end
