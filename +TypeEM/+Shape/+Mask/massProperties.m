function massProps = massProperties(voxelSize, mask)
    % MASSPROPERTIES The goal of this function is to compute
    % mass properties, such as the volume, centroid and the
    % inertia tensor. Unit density is assumed for all voxels
    % with value 'true';
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % determine size
    voxelVolume = prod(voxelSize);
    [sizeX, sizeY, sizeZ] = size(mask);
    
    % prepare output
    massVec   = zeros(sizeZ, 1);
    centMat   = zeros(sizeZ, 3);
    inTensMat = zeros(sizeZ, 3, 3);
    
    % span grid
    % dimensions must agree with mask
    %   first dimension is X
    %   second second dimension is Y
    [gridY, gridX] = meshgrid( ...
        (1:sizeY) * voxelSize(2), ...
        (1:sizeX) * voxelSize(1));
    
    % Go through all slices and compute all of the desired
    % output values locally!
    for curIdxZ = 1:sizeZ
        % correct position along Z
        curZ = curIdxZ * voxelSize(3);
        
        % get mask
        curMask = squeeze(mask(:, :, curIdxZ));
        
        % compute mass (by coun
        curVoxelCount = sum(curMask(:));
        curMass = curVoxelCount * voxelVolume;
        
        % HACK! Due to an error in SegEM it's possible for segments to be
        % disconnected. In the worst case, this manifests itself in an
        % entire Z slice of the mask being free of any "true" entries. This
        % situation is not handled correctly by the functions below (e.g.
        % mean on empty matrix producing NaN). So, we bail out and leave
        % the mass at zero. This should produce the correct result.
        if ~curVoxelCount; continue; end;
        
        % discard unfilled voxels
        curGridX = gridX(curMask);
        curGridY = gridY(curMask);
        
        % compute centroid
        curCent = [mean(curGridX), mean(curGridY), curZ];
        
        % correct grid for centroid
        curGridX = curGridX - curCent(1);
        curGridY = curGridY - curCent(2);
        curGridZ = sqrt(curGridX .^ 2 + curGridY .^ 2);
        
        % cosmetics just for clarity
        %   curGridX is distance from Y AXIS!
        %   curGridY is distance from X AXIS!
        curDistX = curGridY;
        curDistY = curGridX;
        curDistZ = curGridZ;
        
        % compute inertia tensor
        curInTensXX = sum(curDistX .^ 2);
        curInTensYY = sum(curDistY .^ 2);
        curInTensZZ = sum(curDistZ .^ 2);
        curInTensXY = sum(curDistX .* curDistY);
        curInTensXZ = sum(curDistX .* curDistZ);
        curInTensYZ = sum(curDistY .* curDistZ);
        
        curInTensMat = [ ...
            curInTensXX, curInTensXY, curInTensXZ;
            curInTensXY, curInTensYY, curInTensYZ;
            curInTensXZ, curInTensYZ, curInTensZZ];
        
        % write output
        massVec(curIdxZ) = curMass;
        centMat(curIdxZ, :) = curCent;
        inTensMat(curIdxZ, :, :) = curInTensMat;
    end
    
    % centroid
    mass = sum(massVec);
    centVec = sum(bsxfun(@times, centMat, massVec), 1) / mass;
    centVoxelVec = bsxfun(@times, centVec, 1 ./ voxelSize);
    
    % Now we use the Huygens-Steiner theorem (a.k.a. the
    % parallel axis theorem) to build the final result.
    %
    %   J = I + M((R • R) E_3 - R ⊗ R)
    %   
    %   I = slices along first dimension of inTensMat
    %   M = entries of massVec
    %   R = displacement vectors (calculated below)
    dispVec = bsxfun(@minus, centMat, centVec);
    
    % compute R ⊗ R
    outerProdMat = repmat(dispVec, [1, 1, 3]);
    outerProdMat = outerProdMat .* permute(outerProdMat, [1, 3, 2]);
    
    % compute R • R
    dotProdMat =  dot(dispVec, dispVec, 2);
    
    % compute (R • R) E_3
    dotProdMat =  bsxfun(@times, dotProdMat, reshape(eye(3), [1, 3, 3]));
    
    % compute (R • R) E_3 - R ⊗ R
    centOffMat = -bsxfun(@minus, outerProdMat, dotProdMat);
    
    % compute M((R • R) E_3 - R ⊗ R)
    inTens = bsxfun(@times, centOffMat, massVec);
    
    % compute I + M((R • R) E_3 - R ⊗ R) 
    inTens = bsxfun(@plus, inTensMat, inTens);
    
    % finally, sum up everything
    inTens = squeeze(sum(inTens, 1));
    
    % calculate features based on inertia matrix
    inFeats = TypeEM.Shape.buildInertiaFeatures(inTens);
    
    % write result
    massProps = struct;
    massProps.mass = mass;
    massProps.volume = mass;
    massProps.centerOfMass = centVoxelVec;
    massProps.inertiaMatrix = inTens;
    
    % add principal axes, etc.
    massProps = TypeEM.Util.mergeStructs(massProps, inFeats, 'inertia');
end