% heuristic to exclude shaft synapses:
% discard shaft synapses for which the corresponding bouton is also the only
% innervation onto a spine head
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo(false);
d_bouton = 1e3; % um
plotting = false;
suffix = '_SomaSynExcl';


%% load agglos

p = Gaba.getSegParameters('ex145_ROI2017');

% axons
Util.log('Loading axons.');
p.agglo.axonAggloFile = ['/gaba/u/mberning/results/pipeline/' ...
    '20170217_ROI/aggloState/axons_18_a.mat'];
axons = L4.Axons.getLargeAxons(p, true, true);

% dendrites
Util.log('Loading dendrites and target classes.');
[dendFile, targetClass, targetLabels] = L4.Connectome.buildTargetClasses();
m = load(dendFile);
dendrites = m.dendAgglos(targetClass ~= 'Ignore');
targetClass(targetClass == 'Ignore') = [];

% add somata to dendrites
mSom = load(p.agglo.somaFile);
somata = mSom.somaAgglos(:,1);
[dendrites, toDel] = L4.Specificity.addSomasToDendrites(dendrites, ...
    somata, 'separate');
targetClass(toDel) = [];
targetClass(end+1:end+length(somata)) = 'Somata';

% target classes
idxSoma = targetClass == 'Somata';
idxWC = targetClass == 'WholeCell';
idxAD = targetClass == 'ApicalDendrite';
idxSD = targetClass == 'SmoothDendrite';
idxAIS = targetClass == 'AxonInitialSegment';
idxOther = targetClass == 'OtherDendrite';

% synapses
Util.log('Loading synapses.');
% mSyn = load(p.connectome.synFile);
synFile = ['/gaba/u/mberning/results/pipeline/20170217_ROI/' ...
    'connectomeState/SynapseAgglos_v3_ax_spine_clustered.mat'];
mSyn = load(synFile);
synapses = mSyn.synapses;
[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
% not sure why there are uint32 in there
synapses.edgeIdx = cellfun(@double, synapses.edgeIdx, 'uni', 0);
synComs = L4.Synapses.synapseCom(synapses, borderMeta.borderCoM);

% spine heads
shFile = fullfile(p.agglo.saveFolder, 'spine_heads_and_attachment_03.mat');
m = load(shFile);
shAgglos = m.shAgglos;


%% synapse agglo mappings

[ syn2Agglo, pre2syn, post2syn, connectome ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, axons, shAgglos );


%% get spine and soma synapses

maxId = Seg.Global.getMaxSegId(p);
shLUT = Agglo.buildLUT(maxId, shAgglos);
isSpine = shLUT > 0;
isSoma = Agglo.buildLUT(maxId, somata) > 0;
isSpineSyn = cellfun(@(x)any(isSpine(x)), synapses.postsynId);
isSomaSyn = cellfun(@(x)any(isSoma(x)), synapses.postsynId);

% % consistency check
% isequal(mSyn.isSpineSyn, isSpineSyn)

% for debugging
axonLUT = Agglo.buildLUT(maxId, axons);


%% boutons

boutons = L4.Synapses.synapse2BoutonsAxonAgglo(synapses, [], d_bouton, ...
    borderMeta.borderCoM, p.raw.voxelSize, pre2syn);

% keep only boutons with shaft synapses & spine synapses
boutons = boutons(cellfun(@(x)any(~isSpineSyn(x)) & any(isSpineSyn(x)), ...
    boutons));


%% get synapses at same bouton

% add soma and spine synapses to separate arrays
boutonsShaftSyns = cellfun(@(x)x(~isSpineSyn(x)), boutons, 'uni', 0);
boutonsSpineSyns = cellfun(@(x)x(isSpineSyn(x)), boutons, 'uni', 0);

isSingleSpineSyn = false(length(boutonsSpineSyns), 1);
for i = 1:length(boutonsSpineSyns)
    curSingleSpineSyns = false(length(boutonsSpineSyns{i}), 1);
    for j = 1:length(boutonsSpineSyns{i})
        thisShAggloIdx = shLUT(synapses.postsynId{boutonsSpineSyns{i}(j)});
        thisShAggloIdx = setdiff(thisShAggloIdx, 0);
        curSingleSpineSyns(j) = all(arrayfun(@(x)length(post2syn{x}) == 1, ...
            thisShAggloIdx));
    end
    isSingleSpineSyn(i) = any(curSingleSpineSyns);
end
idx = find(isSingleSpineSyn);
numToDel = length(cell2mat(boutonsShaftSyns(idx)));


%% examples to wk

if plotting
    N = 50;
    skel = L4.Util.getSkel();
    sampleIdx = idx(randperm(length(idx), N));
    for i = 1:N
        skel = skel.addTree(sprintf('Bouton_%d_ShaftSyns', ...
            sampleIdx(i)), synComs(boutonsShaftSyns{sampleIdx(i)}, :));
        skel = skel.addTree(sprintf('Bouton_%d_SpineSyns', ...
            sampleIdx(i)), synComs(boutonsSpineSyns{sampleIdx(i)}, :));
    end
    skel = skel.setDescription( ['Samples of boutons with shaft ' ...
        'synapses that also have a spine innervation which is the ' ...
        'only innervation of that spine. The ' ...
        'heuristic would exclude all shaft synapses in this tracing.']);
    skel.write('ShaftSynapseFPExclusionHeuristic.nml');
end
