function out = minicubeFwdPass()

	%% Make a big fwdPass through the network
	out.dateStrings = '20130219T234516';
	out.iter = 18; 
	out.net = 2;
	out.pathRaw.root = '/zdata/manuel/data/cortex/2012-09-28_ex145_07x2/mag1/';
	out.pathRaw.prefix = '2012-09-28_ex145_07x2_mag1';
	out.pathResult.prefix = '2012-09-28_ex145_07x2_mag1'; 
	out.pathRaw.bbox(:,1) = [4001; 4501; 2001];
	out.pathRaw.bbox(:,2) = [5000; 5500; 3000];
	%out.pathRaw.bbox(:,1) = [2001; 2001; 2001];
	%out.pathRaw.bbox(:,2) = [2300; 2300; 2300];

	%% Start evaluation of minicube on CPU
	out.pathResult.root = ['/zdata/manuel/results/CNNfwdPass/2012-09-28_ex145_07x2/' out.dateStrings '/iter' num2str(out.iter, '%.2i') '/net' num2str(out.net, '%.2i') '/'];
	load(['/zdata/manuel/results/parameterSearch/' out.dateStrings '/iter' num2str(out.iter, '%.2i') '/gpu' num2str(out.net, '%.2i') '/' 'saveNet0000000001.mat'], 'cnet');
	out.cnet = cnet.loadLastCNN;
	out.cnet.run.actvtClass = @single;
	startCPU(@fwdPass3DonKhierachy, {out.cnet, out.pathRaw, out.pathResult});

end
