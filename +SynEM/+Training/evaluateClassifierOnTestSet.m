function result = evaluateClassifierOnTestSet( p, classifier, fm, ...
    testSetPath, ver )
%EVALUATECLASSIFIERONTESTSET
% INPUT p: struct or [NxM] int
%           Segmentation parameter struct used to load the information for
%           cube 67 (test cube). Or the feature matrix for the Cube67.
%       classifier: SynEM.Classifier or matlab classifier
%           Classifier for prediction.
%           (see also SynEM.Seg.predictCube)
%       fm: SynEM.FeatureMap
%           Feature map object.
%       testSetPath: string
%           Path to SynEM test set.
%           (see also SynEM.Test.loadInterfaceGT)
%       ver: (Optional) string
%           Tets set version
%           (see also SynEM.Test.loadInterfaceGT)
%           (Default: [])
% OUTPUT result: struct
%           Struct with prediction results. If p was a struct then the
%           features of the test set 'X_test' are also part of result.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('ver', 'var')
    ver = [];
end

if isstruct(p)
    [scoresTest, X_test] = SynEM.Seg.predictCube(p, 67, fm, classifier);
else
    X_test = p;
    [~, scoresTest] = classifier.predict(X_test);
end
scoresTest = reshape(scoresTest, [], 2);
testSet = SynEM.TestSet.loadInterfaceGT( testSetPath, true, false, ver);
group = testSet.group;
toKeep = testSet.toKeep;
[rpTest, thres_test, aucTest] = SynEM.Eval.interfaceRP(group, ...
    max(scoresTest(toKeep,:), [], 2));
result.scoresTest = scoresTest;
if isstruct(p)
    result.X_test = X_test;
end
result.rpTest = rpTest;
result.thres_test = thres_test;
result.aucTest = aucTest;
end

