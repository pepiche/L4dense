function lens = calcPathLengths(param, agglos, verbose)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if ~exist('verbose', 'var') || isempty(verbose)
        verbose = false;
    end
    
    vxSize = param.raw.voxelSize;
    
    points = double(Seg.Global.getSegToPointMap(param));
    points = bsxfun(@times, points, vxSize);
    
    maxDist = 2 .* param.tileSize;
    maxDist = sqrt(sum((maxDist(:) .* vxSize(:)) .^ 2));
    
    lenForAgglo = @(segIds) fromMST(points, segIds, maxDist);
    tic; lens = cellfun(lenForAgglo, agglos); toc;
    
    if ~verbose; return; end;
    fprintf('Sum: %.2f µm\n', sum(lens / 1E3));
    fprintf('Mean: %.2f µm\n', mean(lens / 1E3));
    fprintf('Median: %.2f µm\n', median(lens / 1E3));
    fprintf('Maximum: %.2f µm\n', max(lens / 1E3));
end

function len = fromMST(points, segIds, maxDist)
    len = 0;
    
    % agglomerate too small?
    if numel(segIds) < 2; return; end;
    
    % Based on Benedikt's @skeleton/fromMST.m
    distMat = sparse(squareform(pdist(points(segIds, :))));
    
    % NOTE(amotta): Manuel correctly suggested that many entries of the
    % distance matrix can be set to zero (i.e., removing the possibility of
    % an edge) by realizing that there exists an upper limit for the
    % distance.
    distMat(distMat > maxDist) = 0;
    
    adjMat = graphminspantree(distMat);
    len = full(sum(adjMat(:)));
end
