function data = readRoi(obj, bbox, verbose)
% READROI Wrapper for readKnossosRoi.
% INPUT bbox: [3x2] of [1x6] or [6x1] int
%           Bounding box in the form
%           [x_min x_max; y_min y_max; z_min z_max]
%           If bbox is a vector that it is assumed to be the linearized
%           version of a bbox.
%       verbose: (Optional) logical
%           Flag to see warnings from readKnossosRoi
%           (Default: false)
% see also readKnossosRoi
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('verbose', 'var') || isempty(verbose)
    verbose = false;
end

if isvector(bbox)
    bbox = reshape(bbox, 3, 2);
end

if ~verbose
    warning('off', 'all');
end
switch obj.backend
    case 'wkcube'
        data = readKnossosRoi(obj.root, obj.p.prefix, bbox, obj.p.dtype,...
            obj.p.suffix, obj.p.ending, obj.p.cubesize);
    case 'wkwrap'
        data = wkwLoadRoi(obj.root, double(bbox));
        if obj.p.xyzcOrder && obj.p.numChannels > 1
            data = permute(data, [2 3 4 1]);
        end
end
warning('on', 'all');
end
