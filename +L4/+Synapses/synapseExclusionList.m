function idx = synapseExclusionList( synScores, synT, prob, probT, ...
    edges, myelinScore, myelinT )
%SYNAPSEEXCLUSIONLIST Get all borders that should be excluded as synapse
% detections.
% INPUT synScores: [Nx2] double
%           Synapse predictions for both directions.
%       synT: double
%           Synapse score threshold. If both directions of an interface
%           have a score above this threshold the interface is discarded.
%           (Default: -2)
%       prob: [Nx1] double
%           Neurite contuinity probabilities.
%       probT: double
%           Threshold on prob above which an interface is not assumed to be
%           synaptic.
%           (Default: 0.7)
%       edges: (Optional) [Nx2] int
%           Global edges list.
%           (Only needed if myeline score is used)
%       myelineScores: [Nx1] float
%           Myelin scores for segments.
%           (Default: no exclusion based on myelin).
%       myelinT: float
%           Threshold above which a segment is considered to be at a myelin
%           sheath. Synapses containing segments with myelin are discarded.
%           (Default: 0.375 if myelinScore is provided).
% OUTPUT idx: [Nx1] logical
%           Logical indices for all interfaces that should be discarded.
%
% see also L4.Synapses.getSynapsePredictions
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('synT', 'var') || isempty(synT)
    synT = -2;
end
idx = all(synScores > synT, 2);

if exist('prob', 'var') && ~isempty(prob)
    if ~exist('probT', 'var') || isempty(probT)
        probT = 0.70;
    end
    idx = idx | prob > probT;
end

if exist('edges', 'var') && exist('myelinScore', 'var') && ...
        ~isempty(edges) && ~isempty(myelinScore)
    if ~exist('myelinT', 'var') || isempty(myelinT)
        myelinT = 0.375;
    end
    idx = idx | any(myelinScore(edges) > myelinT, 2);
end

end

