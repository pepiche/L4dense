# SynEM

Code for synapse detection via interface classification.

## Content

* Aux: Auxiliary functions
* Classifier: Wrapper classes for classifiers
* ErrorEstimates: Neuron-to-neuron error estimation framework
* Eval: Evaluation routines for SynEM classifiers
* Feature: Wrapper classes for features
* FeatureMap: Feature map class for interface feature calculation
* Seg: Application of SynEM to a SegEM segmentation
* Svg: Supervoxel graph construction and interface calculation
* Training: Feature calculation for training set and classifier training
* Util: Utility functions