% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
import TypeEM.GroundTruth.loadSet;
import TypeEM.GroundTruth.buildLabelVolume;

clear;
config = loadConfig('ex145_07x2_roi2017');

%% configuration
nmlDir = fullfile( ...
    config.repoDir, 'data', 'tracings', ...
    'ex145-07x2-roi2017', 'dense-v3');

% path to NML files with ground truth data
nmlFiles = {'region-1.nml'; 'region-2.nml'; 'region-3.nml'};
nmlFiles = fullfile(nmlDir, nmlFiles);

rawMargin = [100, 100, 100];

% path to output directory
outDir = '/gaba/u/amotta/data/tensorflow/type-em/ground-truth';
outDir = fullfile(outDir, datestr(now(), 30));

rawRootDir = fullfile(outDir, 'raw');
labelRootDir = fullfile(outDir, 'label');

info = Util.runInfo();

%% sanity checks
assert(all(cellfun(@(f) exist(f, 'file'), nmlFiles)));
assert(~exist(outDir, 'dir'));

%% build and save volumes
% initialize WKW datasets
wkwInit('new', rawRootDir, 32, 32, 'uint8', 1);
wkwInit('new', labelRootDir, 32, 32, 'uint8', 1);

% collect bounding boxes
gts = cell(size(nmlFiles));
boxes = cell(size(nmlFiles));

for curIdx = 1:numel(nmlFiles)
    % load segment labels
    curGt = loadSet(config.param, 'segmentAgglo', nmlFiles{curIdx});
    gts{curIdx} = curGt;
   
    % build voxel labels
   [curVol, curBox] = buildLabelVolume(config.param, curGt);
    boxes{curIdx} = curBox;
    
    % save results
    wkwSaveRoi(labelRootDir, curBox(:, 1), curVol);
    
    % copy raw data
    curBox(:, 1) = curBox(:, 1) - rawMargin(:);
    curBox(:, 2) = curBox(:, 2) + rawMargin(:);
    
    curVol = loadRawData(config.param.raw, curBox);
    wkwSaveRoi(rawRootDir, curBox(:, 1), curVol);
end

%% save run info
out = struct;
out.gts = gts;
out.boxes = boxes;
out.info = info;

outFile = fullfile(outDir, 'meta.mat');
Util.saveStruct(outFile, out);