function idx = lin2logIdx( idx, l )
%LIN2LOGIDX Linear to logical indices.
% INPUT idx: [Nx1] int
%           List of linear indices.
%       l: (int)
%           Length of the output index array.
% OUTPUT idx: [Nx1] logical
%           Logical array of length l where all indices of the input idx
%           are set to true.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

tmp = false(l, 1);
tmp(idx) = true;
idx = tmp;

end

