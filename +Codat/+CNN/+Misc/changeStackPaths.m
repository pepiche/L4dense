function stacks = changeStackPaths( stacks, newPath )
%CHANGESTACKPATHS Change the paths to the stack files.
% INPUT stacks: [Nx1] struct
%           Struct array with information about training stacks. The
%           'stackFile' path is changed to newPath. If 'targetFile' exists
%           it is also changed to the new path. (Not quite sure what is
%           what).
%       newPath: string
%           Path the folder containing the mat-files.
% OUTPUT stacks: [Nx1] struct
%           Stack information with updated paths.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

for i = 1:length(stacks)
    fname = regexp(stacks(i).stackFile, '\w*.mat$', 'match');
    stacks(i).stackFile = fullfile(newPath, fname{1});
    if isfield(stacks, 'targetFile')
        stacks(i).targetFile = fullfile(newPath, fname{1});
    end
end

end

