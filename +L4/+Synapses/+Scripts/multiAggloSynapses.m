% script to look into synapses that are associated with multiple pre- or
% postsynapticagglos
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

p = Gaba.getSegParameters('ex145_ROI2017');

axFile = fullfile(p.agglo.saveFolder, 'axons_18_a.mat');
denFile = fullfile(p.agglo.saveFolder, 'dendrites_wholeCells_01_spine_attachment.mat');
synFile = fullfile(p.connectome.saveFolder, 'SynapseAgglos_v3.mat');

% axons
mAx = load(axFile);
axons = mAx.axons(mAx.indBigAxons);
idx = Superagglos.getPureFlightPathAgglos(axons);
axons(idx) = [];

% dendrites
mDen = load(denFile);
dendrites = mDen.dendAgglos(mDen.indBigDends);

% synapses
mSyn = load(synFile);
synapses = mSyn.synapses;


%% calculate synapse agglo mappings

[ syn2Agglo, pre2syn, post2syn, connectome ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, axons, dendrites );


%% get synapses with multiple pre- or postsynaptic partners

hasMultiplePre = find(cellfun(@length, syn2Agglo.syn2pre) > 1);
hasMultiplePost = find(cellfun(@length, syn2Agglo.syn2post) > 1);


%% look at examples in wk

[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
borderCom = borderMeta.borderCoM;
skel = L4.Util.getSkel();
comsPre = L4.Synapses.synapseCom(synapses(hasMultiplePre,:), borderCom);
comsPost = L4.Synapses.synapseCom(synapses(hasMultiplePost,:), borderCom);
skel = skel.addNodesAsTrees(comsPre, 'MultiplePre');
count = length(hasMultiplePre);
skel = skel.addNodesAsTrees(comsPost, 'MultiplePost');
skel.colors(count + 1:end) = {[0 1 0 1]};
skel.write('MultiAggloSynapses.nml');