% extractApicals 
% script to filter a state of dendrite agglos for apicals (designed for L4)
% -------------------------------------------------------------------------
% Author: Matej Zecevic <matej.zecevic@brain.mpg.de>
% Modified by: Sahil Loomba <sahil.loomba@brain.mpg.de>

setConfig;
outDir = config.outDir;
local = '';
plotFlag = false;

m = load(config.param);
p = m.p;
bounds = p.bbox;
%points = Seg.Global.getSegToPointMap(p);

%Util.log('Loading dendrites...');
%m=load(config.dendriteFile);
%indBigDends = m.indBigDends;
%dendrites = Superagglos.getSegIds(m.dendrites);

% spine attached dendrites
Util.log('Loading dendrites')
m=load(config.spineFile);
dendAgglos = cellfun(@(x) double(x),m.dendAgglos,'uni',0);
shAgglos = cellfun(@(x) double(x),m.shAgglos,'uni',0);
attached = m.attached;
indBigDends = m.indBigDends;

Util.log('Calculating path lengths');
inputfile = fullfile(outDir,'dendriteLengths.mat');
if exist(inputfile,'file')
    m = load(inputfile);
    dendriteLengths = m.dendriteLengths;
else
    dendriteLengths = Util.calcPathLengths(p,dendAgglos)./1E3;
    save(inputfile,'dendriteLengths');
end

agglos = dendAgglos(indBigDends);
dendLens = dendriteLengths(indBigDends);
% collect points from segments
m = load(config.meta);
points = cellfun(@(x) m.point(:,x)',agglos,'uni',0);

Util.log('Filtering apicals...');
%filter using site criterion
tol = 200;
[ filteredAgglos1, filteredPoints1, rInd_1 ] = Apicals.filterSiteCriterion( agglos, points, bounds, tol );

% filter using path length
ulbound = [200000 50000];
[ filteredAgglos2, filteredPoints2, pathLengths, rInd_2 ] = ...
        Apicals.filterPathLength( filteredAgglos1, filteredPoints1, ulbound, rInd_1 );

% plot: histogram for pathLength distribution
if plotFlag
    h = histogram(pathLengths);
    title([ num2str(length(pathLengths)) ' agglos out of ' num2str(length(agglos)) ' - Bin width: ' num2str(h.BinWidth)]);
    xlabel('physical path length'); ylabel('count');
    saveas(gcf,fullfile(outDir,'figures/apicalsPathLengthHist.png'));
    close all
end

% (two steps) filter using PCA (variance explained) and main axis alignment
varThr = 0.98;
spThr = 0.95;
[ filteredAgglos3, filteredPoints3, meanFPC, sps, rInd_3 ] = ...
        Apicals.filterMainAxis( filteredAgglos2, filteredPoints2, varThr, spThr, rInd_2 );

% plot: histogram for distribution of distances (meanFPC from observed)
if plotFlag
    h = histogram(sps);
    title([ num2str(length(sps)) ' agglos out of ' num2str(length(agglos)) ' - Bin width: ' num2str(h.BinWidth)]);
    xlabel('distance from mean FPC'); ylabel('count');
    saveas(gcf,fullfile(outDir,'figures/apicalsDistFPCHist.png'));
    close all
end

Util.log('Saving results...');
idxAD = rInd_3;
agglosAD = filteredAgglos3;
save(fullfile(outDir,'dendritesADState.mat'),'agglosAD','idxAD','rInd_1','rInd_2','rInd_3','filteredAgglos1','filteredAgglos2','filteredAgglos3');

%{
% collect spine head counts for all agglos (!) because of attachment
% seems like the agglos are sorted the way that > 5um come first
% would explain why below worked with this as before
[ shCounts ] = Apicals.collectSHcounts( agglos_all, spineHeads );

% collect spine head densities for the filtered Agglos
shDensity = shCounts(idxAD) ./ dendLens(idxAD);
% replace NaN with zero spine density
shDensity(isnan(shDensity)) = 0; % zero spines

% plot: histogram for spine head densities
if plotFlag
    histogram(shDensity, 10);
    xlabel('Spine density: # spines per um'); ylabel('# dendrite agglos');
    title(['Spine Head density for ' num2str(length(idxAD)) ' agglos filtered']);
end
%}
