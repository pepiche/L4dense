% This function transforms the 'dendrite' class into 'dendriticshaft' class
% by marking spine head segments as negative training examples. This is
% useful to assess the performance on trunks alone.
%
% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
rootDir = '/gaba/u/amotta/data/blog/2017-02-23-ClassEM-Spine-Heads/single-segments';

train = fixLabels(fullfile(rootDir, 'train.mat'));
[classes, classifiers] = TypeEM.buildClassifiers(train);

eval = fixLabels(fullfile(rootDir, 'eval.mat'));
TypeEM.evalClassifiers(eval, classes, classifiers);

function out = fixLabels(path)
    out = load(path);
    out.class = out.class(1, :);
    
    % make spine heads negative in 'dendrite' class
    spineHeadMask = out.label(:, out.class == 'spinehead') > 0;
    out.label(spineHeadMask, out.class == 'dendrite') = -1;
    
    % rename 'dendrite' to 'dendriticshaft'
    out.class(out.class == 'dendrite') = 'dendriticshaft';
end
