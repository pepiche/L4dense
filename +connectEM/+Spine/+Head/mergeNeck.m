function [shAgglos, shEdges] = mergeNeck(shAgglos, shEdges, neckEdges)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    assert(numel(shAgglos) == numel(shEdges));
    assert(numel(shAgglos) == numel(neckEdges));
    
    for curIdx = 1:numel(shAgglos)
        % remove unused edges
        curNeckEdges = neckEdges{curIdx};
        curNeckEdges(~all(curNeckEdges, 2), :) = [];
        curNeckEdges = sort(curNeckEdges, 2);
        
        % merge
        shAgglos{curIdx} = unique(cat( ...
            1, shAgglos{curIdx}, curNeckEdges(:)));
        shEdges{curIdx} = unique(cat( ...
            1, shEdges{curIdx}, curNeckEdges), 'rows');
    end
end