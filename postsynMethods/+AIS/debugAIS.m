
folder = '/tmpscratch/sahilloo/L4_borderCells_GT/';
files = dir(fullfile(folder,'*.nml'))

skel = cellfun(@(x) skeleton(fullfile(folder,x)),{files.name});

skelAIS = {}; j =1;
for i = 1:numel(skel)
    thisSkel = skel(i);
    c = getAllComments(thisSkel);
    for k=1:thisSkel.numTrees
        treeInd = false(thisSkel.numTrees,1);
        treeInd(k) = true;
        axNode = thisSkel.getNodesWithComment('axon',treeInd);
        if ~isempty(axNode)
            skelAIS{j} = thisSkel.deleteTrees(~treeInd);
            j = j+1; 
         continue;
        end
    end
end

for i =1:numel(skelAIS)

    skel = skelAIS{i};
    skel.write(fullfile('/tmpscratch/sahilloo/L4_borderCells_AIS_GT',['ais_' num2str(i) '.nml']));
 
end

% for rewriting 58 soma ais


for i =1:numel(skelAll)

    skel = skelAll(i);
    skel.write(fullfile('/tmpscratch/sahilloo/L4_soma_AIS/',['ais_' num2str(i) '.nml']));

end

