function weightarray=makeWA()
weightarray=cell(3,1);
x=Inf;
o=-Inf;



weightarray{1}=[
       x       x       x       x       x       x       x       x; %1
      11      11      10      10      10      12       x       x;%2
      11      10      10       4       4      12       x       x;%3
      11       8       o       o       o       o       x       x;%4
      11       4       o       o       o       o      -8       x;%5
      11       o       o       o       o       o      -9       x;%6
      11       o       o       o       o       o      -9       x;%7
      11       o       o       o       o      -12    -11       x;%8
      11      -2       o       o       o      -10    -10       x;%9
      11      -4       o       o       o      -9       x       x;
      11       7       o       o      -8      -4       x       x;
      14      10       7      12       x       x       x       x;
       x       x       x       x       x       x       x       x];


weightarray{2}=[
    x       x       x       x       x       x       x       x       x;%1
   11      11      10      10       8      11      12       x       x;%2
   11      10       8       o       o       o      12       x       x;%3
   11       9       3       o       o       o      -9       x       x;%4
   11       7       o       o       o       o      -9       x       x;%5
   11       8       o       o       o       o     -10      -8       x;%6
   11       5       o       o       o       o     -10      -8       x;%7
   11       6       o       o       o       o     -10      -7       x;%8
   11       7       2       o       o     -10      -8       x       x;%9
   15       8       2       o       o       o       x       x       x;
    x      10       7       o       o      -9       x       x       x;
    x       x       x       x       x       x       x       x       x];

weightarray{3}=[
    x       x       x       x       x       x       x       x       x;%1
   12      10      10      10      10      11      12       x       x;%2
   11      10      10       9       4      10       x       x       x;%3
   11       9       9       o       o      13       x       x       x;%4
   11       9       o       o       o       o      -9       x       x;%5
   11       6       o       o       o       o     -10      -8       x;%6
   11       6       o       o       o       o     -10      -9       x;%7
   11       5       o       o       o       o     -10      -8       x;%8
   11       6       o       o       o     -11      -9       x       x;%9
   14       8       2       o       o      -9      -4       x       x;
    x       8       6       o      -9       x       x       x       x;
    x      14      10       8       x       x       x       x       x;
    x       x       x       x       x       x       x       x       x];
for i=1:3
weightarray{i}=-250*sign(weightarray{i})+500*weightarray{i};
end
