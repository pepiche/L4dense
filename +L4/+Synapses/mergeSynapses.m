function [synapses, toDel] = mergeSynapses( synapses, toMergeIdx )
%MERGESYNAPSES Merge synapses.
% INPUT synapses: table
%           See output of L4.Synapses.clusterSynapticInterfaces.
%       toMergeIdx: [Nx1] cell
%           Each cell contains a list of linear indices of synapses that
%           should be merged.
% OUTPUT synapses: table
%           The updated synpase table.
%        toDel: [Nx1] int
%           Linear indices of the synapses that were deleted.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

toMergeIdx = cellfun(@sort, toMergeIdx, 'uni', 0);
synC = cellfun(@uint32, synapses{:,:}, 'uni', 0); % not sure why needed
tmp = cellfun(@(x)cell2mat(synC(x,:)), toMergeIdx, 'uni', 0);
tmp = cellfun(@(x)num2cell(x, 1), tmp, 'uni', 0);
tmp = vertcat(tmp{:});
idx1 = cellfun(@(x)x(1), toMergeIdx);
toDel = cell2mat(cellfun(@(x)x(2:end), toMergeIdx, 'uni', 0));
synapses{idx1,:} = tmp;
synapses(toDel,:) = [];

end

