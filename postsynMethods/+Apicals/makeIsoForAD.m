
setConfig;
%outDir = [config.outDir 'filter_9_4/'];
outDir = '/gaba/u/mberning/results/pipeline/20170217_ROI/postSynIso/apicalDendrites_filter9-8/';
if ~exist(outDir,'dir')
    mkdir(outDir);
end

load(config.param);
Util.log('Loading stuff..')
m=load(config.dendriteFile);
indBigDends = m.indBigDends;
dendrites = Superagglos.getSegIds(m.dendrites(indBigDends));

load(fullfile(config.outDir,'dendritesADState.mat'));

agglos = dendrites(idxAD);
assert(isequal(agglos,agglosAD));

Util.log('Now running main viz')
%p.seg.root='/tmpscratch/amotta/l4/2012-09-28_ex145_07x2_ROI2017/segmentation/1/';
p.seg.root = ['/gabaghi/wKcubes_archive/2012-09-28_ex145_07x2_ROI2017/segmentation/1/'];
p.seg.backend = 'wkwrap';
%Visualization.exportAggloToAmira(p,agglos,outDir);
Visualization.exportAggloToAmira(p,agglos,outDir,'reduce',0.5,'smoothSizeHalf',4,'smoothWidth',8);
info=Util.runInfo(false);
save(fullfile(outDir,'info.mat'),'info','dendrites','idxAD');
