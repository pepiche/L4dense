function [post,edgeIdx,postIdxPseudo] = findShaftSeg()

% find which synapses are shaft
outDir = '/tmpscratch/sahilloo/L4/data/';

m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
m = load([p.saveFolder 'globalEdges.mat']);
edges = m.edges;

m = load([p.saveFolder 'globalSynScores.mat']);
synScores = nan(size(edges));
synScores(m.edgeIdx, :) = m.synScores;

rng(0);
points = Seg.Global.getSegToPointMap(p);
voxelSize = p.raw.voxelSize;

% synapses
synThr = -2.06;
idx = synScores > synThr;
% ignore where both edges predicted pre
%preIdx = idx; preIdx(and(idx(:,1),idx(:,2)),:) = 0;
postIdx = ~idx; postIdx(~xor(idx(:,1),idx(:,2)),:)=0;
postIdxPseudo = find(postIdx);
edgeIdx = postIdx(:,1) | postIdx(:,2);
%pre =  struct('segId',[],'probs',[],'class',[]);
post =  struct('segId',[],'probsMulti',[],'probs',[],'class',[]);  
%pre.segId = edges(preIdx);
post.segId = edges(postIdx);

fileID = fopen(fullfile(outDir,'shaftSynapseNumbers.txt'),'w');
count = length(post.segId); countUnique = length(unique(post.segId));
fprintf(fileID,'After synEM:- \n unique segments %d - actual synapses %d\n',countUnique,count);

% load segment scores
m = load([p.saveFolder 'segmentPredictions.mat']);
n = load([p.saveFolder 'segmentAggloPredictions.mat']);

% remove post segments not present in segmentPredictions or segmentAggloPredictions
postIdxPseudo = postIdxPseudo(ismember(post.segId,m.segId) & ismember(post.segId,n.segId));
[r,c] = ind2sub(size(edges),postIdxPseudo);
edgeIdx = ismember([1:size(edges,1)],r);
post.segId( ~(ismember(post.segId,m.segId) & ismember(post.segId,n.segId)))=[];

count=length(post.segId); countUnique=length(unique(post.segId));
fprintf(fileID,'After removing segments not found in typeEM or segmentAggloPredictions:- \n unique segments %d - actual synapses %d \n',countUnique,count); 

% assign probs values to post
segIds = unique(post.segId);
[~,~,ia]=intersect(post.segId,m.segId);
probs = m.probs(ia,:);
[~,~,ib]=intersect(post.segId,n.segId);
probsMulti = n.probsMulti(ib,:);

% remove segIds that are spine heads
shThr = 0.5;
shIdx = probs(:,4)>shThr;

segIds = segIds(~shIdx);
probs = probs(~shIdx,:);
probsMulti = probsMulti(~shIdx,:);

% remove post that are spine heads
postIdxPseudo = postIdxPseudo(ismember(post.segId,segIds));
[r,c] = ind2sub(size(edges),postIdxPseudo);
edgeIdx = ismember([1:size(edges,1)],r);
post.segId(~ismember(post.segId,segIds))=[];
%clear shIdx

count = length(post.segId);  countUnique=length(unique(post.segId));
fprintf(fileID,'After removing >0.50 spinehead prob:-\n unique segments %d - actual synapses %d \n',countUnique,count); 

% remove segIds that are not dendrites
denThr = 0.3;
denIdx = probsMulti(:,3)>denThr;

% write out segIds that are not dendrites
outSeg = segIds(~denIdx);
idxToPlot = randperm(length(outSeg),50);
nodes = points(outSeg(idxToPlot),:);
skel =  skeleton();
skel = Skeleton.setParams4Pipeline(skel, p);
skel = skel.addNodesAsTrees(nodes);
skel.write(fullfile(outDir, ['nmls/segmentsNotDendrites.nml']));

segIds = segIds(denIdx);
probs = probs(denIdx,:);
probsMulti = probsMulti(denIdx,:);

% remove post that are spine heads
postIdxPseudo = postIdxPseudo(ismember(post.segId,segIds));
[r,c] = ind2sub(size(edges),postIdxPseudo);
edgeIdx = ismember([1:size(edges,1)],r);
post.segId(~ismember(post.segId,segIds))=[];

count = length(post.segId); countUnique=length(unique(post.segId));
fprintf(fileID,'After removing <0.30 typeEM dendrite prob:-\n unique segments %d - actual synapses %d\n',countUnique,count); 

% load dendrite agglos
k = load('/tmpscratch/sahilloo/L4/dendriteQueryResults2/dendritesPostQueryState.mat'); % no mask of 5um distance, only 200k vx size
dendritesPostQuery = k.dendritesPostQuery;


% remove post that are not in dendritePostQueries
postIdxPseudo = postIdxPseudo(ismember(post.segId,vertcat(dendritesPostQuery{:})));
[r,c] = ind2sub(size(edges),postIdxPseudo);
edgeIdx = ismember([1:size(edges,1)],r);
post.segId(~ismember(post.segId,vertcat(dendritesPostQuery{:})))=[];

idxKeep = ismember(segIds,vertcat(dendritesPostQuery{:}));
outSeg = segIds(~idxKeep);
idxToPlot = randi(length(outSeg),1,50);
nodes = points(outSeg(idxToPlot),:);
skel = skeleton();
skel = Skeleton.setParams4Pipeline(skel, p);
skel = skel.addNodesAsTrees(nodes);
skel.write(fullfile(outDir, ['nmls/segmentsNotInDendritesPostQuery.nml']));

segIds = segIds(idxKeep);
probs = probs(idxKeep,:);
probsMulti = probsMulti(idxKeep,:);

% post-syn found in dendritePostQueries
count = length(post.segId); countUnique =length(unique(post.segId));
fprintf(fileID,'After removing segments not contained in dendritePostQueries:-\n unique segments %d - actual synapses %d \n',countUnique,count);
fclose(fileID)

end






