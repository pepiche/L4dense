function [vol, box] = buildLabelVolume(param, gt)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % In order to build a label volume, we need the classes in `gt` to be
    % mutually exlusive. That is, there is at most one positive entry per
    % row of `gt.label`.
    assert(all(sum(gt.label > 0, 2) <= 1));
    
    % build LUT
    findClassId = @(l) max(cat(2, 0, find(l > 0)));
    gt.classId = cellfun(findClassId, num2cell(gt.label, 2));
    
    maxSegId = Seg.Global.getMaxSegId(param);
    lut = zeros(maxSegId + 1, 1, 'uint8');
    lut(gt.segId) = gt.classId;
    
    % build bounding box
    box = Seg.Global.getSegToBoxMap(param);
    box = box(:, :, gt.segId);
    
    box = horzcat( ...
        min(box(:, 1, :), [], 3), ....
        max(box(:, 2, :), [], 3));
    
    % load segmentation
    vol = loadSegDataGlobal(param.seg, box);
    vol(~vol) = maxSegId + 1;
    vol = lut(vol);
end