function [ gradient, loss ] = calculateGradient( cnet, input, target, weights )
%CALCULATEGRADIENT Calculate the gradient of the model.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

cnet.isTraining = true;
[ activity, dropoutMask, mpInd, bn ] = forwardPass( cnet, input );

%set moving bn inference parameters to exp moving averages from training
cnet.bn_muInf = bn(:,4);
cnet.bn_sig2Inf = bn(:,5);

[ gradient, loss ] = backprop( cnet, activity, dropoutMask, mpInd, bn, ...
    target, weights );

end

