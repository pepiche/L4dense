classdef LoadWkData < SynEM.Feature.TextureFeature
    %LOADWKDATA Load precompute features from a WkDataset.
    % Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

    properties
        wkDat
        bbox = [];                      % bounding box that is returned
                                        % by the feature
        f_norm = [];                    % optional normalization of the
                                        % loaded data (is always stored as
                                        % string and only converted for
                                        % calculation)qstat
        channels = [];                  % linear indices of the channels to
                                        % load which is done before
                                        % applying f_norm
    end

    methods
        function obj = LoadWkData(wkDataset, bbox, f_norm, channels)
            obj.name = 'LoadWkDataset';
            obj.wkDat = wkDataset;
            
            if exist('bbox', 'var') && ~isempty(bbox)
                obj.bbox = bbox;
            end
            
            if exist('channels', 'var') && ~isempty(channels)
                obj.channels = channels;
                obj.numChannels = length(channels);
                assert(max(channels) <= wkDataset.numChannels);
            else
                obj.numChannels = wkDataset.numChannels;
            end

            if exist('f_norm', 'var') && ~isempty(f_norm)
                if ischar(f_norm)
                    obj.f_norm = f_norm;
                else
                    obj.f_norm = func2str(f_norm);
                end
            end
            obj.border = [0, 0, 0];
        end

        function fm = calc(obj, ~)
            if ~isempty(obj.bbox)
                if isa(obj.wkDat, 'Datasets.WkDataset')
                    fm = obj.wkDat.readRoi(obj.bbox);
                    if ~isempty(obj.channels)
                        fm = fm(:,:,:,obj.channels);
                    end
                elseif isa(obj.wkDat, 'Datasets.McWkwDataset')
                    if ~isempty(obj.channels)
                        fm = obj.wkDat.readRoi(obj.bbox, obj.channels);
                    else
                        fm = obj.wkDat.readRoi(obj.bbox);
                    end
                else
                    error('Unknown wkDataset type %s.', class(obj.wkDat));
                end
                
                if ~isempty(obj.f_norm)
                    fh_norm = str2func(obj.f_norm);
                    fm = fh_norm(fm);
                end
                
%                 fm = squeeze(num2cell(fm, 1:3)); % cell output
                
                % hack to avoid duplication of memory (avoids out of
                % memory error in some cases)
                tmp = cell(size(fm, 4), 1);
                for i = 1:size(fm, 4)
                    tmp{i} = fm(:,:,:,1);
                    fm = fm(:,:,:,2:end);
                end
                fm = tmp;
                
            else
                error('No bounding box specified for features.');
            end
        end 
    end
end
