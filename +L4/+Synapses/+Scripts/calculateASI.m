% script to calculate axon-spine interface for spine head synapses
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo();


%% load data

p = Gaba.getSegParameters('ex145_ROI2017');

% synapses (use those that are clustered for same axon - same spine)
synFile = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v3_ax_spine_clustered.mat');
m = load(synFile);
synapses = m.synapses(m.isSpineSyn,:);
asi = zeros(size(synapses, 1), 1);

% spine heads
shFile = fullfile(p.agglo.saveFolder, 'spine_heads_and_attachment_03.mat');
m = load(shFile);
shAgglos = m.shAgglos;

% axons
[axons, indAxons] = L4.Axons.getLargeAxons(p, true, true); % axons_18_a.mat at time of coding
idx = cellfun(@isempty, axons);
axons = axons(~idx);
indAxons = indAxons(~idx);

% graph
graph = Seg.IO.loadGraph(p, false);
edgeLUT = Seg.Global.getEdgeLookupTable(graph.edges);
axonLUT = L4.Agglo.buildLUT(axons, max(graph.edges(:)));

% physical border area
m = load(p.svg.borderMetaFile, 'borderArea');
borderArea = nan(size(graph.edges, 1), 1);
borderArea(~isnan(graph.borderIdx)) = m.borderArea;


%% get connectome for axons and spine heads

[ ~, ~, ~, connectome ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, axons, shAgglos );

% this should not happen after the same spine same axon clustering
if any(cellfun(@length, connectome.synIdx) > 1)
    warning('There are synapses associated to multiple axons.');
end


%% get asi for synapses

% synapses associated to an axon agglo
hasAxon = false(size(synapses, 1), 1);
hasAxon(cell2mat(connectome.synIdx)) = true;

% for those who do not have it use synapse edges
asi(~hasAxon) = cellfun(@(x)sum(borderArea(x)), ...
    synapses.edgeIdx(~hasAxon));

% for others lookup all edges between axon and spine head and sum the area
% of those (could be slow this way though)
Util.log('Calculating ASI.');
% replace loop by L4.Agglo.findEdgesBetweenAgglo2 in the future
for i = 1:size(connectome, 1)
    sh = shAgglos{connectome.edges(i,2)};
    ax = axons{connectome.edges(i,1)};
    thisEdgesIdx = unique(cell2mat(edgeLUT(sh)));
    shAxEdges = any(ismember(graph.edges(thisEdgesIdx,:), ax), 2);
    shAxEdgesIdx = thisEdgesIdx(shAxEdges);
    asi(connectome.synIdx{i}) = sum(borderArea(shAxEdgesIdx));
end


%% add output to spine syn file

outFile = fullfile(p.connectome.saveFolder, ...
    'SynapseAgglos_v3_ax_spine_clustered_spineSynOnly.mat');
asi_result.info = info;
asi_result.asi = asi;

Util.log('Appending ASI to file %s.', outFile);
save(outFile, 'asi_result', '-append');
