% analyze the dendrites for which the ff was overlapped with axons. Are there any AIS?
setConfig;
config.outDir = '/tmpscratch/sahilloo/L4/forIso/';
outDirNmls = '/tmpscratch/sahilloo/L4/forIso/AISNmls/';
config.axonFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/axons_08_a.mat';
config.dendriteFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_flight_01.mat';
overlapFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/axonDendriteQueryOverlaps_1.0.mat';
wkFlag = false;

if ~exist(outDirNmls,'dir')
    mkdir(outDirNmls);
end
Util.log('Loading stuff...');
m=load(config.param);
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

m = load(config.axonFile);
axons = m.axons(m.indBigAxons);
if isfield(axons,'nodes')
    axons = Superagglos.getSegIds(axons);
end

m = load(config.dendriteFile);
dendritesSmall = m.dendrites(~m.indBigDends);
if isfield(dendritesSmall,'nodes')
    dendritesSmall = Superagglos.getSegIds(dendritesSmall);
end
dendrites = m.dendrites(m.indBigDends);
if isfield(dendrites,'nodes')
    dendrites = Superagglos.getSegIds(dendrites);
end

m = load(overlapFile);
startDendrite = m.results.startDendrite;
 
endAxon = m.results.endAxon;
ff = m.results.ff.nodes';
ffSeg = m.results.ff.segIds;

toDel = cellfun(@isempty,endAxon);
startDendrite(toDel) = [];
endAxon(toDel) = [];
ff(toDel) = [];
ffSeg(toDel) = [];
if wkFlag
    Util.log(['Now writing out the nmls for ' num2str(numel(endAxon)) '...'])
    rng(0);
    idxToPlot = randperm(numel(endAxon),20);

    for i = idxToPlot
        thisD = dendrites(startDendrite{i});
        thisA = axons(endAxon{i});
        thisFF = ff{i};

        skel = Skeleton.fromMST({points(thisD{:},:)},voxelSize);
        skel.names(1) = {'dendrite'};

        skelFF = skeleton();
        skelFF = skelFF.addTree('ff',thisFF);
        skel = skel.addTreeFromSkel(skelFF);
        for j = 1:numel(thisA)
            skelAx = Skeleton.fromMST({points(thisA{j},:)},voxelSize); 
            skelAx.names(1) = {'axon'};
            skel = skel.addTreeFromSkel(skelAx);
        end

        skel = Skeleton.setParams4Pipeline(skel, p);
        skel.write(fullfile(outDirNmls, ['overlapping_' ...
            num2str(i) '_' datestr(now,'yyyymmdd') '.nml']));
    end
end

%% find straight things
bounds = p.bbox;

Util.log('Filtering STs...');
%filter using site criterion                                                                                                            
tol = 500;
agglos = {};
for i = 1:numel(endAxon)
    thisD = dendrites{startDendrite{i}};
    thisA = vertcat(axons{endAxon{i}});
    thisFF = ffSeg{i};
    agglos{i} = unique(nonzeros(vertcat(thisD, thisA, thisFF)));
end

m = load(config.meta);
pointsAgglos = cellfun(@(x) m.point(:,x)',agglos,'uni',0);

[ filteredAgglos1, filteredPoints1, rInd_1 ] = ...
    Apicals.filterSiteCriterion( agglos, pointsAgglos, bounds, tol );
Util.log('Saving STs...');
save(fullfile(config.outDir,'STfromDendAxOL.mat'),'filteredAgglos1',...
    'rInd_1','agglos');

% write out random rInd_1 STs as dend,ff,axon
rng(0);
idxST = rInd_1;
%idxToPlot = idxST(randperm(numel(idxST),20));
idxToPlot = idxST;
Util.log(['Writing nmls for ' num2str(numel(idxST)) ' STs found']);
for i = 1:numel(idxToPlot)
    %thisD = dendrites(startDendrite{idxToPlot(i)});
    %thisA = axons(endAxon{idxToPlot(i)});
    thisFF = ff{idxToPlot(i)};
    appendName  = num2str(i,'%.3i');
    %skel = Skeleton.fromMST({points(thisD{:},:)},voxelSize);
    %skel.names(1) = {[appendName 'dendrite']};

    skelFF = skeleton();
    skelFF = skelFF.addTree([appendName 'ff'],thisFF);
    skel = skelFF;
    %skel = skel.addTreeFromSkel(skelFF);
    %{
    for j = 1:numel(thisA)
        skelAx = Skeleton.fromMST({points(thisA{j},:)},voxelSize);
        skelAx.names(1) = {[appendName 'axon']};
        skel = skel.addTreeFromSkel(skelAx);
    end
    %}
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDirNmls, ['overlappingST_' ...
        num2str(idxToPlot(i)) '_' datestr(now,'yyyymmdd') '.nml']));
end

%% after manually annotation AIS
m=load(fullfile(config.outDir,'STfromDendAxOL.mat'));
idxST = m.rInd_1;

m = load(fullfile(config.outDir,'idxAISinST.mat'));
idxAISinST = m.idxAISinST;
idxAISinff = idxST(idxAISinST);

outdir = [config.outDir 'aisManuallyExtracted/'];
if ~exist(outdir,'dir')
        mkdir(outdir);
end
arrayfun(@(x) copyfile(fullfile(outDirNmls, ['overlappingST_' ...
    num2str(x) '_' num2str(20171106) '.nml']),outdir),idxAISinff);

%% plot ais with dend
idxToPlot = idxAISinff;
if ~isrow(idxToPlot)
    idxToPlot = idxToPlot';
end
for i = idxToPlot
    thisD = dendrites(startDendrite{i});
    thisA = axons(endAxon{i});
    thisFF = ff{i};

    skel = Skeleton.fromMST({points(thisD{:},:)},voxelSize);
    skel.names(1) = {'dendrite'};

    skelFF = skeleton();
    skelFF = skelFF.addTree('ff',thisFF,'',[0,0,1,1]);
    skel = skel.addTreeFromSkel(skelFF);
    for j = 1:numel(thisA)
        skelAx = Skeleton.fromMST({points(thisA{j},:)},voxelSize); 
        skelAx.names(1) = {'axon'};
        skel = skel.addTreeFromSkel(skelAx);
    end
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDirNmls, ['aisWithDend_' ...
        num2str(i) '_' datestr(now,'yyyymmdd') '.nml']));
end

%% check for overlap of AIS ff with less than 5um dend agglos
temp = ff(idxAISinff);
fpAIS = struct;
[fpAIS(1:numel(temp)).nodes] = temp{:};
ov = Superagglos.flightPathAggloOverlap( p, fpAIS, dendritesSmall );

dendAIS = dendrites([startDendrite{idxAISinff}]);
ffAIS = ff(idxAISinff);
ovNonEmpty = find(~cellfun(@isempty,ov));

for i = ovNonEmpty'
    thisD = dendAIS(i);
    thisFF = ffAIS{i};
    thisOV = dendritesSmall(ov{i});
    skel = Skeleton.fromMST({points(thisD{:},:)},voxelSize);
    skel.names(1) = {'dendrite'};

    skelFF = skeleton();
    skelFF = skelFF.addTree('ff',thisFF,'',[0,0,1,1]);
    skel = skel.addTreeFromSkel(skelFF);
     
    for j = 1:numel(thisOV)
        skelOv = Skeleton.fromMST({points(thisOV{j},:)},voxelSize); 
        skelOv.names(1) = {'ov'};
        skel = skel.addTreeFromSkel(skelOv);
    end
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDirNmls, ['aisWithDendAndOv_' ...
        num2str(i) '_' datestr(now,'yyyymmdd') '.nml']));
end
save(fullfile(config.outDir,'aisOVwithDendSmall.mat'),'ov')
