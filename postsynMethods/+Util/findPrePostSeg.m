function [pre,post] = findPrePostSeg()

% find which synapses are shaft
outDir = '/tmpscratch/sahilloo/L4/data/';

m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
m = load([p.saveFolder 'globalEdges.mat']);
edges = m.edges;

m = load([p.saveFolder 'globalSynScores.mat']);
synScores = nan(size(edges));
synScores(m.edgeIdx, :) = m.synScores;

rng(0);
points = Seg.Global.getSegToPointMap(p);
voxelSize = p.raw.voxelSize;

% synapses
synThr = -2.06;
idx = synScores > synThr;

% ignore where both edges predicted pre
preIdx = idx; preIdx(~xor(idx(:,1),idx(:,2)),:)=0; 
postIdx = ~idx; postIdx(~xor(idx(:,1),idx(:,2)),:)=0;

preEdgeIdx = preIdx(:,1) | preIdx(:,2);
postEdgeIdx = postIdx(:,1) | postIdx(:,2);

pre = unique(edges(preIdx));
post = unique(edges(postIdx));
save(fullfile(outDir,'segmentsPrePost.mat'),'pre','post');
end






