function a = setPlotDefault( a , xLimRestrict)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
% modified by SL
if ~exist('a', 'var') || isempty(a)
    a = gca;
end
if ~exist('xLimRestrict', 'var') || isempty(xLimRestrict)
    xLimRestrict = false;
end      
a.FontName = 'Arial';
a.TickDir = 'out';

if xLimRestrict
    a.XLim = [0 1];
    a.XTick = [0 0.5 1];
end
box off

end

