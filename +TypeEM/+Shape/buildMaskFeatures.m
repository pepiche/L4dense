function maskFeat = buildMaskFeatures(voxelSize, mask)
    % FEATMASK Calculates plenty of features related
    % to the segment mask.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % compute mass properties
    massProps = TypeEM.Shape.Mask.massProperties(voxelSize, mask);
    
    maskFeat = struct;
    maskFeat.voxelCount = sum(mask(:));
    maskFeat = TypeEM.Util.mergeStructs(maskFeat, massProps);
end
