function openClassMethod( name )
%OPENCLASSMETHOD Like open for class methods.
%
% Usage
%
%   Util.openClassMethod('obj.someMemberFct')
%   Util.openClassMethod(@obj.memberFct)
% 
% INPUT name: string of function handle
%           Method name as a string or a function handle to with a class
%           method calling that function.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ischar(name)
    name = strrep(name, '.', filesep);
    open(name);
elseif isa(name, 'function_handle')
    tmp = functions(name);
    wkspc = tmp.workspace{1};
    fnames = fieldnames(tmp.workspace{1});
    className = class(wkspc.(fnames{1}));
    fctName = regexp(tmp.function, '.*\.(\w*)(.*', 'tokens');
    name = [className filesep fctName{1}{1}];
    idx = strfind(name, '.');
    if ~isempty(idx)
        name = insertAfter(name, idx(end), '@');
        idx(end) = [];
        for i = length(idx):-1:1
            name = insertAfter(name, idx(i), '+');
        end
        name = ['+' name];
        name = strrep(name, '.', filesep);
    end
    name = [name '.m'];
    open(name);
else
    error('Input of type %s is not supported.', class(name));
end
end

function newStr = insertAfter(str, startPos, newText)
    newStr = [str(1:startPos) newText str((startPos+1):end)];
end

