function p = getSegParameter()
%GETSEGPARAMETER
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

p = Gaba.getSegParameters('ex145_ROI2017');

end

