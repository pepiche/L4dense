function skels = evaluateFalsePos(param, classifiers, gt)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    import TypeEM.*;
    
    % load feature values
    gt = GroundTruth.loadFeatures( ...
        param, classifiers.featureSetName, gt);
    gt.scores = Classifier.apply(classifiers, gt.featMat);
    gt.probs = Classifier.applyPlatt(classifiers, gt.scores);
    
    % load segment meta data
    metaFile = fullfile(param.saveFolder, 'segmentMeta.mat');
    meta = load(metaFile, 'segIds', 'point');
    
    % build skeletons
    skels = arrayfun( ...
        @(idx) main(param, classifiers, gt, meta, idx), ...
        1:numel(classifiers.classes), 'UniformOutput', false);
end

function skel = main(param, classifiers, gt, meta, classIdx)
    % config
    segCount = 100;
    
    % find segment to look at
    mask = gt.label(:, classIdx) < 0;
    
    data = table;
    data.segId = gt.segId(mask);
    data.prob = gt.probs(mask, classIdx);
    
    % sort and limit
    data = sortrows(data, 'prob', 'descend');
    data = data(1:segCount, :);
    
    % find points for segments
    [~, rows] = ismember(data.segId, meta.segIds);
    
    % build names
    className = classifiers.classes(classIdx);
    treeNames = arrayfun(@(n) {sprintf('%.1f%%', n)}, 100 .* data.prob);
    treeNames = strcat(char(className), {' '}, treeNames);
    
    % build skeleton
    skel = skeleton();
    skel = skel.addNodesAsTrees(meta.point(:, rows)');
    skel = Skeleton.setParams4Pipeline(skel, param);
    skel.names = treeNames;
end