for myslice=1:3424
    
    size_final_canvas = [15000, 10000];
    columns = 36;
    rows = 24;
    slices = 3424;
    shifts = reshape(ttemp.shifts,[columns, rows, slices, 2]);
    
    canvas_x_y_final = zeros(size_final_canvas, 'uint8');
    for x_it= 1 : 3
        for y_it = 1 : 3
            y_it
            canvas_x_y = zeros(size_final_canvas, 'uint8');
            filename_image = ...
                ['' all_fns{x_it * (columns / old_columns), y_it * (rows / old_rows), myslice}(1:end)];
            image_x_y = imread(filename_image)'+1;
            image_x_y_padded = padarray(image_x_y, [padding, padding]);
            [x_move, y_move] = findMatchingFramesNew(shifts, columns, ...
                rows, slices, x_it, y_it, myslice, {1:3072, 1:2048}, ttemp);
            image_x_y_warped= zeros(5072,4048,'uint8');
            for i=1:size(x_move,1)
                for j=1:size(x_move,2)
                    image_x_y_warped(x_move(i,j), y_move(i,j)) = image_x_y(i,j);
                end
            end
            canvas_x_y = zeros(15000,10000, 'uint8');
            canvas_x_y(3072*(x_it-1)+1:3072*(x_it)+2000,2048*(y_it-1)+1:2048*(y_it)+2000) = image_x_y_warped;
            canvas_x_y_final(canvas_x_y_final == 0) = canvas_x_y(canvas_x_y_final == 0);
        end
    end
    canvas_x_y_final_map = canvas_x_y_final == 0;
    canvas_x_y_final_medfilt = medfilt2(canvas_x_y_final);
    canvas_x_y_final_final = canvas_x_y_final;
    canvas_x_y_final_final(canvas_x_y_final_map) = canvas_x_y_final_medfilt(canvas_x_y_final_map);
    imwrite(canvas_x_y_final_final, ['Z:\Data\boergensk\st07x2_redone\tif_output\' num2str(myslice) '.png']);
    imwrite(imresize(canvas_x_y_final,0.1), ['Z:\Data\boergensk\st07x2_redone\tif_output\small_' num2str(myslice) '.png']);
    
end