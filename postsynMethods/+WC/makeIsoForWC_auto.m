
setConfig;
load(config.param);
config.outDir = '/tmpscratch/sahilloo/L4/dataPostSyn/';
aggloStateFolder = fullfile(p.saveFolder,'aggloState');
config.dendriteFile = fullfile(aggloStateFolder,'dendrites_wholeCells_01_auto.mat');

outDir = fullfile(config.outDir,'isoForWC_auto');
m = load(config.param);
p = m.p;
points = Seg.Global.getSegToPointMap(p);
voxelSize = p.raw.voxelSize;

if ~exist(outDir,'dir')
    mkdir(outDir);
end

Util.log('Loading stuff..')
maxSegId = Seg.Global.getMaxSegId(p);
m=load(config.dendriteFile);
dendAgglos = m.dendAgglos;
dendAggloLUT = Agglo.buildLUT(maxSegId,dendAgglos);

Util.log('Find wholecell indices ...');
somaAgglos = connectEM.getSomaAgglos(fullfile(aggloStateFolder,'somas_with_merged_somas.mat'),'all');
somaAgglos = somaAgglos(~cellfun(@isempty,somaAgglos)); % remove not found somata
somaSegIds = cell2mat(somaAgglos);

[~,ic] = unique(somaSegIds);
duplicates = somaSegIds(setdiff(1:numel(somaSegIds),ic));
% somaDuplicateIds = cellfun(@(x) any(intersect(x,duplicates)),somaAgglos);
somaAgglos = cellfun(@(x) setdiff(x,duplicates),somaAgglos,'uni',0);
somaSegIds = cell2mat(somaAgglos);
somaLUT(somaSegIds) = repelem(1:numel(somaAgglos),cellfun(@numel,somaAgglos));

dendriteSegIds = cat(1,dendAgglos{:});
[ismem,ind] = ismember(somaSegIds,dendriteSegIds);
indWholeCells = accumarray(somaLUT(somaSegIds(ismem))',dendAggloLUT(dendriteSegIds(ind(ismem)))',[],@mode);

agglos = dendAgglos(indWholeCells);
%agglos = agglos(24);
                                                                                                                                                                             
Util.log('Writing skeletons...')
% name output nmls to shCount and wc index
tic;
endStep = length(agglos);
for i=1:endStep
    nodes = agglos(i);
    skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);                                                                                                                 
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDir, ['wc_' num2str(i) '.nml']));
    Util.progressBar(i,endStep);
end
Util.log('Finished creating nmls to view');
return

Util.log('Now running main viz')
%p.seg.root='/tmpscratch/amotta/l4/2012-09-28_ex145_07x2_ROI2017/segmentation/1/';
p.seg.root = ['/gabaghi/wKcubes_archive/2012-09-28_ex145_07x2_ROI2017/segmentation/1/'];
p.seg.backend = 'wkwrap';
Visualization.exportAggloToAmira(p,agglos,outDir,'reduce',0.05,'smoothSizeHalf',4,'smoothWidth',8);
info=Util.runInfo(false);
save(fullfile(outDir,'info.mat'),'info');
