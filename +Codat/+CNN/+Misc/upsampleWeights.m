function w_out = upsampleWeights( w, out_shape)
%UPSAMPLEWEIGHTS Upsample weights to a given output shape.
% INPUT w: 5d double array
%           The cnn weights.
%       out_shape: [1x3] int
%           The shape of the output weights in the first three (=spatial)
%           dimensions (i.e. feature maps are not upsampled).
% OUTPUT w_out: 5d double array
%           The upsampled weights of size
%           [out_shape size(w, 4) size(w, 5)].
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('d', 'var') || isempty(d)
    d =  [1, 1, 1];
end

siz = size(w);
siz(end+1:5) = 1;

w_out = zeros([out_shape(:)' siz(4:5)]);

for i = 1:siz(4)
    for j = 1:siz(5)
        [x, y, z] = ndgrid(linspace(1,siz(1), out_shape(1)), ...
                           linspace(1,siz(2), out_shape(2)), ...
                           linspace(1,siz(3), out_shape(3)));
        w_out(:,:,:,i,j) = interp3(w(:,:,:,i,j), y, x, z, 'spline');

        % correct for larger fov
        w_out(:,:,:,i,j) = w_out(:,:,:,i,j) .* ...
            (prod(siz(1:3))/prod(out_shape));
    end
end

end
