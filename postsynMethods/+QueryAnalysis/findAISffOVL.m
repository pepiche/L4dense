% Check for overlap of ff of the AIS with dendrites/ pick up the segments of ff
setConfig;
outDir = '/tmpscratch/sahilloo/L4/forIso/';
axonFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/axons_08_a.mat';
dendriteFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_flight_01.mat';
overlapFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/axonDendriteQueryOverlaps_1.0.mat';

info = Util.runInfo();

Util.log('Loading stuff...');
m=load(config.param);
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

m = load(axonFile);
axons = m.axons(m.indBigAxons);
if isfield(axons,'nodes')
    axons = Superagglos.getSegIds(axons);
end

m = load(dendriteFile);
dendritesSmall = m.dendrites(~m.indBigDends);
if isfield(dendritesSmall,'nodes')
    dendritesSmall = Superagglos.getSegIds(dendritesSmall);
end
dendrites = m.dendrites(m.indBigDends);
if isfield(dendrites,'nodes')
    dendrites = Superagglos.getSegIds(dendrites);
end

% load the ff-dend-axon processing. Also the ff nodes and segIds
m = load(overlapFile);
startDendrite = m.results.startDendrite;
endAxon = m.results.endAxon;
ff = m.results.ff.nodes';
ffSeg = m.results.ff.segIds;

toDel = cellfun(@isempty,endAxon);
startDendrite(toDel) = [];
endAxon(toDel) = [];
ff(toDel) = [];
ffSeg(toDel) = [];

Util.log('finding the indices of AIS manually annotated...')
m=load(fullfile(outDir,'STfromDendAxOL.mat'));
idxST = m.rInd_1;
m = load(fullfile(outDir,'idxAISinST.mat'));
idxAISinST = m.idxAISinST;
idxAISinff = idxST(idxAISinST);

Util.log('extract the good final stuff...')
dendAIS = dendrites([startDendrite{idxAISinff}]);
ffAIS = ff(idxAISinff);
ffSegAIS = ffSeg(idxAISinff);
axonAIS = axons([endAxon{idxAISinff}]);

% transfer to new version
m = load(fullfile(config.outDir,'spineTable.mat'));
spineTable = m.spineTable;

idxNewAIS = Agglo.findAggloAcrossVersions(p,dendAIS,spineTable.dendritesExt);
newAISTable = spineTable(idxNewAIS,:);

idxAIS = idxNewAIS;

% post processing
% discard 1st and 14th
toDel = [1,14];
idxAIS(toDel)=[];
dendAIS(toDel)=[];
ffAIS(toDel) = [];
ffSegAIS(toDel)=[];
axonAIS(toDel)=[];
ffSegAIS = cellfun(@nonzeros,ffSegAIS,'uni',0);

save(fullfile(config.outDir, 'dendritesAISStateFull.mat'), ...
    'info','idxAIS','dendAIS','ffAIS','ffSegAIS');

% write out nmls with dend_ffSeg as two trees
for i = 1:length(dendAIS)
    thisD = dendAIS{i};
    thisFF = ffSegAIS{i};

    skel = Skeleton.fromMST({points(thisD(:),:)},voxelSize);
    skel.names(1) = {'dendrite'};

    skelFF = Skeleton.fromMST({points(thisFF(:),:)},voxelSize);
    skel = skel.addTreeFromSkel(skelFF);
    skel.names(2) = {'ff'};
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(config.outDir,'nmls', ['aisWDend_ff_' ...
        num2str(i) '.nml']));
end

% write out nmls with dend_ffSeg as one tree
for i = 1:length(dendAIS)
    thisD = dendAIS{i};
    thisFF = ffSegAIS{i};
    nodes = vertcat(thisD,thisFF);
    skel = Skeleton.fromMST({points(nodes(:),:)},voxelSize);
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(config.outDir,'nmls', ['aisWDendff_' ...
        num2str(i) '.nml']));
end 

% make WK mapping
out = cellfun(@(x,y) unique(vertcat(x,y)),dendAIS,ffSegAIS,'uni',0);
segs_to_zero = setdiff((0:15030572)',vertcat(out{:}));
components = cat(1,{segs_to_zero},out);
WK.makeWKMapping(components,'ais_59','/tmpscratch/sahilloo/L4/dataPostSyn/')

