function [c, nc] = ucounts(x)
%UCOUNTS Get unique values and their counts.
% INPUT x: nd numeric array
%           Numeric array of arbitrary size.
% OUTPUT c: [Nx1] numeric (like x)
%           Unique values in x.
%        nc: [Nx1] int
%           Number of occurrences of the corresponding value in c.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

[c, ~, ic] = unique(x);
nc = accumarray(ic, 1);

end
