function evaluateSpineHeads(param, classifiers, gt)
    % evaluateSpineHeads(param, classifiers, gt)
    %   Evaluates the spine head classifier on entire spine heads (i.e.,
    %   spine head agglomerates) instead of single segments.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % config
    minEdgeProb = 0.9;

    % find spine head segment ID
    shLabels = gt.label(:, gt.class == 'spinehead');
    shSegIds = gt.segId(shLabels > 0);
    
    % build subgraph on spine heads
    graph = Graph.load(param.saveFolder);
    graph(graph.prob < minEdgeProb, :) = [];
    graph(not(all(ismember(graph.edges, shSegIds), 2)), :) = [];
    
    % group segments into connected components
    maxSegId = Seg.Global.getMaxSegId(param);
    shAggloLUT = Graph.buildConnectedComponents(maxSegId, graph.edges);
   
    % do prediction
    gt = TypeEM.GroundTruth.loadFeatures( ...
        param, classifiers.featureSetName, gt);
    gt.scores = TypeEM.Classifier.apply(classifiers, gt.featMat);
    shScores = gt.scores(:, classifiers.classes == 'spinehead');
    
    % reduce agglomerates to single segment
    gt.agglo = shAggloLUT(gt.segId);
    gt.agglo(~gt.agglo) = (1:sum(~gt.agglo)) + max(gt.agglo);
    
    aggloScores = accumarray(gt.agglo, shScores, [], @max);
    aggloLabels = accumarray(gt.agglo, shLabels, [], @min);
    
    % clean up
    aggloScores(not(aggloLabels)) = [];
    aggloLabels(not(aggloLabels)) = [];
    
    fig = figure();
    ax = axes(fig);
    
    [precVec, recVec] = ...
        TypeEM.Classifier.buildPrecRec(aggloScores, aggloLabels);
    TypeEM.Classifier.plotPrecisionRecall(ax, precVec, recVec);end
