classdef sgdHD < Codat.Optimizer.optimizer
    %SGDHD Optimization by stochastic gradient descent with hyper gradient.
    % see arXiv:1703.04782v1
    % Properties learningRate: Prefactor of gradient. Can be used to modify
    %               the overall learning rate via decay policies.
    %               (see also Codat.CNN.cnn.train)
    %            hyp_lr: Hypergradient learning rate.
    %            lr: Individual learning rate for the parameters.
    %               Specify as init_lr as a single float in constructur
    %               which will be used for individually for each weight.
    %            momentum: Momentum strength
    % Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

    properties
        learningRate = 1
        momentum
        v
        hyp_lr
        g_prev
        lr
    end

    methods
        function gd = sgdHD(hyp_lr, init_lr, momentum)
            gd = gd@Codat.Optimizer.optimizer;
            gd.hyp_lr = hyp_lr;
            gd.lr = init_lr;
            gd.momentum = momentum;
        end

        function gd = init(gd, numParams)
            gd.v = zeros(numParams,1,'single');
            gd.g_prev = zeros(numParams,1,'single');
            gd.lr = gd.lr.*ones(numParams, 1, 'single');
        end

        function [paramsNew, gd] = optimize(gd, paramsOld, gradient)
            gd.lr = gd.lr + gd.hyp_lr.*gradient.*(gd.g_prev + gd.momentum.*gd.v);
            gd.v = gd.momentum.*gd.v + gradient;
            paramsNew = paramsOld - ...
                gd.learningRate.*gd.lr.*(gradient + gd.momentum.*gd.v);
            gd.g_prev = gradient;
        end

        function optimizer = setParamsToActvtClass(optimizer,actvtClass)
            optimizer.v = actvtClass(gather(optimizer.v));
            optimizer.g_prev = actvtClass(gather(optimizer.g_prev));
        end
    end
end
