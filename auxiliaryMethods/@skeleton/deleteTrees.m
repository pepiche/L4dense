function obj = deleteTrees(obj, treeIndices, complement)
% Delete specified trees.
% INPUT treeIndices: [Nx1] int or [Nx1] logical
%           Array with linear or logical indices of trees to delete.
%       complement: logical (default: false)
%           the complement set of trees to treeIndices would be deleted if
%           true
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
% Modified: Ali Karimi <ali.karimi@brain.mpg.de>
% Modified: Alessandro Motta <alessandro.motta@brain.mpg.de>
if ~exist('complement', 'var') || isempty(complement)
    complement = false;
end

if islogical(treeIndices)
    treeIndices = find(treeIndices);
end

if complement
    treeIndices = setdiff(1:obj.numTrees(), treeIndices);
end

treeIndices = reshape(treeIndices, 1, []);

%get ids of deleted nodes
nodeIds = obj.nodesNumDataAll(treeIndices);
nodeIds = cell2mat(cellfun(@(x) x(:, 1), nodeIds, 'UniformOutput', false));

%delete tree
obj.nodes(treeIndices) = [];
obj.nodesAsStruct(treeIndices) = [];
obj.nodesNumDataAll(treeIndices) = [];
obj.edges(treeIndices) = [];
obj.thingIDs(treeIndices) = [];
obj.names(treeIndices) = [];
obj.colors(treeIndices) = [];
obj.groupId(treeIndices) = [];
obj.branchpoints = setdiff(obj.branchpoints, nodeIds);

obj.largestID = cellfun(@(x) max([0; x(:, 1)]), obj.nodesNumDataAll);
obj.largestID = max([0; obj.largestID(:)]);

end
