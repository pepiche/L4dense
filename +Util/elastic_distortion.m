function [ im_d ] = elastic_distortion( im, sigma, grid_size )
%ELASTIC_DISTORTION U-net style elastic image distortion.
% INPUT im: 2d or 3d image
%       sigma: double or [2x1] double (2d im) or [3x1] double (3d im)
%       grid_size: [2x1] (2d im) or [3x1] int (3d im)
%           Grid that is coarser than the number of pixels in each
%           dimension of im on which the distortion vectors are sampled
%           pixelwise disortions are then calculated using interpolation
%           between grid disortions.
% OUPUT im_d: 2d or 3d image
%           Disorted image.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

siz = size(im);
nd = ndims(im);
if isscalar(sigma)
    sigma(end+1:ndim) = sigma(1);
end
if isscalar(grid_size)
    grid_size(end+1:ndim) = grid_size(1);
end

disp_points = ceil(siz./grid_size) + 1; % add 1 to definitely go beyond the image

if nd == 2
    [x, y] = meshgrid(1:siz(1), 1:siz(2));
    dg_x = randn(disp_points).*sigma(1);
    dg_y = randn(disp_points).*sigma(2);
    dx = griddata(1:grid_size(1):grid_size(1)*disp_points(1), ...
                  1:grid_size(2):grid_size(2)*disp_points(2), ...
                  dg_x, x, y);
    dy = griddata(1:grid_size(1):grid_size(1)*disp_points(1), ...
                  1:grid_size(2):grid_size(2)*disp_points(2), ...
                  dg_y, x, y);
    % quiver(x, y, dx, dx, 0, 'r') % plot displacements
    im_d = uint8(griddata(x, y, double(im), x + dx, y + dy));
elseif nd == 3
    [x, y, z] = meshgrid(1:siz(1), 1:siz(2), 1:siz(3));
    dg_x = randn(disp_points).*sigma(1);
    dg_y = randn(disp_points).*sigma(2);
    dg_z = randn(disp_points).*sigma(3);
    [xc, yc, zc] = meshgrid(1:grid_size(1):grid_size(1)*disp_points(1), ...
                        1:grid_size(2):grid_size(2)*disp_points(2), ...
                        1:grid_size(3):grid_size(3)*disp_points(3));
    dx = griddata(xc, yc, zc, dg_x, x, y, z);
    dy = griddata(xc, yc, zc, dg_y, x, y, z);
    dz = griddata(xc, yc, zc, dg_z, x, y, z);
    im_d = uint8(griddata(x, y, z, double(im), x + dx, y + dy, z + dz, 'nearest'));
else
    error('Number of input dimensions is not supported.');
end
end

