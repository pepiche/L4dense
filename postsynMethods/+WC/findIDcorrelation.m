% find the wholecell IDs corresponding to this list
% https://docs.google.com/spreadsheets/d/1KeQZNw07cc-W-pNAaKTSRt6jFUiUmTHiMlZ6l-h-rKc/edit#gid=0
% center wholecells

sourceFile = fullfile(config.outDir,'L4 CenterLineCells ROI2017.xlsx');
setConfig;
outDir = config.outDir;
load(config.param);

m=load(fullfile(outDir,'wcTable.mat'));
wcTable = m.wcTable;
wcExt = wcTable.wcExt;
load(config.param)
maxSegId = Seg.Global.getMaxSegId(p);
aggloLUT = Agglo.buildLUT(maxSegId,wcExt); 

% segment ids peresent in each nml
[~,~,xl] = xlsread(sourceFile,'A2:F28');
xlTable = cell2table(xl);
coords = cell2mat(cellfun(@(x) str2num(x),xlTable.xl4,'uni',0));

% coords dont always correspond to a segment
ids = Seg.Global.getSegIds(p,coords);

thisID = zeros(27,1);
tic;
for i=1:27
    skelSegIds = ids(i);
    thisID(i) = mode(aggloLUT(skelSegIds));
    Util.progressBar(i,4);
end
