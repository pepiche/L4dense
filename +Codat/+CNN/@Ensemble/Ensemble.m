classdef Ensemble
    %ENSEMBLE Very simple ensemble of CNNs
    % Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
    
    properties
        cnets
        weights
        border
    end
    
    methods
        function ens = Ensemble(varargin)
            % Class constructor
            % ens = Ensemble() creates an empty ensemble object.
            % ens = Ensemble(cnets, weights)
            % INPUT cnets: [Nx1] cell array of cnet objects.
            %       weights: (Optional)
            
            if length(varargin) >= 1
                ens.cnets = vertcat(varargin{1}(:));
                border = cellfun(@(x)x.border,ens.cnets, ...
                    'UniformOutput',false);
                ens.border = max(cell2mat(border),[],1);
            end
            if length(varargin) >= 2
                ens.weights = varargin{2};
            else
                ens.weights = ones(length(ens.cnets),1)./length(ens.cnets);
            end
        end
        
        function pred = predict(ens, in)
            % INPUT in: Input array.
            % OUTPUT pred: Output array which is smaller by ens.border in
            %           the first three dimension (i.e. by the largest
            %           border of all cnets contained in ens).
            
            %allocate output
            pred = zeros([size(in) - ens.border, ...
                ens.cnets{1}.featureMaps(end)], 'single');
            
            %predict each cnn separately
            for i = 1:length(ens.cnets)
                %reduce border further if necessary
                bd = (ens.border - ens.cnets{i}.border)./2;
                pred = pred + ens.weights(i).*gather(...
                    ens.cnets{i}.predict( in(bd(1)+1:end-bd(1),...
                    bd(2)+1:end-bd(2),bd(3)+1:end-bd(3))));
            end
        end
        
        function ens = setParamsToActvtClass(ens, convHandle)
            % see cnn.setParamsToActvtClass
            ens.cnets = cellfun(@(x)x.setParamsToActvtClass(convHandle),...
                ens.cnets,'UniformOutput',false);
        end
        
        function ens = setConvMode(ens, convAlg)
            % see cnn.setConvMode
            ens.cnets = cellfun(@(x)x.setConvMode(convAlg),...
                ens.cnets,'UniformOutput',false);
        end
    end
    
end

