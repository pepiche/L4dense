% Based on
%   File: L4/+WC/cutoutAxonsInWc.m
%   Repository: https://gitlab.mpcdf.mpg.de/connectomics/loombas
%   Commit: da06153bb49828d7e429348293a605b8958937a5
%
% Written by
%   Sahil Loomba <sahil.loomba@brain.mpg.de>

%% Configuration
setConfig;
wkFlag = false;
pdfFlag = false;

load(config.param);

aggloDir = fullfile(p.saveFolder, 'aggloState');

wcFile = fullfile(aggloDir,'wholeCells_GTAxon_08_v4.mat');

info = Util.runInfo();

Util.log('Converting axon parts to superagglos....');
wc = load(wcFile);
wholeCells = wc.wholeCells;

undefAxon = arrayfun(@(x) any(isnan(x.axon)) | ~any(x.axon) ,wholeCells); % where axon was not found: NaN or 0's
wholeCellsWithAxon = wholeCells(~undefAxon);
nodesToDelete = arrayfun(@(x) find(~x.axon),wholeCellsWithAxon,'uni',0); % all except axon nodes

idxToCheck = false(numel(wholeCellsWithAxon),1);
tmpToCheck = cell(numel(wholeCellsWithAxon),1);
nodeThr = 10;

for n = 1:numel(wholeCellsWithAxon)
    tmp = Superagglos.removeNodesFromAgglo(wholeCellsWithAxon(n),nodesToDelete(n));
    if numel(tmp)>1
        warning('More than one axon');
        s = arrayfun(@(x) size(x.nodes,1),tmp);
        garbage = s < nodeThr;
        idxToCheck(n) = true;
        tmpToCheck{n} = tmp;
        s = arrayfun(@(x) size(x.nodes,1),tmp);
        garbage = s < nodeThr; 
        tmp = tmp(~garbage);
        [~,ind] = min(arrayfun(@(x) size(x.nodes,1),tmp)); % to remove in wc 61 the dendrite part
    else
        ind=1;
    end
    axonsToAdd(n) = tmp(ind);
end
ais = arrayfun(@(x) rmfield(x,'axon'),axonsToAdd);

%% Complete output
Util.log('Cleaning ais');
ais = SuperAgglo.clean(ais);

if wkFlag
    Util.log('Now writing nmls to view in wK...')
    outDirNml = fullfile(config.outDir,'aisFromWCNmls');      
    if ~exist(outDirNml,'dir')
        mkdir(outDirNml);
    end
    
    tic;
    endStep = length(ais);
    idxToPlot = 1:endStep;
    for i = idxToPlot
        thisAIS = ais(i);
        skel = Superagglos.toSkel(thisAIS);
        skel.write(fullfile(outDirNml, ['ais_' num2str(i) '.nml']));
        Util.progressBar(i,endStep);
    end
end 

if pdfFlag
    outDirPdf = fullfile(config.outDir,'aisFromWCNmls');
    if ~exist(outDirPdf,'dir')
        mkdir(outDirPdf);
    end 
    
    bbox = [129 5574; 129 8509; 129 3414];
    bbox = bsxfun(@times, bbox, [11.24; 11.24; 28]./1000);
    Util.log('Skeletonizing...')
    ovSkels = {};
    endStep = length(ais);
    idxToPlot = 1:endStep;
    for i = idxToPlot
        thisAIS = ais(i);
        skel = Superagglos.toSkel(thisAIS);
        ovSkels{i} = skel;
    end 
    Util.log('Writing out pdf gallery...')
    tic;
    for i = idxToPlot
        %set f to din A4 size
        f = figure;
        f.Units = 'centimeters';
        f.Position = [1 1 21 29.7];
        Visualization.Figure.adaptPaperPosition();
        for j = 1:4
            a = subplot(2, 2, j);
            ovSkels{i}.plot(1, [0, 0, 0], true, 2);
            hold on
            xlabel('x (um)');
            ylabel('y (um)');
            zlabel('z (um)');
            grid on
            switch j
                case 1
                    %xy
                    view([0, 90]);
                case 2
                    %xz
                    view([0, 0]);
                case 3
                    %yz
                    view([90, 0]);
                case 4
                    % default 3d view
            end     
            axis equal
            a.XLim = bbox(1,:);
            a.YLim = bbox(2,:);
            a.ZLim = bbox(3,:);
        end
        ovSkels{i}.filename = sprintf('ais%03d', i);
        outfile = fullfile(outDirPdf, ...
            sprintf('ais%02d_%s.pdf', i, ovSkels{i}.filename));
        print(outfile, '-dpdf')
        Util.log('Saving file %s.', outfile);
        close(f);
    end
    Util.log('Finished creating pdfs to view');

end
Util.save(fullfile(config.outDir,'aisFromWCState.mat'),'ais','','info')
