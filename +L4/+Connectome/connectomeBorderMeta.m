function meta = connectomeBorderMeta( connectome, synapses, borderMeta )
%CONNECTOMEBORDERMETA Get meta information about the connectome synapses.
% INPUT connectome: [Nx2] table
%           Connectome as a table containing the row 'synIdx' with the 
%           linear indices w.r.t. synapses for each pair
%           of connected processes.
%           (see also L4.Synapses.connectomeFromSyn2Agglo)
%       synapses: [Nx3] table
%           Table of all synapses.
%           (see also L4.Synapses.clusterSynapticInterfaces)
%       borderMeta: struct
%           Struct with border meta information for each border in the svg.
%           Must contain the fields 'borderArea' and 'borderCoM'
% OUTPUT meta: struct
%           Struct with connectome meta information
%           'noSynapses': Total number of synapses in the connectome.
%           'contactArea': Vector of total synapse area for each connectome
%               entry.
%           'coms': [Nx3] numerical matrix with coms for each synapse.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% total number of synapses
synIdx = connectome.synIdx;
meta.noSynapses = sum(cellfun(@length, synIdx));

% synapse area and coms
meta.contactArea = cell(length(synIdx), 1);
meta.coms = cellfun(@(x)zeros(length(x), 3), synIdx, 'uni', 0);
for i = 1:length(synIdx)
    meta.contactArea{i} = cellfun(@(x)sum(borderMeta.borderArea(x)), ...
        synapses.edgeIdx(synIdx{i}));
    meta.coms{i} = cell2mat(cellfun(@(x) ...
        round(mean(borderMeta.borderCoM(x, :), 1)), ...
        synapses.edgeIdx(synIdx{i}), 'uni', 0));
end


end

