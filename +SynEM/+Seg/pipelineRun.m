function [p, job] = pipelineRun( p, classifierPath, cluster )
%PIPELINERUN Run SynEM as part of the pipeline.
% INPUT p: struct
%           Segmentation parameter struct.
%       classifierPath: string
%           Path to matfile containing the synem classifier in the variable
%           'classifier'.
%       cluster: (Optional) parallel.cluster object
%           The cluster object for prediction.
%           (Default: Cluster.getCluster())
% OUTPUT p: struct
%           Segmentation parameter struct updated with the fields
%           p.synEM: Path of classifier and feature map in the
%               segmentation main folder.
%           p.local.synapseFile: Path to the synapse scores in each local
%               segmentation cube folder.
%           p.local.interfaceFeatureFile: Path to the interface features in
%               each local segmentation cube folder.
%        job: parallel.job object
%           The synapse detection cluster job object.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% load classifier
m = load(classifierPath, 'classifier');
classifier = m.classifier;
fm = classifier.options.fm;

% get classifier
if ~exist('cluster', 'var') || isempty(cluster)
    cluster = Cluster.config('memory', 24, 'time', '100:00:00');
end

% run prediction
[p, job] = SynEM.Seg.predictDataset(p, fm, classifier, [], cluster, ...
    [], true, false, true);

end

