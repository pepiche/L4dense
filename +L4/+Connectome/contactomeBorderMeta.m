function meta = contactomeBorderMeta( contactome, borderMeta )
%CONTACTOMEBORDERMETA Get border meta information for a contactome.
% INPUT contactome: [Nx2] table
%           Connectome as a table containing the row 'synIdx' with the 
%           linear indices w.r.t. synapses for each pair
%           of connected processes.
%           (see also L4.Agglo.findEdgesBetweenAgglos2)
%       borderMeta: struct
%           Struct with border meta information for each border in the svg.
%           Must contain the fields 'borderArea' and 'borderCoM'
% OUTPUT meta: struct
%           Struct with connectome meta information
%           'noContacts': Total number of synapses in the connectome.
%           'contactArea': Vector of total synapse area for each connectome
%               entry.
%           'coms': [Nx3] numerical matrix with coms for each synapse.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% number of contacts
edgeIdx = contactome.edgeIdx;
meta.noContacts = cellfun(@length, edgeIdx);

% area and coms
meta.contactArea = cell(length(edgeIdx), 1);
meta.coms = cellfun(@(x)zeros(length(x), 3), edgeIdx, 'uni', 0);
for i = 1:length(edgeIdx)
    meta.contactArea{i} = borderMeta.borderArea(edgeIdx{i}, :);
    meta.coms{i} = borderMeta.borderCoM(edgeIdx{i}, :);
end

end

