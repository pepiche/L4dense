% use the annotated AD from extract ST in 'forIso' folder and find them in new dendrite state
setConfig;
outFile = fullfile(config.outDir, 'dendritesADState_v2.mat');
info = Util.runInfo();

load(config.param)
plotFlag = false;
isoFlag = false;

% older version annotation and analysis
outDir = '/tmpscratch/sahilloo/L4/forIso/';
m = load(fullfile(outDir,'spineTable.mat'));
spineTable = m.spineTable;

m = load(fullfile(outDir,'straightThingsState.mat'));
idxST = m.idxST;

m=load(fullfile(outDir,'straightThingsFromMH.mat'));
idxADInST = m.sortedCellIds(m.agglosSortValues==1);

% spine analysis on stFromMH
tableST = spineTable(idxST,:);
tableSTFromMH = tableST(idxADInST,:);
oldAD = tableSTFromMH.dendritesExt;

% transfer to new version
m = load(fullfile(config.outDir,'spineTable.mat'));
spineTable = m.spineTable;

idxNewAD = Agglo.findAggloAcrossVersions(p,oldAD,spineTable.dendritesExt);
newADTable = spineTable(idxNewAD,:);
newAD_shDensity = newADTable.shDensity;

% save result
out = struct;
out.idxAD = idxNewAD(:);
out.agglos = oldAD(:);
out.info = info;

Util.saveStruct(outFile, out);
Util.protect(outFile);

% plot
commentType = 'sorted 1: AD';
if plotFlag
    fig = figure;
    histogram(newAD_shDensity,'BinWidth',0.05)
    xlabel('Spine density: # spines per um');
    ylabel('#');
    legend({commentType});
    Util.setPlotDefault();
    saveas(fig,fullfile(config.outDir,'figures/spineDensityAD.png'))
    close all
end

% remove smooth dendrites from the AD list

if isoFlag
    % make iso for 183 ADs
    outDirIso = fullfile(config.outDir,'isoForAD');
    if ~exist(outDirIso,'dir')
        mkdir(outDirIso)
    end
    Util.log('Now running main viz')
    agglos = newADTable.dendritesExt;
    p.seg.root = '/gabaghi/wKcubes_archive/2012-09-28_ex145_07x2_ROI2017/segmentation/1/';
    p.seg.backend = 'wkwrap';
    %Visualization.exportAggloToAmira(p,agglos,outDir);
    Visualization.exportAggloToAmira(p,agglos,outDirIso,'reduce',0.1,'smoothSizeHalf',4,'smoothWidth',8);
    save(fullfile(outDirIso,'info.mat'),'info');
end

