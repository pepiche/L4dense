classdef KMBwait
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        currentThread;
        C;
        looking;
    end
    
    methods
        function obj=KMBwait(currentThread,noThreads,C,looking,doIt)
            thisThreadsTurn=false;
            if doIt
                while not(thisThreadsTurn)
                    thisThreadsTurn=true;
                    for i=1:currentThread-noThreads
                        if exist(strcat(C.matdir,C.knumberstr,'_thread_',sprintf('%u',i),looking,'.mat'),'file')==0
                            thisThreadsTurn=false;
                        end
                    end
                    if not(thisThreadsTurn)
                        pause(1);
                    end
                end
            end
            obj.currentThread=currentThread;
            obj.C=C;
            obj.looking=looking;
            
        end
        function closure(obj)
            x=1;
            save(strcat(obj.C.matdir,obj.C.knumberstr,'_thread_',sprintf('%u',obj.currentThread),obj.looking,'.mat'),'x');
        end
    end
    
end