function [ id2pre, id2post ] = synapseSegmentMapping( synapses, maxId )
%SYNAPSESEGMENTMAPPING Mapping of pre- and postsynaptic segments to
%synapses.
% INPUT synapses: table
%           Table containing the 'presynId' and 'postsynId' colums with
%           pre- and postsynaptic segment ids for each synapse.
%           (see also L4.Synapses.clusterSynapticInterfaces)
% OUTPUT id2pre: [Nx1] cell
%           Contains the linear indices of all synapses that contain a
%           segment as presynaptic process.
%        id2post: [Nx1] cell
%           Contains the linear indices of all synapses that contain a
%           segment as postsynaptic process.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('maxId', 'var')
    maxId = [];
end

id2pre = L4.Agglo.buildLUT(synapses.presynId, maxId, true);
id2post = L4.Agglo.buildLUT(synapses.postsynId, maxId, true);

end

