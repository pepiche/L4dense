function [ ind ] = translateInd( ind, sizOrig, sizNew, t )
%TRANSLATEIND Transform linear indices from a smaller to a larger array.
% INPUT ind: [Nx1] array of integer of linear indices w.r.t. to a cube of
%           size sizeOrig.
%       sizOrig: [Nx1] array of integer specifying the size of the original
%           cube.
%       sizeNew: [Nx1] array of integer specifying the size of the new
%           cube. All entries of sizNew must be larger or equal to the
%           corresponding entry in sizeOrig.
%       t: (Optional) [Nx1] vector of integer specifying the position of
%           the origin of the original cube (i.e. the point sizOrig(1,1))
%           in the new cube e.g. if the original cube is centered in the
%           new one use (sizNew - sizOrig)./2 + 1)
%           (Default assumes that the old cube is centered in the new one)
% OUTPUT ind: The linear indices w.r.t. to the new cube size.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if any( sizNew < sizOrig)
    error('sizNew must be larger than sizOrig.');
end
if isrow(ind)
    ind = ind';
end
if ~exist('t','var') || isempty(t)
    t = (sizNew - sizOrig)./2 + 1;
end
if ~isrow(t)
    t = t';
end

X = cell(1,length(sizOrig));
[X{:}] = ind2sub(sizOrig,ind);
X = cellfun(@(x,y) x + y - 1, X, num2cell(t),'UniformOutput',false);
ind = sub2ind(sizNew,X{:});

end

