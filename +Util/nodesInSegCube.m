function [ st, t ] = nodesInSegCube( p, nodes )
%NODESINSEGCUBE Get the number of nodes in each seg cube.
% INPUT p: struct
%           Segmentation parameter struct.
%       nodes: [Nx3] int
%           The node coordinates.
% OUTPUT st: [Nx3] double
%           Frequency table of the segmentation cube indices for all input
%           nodes (i.e. output of tabulate on the segmentation cube
%           indices) sorted by ndoes in the cubes in descending order.
%        t: [Nx3] double
%           The frequency table sorted by cube indices.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

cubeIdx = Skeleton.findSegCubeIdxOfNodes(double(nodes), p);
t = tabulate(cubeIdx);
st = sortrows(t, 2);
st = st(end:-1:1,:); % descend not possible in R2015b

end

