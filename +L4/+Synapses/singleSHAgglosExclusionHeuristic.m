function [toDel, debug] = singleSHAgglosExclusionHeuristic( synapses, ...
    synScores, shAgglos )
%SINGLESHAGGLOSEXCLUSIONHEURISTIC For spine heads that consist only of a
% single segment, only the synapse with highest score is kept.
% INPUT synapses: [Nx3] table
%           Synapse agglomerate table.
%           (see also L4.Synapses.clusterSynapticInterfaces=
%       synScores: [Nx1] double
%           Synapse scores for the edge indices specified in synapses.
%       shAgglos: [Nx1] cell
%           Cell array with spine head agglos.
% OUTPUT toDel: [Nx1] int
%           Linear indices of the synapses that should be deleted according
%           to the heuristic.
%        debug: struct
%           Structure with some additional data for
%           visualization/debugging.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% just to make sure
synScores = max(synScores, [], 2);

% only spine head consisting of single segments
shAgglos(cellfun(@length, shAgglos) > 1) = [];

% synapse spine head mapping

[ ~, ~, post2syn ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, [], shAgglos );


% get synapse with maximal score for spine heads with multiple synapses

hasMultipleSyns = cellfun(@length, post2syn) > 1;
post2syn = post2syn(hasMultipleSyns);
maxScoreSyn = length(post2syn);
% deleted synapse with highest score from post2syn
for i = 1:length(post2syn)
    thisScores = cellfun(@(x)max(synScores(x)), ...
        synapses.edgeIdx(post2syn{i}));
    [~, idx] = max(thisScores);
    maxScoreSyn(i) = post2syn{i}(idx);
    post2syn{i}(idx) = []; % i.e. only the ones to delete are kept
end
toDel = cell2mat(post2syn);

debug.maxScoreSyn = maxScoreSyn;
debug.post2syn = post2syn;

end

