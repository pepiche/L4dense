function [precRecPlot, probRecPlot] = ...
        plotPrecisionRecall(ax, precVec, recVec, probVec)
    % [precRecPlot, probRecPlot] = ...
    %    plotPrecisionRecall(ax, precVec, recVec, probVec)
    %
    % Plots the precision / recall curve. Additionally, the
    % probabilities can be specified and will be shown as
    % well.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % turn on grid
    grid(ax, 'on');
    
    % default to empty
    probRecPlot = [];
    
    if exist('probVec', 'var') ...
            && ~isempty(probVec)
        % swith to right
        yyaxis(ax, 'right');
        
        % plot curve
        probRecPlot = plot(ax, 100 * recVec, 100 * probVec);
        probRecPlot.LineWidth = 1;
        
        % plot ticks
        ylim(ax, [0, 100]);
        yticks(ax, 0:10:100);
        ylabel('Probability (%)');
        
        % back to default
        yyaxis(ax, 'left');
    end
    
    % precision / recall curve
    precRecPlot = plot(ax, 100 * recVec, 100 * precVec);
    precRecPlot.LineWidth = 2;
    
    % X and Y ticks
    xlim(ax, [0, 100]); xticks(ax, 0:10:100); xlabel('Recall (%)');
    ylim(ax, [0, 100]); yticks(ax, 0:10:100); ylabel('Precision (%)');
end
