function [scores] = findPrePostInterfaceScoresDendrites()

outDir = '/tmpscratch/sahilloo/L4/data/';

%load all pre and post segments by synEM
[pre,post]=findPrePostInterfaces();
pre = unique(pre,'rows');
post = unique(post,'rows');

% load dendrite agglos
k = load('/tmpscratch/sahilloo/L4/dendriteQueryResults2/dendritesPostQueryState.mat'); % no mask of 5um distance, only 200k vx size
dendrites = k.dendritesPostQuery;

% load edges
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
m = load([p.saveFolder 'globalEdges.mat']);
edges = m.edges;
disp('Now calculating scores per agglo...')
scores = struct('pre',[],'post',[]);

for i=1:size(dendrites,1)
    agglo = dendrites{i};
    % find interfaces of the agglo
    allPre = pre(ismember(pre(:,1),agglo),:);
    allPost = post(ismember(post(:,1),agglo),:);
    % remove internal interfaces
    idxDel = ismember(allPre(:,2),agglo);
    allPre(idxDel,:)=[];
    idxDel = ismember(allPost(:,2),agglo);
    allPost(idxDel,:)=[];
    
    scores(i).pre = size(allPre,1);
    scores(i).post = size(allPost,1);
    
    clear agglo allPre allPost 
    fprintf('Finished agglo %d \n',i);
end
save(fullfile(outDir,'dendritesPrePostInterfaceScores.mat'),'scores');
end