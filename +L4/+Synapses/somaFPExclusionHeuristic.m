function toDel = somaFPExclusionHeuristic( p, synapses, axons, somata, ...
    shAgglos, borderCom, d_bouton )
%SOMAFPEXCLUSIONHEURISTIC False positive exclusion heuristic.
% Synapses are first grouped into boutons based on a maximal distance
% between synapses. For each bouton that has a spine synapse which is the
% only synapse onto that spine, all soma synapses are deleted.
% INPUT p: struct
%           Segmentation parameter struct.
%       synapses: [Nx3] table
%           Synapse agglomerate table
%           (see also L4.Synapses.clusterSynapticInterfaces)
%       axons: [Nx1] cell
%           Axon agglomerates in the agglo format, that is lists of segment
%           ids of the same process.
%       somata: [Nx1] cell
%           Soma agglomerates in the agglo format.
%       shAgglos: [Nx1] cell
%           Spine head agglomerates in the agglo format.
%       borderCom: [Nx3] double
%           Coms of the borders in synapses.edgeIdx in voxel space.
%       d_bouton: (Optional) double
%           Distance in units of p.raw.voxelSize that is used to cluster
%           synapses into boutons.
%           (see also L4.Synapses.synapse2BoutonsAxonAgglo)
%           (Default: 1e3)
% OUTPUT toDel: [Nx1] int
%           Linear indices of the synapses that should be deleted according
%           to the heuristic.
%
% see also L4.Synapses.executeSomaFPExclusionHeuristic
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('d_bouton', 'var') || isempty(d_bouton)
    d_bouton = 1e3;
end


%% synapse agglo mappings

[ ~, pre2syn, post2syn, ~ ] = L4.Synapses.synapsesAggloMapping( ...
    synapses, axons, shAgglos );


%% get spine and soma synapses

maxId = Seg.Global.getMaxSegId(p);
shLUT = Agglo.buildLUT(maxId, shAgglos);
isSpine = shLUT > 0;
isSoma = Agglo.buildLUT(maxId, somata) > 0;
isSpineSyn = cellfun(@(x)any(isSpine(x)), synapses.postsynId);
isSomaSyn = cellfun(@(x)any(isSoma(x)), synapses.postsynId);


%% boutons

boutons = L4.Synapses.synapse2BoutonsAxonAgglo(synapses, [], d_bouton, ...
    borderCom, p.raw.voxelSize, pre2syn);

% keep only boutons with soma synapses & spine synapses
boutons = boutons(cellfun(@(x)any(isSomaSyn(x)) & any(isSpineSyn(x)), ...
    boutons));

% delete syns that are both spine and soma
tmp = cellfun(@(x)any(isSpineSyn(x) & isSomaSyn(x)), boutons);
% boutonsHybridSyns = boutons(tmp);
% hybridSyns = cellfun(@(x)x(isSpineSyn(x) & isSomaSyn(x)), ...
%         boutonsHybridSyns, 'uni', 0);
boutons = boutons(~tmp);


%% get synapses at same bouton and calculate exclusion principle

% add soma and spine synapses to separate arrays
boutonsSomaSyns = cellfun(@(x)x(isSomaSyn(x)), boutons, 'uni', 0);
boutonsSpineSyns = cellfun(@(x)x(isSpineSyn(x)), boutons, 'uni', 0);

isSingleSpineSyn = false(length(boutonsSpineSyns), 1);
for i = 1:length(boutonsSpineSyns)
    curSingleSpineSyns = false(length(boutonsSpineSyns{i}), 1);
    for j = 1:length(boutonsSpineSyns{i})
        thisShAggloIdx = shLUT(synapses.postsynId{boutonsSpineSyns{i}(j)});
        thisShAggloIdx = setdiff(thisShAggloIdx, 0);
        curSingleSpineSyns(j) = all(arrayfun(@(x)length(post2syn{x}) == 1, ...
            thisShAggloIdx));
    end
    isSingleSpineSyn(i) = any(curSingleSpineSyns);
end
toDel = cell2mat(boutonsSomaSyns(isSingleSpineSyn));

end

