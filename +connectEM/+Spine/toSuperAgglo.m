function out = toSuperAgglo(segPos, agglos, edges)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    assert(numel(agglos) == numel(edges));
    
    outNodes = cell(numel(agglos), 1);
    outEdges = cell(numel(agglos), 1);
    
    for curIdx = 1:numel(agglos)
        curSegIds = agglos{curIdx};
        curEdges = edges{curIdx};
        
        outNodes{curIdx} = horzcat( ...
            segPos(curSegIds, :), double(curSegIds));
       [~, outEdges{curIdx}] = ismember(curEdges, curSegIds);
    end
    
    out = struct( ...
        'nodes', outNodes, ...
        'edges', outEdges);
end