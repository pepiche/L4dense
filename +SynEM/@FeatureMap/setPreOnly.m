function setPreOnly( obj, preOnly )
%SETPREONLY Setter for preOnly.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

assert(islogical(preOnly));

obj.preOnly = preOnly;
obj.calculateSize();

end

