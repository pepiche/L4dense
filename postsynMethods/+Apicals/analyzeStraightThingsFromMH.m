setConfig;
config.outDir = '/tmpscratch/sahilloo/L4/forIso/';
outDir = config.outDir;
config.dendriteFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_03.mat';
config.spineFile = '/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/spine_heads_and_attachment_03.mat';

plotFlag= true;

Util.log('Loading stuff...');
config
m=load(config.param);
p = m.p;
points = Seg.Global.getSegToPointMap(p);
voxelSize = p.raw.voxelSize;

m = load(fullfile(outDir,'spineTable.mat'));
spineTable = m.spineTable;

m=load(config.dendriteFile);
indBigDends = m.indBigDends;
dendritesSA = m.dendrites(indBigDends);
dendrites = Superagglos.getSegIds(m.dendrites(indBigDends));

m=load(fullfile(outDir,'straightThingsFromMH.mat'));
agglosToPrint = m.agglosSorted(m.agglosSortValues==1); % superagglos
idxAgglosToPrintInST = m.sortedCellIds(m.agglosSortValues==1);
commentType = 'sorted 1 AD';

m = load(fullfile(outDir,'straightThingsState.mat'));
straightThings = m.straightThings;
idxST = m.idxST;

% spine analysis on stFromMH
tableST = spineTable(idxST,:);
tableSTFromMH = tableST(idxAgglosToPrintInST,:);
shDensitySTFromMH = tableSTFromMH.shDensity;

% plot
if plotFlag
    fig = figure;
    histogram(shDensitySTFromMH,'BinWidth',0.05)
    xlabel('Spine density: # spines per um');
    ylabel('#');
    legend({commentType});
    Util.setPlotDefault();
    saveas(fig,fullfile(outDir,'figures/spineDensitySTFromMH.png'))
    close all
end

% separate AD/AIS at thr=0.4
[sdSorted,idxSortSD] = sort(shDensitySTFromMH,'ascend');
% idxAD = shDensitySTFromMH >= 0.4;
% agglosAD = agglosToPrint(idxAD);
% agglosAIS = agglosToPrint(~idxAD);

% append 4ADs and 1 myAx from dendrites08.mat;
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_08.mat');
dendrites08 = m.dendrites;

idxInDend08 = [1816984, 1816993, 1817005,1817020,1816992];
stToAdd = dendrites08(idxInDend08);
sdToAdd = [NaN;NaN;NaN;NaN;NaN];

agglosToWrite = agglosToPrint(idxSortSD); % agglosAD; %agglosAD;
agglosToWrite = vertcat(agglosToWrite,stToAdd);
sdSorted = vertcat(sdSorted,sdToAdd);

plotFlag = false;
tic;
endStep = numel(agglosToWrite);
for i =1:endStep
    skel = Superagglos.toSkel(agglosToWrite(i));
%     fileName = ['AD_thr0.4'];
    fileName = ['SD:' num2str(sdSorted(i),2)];
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDirForNml, ['straightThingF_' num2str(i,'%.3i') '_' fileName '.nml']));

    if plotFlag
    fig = figure('Units','centimeters','Position',[0 -2 23 30]); 
    hold on;
    Visualization.plotBbox(bbox);
    skel.plot
    axis equal
    view(2)
    xlim([0 70000])
    ylim([0 100000])
    zlim([0 100000])
    
    set(findall(gcf,'-property','FontSize'),'FontSize',20);
    set(gcf, 'PaperSize', [29.7 21]);
    set(gcf, 'PaperPosition', [0 0 29.7 21]);
    
    pdfFile = fullfile(outDirForPdf,['straightThing_' fileName '.nml']);
    print(gcf,pdfFile,'-dpdf')
    close(fig);
    end
    Util.progressBar(i,endStep)
end
 
