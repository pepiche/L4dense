function [ a ] = unsort( a, idx )
%UNSORT Reverse the sorting of an array.
% INPUT a: [Nx1] numerical
%           The array that should be unsorted.
%       idx: [Nx1] int
%           The sorting indices (second output of sort).
% OUTPUT a: [Nx1] numerical
%           The unsorted array.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

unsortIdx(idx) = 1:length(idx);
a = a(unsortIdx);

end

