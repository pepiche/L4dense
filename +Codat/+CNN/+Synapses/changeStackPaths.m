function [stacksT, stacksV] = changeStackPaths(stacksT, stacksV, newPath)
%CHANGESTACKPATHS Change the paths in the stacks.
% INPUT stacksT: [1xN] struct
%           Struct to training cubes containing the field 'stackFile'.
%       stacksV: [1xN] struct
%           Struct to validation cubes containing the field 'stackFile'.
%       newPath: string
%           Path to training data main folder
%           (see targetDir in Codat.CNN.Synapses.makeTrainingData)
% OUTPUT stacks: [1xN] struct
%           Input struct with replaced paths.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

for i = 1:length(stacksT)
    fname = regexp(stacksT(i).stackFile, '\w*.mat$', 'match');
    stacksT(i).stackFile = fullfile(newPath, 'train', fname{1});
end

for i = 1:length(stacksV)
    fname = regexp(stacksV(i).stackFile, '\w*.mat$', 'match');
    stacksV(i).stackFile = fullfile(newPath, 'val', fname{1});
end


end

