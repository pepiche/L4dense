% take the axon cut wholecells state and add them to axons
setConfig;
load(config.param);
    
outputFolder = fullfile(p.saveFolder, 'aggloState');
info = Util.runInfo();
    
m = load(fullfile(config.outDir,'axonsFromWCState.mat'));
axonsFromWC = m.axons;
axonsFromWCIds = m.axonsFromWCIds;
if isrow(axonsFromWCIds)
    axonsFromWCIds = axonsFromWCIds';
end
if isrow(axonsFromWC)
    axonsFromWC = axonsFromWC';
end

Util.log('restructure to match axons')
for i=1:numel(axonsFromWC)
    axonsFromWC(i).endings = [];
    axonsFromWC(i).solvedChiasma = false;
end

% use axon state
m=load(fullfile(outputFolder,'axons_18_b.mat'));
axons = m.axons;
indBigAxons = m.indBigAxons;
parentIds = m.parentIds;

Util.log('concatenate axons and make new state');
out = struct;
out.axons = cat(1,axons,axonsFromWC);
out.indBigAxons = cat(1,indBigAxons,true(numel(axonsFromWC),1));
out.parentIds = parentIds;
out.indWcAxon = cat(1,false(numel(axons),1),true(numel(axonsFromWC),1));
out.idxWcAxon = cat(1,zeros(numel(axons),1),axonsFromWCIds);
out.info = info;

Util.log('Saving axons...');
Util.saveStruct(fullfile(outputFolder,'axons_19.mat'),out);

