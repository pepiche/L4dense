# TypeEM

This module contains all code and data concerning the TypeEM segment
type classifier. The two main entry points are:

* **[TypeEM.Pipeline.buildFeatures](./+Pipeline/buildFeatures.m)** to
  calculate the texture and shape features for individual segments or
  segment agglomerates.
* **[TypeEM.Pipeline.buildPredictions](./+Pipeline/buildPredictions.m)**
  to calculate the axon, dendrite, astrocyte and spine head
  probabilities of each segment.

An example of how to train and evalute the TypeEM classifiers is
provided in [TypeEM.Script.testReproducibility](./+Script/testReproducibility.m).

## Configuration used for L4 project

For the classification of single segments (spine head probabilities):
```
% Calculate features
typeEmConfig = struct;
TypeEM.Pipeline.buildFeatures(param, typeEmConfig, 'segmentFeatures.mat');

% Train and save segment classifier
segmentClassifier = TypeEM.Classifier.train(param, 'segment');
save('segmentClassifier.mat', 'segmentClassifier');

% Predict on whole dataset
TypeEM.Pipeline.buildPredictions(param, '-struct', 'segmentClassifier.mat');
```

For the classification of segment agglomerates (axon, dendrite, and
astrocyte probabilities):
```
% Calculate features
typeEmConfig = struct;
typeEmConfig.agglo.padSize = [256, 256, 128];
typeEmConfig.agglo.minEdgeProb = 0.92;
typeEmConfig.agglo.maxSegCount = 5;

TypeEM.Pipeline.buildFeatures(param, typeEmConfig, 'segmentAggloFeatures.mat');

% Train and save segment agglomerate classifier
segmentAggloClassifier = TypeEM.Classifier.train(param, 'segmentAgglo');
save('segmentAggloClassifier.mat', '-struct', 'segmentAggloClassifier');

% Predict on whole dataset
TypeEM.Pipeline.buildPredictions(param, 'segmentAggloClassifier.mat');
```

Here, `param` is the parameter structure produced by
[configuration.m](configuration.m) upon pipeline initialization.

## Provenience

This module was extracted from amotta's personal repository (commit
70d131d72c53bdaa379be1cee44763de07ca74d7).
