function findProblemLoops(ttemp)
problem = rescueProblems(ttemp);

prob_ag{3, 1} = problem{1}(:, 1 : end-1, :) + problem{1}(:, 2 : end, :);
prob_ag{3, 2} = problem{2}(1 : end-1, :, :) + problem{2}(2 : end, :, :);
prob_ag{2, 1} = problem{1}(:, :, 1 : end-1) + problem{1}(:, :, 2 : end);
prob_ag{2, 3} = problem{3}(1 : end-1, :, :) + problem{3}(2 : end, :, :);
prob_ag{1, 2} = problem{2}(:, :, 1 : end-1) + problem{2}(:, :, 2 : end);
prob_ag{1, 3} = problem{3}(:, 1 : end-1, :) + problem{3}(:, 2 : end, :);

prob_sum = cell(1, 3);
for i = 1 :3
    for j = 1 : 3
        if ~isempty(prob_ag{i, j})
            if isempty(prob_sum{i})
                prob_sum{i} = prob_ag{i, j};
            else
                prob_sum{i} = prob_sum{i} + prob_ag{i, j};
            end
        end
    end
end

findMaximumProblem(prob_sum, 20, 20)
