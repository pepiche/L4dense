function [ X ] = renumberBwd( Y, mapping )
%RENUMBERBWD Inverse to renumber.
% INPUT Y: Output from renumber.
%       mapping: Output from renumber.
% OUTPUT X: Input from renumber.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

X = mapping(Y,1);
X = reshape(X,size(Y));

end

