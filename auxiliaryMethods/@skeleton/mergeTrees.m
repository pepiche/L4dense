function skel = mergeTrees( skel, varargin )
%MERGETREES Merge two trees of a skeleton.
% USAGE
%   skel.mergeTrees(id1, id2)
%       Connect the nodes with id1 and id2 and merge the corresponding
%       trees.
%   skel.mergeTrees(tree1, node1, tree2, node2)
%       Merge the trees with linear indices tree1 and tree2 by connecting
%       the nodes with linear indices node1 and node2. The node indices are
%       linear indices w.r.t. to the corresponding tree.
%   skel.mergeTrees(treeIndices)
%       cell array of tree indices that should be merged together (no
%       edge concatenation)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
%         Marcel Beining <marcel.beining@brain.mpg.de>

%add all nodes of second tree to first tree
switch length(varargin)
    case 1
        % merge trees according to list of treeIndices (equivalenceClass)
        % without concatenation
        if iscell(varargin{1})
            equivalenceClass = varargin{1}(:);
            % add treeIdx that are not yet part of the treeIndices list
            equivalenceClass = cat(1,num2cell(setdiff(1:skel.numTrees,cell2mat(equivalenceClass)))',equivalenceClass);

            numNodes = cellfun(@(x) size(x,1),skel.nodes);
            numEdges = cellfun(@numel,skel.edges)/2;
            numTrees = skel.numTrees;
            fNames = fieldnames(skel);
            for f = 1:numel(fNames)
                if size(skel.(fNames{f}),1) == numTrees
                    if isnumeric(skel.(fNames{f}))
                        if strcmp(fNames{f},'thingIDs')
                            skel.thingIDs = (1:numel(equivalenceClass))';
                        else
                            skel.(fNames{f}) = skel.(fNames{f})(cellfun(@(x) x(1),equivalenceClass));
                        end
                    else
                        try
                            skel.(fNames{f}) = cellfun(@(x) cat(1,skel.(fNames{f}){x}),equivalenceClass,'uni',0);
                        catch
                            skel.(fNames{f}) = cellfun(@(x) cat(2,skel.(fNames{f}){x}),equivalenceClass,'uni',0);
                        end
                    end
                elseif strcmp(fNames{f},'edges')
                    % concatenate the edges of different skels by adding number of
                    % nodes of agglo 1:n to the edge indices of agglo n+1
                    skel.edges = cellfun(@(x) cat(1,skel.edges{x}) + repmat(reshape(repelem(cumsum(cat(1,0,numNodes(x(1:end-1)))),numEdges(x)),sum(numEdges(x)),1),1,2),equivalenceClass,'uni',0);
                end
            end            
        else
            error('Wrong input parameter specified (should be cell array).');
        end
    case 4
        [tree1, node1, tree2, node2] = varargin{:};
        for n = 1:numel(tree1)
            numNodes1 = size(skel.nodes{tree1(n)}, 1);
            skel.nodes{tree1(n)} = [skel.nodes{tree1(n)}; skel.nodes{tree2(n)}];
            skel.nodesAsStruct{tree1(n)} = [skel.nodesAsStruct{tree1(n)}, ...
                skel.nodesAsStruct{tree2(n)}];
            skel.nodesNumDataAll{tree1(n)} = [skel.nodesNumDataAll{tree1(n)}; ...
                skel.nodesNumDataAll{tree2(n)}];
            skel.edges{tree1(n)} = [skel.edges{tree1(n)}; skel.edges{tree2(n)} + numNodes1; ...
                node1(n), node2(n) + numNodes1];
        end
        skel = skel.deleteTrees(tree2);
    case 2
        [tree1, node1] = skel.getNodesWithIDs(varargin{1});
        [tree2, node2] = skel.getNodesWithIDs(varargin{2});
        if tree1 == tree2
            error('Both ids belong to the same node.');
        end
        skel = skel.mergeTrees(tree1, node1, tree2, node2);
    otherwise
        error('Wrong number of input parameters specified.');
end



end

