function somata = getSomaList( filePath )
%GETSOMALIST Get the locations of all somata.
% INPUT filePath: string
%           Path to KAMIN_cells.xlsx file.
% OUTPUT somata: table
%           Table with somata
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

somaCen = xlsread(filePath, 'C2:E134');
[~, glia] = xlsread(filePath, 'H2:H134');
isGlia = ~strcmp(glia, 'not glia') & ~cellfun(@isempty, glia);

somata = table(somaCen, isGlia, 'VariableNames', {'Centroid', 'isGlia'});

end

