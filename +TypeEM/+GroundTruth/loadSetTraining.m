function gt = loadSetTraining(param, featureSetName)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    nmlDir = TypeEM.Data.getFile( ...
        fullfile('tracings', 'ex145-07x2-roi2017'));
    nmlFiles = fullfile(nmlDir, 'dense-v3', ...
        {'region-1.nml', 'region-2.nml', 'region-3.nml'});
    
    % load training set
    gt = TypeEM.GroundTruth.loadSet( ...
        param, featureSetName, nmlFiles);
end
