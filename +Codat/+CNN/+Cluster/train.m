function jobs = train( type, cnets, saveFolder, snapFiles, cluster, ...
    comment, mode, varargin )
%TRAIN Cnet training wrapper for cluster.
% INPUT type: Type of cnet to train (see switch below).
%       cnets: Cnet or cell array of cnets to train on task.
%       saveFolder: Folder where the snapshot files are save (without
%                   filesep at the end).
%       snapFiles: Cell array containing the file locations for the cnet
%                  snapshots. If only one name is provided the names will
%                  automatically get index of the cnet attached.
%       cluster: (Optional) A parcluster object.
%                (Default: getCluster('gpu'))
%       comment: (Optional) Variable that will be attached to the options
%                cnn option struct and can contain additional information
%                about the training.
%                (If not specified or empty: No comment will be saved).
%       mode: (Optional) String specifying preset option modes. See below
%           for mode settings.
%       varargin: Name-value pairs overwriting any of the options listed
%           below. Name should be the fieldname of options.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

try
    git_hash = Util.getGitHash();
catch
    warning('Could not get git commit hash.');
    git_hash = [];
end
options.git_hash = git_hash;
options.type = type;

%parse input
if ~iscell(cnets)
    cnets = {cnets};
end
if ~exist('cluster','var') || isempty(cluster)
    cluster = getCluster('gpu');
end
if exist('comment','var') && ~isempty(comment)
    options.comment = comment;
end
if ~exist('mode','var') || isempty(mode)
    mode = 'smallCube';
end

%only important for MMV training
% mmvStacksT = [1:6 10 13 15 18 94 106 118 190 251 265];
%discard 94 and 106 due to difficult mitos for now
mmvStacksT = [1:6 10 13 15 18 118 190 251 265];
mmvStacksV = 19:20;

%indices if exclude is applied first
% mmvStacksWExclT = [1:6 8 10 12 15 65 77 88 158 215 229];
%discard 94 and 106
mmvStacksWExclT = [1:6 8 10 12 15 88 158 215 229];
mmvStacksWExclV = 16:17;

mmvStacksT_orig = [1:6 10 13 15 18];
mmvStacksV_orig = 19:20;

switch type
    case 'membrane'
        options.data_pre = @(x)Codat.CNN.Segmentation.loadData(x, 1);
        %training data paths
        m = load('cortexTrainingDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksT = {stacks(:).stackFile};
        %test data path
        m = load('cortexTestDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksV = {stacks(1:10).targetFile};

    case 'membraneWeighted'
        options.data_pre = @(x)Codat.CNN.Segmentation.loadData(x, 6);
        % training data paths
        m = load('cortexTrainingDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksT = {stacks(:).stackFile};
        % test data path
        m = load('cortexTestDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksV = {stacks(1:10).targetFile};

    case 'membraneWeighted_r'
        options.data_pre = @(x)Codat.CNN.Segmentation.load_rMembrane(x, 3);
        recPath = ['/u/bstaffle/data/datasets/SegEM/cortexTrainingData' ...
            '/membranePred'];

        %training data paths
        m = load('cortexTrainingDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksT = {stacks(:).stackFile};
        idx = 1:length(m.stacks);
        idx = idx(~m.exclude);
        stacksT_r = arrayfun( ...
            @(x)sprintf('%s/training/Cube_%03d_recIn.mat', recPath, x), ...
            idx(:), 'UniformOutput', false);
        stacksT = num2cell([stacksT(:), stacksT_r(:)], 2);

        %test data path
        m = load('cortexTestDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksV = {stacks(1:10).targetFile};
        idx = 1:length(m.stacks);
        idx = idx(~m.exclude);
        idx = idx(1:10);
        stacksV_r = arrayfun( ...
            @(x)sprintf('%s/test/Cube_%03d_recIn.mat', recPath, x), ...
            idx(:), 'UniformOutput', false);
        stacksV = num2cell([stacksV(:), stacksV_r(:)], 2);

	case 'membraneWeightedsT'
        options.data_pre = @(x)Codat.CNN.Segmentation.loadData(x,6);
        %training data paths
        m = load(['/u/bstaffle/data/cortexTrainingData_BS_smallRaw/' ...
            'stacks.mat']);
        stacks = m.stacks(~m.exclude);
        stacksT = {stacks(:).targetFile};
        %test data path
        m = load(['/gaba/u/bstaffle/data/cortexTrainingData/' ...
            'cortexTestDataParameter.mat']);
        stacks = m.stacks(~m.exclude);
        stacksV = {stacks(1:10).targetFile};

    case 'membraneWeighted_sigmoid_sT'
        options.data_pre = ...
            @(x)Codat.CNN.Segmentation.loadData(x,6,'sigmoid',1);
        %training data paths
        m = load('cortexTrainingDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksT = {stacks(:).stackFile};
        %test data path
        m = load('cortexTestDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksV = {stacks(1:10).targetFile};

    case 'membraneRMMVWeighed'
        options.data_pre = @(x)Codat.CNN.Segmentation.loadDataRMMV(x, 8);
        %training data paths
        m = load('cortexTrainingDataParameterMMV.mat');
        stacks = m.stacks(~m.exclude);
        stacksT = {stacks(:).stackFile};
        %test data path
        m = load('cortexTestDataParameterMMV.mat');
        stacks = m.stacks(~m.exclude);
        stacksV = {stacks(1:10).targetFile};

    case 'membraneRMMVWeighedsT'
        options.data_pre = ...
            @(x)Codat.CNN.Segmentation.loadDataRMMV(x, 10, true);
        %training data paths
        m = load('cortexTrainingDataParameterMMV.mat');
        stacks = m.stacks(~m.exclude);
        stacksT = {stacks(:).stackFile};
        %test data path
        m = load('cortexTestDataParameterMMV.mat');
        stacks = m.stacks(~m.exclude);
        stacksV = {stacks(1:10).targetFile};

    case 'membraneSig'
        options.data_pre = ...
            @(x)Codat.CNN.Segmentation.loadData(x, 1, 'sigmoid');
        %training data paths
        m = load('cortexTrainingDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksT = {stacks(:).stackFile};
        %test data path
        m = load('cortexTestDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksV = {stacks(1:10).targetFile};

    case 'membraneWeightedSig'
        options.data_pre = ...
            @(x)Codat.CNN.Segmentation.loadData(x, 3, 'sigmoid');
        %training data paths
        m = load('cortexTrainingDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksT = {stacks(:).stackFile};
        %test data path
        m = load('cortexTestDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksV = {stacks(1:10).targetFile};

    case 'MMV'
        options.data_pre = @(x)Codat.CNN.Segmentation.loadDataMMV(x, 1);
        %training & test paths
        m = load('cortexTrainingDataParameterMMV.mat');
        stacks = m.stacks;
        stacksT = {stacks(mmvStacksT).stackFile};
        stacksV = {stacks(mmvStacksV).stackFile};

    case 'MMVWeighted_orig'
        options.data_pre = @(x)Codat.CNN.Segmentation.loadDataMMV(x, 6);
        %training & test paths
        m = load('cortexTrainingDataParameter.mat');
        stacks = m.stacks;
        stacksT = {stacks(mmvStacksT_orig).stackFile};
        stacksV = {stacks(mmvStacksV_orig).stackFile};

    case 'MMVWeighted'
        options.data_pre = @(x)Codat.CNN.Segmentation.loadDataMMV(x, 6);
        %training & test paths
        m = load('cortexTrainingDataParameter.mat');
        stacks = m.stacks;
        stacksT = {stacks(mmvStacksT).stackFile};
        stacksV = {stacks(mmvStacksV).stackFile};

    case 'MMVMasked'
        options.data_pre = @(x)Codat.CNN.Segmentation.loadDataMMV(x, 1);
        %training data paths
        m = load('cortexTrainingDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksT = [stacks(:).stackFile ...
            repmat({stacks(mmvStacksWExclT).stackFile}, ...
            1,ceil(length(stacks)/length(mmvStacksWExclT)) - 1)];
        stacksV = {stacks(mmvStacksWExclV).stackFile};
        %test data paths
        m = load('cortexTestDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksV = [stacksV {stacks(1:10).targetFile}];

    case 'MMVMaskWeigh'
        options.data_pre = @(x)Codat.CNN.Segmentation.loadDataMMV(x, 6);
        %training data paths
        m = load('cortexTrainingDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksT = [stacks(:).stackFile ...
            repmat({stacks(mmvStacksWExclT).stackFile}, 1, ...
            ceil(length(stacks)/length(mmvStacksWExclT)) - 1)];
        stacksV = {stacks(mmvStacksWExclV).stackFile};
        %test data paths
        m = load('cortexTestDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksV = [stacksV {stacks(1:10).targetFile}];

    case 'MMVMaskWeighSig'
        options.data_pre = ...
            @(x)Codat.CNN.Segmentation.loadDataMMV(x, 6, 'sigmoid');
        %training data paths
        m = load('cortexTrainingDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksT = [stacks(:).stackFile ...
            repmat({stacks(mmvStacksWExclT).stackFile}, 1, ...
            ceil(length(stacks)/length(mmvStacksWExclT)) - 1)];
        stacksV = {stacks(mmvStacksWExclV).stackFile};
        %test data paths
        m = load('cortexTestDataParameter.mat');
        stacks = m.stacks(~m.exclude);
        stacksV = [stacksV {stacks(1:10).targetFile}];

    case 'MMVWeightedSig'
        options.data_pre = ...
            @(x)Codat.CNN.Segmentation.loadDataMMV(x, 6, 'sigmoid');
        %training & test paths
        m = load('cortexTrainingDataParameter.mat');
        stacksT = {m.stacks(mmvStacksT).stackFile};
        stacksV = {m.stacks(mmvStacksV).stackFile};

    case 'Synapses'
        options.data_pre = @(x)Codat.CNN.Synapses.loadData(x, 1);
        m = load('psdTrainingData.mat');
        stacksT = {m.stacksT(:).stackFile};
        stacksV = {m.stacksV.stackFile};

    case 'PSD_sigmoid'
        options.data_pre = @(x)Codat.CNN.Synapses.loadDataSVM(x, [], ...
            'sigmoid', true);
        m = load('cortexTrainingDataSVM.mat');
        stacksT = {m.stacks.stackFile};
        stacksV = {m.stacks(1).stackFile};
        for i = 1:length(cnets)
            assert(cnets{i}.featureMaps(end) == 1, ...
                'Wrong number of output features maps in cnet.');
        end

    case 'PSD_softmax'
        options.data_pre = @(x)Codat.CNN.Synapses.loadDataSVM(x, [], ...
            'softmax', true);
        m = load('cortexTrainingDataSVM.mat');
        stacksT = {m.stacks.stackFile};
        stacksV = {m.stacks(1).stackFile};
        for i = 1:length(cnets)
            assert(cnets{i}.featureMaps(end) == 2, ...
                'Wrong number of output features maps in cnet.');
        end

    case 'SynapsesWeighted'
        options.data_pre = @(x)Codat.CNN.Synapses.loadData(x, 6, 0);
        m = load('psdTrainingData.mat');
        stacksT = {m.stacksT(:).stackFile};
        stacksV = {m.stacksV.stackFile};

    case 'SynapsesWeightedMasked'
        options.data_pre = @(x)Codat.CNN.Synapses.loadData(x, 6, 6);
        m = load('psdTrainingData.mat');
        stacksT = {m.stacksT(:).stackFile};
        stacksV = {m.stacksV.stackFile};

    case 'SynapsesOld'
        options.data_pre = @(x)Codat.CNN.Synapses.loadData(x, 1, 6);
        m = load('synapseVoxelTrainingDataParameterOld.mat');
        stacksT = {m.stacks(:).stackFile};
        stacksV = {m.stacks([12, 16, 17, 21, 44, 73, 96, 157]).stackFile};
        if isempty(varargin)
            varargin(1:2) = {'augment', true};
        else
            %cat to beginning to enable overwrite
            varargin(1:2) = [{'augment', true}, varargin];
        end

    case 'SVM'
        options.data_pre = @(x)Codat.CNN.Synapses.loadDataSVM(x);
        m = load('cortexTrainingDataSVM.mat');
        stacksT = {m.stacks.stackFile};
        stacksV = {m.stacks(1).stackFile};
        for i = 1:length(cnets)
            assert(cnets{i}.featureMaps(end) == 3, ...
                'Wrong number of output features maps in cnet.');
        end

    case 'SVM_softmax'
        options.data_pre = @(x)Codat.CNN.Synapses.loadDataSVM(x, [], 'softmax');
        m = load('cortexTrainingDataSVM.mat');
        stacksT = {m.stacks.stackFile};
        stacksV = {m.stacks(1).stackFile};
        for i = 1:length(cnets)
            assert(cnets{i}.featureMaps(end) == 4, ...
                'Wrong number of output features maps in cnet.');
            assert(strcmp(cnets{i}.lossFunction, 'softmax'), ...
                'Cnet output/loss should be softmax.');
        end
        varargin{end+1} = 'augment';
        varargin{end+1} = true;

    case 'SVM_distT'
        % synapse, vesicle, mito using the MMV and synapse voxel training
        % data from different cubes
        options.data_pre = ...
            @(x)Codat.CNN.Segmentation.loadDataSVM_distT(x, 6, 6);
        m = load('psdTrainingData.mat');
        stacksT = {m.stacksT(:).stackFile};
        stacksV = {m.stacksV.stackFile};
        m = load('cortexTrainingDataParameter.mat');
        stacks = m.stacks;
        stacksT = cat(2, stacksT, ...
            repmat({stacks(mmvStacksT).stackFile}, 1, ...
                floor(length(stacksT)/length(mmvStacksT))));
        stacksV = cat(2, stacksV, {stacks(mmvStacksV).stackFile});


    case 'membraneWeightedInterpolatedSphereDilated'
        % membrane weight: 6
        % upsampling: two-fold
        % small targets: yes + dilated with sphere

        sphereStrel = false(3, 3, 3);
        sphereStrel(2, 2, 1) = true;
        sphereStrel(2, 2, 3) = true;
        sphereStrel(:, 2, 2) = true;
        sphereStrel(2, :, 2) = true;

        sT = struct;
        sT.strel = strel(sphereStrel);

        options.data_pre = @(s) ...
            Codat.CNN.Segmentation.loadData(s, 6, [], sT, 1);

        dataDir = '/gaba/u/bstaffle/data/datasets/SegEM/cortexTrainingData';
        data = load(fullfile(dataDir, 'stacks.mat'));
        clear dataDir;

        % exclude some stacks
        stacksT = data.stacksTraining(~data.excludeTraining);
        stacksV = data.stacksTest(~data.excludeTest);
        clear data;

        % to file names
        stacksT = {stacksT(:).stackFile};
        stacksV = {stacksV(1:10).stackFile};

    case 'amotta_membraneWeightedInterpolated'
        % membrane weight: 6
        % upsampling: two-fold
        % small targets: yes

        options.data_pre = @(s) ...
            Codat.CNN.Segmentation.loadData(s, 6, [], true, 1);

        dataDir = '/gaba/u/amotta/data/datasets/SegEM/cortexTrainingData';
        data = load(fullfile(dataDir, 'stacks.mat'));
        clear dataDir;

        % exclude some stacks
        stacksT = data.stacksTraining(~data.excludeTraining);
        stacksV = data.stacksTest(~data.excludeTest);
        clear data;

        % to file names
        stacksT = {stacksT(:).stackFile};
        stacksV = {stacksV(1:10).stackFile};

    case 'amotta_membraneWeightedInterpolatedSphereDilated'
        % membrane weight: 6
        % upsampling: two-fold
        % small targets: yes + dilated with sphere

        sphereStrel = false(3, 3, 3);
        sphereStrel(2, 2, 1) = true;
        sphereStrel(2, 2, 3) = true;
        sphereStrel(:, 2, 2) = true;
        sphereStrel(2, :, 2) = true;

        sT = struct;
        sT.strel = strel(sphereStrel);

        options.data_pre = @(s) ...
            Codat.CNN.Segmentation.loadData(s, 6, [], sT, 1);

        dataDir = '/gaba/u/amotta/data/datasets/SegEM/cortexTrainingData';
        data = load(fullfile(dataDir, 'stacks.mat'));
        clear dataDir;

        % exclude some stacks
        stacksT = data.stacksTraining(~data.excludeTraining);
        stacksV = data.stacksTest(~data.excludeTest);
        clear data;

        % to file names
        stacksT = {stacksT(:).stackFile};
        stacksV = {stacksV(1:10).stackFile};

    case 'Myelin_sig'
        m = load('/gaba/u/bstaffle/code/pipeline/stacksMyelin.mat');
        stacksT = m.stacks(1:4);
        stacksV = m.stacks(5);
        options.data_pre = @(x)Codat.CNN.Segmentation.loadDataMyelin( ...
            x, 1, 'sigmoid');

    case 'Myelin_tanh'
        m = load('/gaba/u/bstaffle/code/pipeline/stacksMyelin.mat');
        stacksT = m.stacks(1:4);
        stacksV = m.stacks(5);
        options.data_pre = @(x)Codat.CNN.Segmentation.loadDataMyelin( ...
            x, 1, 'tanh');

    otherwise
        error('Unknown type %s.\n',type);
end

%define training options
Util.log('Training mode set to %s.', mode);
switch mode
    case 'largeCube'
        options.display = 100;
        options.tr_size = [50, 50, 50];
        options.tr_fwd_alg = 'fft2';
        options.val_size = [50, 50, 50];
        options.val_iter = 5000;
        options.val_fwd_alg = 'fft2';
        options.gpuDev = true;
        options.max_iter = 200000;
        options.augment = false;
        options.snapshot = 5000;
        options.save_imp = false;
        options.class_ratio = [];
        options.skip_single_class_cubes = false;
    case 'smallCube'
        options.display = 250;
        options.tr_size = [20, 20, 10];
        options.tr_fwd_alg = 'fft1';
        options.val_size = [50, 50, 50];
        options.val_iter = 5000;
        options.val_fwd_alg = 'fft2';
        options.gpuDev = true;
        options.max_iter = 200000;
        options.augment = false;
        options.snapshot = 5000;
        options.save_imp = false;
        options.class_ratio = [];
        options.skip_single_class_cubes = false;
    otherwise
        error('Unknown mode %s.', mode);
end

options.lr_policy = 'step';
options.step_size = 1000;
options.gamma = 0.97;
options.rot_inv = false;
options.white_noise_std = 0;

%parse name/value inputs to change standard options
nArgs = length(varargin);
optionNames = fieldnames(options);
if floor(nArgs/2) ~= nArgs/2
    error('Optional arguments have to be name/value pairs');
end
for pair = reshape(varargin,2,[])
    name = pair{1};
    if any(strcmp(name,optionNames))
        options.(name) = pair{2};
    else
        error('%s is not a recognized parameter name',name);
    end
end

%attach number of cnet in case of a single name
if ischar(snapFiles)
    tmp = snapFiles;
    snapFiles = cell(length(cnets),1);
    for i = 1:length(cnets)
        snapFiles{i} = [tmp '_' num2str(i)];
    end
elseif length(snapFiles) == 1 && length(cnets) > 1
    for i = 1:length(cnets)
        snapFiles{i} = [snapFiles{1} '_' num2str(i)];
    end
end

%submit jobs
for i = 1:length(cnets)
    cnet = cnets{i};
    options.snapshot_name = [Util.addFilesep(saveFolder), snapFiles{i}];
    jobs(i) = createJobRepo(cluster); %#ok<*AGROW>
    jobs(i).Name = [snapFiles{i} '_' num2str(jobs(i).ID)];
    t = createTask(jobs(i),@jobWrapper, 2, ...
        {cnet, stacksT, stacksV, options});
    t.CaptureDiary = true;
    submit(jobs(i));
    pause(10);
end

end

function [cnet, valLoss] = jobWrapper(cnet, stacksT, stacksV, options)
[~,hostname] = system('hostname');
options.hostname = hostname(1:end-1);
Util.log('Training on worker %s.', options.hostname);
Util.log('Nvidia status:');
try
    [~,smi_stat] = system('nvidia-smi');
    fprintf('%s', smi_stat);
catch
    Util.log('An error occurred while running ''nvidia-smi''');
end
selectEmptyGPUDev();
[~,smi_stat] = system('nvidia-smi');
fprintf('%s', smi_stat);
Util.log('Starting cnet training.');
try
    [cnet, valLoss] = cnet.train(stacksT, stacksV, options);
catch err
    %logging of nvidia-smi in case of error
    Util.log('An unexpected error occurred: %s.', err.message);
    Util.log('Nvidia status:');
    [~,smi_stat] = system('nvidia-smi');
    fprintf('%s', smi_stat);
    rethrow(err)
end
end
