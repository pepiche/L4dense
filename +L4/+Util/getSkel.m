function skel = getSkel()
%GETSKEL Empty skeleton for ex145_07x2.
% OUTPUT skel: skeleton object
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

skel = Skeleton.setParams4Dataset([], 'ex145_ROI2017');

end

