% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear;

%% Configuration
rootDir = '/gaba/u/mberning/results/pipeline/20170217_ROI';

dendFile = fullfile( ...
    rootDir, 'aggloState', ...
    'dendrites_wholeCells_02_v3_auto.mat');

% TODO(amotta): Get rid of that
tracingFiles = '/tmpscratch/sahilloo/L4/forIso/';
tracingFiles = fullfile(tracingFiles, { ...
    'spineHeadsToDendritesQueryState_1.mat', ...
    'spineHeadsToDendritesQueryState_2.mat'});

[~, outFile] = fileparts(dendFile);
outFile = sprintf('%s-and-manual.mat', outFile);
outFile = fullfile(fileparts(dendFile), outFile);

info = Util.runInfo();

%% Load dendrite state
param = load(fullfile(rootDir, 'allParameter.mat'));
param = param.p;

Util.log('Loading dendrite agglomerates');
dend = load(dendFile);
superAgglos = dend.dendrites;

% TODO(amotta): Sanity check
% SuperAgglo.check(superAgglos)

%% Load spine heads
Util.log('Loading spine heads');
shT = table;
shT.segIds = dend.shAgglos;
shT.edges = dend.shEdges;
shT.attached = (dend.attached > 0);

shT.id = reshape(1:size(shT, 1), [], 1);
shT(shT.attached, :) = [];

%% Loading tracings
Util.log('Loading tracings');

trT = struct( ...
    'nodes', {}, 'edges', {}, ...
    'segIds', {}, 'comment', {});

for curIdx = 1:numel(tracingFiles)
    curTracings = load(tracingFiles{curIdx});
    curTracings = curTracings.ff;

    curNodes = curTracings.nodes(:);
   [curNodeCount, ~] = cellfun(@size, curNodes);
    curComments = curTracings.comments(:);

    curEdges = arrayfun( ...
        @(n) horzcat((1:(n - 1))', (2:n)'), ...
        curNodeCount, 'UniformOutput', false);

    curSegIds = cellfun( ...
        @horzcat, ...
        curTracings.segIds(:), ...
        curTracings.neighbours(:), ...
        'UniformOutput', false);

    trT = vertcat( ...
        trT, struct( ...
            'nodes', curNodes, ...
            'edges', curEdges, ...
            'segIds', curSegIds, ...
            'comment', curComments)); %#ok
end

trT = struct2table(trT);

%% Actually running spine attachment
Util.log('Running spine attachment');

[superAgglos, trT] = ...
    connectEM.Spine.Head.attachWithTracings( ...
        config.param, superAgglos, shT, trT);
    
% TODO(amotta): Reenable check when possible
superAgglos = SuperAgglo.clean(superAgglos, false);

%% Build dendrite output
dendT = table;
dendT.parentId = (1:numel(superAgglos))';
dendT.superAgglo = superAgglos;

% Remove picked-up agglomerates
dendT(trT.otherId, :) = [];

%% Completing output
Util.log('Completing output');

out = struct;
out.dendrites = dendT.superAgglo;
out.parentIds = dendT.parentId;

% Translate indices
indFields = sort(fieldnames(dend));
indFields = indFields( ...
    startsWith(indFields, 'ind') ...
  | startsWith(indFields, 'idx'));

for curIdx = 1:numel(indFields)
    curName = indFields{curIdx};
    curVal = dend.(curName);
    curVal = curVal(out.parentIds);
    
    out.(curName) = curVal;
    clear curName curVal;
end

out.dendAgglos = ...
    Agglo.fromSuperAgglo(out.dendrites);
out.indBigDends = ...
    Agglo.isMaxBorderToBorderDistAbove( ...
        config.param, 5000, out.dendAgglos);

% Keep track of spine heads
out.shAgglos = dend.shAgglos;
out.shEdges = dend.shEdges;
out.edges = dend.edges;

out.attached = dend.attached;
out.attached(trT.shId) = trT.trunkId;

% Translate to new dendrite indices
[~, out.attached] = ismember( ...
    out.attached, out.parentIds);

out.info = info;

%% Saving result
Util.log('Writing output');
Util.saveStruct(outFile, out);
Util.protect(outFile);

Util.log('Done');
