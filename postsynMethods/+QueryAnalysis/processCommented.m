
setConfig;
config.outDir = '/tmpscratch/sahilloo/L4/forIso/';
outDir= config.outDir;
m=load(fullfile(outDir,'spineHeadsToDendritesQueryState.mat'));
ff = m.ff;

commented = ~cellfun(@isempty,vertcat(ff.comments));

ffC.nodes = ff.nodes(commented);
ffC.segIds = ff.segIds(commented);
ffC.startNode = ff.startNode(commented);
ffC.comments = ff.comments(commented);
ffC.filenames =ff.filenames(commented);
ffC.neighbours = ff.neighbours(commented);

unsolved = cellfun(@(x) any(regexp(x,'unsolved')),vertcat(ffC.comments{:}));

ffOut.nodes = ff.nodes(unsolved);
ffOut.segIds = ff.segIds(unsolved);
ffOut.startNode = ff.startNode(unsolved);
ffOut.comments = ff.comments(unsolved);
ffOut.filenames =ff.filenames(unsolved);
ffOut.neighbours = ff.neighbours(unsolved);

outDirNml = '/tmpscratch/sahilloo/L4/spineHeadQueriesToLook/';
if ~exist(outDirNml,'dir')
    mkdir(outDirNml);
end
outfiles = vertcat(ffOut.filenames);
idx = randperm(numel(outfiles),20);
cellfun(@(x) copyfile(x,outDirNml),outfiles(idx));

%%  generate new tasks from the ffOut.startNode
Util.log(['Found ' num2str(numel(ff.nodes)) ' tracings commented unsolved']);
info = Util.runInfo(false);
outFile = '/tmpscratch/sahilloo/L4/spineHeadQueries_unsolved.mat';
queryPoints = vertcat(ffOut.startNode{:});
save(outFile, 'queryPoints', 'info');

rng(0)
idxRand = randperm(size(queryPoints,1),100);   
outQPts = queryPoints(idxRand,:);
fileID = '/tmpscratch/sahilloo/L4/randomSpineQPoints.txt';
dlmwrite(fileID,outQPts);
