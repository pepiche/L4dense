function [ovL, recL] = aggloSkelPathLengthOverlap( skels, segIds, ...
    skelToAgglos, agglos, keepZeros )
%AGGLOSKELPATHLENGTHOVERLAP Calculate the skeleton path length that is
% covered by overlapping agglos. The length is calculated by discarding all
% nodes that are not within an agglo and calculating the path lenght of the
% remaining (possibly split) skeleton. Nodes with 0 ids are kept for each
% overlapping agglo (except components of all zero nodes or all nodes at
% the ends of a components).
% INPUT skels: [Nx1] cell of skeleton objects
%           The ground truth skeletons.
%       segIds: [Nx1] cell
%           The segment ids for the corresponding skeleton in skels.
%       skelToAgglos: [Nx1] cell
%           The indices of the agglos that contain skeleton nodes.
%           (see output of L4.Agglo.aggloSkelOverlap)
%       agglos: [Nx1] struct or cell
%           The agglomerates in the agglo or superagglo format.
%       keepZeros: (Optional) flag
%           Keep the zero nodes if they are connected to other nodes that
%           are contained in an agglo. If there is a component of zero
%           nodes only it will be discarded.
%           (Default: true)
% OUTPUT ovL: [Nx1] cell
%           Contains the overlapping length in the skeleton for each
%           overlapping agglo in skelToAgglos in um.
%        recL: double
%           Total recovered path length covered by all overlapping agglos.
%           This can deviate slightly from cellfun(@sum, ovL) due to the
%           zero id handling.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('keepZeros', 'var') || isempty(keepZeros)
    keepZeros = true;
end

ovL = cellfun(@(x)zeros(size(x, 1), 1), skelToAgglos, 'uni', 0);
recL = zeros(length(skels), 1);
if iscell(segIds{1})
    segIds = cellfun(@(x)x{1}, segIds, 'uni', 0);
end

for i = 1:length(skelToAgglos)
    
    % get segment ids in agglos
    if isstruct(agglos)
        aggloIds = Superagglos.getSegIds(agglos(skelToAgglos{i}));
    else
        aggloIds = agglos(i);
    end
    
    % for each agglo keep only nodes belonging to that agglo and calculate
    % the resulting path length
    for j = 1:size(skelToAgglos{i}, 1)
        skel = skels{i};
        if keepZeros
            toDelNodes = ~ismember(segIds{i}, cat(1, aggloIds{j}, 0));
        else
            toDelNodes = ~ismember(segIds{i}, aggloIds{j});
        end
        skel = skel.deleteNodes(1, toDelNodes);
        idsAfterDel = segIds{i}(~toDelNodes);
        [skel, ~, nodeIdx] = skel.splitCC();
        compSegIds = cellfun(@(x)idsAfterDel(x), nodeIdx, 'uni', 0);
        toDelTrees = cellfun(@(x)all(x == 0), compSegIds);
        skel = skel.deleteTrees(toDelTrees);
        compSegIds(toDelTrees) = [];
        
        % get rid of degree 0 nodes if at the end of a tree
        if keepZeros
            checkForZerosAtEnding = true;
            while checkForZerosAtEnding
                d = skel.calculateNodeDegree();
                toDelNodes = cellfun(@(deg, id)deg == 1 & id == 0, d, ...
                    compSegIds, 'uni', 0);
                if ~any(cellfun(@any, toDelNodes))
                    checkForZerosAtEnding = false;
                else
                    for k = 1:skel.numTrees()
                        if any(toDelNodes{k})
                            skel = skel.deleteNodes(k, toDelNodes{k});
                        end
                    end
                    compSegIds = cellfun(@(x, y)x(~y), ...
                        compSegIds, toDelNodes, 'uni', 0);
                end
            end
        end
        
        ovL{i}(j) = sum(skel.pathLength())./1000;
    end
    
    % the same thing for all agglos combined
    if nargout > 1
        skel = skels{i};
        aggloIds = cell2mat(aggloIds);
        if keepZeros
            toDelNodes = ~ismember(segIds{i}, cat(1, aggloIds, 0));
        else
            toDelNodes = ~ismember(segIds{i}, aggloIds);
        end
        skel = skel.deleteNodes(1, toDelNodes);
        idsAfterDel = segIds{i}(~toDelNodes);
        [skel, ~, nodeIdx] = skel.splitCC();
        compSegIds = cellfun(@(x)idsAfterDel(x), nodeIdx, 'uni', 0);
        toDelTrees = cellfun(@(x)all(x == 0), compSegIds);
        skel = skel.deleteTrees(toDelTrees);
        compSegIds(toDelTrees) = [];
        
        % get rid of degree 0 nodes if at the end of a tree
        if keepZeros
            checkForZerosAtEnding = true;
            while checkForZerosAtEnding
                d = skel.calculateNodeDegree();
                toDelNodes = cellfun(@(deg, id)deg == 1 & id == 0, d, ...
                    compSegIds, 'uni', 0);
                if ~any(cellfun(@any, toDelNodes))
                    checkForZerosAtEnding = false;
                else
                    for j = 1:skel.numTrees()
                        if any(toDelNodes{j})
                            skel = skel.deleteNodes(j, toDelNodes{j});
                        end
                    end
                    compSegIds = cellfun(@(x, y)x(~y), ...
                        compSegIds, toDelNodes, 'uni', 0);
                end
            end
        end
        
        recL(i) = sum(skel.pathLength())./1000;
    end
end


end

