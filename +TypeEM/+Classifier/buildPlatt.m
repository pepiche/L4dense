function platt = buildPlatt(a, b)
    % platt = buildPlatt(a, b)
    %   Returns a closure which can be used to transform
    %   classifier outputs into probability estimates using
    %   Platt scaling.
    %
    %   Check TypeEM.Classifier.learnPlattParams for details.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    platt = @(s) 1 ./ (1 + exp(s .* a + b));
end
