function [out, cubeIds] = forSegsByCube(param, segIds, func)
    % out = forSegsByCube(param, segIds, func)
    %   Groups the segments in `segIds` by segmentation cube and applies
    %   the function `func` to each of these groups.
    %
    % Input arguments
    %   param
    %     Parameter structure
    %
    %   segIds
    %     A n-dimensional matrix with global segment IDs.
    %
    %   func
    %     Handle of a function which accepts
    %     1) the cube index, and
    %     2) a list of segment IDs residing within said cubes.
    %
    % Output arguments
    %   out
    %     A column vector with one entry per segmentation cube
    %
    %   cubeIds
    %     A column vector of the same size as `out`. It contains the linear
    %     segmentation cube IDs for the results in `out`.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    t = table();
    t.segId = segIds(:);
    t.cubeIdx = cubeIdsForSegs(param.saveFolder, t.segId);
    
    % split into groups
    t = sortrows(t, 'cubeIdx');
    cubeIds = unique(t.cubeIdx);
    cubeIds = cubeIds(:);
    
    % apply function
    closure = @(curIdx) func(curIdx, t.segId(t.cubeIdx == curIdx)); 
    out = arrayfun(closure, cubeIds, 'UniformOutput', false);
end

function cubeIds = cubeIdsForSegs(rootDir, segIds)
    % load meta data
    metaFile = fullfile(rootDir, 'segmentMeta.mat');
    meta = load(metaFile, 'segIds', 'cubeIdx');
    
    [found, row] = ismember(segIds, meta.segIds);
    
    assert(all(found));
    cubeIds = meta.cubeIdx(row);
end