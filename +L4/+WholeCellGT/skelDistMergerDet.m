function mergers = skelDistMergerDet( skels, p, agglos, edges, ...
    borderCom, skelToAgglos, distT, scale, bboxData, bboxExclSoma )
%SKELDISTMERGERDET Detection potential mergers of axons overlapping with a
% skeleton based on the distance of borderComs along the axon to the
% skeleton nodes. If there are several input skels also segments that are
% hit by multiple skeletons are marked.
% INPUT skels: [Nx1] cell
%           Cell array of skeleton objects.
%       p: struct
%           Segmentation parameter struct.
%           An [Nx1] cell array of segment ids can be supplied instead
%           (i.e. the result of
%            Skeleton.getSegmentIdsOfSkelCellArray(p, skels)).
%       agglos: [Nx1] struct cell
%           Agglos in the agglo or superagglo format.
%       edges: [Nx2] int
%           Global edge list.
%       borderCom: [Nx3] double
%           List of global border coms.
%       skelToAgglos: [Nx1] cell
%           Cell array containing the overlapping agglos for the
%           corresponding skeleton.
%       distT: (Optional) double
%           Distance threshold from skeleton for heuristic merger
%           detection in units of scale.
%           (Default: 3e3)
%       scale: (Optional) [1x3] double
%           Voxel scaling to physical units.
%           (Default: [1, 1, 1])
%       bboxData: (Optional) [3x2] int
%           Bounding box to which the borderComs are restricted.
%       bboxExclSoma: (Optional) [Nx1] cell of [3x2] int
%           Bounding box for each skeleton to discard interfaces close to
%           the soma.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

maxId = max(edges(:));
if exist('bboxData', 'var') && ~isempty(bboxData)
    idx = Util.isInBBox(borderCom, bboxData);
    borderCom = borderCom(idx, :);
    edges = edges(idx, :);
end

if ~exist('scale', 'var') || isempty(scale)
    scale = [1, 1, 1];
end
scale = scale(:)';

if isstruct(p)
    segIds = Skeleton.getSegmentIdsOfSkelCellArray(p, skels);
else
    segIds = p;
end
if iscell(segIds{1})
    segIds = cellfun(@(x)x{1}, segIds, 'uni', 0);
end

if isstruct(agglos)
    agglos = Superagglos.getSegIds(agglos);
end
agglos = cellfun(@sort, agglos, 'uni', 0);

aggloLUT = L4.Agglo.buildLUT(agglos, maxId);
segIdsMapped = segIds;
for i = 1:length(segIds)
    segIdsMapped{i}(segIdsMapped{i} > 0) = ...
        aggloLUT(segIdsMapped{i}(segIdsMapped{i} > 0));
end

% find mergers between skeletons
Util.log('Determine inter-skeleton mergers.');
lut = L4.Agglo.buildLUT(cellfun(@(x)setdiff(x, 0), segIdsMapped, ...
    'uni', 0), [], true);
mergerIds = find(cellfun(@length, lut) > 1);
mergers.interSkelMergers = table(mergerIds, lut(mergerIds), ...
    'VariableNames', {'AggloIdx', 'SkeletonIdx'});

Util.log('Heuristic merger detection.')
if ~exist('skelToAgglos', 'var') || isempty(skelToAgglos)
    skelToAgglos = L4.Agglo.aggloSkelOverlap(segIds, [], agglos);
end


[comps, id] = cellfun(@(x)Util.group2Cell(x), segIdsMapped, 'uni', 0);
for i = 1:length(comps)
    comps{i}(id{i} == 0) = [];
    id{i}(id{i} == 0) = [];
    comps{i}(~ismember(id{i}, skelToAgglos{i}(:,1))) = [];
    id{i}(~ismember(id{i}, skelToAgglos{i}(:,1))) = [];
end

mergers.agglos = cell(length(skels), 1);
mergers.borderCom = cell(length(skels), 1);
edgeLUT = Seg.Global.EdgeLookup(edges);
for i = 1:length(skels)
    if exist('bboxExclSoma', 'var') && ~isempty(bboxExclSoma)
        isNearSoma = find(Util.isInBBox(borderCom, bboxExclSoma{i}));
    else
        isNearSoma = [];
    end
    for j = 1:length(skelToAgglos{i}(:,1))
        thisEdgeIdx = setdiff(edgeLUT.edgeIdx(agglos{id{i}(j)}, false), ...
            isNearSoma);
        bCom = borderCom(thisEdgeIdx, :);
        d = pdist2(bsxfun(@times, double(bCom), scale), ...
            bsxfun(@times, skels{i}.nodes{1}(comps{i}{j}, 1:3), scale));
        d = min(d, [], 2);
        if max(d) > distT
            mergers.agglos{i}(end+1) = id{i}(j);
            mergers.borderCom{i}{j} = bCom(d > distT, :);
        end
    end
end

end

