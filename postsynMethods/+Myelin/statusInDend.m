% check myelin state in dendrites
setConfig;
m=load(config.param);
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/aggloState/dendrites_16.mat');
dendrites = m.dendrites(m.indBigDends);
dendrites = Superagglos.getSegIds(dendrites);

myelinDend = m.myelinDend(m.indBigDends);
outDirNmls = '/tmpscratch/sahilloo/L4/dataPostSyn/myelinDend/';
if ~exist(outDirNmls,'dir')
    mkdir(outDirNmls);
end

thr = 0.2;
outDendIdx = myelinDend > thr;
outDend = dendrites(outDendIdx);
Util.log(['Found ' num2str(sum(outDendIdx)) ' myelinDend with thr ' num2str(thr)])

Util.log('Now writing out the nmls')
rng(0);
idxToPlot = randperm(sum(outDendIdx),20);

for i = idxToPlot
    thisD = outDend(i);

    skel = Skeleton.fromMST({points(thisD{:},:)},voxelSize);
    skel = Skeleton.setParams4Pipeline(skel, p);
    skel.write(fullfile(outDirNmls, ['myDend_' ...
        num2str(i) '.nml']));
end


