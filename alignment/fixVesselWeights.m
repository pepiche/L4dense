function weight_col = fixVesselWeights(weight_col_temp, conts, thresholds)
weight_col = weight_col_temp;
for typeIt = 1 : 2
    for dimIt = 1 : 3
        ref1 = [1, 1, 1; size(conts{typeIt})];
        ref2 = [1, 1, 1; size(conts{typeIt})];
        ref1(2, dimIt) = ref1(2, dimIt) - 1;
        ref2(1, dimIt) = ref2(1, dimIt) + 1;
        vesselcont_ref1 = conts{typeIt}(ref1(1, 1) : ref1(2, 1), ref1(1, 2) : ref1(2, 2), ref1(1, 3) : ref1(2, 3));
        vesselcont_ref2 = conts{typeIt}(ref2(1, 1) : ref2(2, 1), ref2(1, 2) : ref2(2, 2), ref2(1, 3) : ref2(2, 3));
        vesselcont_thres{dimIt} = (vesselcont_ref1 > thresholds(typeIt)) & (vesselcont_ref2 > thresholds(typeIt));
    end
    
    cutpattern{1} = false(35, 24, 3424);
    cutpattern{1}([12, 24], :, :) = true;
    cutpattern{2} = false(36, 23, 3424);
    cutpattern{2}(:,[8, 16], :) = true;
    
    for dimIt = 1:2
        vesselcont_thres{dimIt} = vesselcont_thres{dimIt} & cutpattern{dimIt};
    end
    
    for dimIt = 1 : 3
        weight_col{dimIt}(vesselcont_thres{dimIt}) = 1E-7;
    end
end