% take the axon cut wholecells state and add dendrites to it
setConfig;
load(config.param);
    
outputFolder = fullfile(p.saveFolder, 'aggloState');
info = Util.runInfo();
    
m=load(fullfile(outputFolder,'wholeCells_GTAxon_08_v4.mat'));
wholeCellsNoAxon = m.wholeCellsNoAxon;
if isrow(wholeCellsNoAxon)
   wholeCellsNoAxon = wholeCellsNoAxon'; 
end
wholeCellsNoAxon = SuperAgglo.clean(wholeCellsNoAxon);

% use dendrite state in which indAxon was added
m=load(fullfile(outputFolder,'dendrites_18_b.mat'));
dendrites = m.dendrites;
indBigDends = m.indBigDends;
indAIS = m.indAIS;
idxAIS = m.idxAIS;

Util.log('concatenate truncated whole cells with dendrite class and make new state');
out = struct;
out.indWholeCells = cat(1,false(numel(dendrites),1),true(numel(wholeCellsNoAxon),1));
out.dendrites = cat(1,dendrites,wholeCellsNoAxon);
out.indBigDends = cat(1,indBigDends,true(numel(wholeCellsNoAxon),1));
out.indAIS = cat(1,indAIS,false(numel(wholeCellsNoAxon),1));
out.idxAIS = cat(1,idxAIS,zeros(numel(wholeCellsNoAxon),1));
out.idxWholeCells = cat(1,zeros(numel(dendrites),1),[1:numel(wholeCellsNoAxon)]');
out.info = info;

% NOTE(amotta): Perform basic sanity check
% This currently fails due to an overlap between super-agglomerates 119500 (
% non-whole cell axon initial segment) and 1815645 (whole cell).
%SuperAgglo.check(out.dendrites)

Util.log('Saving dendrites and wholecells state...');
Util.saveStruct(fullfile(outputFolder,'dendrites_wholeCells_02_b.mat'),out);
