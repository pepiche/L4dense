function deleteFeature(obj, idx)
%DELETEFEATURE Delete the specified features.
% INPUT idx: [Nx1] int
%           The indices of the features to delete. The index couting starts
%           with the texture features and continues with the shape
%           features.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

numFeatTexture = length(obj.featTexture);
toDelTexture = idx(idx <= numFeatTexture);
toDelShape = idx(idx > numFeatTexture) - numFeatTexture;

% delete texture features
obj.featTexture(toDelTexture) = [];

% delete shape features
obj.featShape(toDelShape) = [];
obj.featShapeDirDep(toDelShape) = [];
obj.featShapeSubvols(toDelShape) = [];

% delete selected features
obj.selectedFeat(idx) = [];

% update border and number of features
obj.calculateBorder();
obj.calculateSize();
obj.numFeaturesSelected = sum(cellfun(@(x)sum(x(:)), obj.selectedFeat));

end
