function idx_out = viewSmoothDendritesAllInRange(preScoreRange,postScoreRange,wkFlag)
% findSmoothDendrites without myelin with thresholds on typeEM score and pre-post score
% preScoreRange = {min , max};
% postScoreRange = {min , max};
% wkFlag = boolean to specify creating nmls or not
if ~exist('wkFlag','var') || isempty(wkFlag)
    wkFlag = false;
end

outDir = '/tmpscratch/sahilloo/L4/data/';
m = load('/gaba/u/mberning/results/pipeline/20170217_ROI/allParameter.mat');
p = m.p;
points = Seg.Global.getSegToPointMap(p);                                                                                                
voxelSize = p.raw.voxelSize;

% load smooth dendrites results
m=load(fullfile(outDir,'dendritesSmoothState.mat'));
dendrites = m.dendrites;
idx_smooth = m.idx_smooth';

% load myelin fraction scores
n=load(fullfile(outDir,'fracMyelinAggloBorder_Dend.mat'));
myFrac = n.fracMyelinAggloBorder_Dend;
idx_non_myelin = myFrac<0.1;

m=load(fullfile(outDir,'spineDistribution.mat'));
spineTable = m.spineTable;
% add myFrac to spineTable
spineTable = [spineTable table(myFrac)];

idx_longPathLength = spineTable{:,2}>10;

%load typeEM scores for agglos
m=load(fullfile(outDir,'allAgglos_typeEM_scores.mat'));
typeEMscores =m.scores;  
thr = 0.5;
g=[typeEMscores(:).astrocyte];
a=[typeEMscores(:).axon];
d=[typeEMscores(:).dendrite];
% idx_out = d>thr & g<thr & a<thr; % absolute thr
idx_typeEM = (d>a & d>g)';% relative thr

% load post vs pre interface scores
if exist(fullfile(outDir,'dendritesPrePostInterfaceScores.mat'),'file')
    m=load(fullfile(outDir,'dendritesPrePostInterfaceScores.mat'));
else
    m.scores = Util.findPrePostInterfaceScoresDendrites(); 
end
post = [m.scores(:).post]';
pre  = [m.scores(:).pre]';
% default all true
preL = true(length(pre),1);
preU = preL;
postL = true(length(post),1);
postU = postL;
if ~isempty(preScoreRange{1})
    preL = pre > preScoreRange{1};
end
if ~isempty(preScoreRange{2})
    preU = pre < preScoreRange{2};
end
if ~isempty(postScoreRange{1})
    postL = post > postScoreRange{1};
end
if ~isempty(postScoreRange{2})
    postU = post < postScoreRange{2};
end
idx_score = preL & preU & postL & postU;

if isrow(idx_score)
    idx_score = idx_score';
end

clear myFrac m n

%% agglos smooth non myelin with path lengths > 10um
rng(0);
idx_out = idx_smooth & idx_non_myelin & idx_longPathLength & idx_typeEM & idx_score;
if any(idx_out) && wkFlag
    table_out  = spineTable(idx_out,:);
    scores = typeEMscores(idx_out);

    idxToPlot = randperm(size(table_out,1),20);
    for i=1:length(idxToPlot)
        nodes = table_out{idxToPlot(i),1};
        if numel(table_out{idxToPlot(i),1}{1})==1
            fprintf('DendritesAgglo %d is single segId \n',idxToPlot(i));
            continue;
        end
        skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
        skel.names(1) = {['- sd:' num2str(table_out{idxToPlot(i),5}) ' - '...
            num2str(table_out{idxToPlot(i),2}) 'um ' '- my:' ...
            num2str(table_out{idxToPlot(i),6}) ' - ' ...
            num2str(scores(idxToPlot(i)).astrocyte,'%.2f') '/' ...
            num2str(scores(idxToPlot(i)).axon,'%.2f') '/' ...
            num2str(scores(idxToPlot(i)).dendrite,'%.2f')]};
        skel = Skeleton.setParams4Pipeline(skel, p);
        skel.write(fullfile(outDir, ['nmls/dendritesAllIn_' num2str(i) '_' datestr(now,'yyyymmdd') '.nml']));
    end
else
    fprintf('Agglos smooth have nothing to meet your conditions! or wkFlag was false \n');
end

% agglos with non zero, low spine density and long path lengths > 10um
rng(0);
idx_lowSpineDensity = spineTable{:,5}<0.25 & spineTable{:,5}~=0;
idx_out = idx_lowSpineDensity & idx_non_myelin & idx_longPathLength & idx_typeEM & idx_score;
if any(idx_out) && wkFlag
    table_out  = spineTable(idx_out,:);
    scores = typeEMscores(idx_out);

    idxToPlot = randperm(size(table_out,1),20);
    for i=1:length(idxToPlot)
        nodes = table_out{idxToPlot(i),1};
        if numel(table_out{idxToPlot(i),1}{1})==1
            fprintf('DendritesAgglo %d is single segId \n',idxToPlot(i));
            continue;
        end
        skel = Skeleton.fromMST({points(nodes{:},:)},voxelSize);
        skel.names(1) = {['- sd:' num2str(table_out{idxToPlot(i),5}) ' - '...
            num2str(table_out{idxToPlot(i),2}) 'um ' '- my:' ...
            num2str(table_out{idxToPlot(i),6}) ' - ' ...
            num2str(scores(idxToPlot(i)).astrocyte,'%.2f') '/' ...
            num2str(scores(idxToPlot(i)).axon,'%.2f') '/' ...
            num2str(scores(idxToPlot(i)).dendrite,'%.2f')]};
        skel = Skeleton.setParams4Pipeline(skel, p);
        skel.write(fullfile(outDir, ['nmls/dendritesAllIn_low_' num2str(i) '_' datestr(now,'yyyymmdd') '.nml']));
    end
else
    fprintf('Agglos low spine density have nothing to meet your conditions! or wkFlag was false \n');    
end

end
