function files = getFile(files)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    thisDir = fileparts(mfilename('fullpath'));
    files = fullfile(thisDir, files);
end
