function [idxNew,aggloNew] = findAggloAcrossVersions(p,agglosOld,agglosNew)
% find corresponding agglo idx in newer version

if isfield(agglosNew,'nodes')
    agglosNew = Superagglos.getSegIds(agglosNew);
end

if isfield(agglosOld,'nodes')
    agglosOld = Superagglos.getSegIds(agglosOld);
end

maxSegId = Seg.Global.getMaxSegId(p);
lut = Agglo.buildLUT(maxSegId,agglosNew);

idxNew = cellfun(@(x) mode(nonzeros(lut(x))),agglosOld); % zeros of shAgglos possible
end
