function conn_matrix = cleanupShifts_makeConnMatrix()
conn_matrix = zeros(36 * 24);
for m = 1 : 36
    for n = 1 : 24
        if m < 36
            conn_matrix((n-1) * 36 + m, (n-1) * 36 + m + 1) = 1;
        end
        if n < 24
            conn_matrix((n-1) * 36 + m, n * 36 + m) = 1;
        end
    end
end
conn_matrix = conn_matrix + conn_matrix' + eye(24 * 36);