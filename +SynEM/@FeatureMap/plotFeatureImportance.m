function plotDat = plotFeatureImportance( obj, imp, plotNo, noGrouping, ...
    splitChannels, useFeatNames )
%PLOTFEATUREIMPORTANCE Plot feature importance.
% INPUT imp: [Nx1] double where N = obj.numFeatures and contains a positive
%           number measuring the importance of each feature (e.g. from the
%           return from predictorImportance).
%       plotNo: (Optional) [Nx1] int array which plots to produce (see
%           code below).
%           (Default: plots all implemented importance plots)
%       noGrouping: (Optional) logical
%           Flag to indicate that each feature in the feature map is
%           treataed separately and no grouping is done based on the feature
%           class (e.g. structure tensor).
%           (Default: false)
%       splitChannels: (Optional) logical
%           Split the channels of texture features into separate features.
%           (Default: false)
%       useFeatNames: (Optional) logical
%           Use the names of features instead of the class. Note that in
%           case features of the same class are grouped only the name of
%           the first feature of that group will be used.
%           (Default: false)
% OUTPUT plotDat: struct
%           Structure containing the data that was plotted.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('plotNo','var') || isempty(plotNo)
    plotNo = 1:6;
end
makePlot = false(6,1);
makePlot(plotNo) = true;

if ~exist('noGrouping','var') || isempty(noGrouping)
    noGrouping = false;
end

if ~exist('useFeatNames', 'var') || isempty(useFeatNames)
    useFeatNames = false;
end

if isrow(imp)
    imp = imp';
end

obj.setSelectedFeat(true(obj.numFeatures,1));

% structure the feature importance for each feature in the same way as the
% selected features:
% texture features: matrix with numSumStats x numSubvols x channels
% shape features: channels x 1
imp = mat2cell(imp,cellfun(@numel,obj.selectedFeat));
imp = cellfun(@(x,y)reshape(x,size(y)),imp,obj.selectedFeat, ...
    'UniformOutput',false);

featTextClass = cellfun(@(x)strsplit(class(x),'.'), ...
    obj.featTexture, 'UniformOutput', false);
featTextClass = cellfun(@(x)x{end},featTextClass,'UniformOutput',false);
featShapeClass = cellfun(@(x)strsplit(class(x),'.'), ...
    obj.featShape,'UniformOutput',false);
featShapeClass = cellfun(@(x)x{end},featShapeClass,'UniformOutput',false);
featTextName = cellfun(@(x)x.name, obj.featTexture, 'uni', 0);
featShapeName = cellfun(@(x)x.name, obj.featShape, 'uni', 0);

if exist('splitChannels', 'var') && splitChannels
    % split imp by channels (= 3rd dimension)
    imp(1:length(featTextClass)) = cellfun(@(x)num2cell(x, 1:2), ...
        imp(1:length(featTextClass)), 'uni', 0);
    imp = cellfun(@squeeze, imp, 'uni', 0);
    imp = vertcat(imp{:});
    
    % add text feat class for splitted ones with channel number
    noChannel = cellfun(@(x)x.numChannels, obj.featTexture);
    featTextClassNew = cell(sum(noChannel), 1);
    featTextNameNew = cell(sum(noChannel), 1);
    count = 1;
    for i = 1:length(noChannel)
        if noChannel(i) > 1
            for j = 1:noChannel(i)
                featTextClassNew{count} = ...
                    sprintf('%s\\_c%d', featTextClass{i}, j);
                featTextNameNew{count} = ...
                    sprintf('%s\\_c%d', featTextName{i}, j);
                count = count + 1;
            end
        else
            featTextClassNew{count} = featTextClass{i};
            featTextNameNew{count} = featTextName{i};
            count = count + 1;
        end
    end
    featTextClass = featTextClassNew;
    featTextName = featTextNameNew;
end
noFText = length(featTextClass);

% determine features of same class
if noGrouping
    groupNamesText = featTextClass;
    groupNamesShape = featShapeClass;
    textGroup = (1:length(groupNamesText))';
    shapeGroup = (1:length(featShapeClass))' + max(textGroup);
    if useFeatNames
        groupNamesText = featTextName;
        groupNamesShape = featShapeName;
    end
else
    [groupNamesText, idx1, textGroup] = unique(featTextClass, 'stable');
    [groupNamesShape, idx2, shapeGroup] = unique(featShapeClass, 'stable');
    shapeGroup = shapeGroup + max(textGroup);
    if useFeatNames
        groupNamesText = featTextName(idx1);
        groupNamesShape = featShapeName(idx2);
    end
end
groupNames = cat(1, groupNamesText, groupNamesShape);

% %single feature importance
% figure;
% groups = [textGroup; shapeGroup];
% impTmp = cellfun(@(x)x(:),imp,'UniformOutput',false);
% count = 1;
% for i = 1:max(groups)
%     currImp = cell2mat(impTmp(groups == i));
%     barh(count:count+length(currImp)-1,currImp,'EdgeAlpha',0);
%     hold on
%     count = count + length(currImp);
% end

%total feature importance
totalImp = cellfun(@(x)sum(x(:)),imp);
totalImp = accumarray([textGroup; shapeGroup], totalImp);
if makePlot(1)
    figure;
    barh(flip(totalImp(totalImp > 0)));
    h = gca;
    h.YTick = 1:(sum(totalImp > 0));
    h.YTickLabel = flip(groupNames(totalImp > 0));
    title('Feature class importance')
end

%total feature importance sorted
if makePlot(2)
    figure;
    [~,sortInd] = sort(totalImp, 'descend');
    barh(totalImp(sortInd));
    h = gca;
    h.YTick = 1:length(totalImp);
    h.YTickLabel = groupNames(sortInd);
    title('Feature class importance')
    plotDat.imp = totalImp(sortInd);
    plotDat.label = groupNames(sortInd);
end

%subvolume importance (subseg combined)
if makePlot(3)
    if obj.numSubvolumes > 0
        figure;
        subvolImp = cellfun(@(x)sum(sum(x,1),3), ...
            imp(1:length(featTextClass)), 'UniformOutput',false);
        subvolImp = cell2mat(subvolImp);

        %combine subsegments of subvolumes
        subvolImp = mat2cell(subvolImp,ones(1,size(subvolImp,1)), ...
            [1 2.*ones(1,obj.numSubvolumes)]);
        subvolImp = cellfun(@(x)sum(x(:)),subvolImp);
        subvolImpGroup = zeros(max(textGroup),1 + obj.numSubvolumes);
        for i = 1:max(textGroup)
            subvolImpGroup(i,:) = sum(subvolImp(textGroup == i,:),1);
        end
        barh(subvolImpGroup,'stacked');
        h = gca;
        h.YTick = 1:(length(groupNamesText));
        h.YTickLabel = groupNamesText;
        title('Subvolume importance')
        subvolNames = arrayfun(@(x)['Subvol', num2str(x)],...
            1:obj.numSubvolumes,'UniformOutput',false);
        legend('Surface',subvolNames{:});
    end
end

%summary statistic importance
if makePlot(4)
    figure;
    sumStatImp = cellfun(@(x)sum(sum(x,2),3)', ...
        imp(1:length(featTextClass)), 'UniformOutput',false);
    sumStatImp = cell2mat(sumStatImp);
    sumStatImpGroup = zeros(max(textGroup),4 + length(obj.quantiles));
    for i = 1:max(textGroup)
        sumStatImpGroup(i,:) = sum(sumStatImp(textGroup == i,:),1);
    end
    barh(sumStatImpGroup,'stacked');
    h = gca;
    h.YTick = 1:(length(groupNamesText));
    h.YTickLabel = groupNamesText;
    title('Summary statistic importance')
    sumStatNames = arrayfun(@(x)['SumStat', num2str(x)],...
        1:(4 + length(obj.quantiles)),'UniformOutput',false);
    legend(sumStatNames{:});
end

% subsegment (=pre/post) importance
if makePlot(5)
    figure;
    subsegImp = cellfun(@(x)sum(sum(x, 1), 3), imp(1:noFText), 'uni', 0);
    subsegImp = cellfun(@(x)[x(1) sum(x(2:2:end)) sum(x(3:2:end))], ...
        subsegImp, 'uni', 0);
    subsegImp = cell2mat(subsegImp);
    subsegImpGroup = zeros(max(textGroup), 3);
    for i = 1:max(textGroup)
        subsegImpGroup(i,:) = sum(subsegImp(textGroup == i,:), 1);
    end
    barh(subsegImpGroup,'stacked');
    h = gca;
    h.YTick = 1:(length(groupNamesText));
    h.YTickLabel = groupNamesText;
    title('Subsegment importance')
    subvolNames = {'border', 'subseg1 (dir-mode: pre)', ...
        'subseg2 (dir-mode: post)'};
    legend(subvolNames{:});
end

%subvolume importance 
if makePlot(6)
    if obj.numSubvolumes > 0
        figure;
        subvolImp = cellfun(@(x)sum(sum(x,1),3), ...
            imp(1:length(featTextClass)), 'UniformOutput',false);
        subvolImp = cell2mat(subvolImp);
        subvolImp = sum(subvolImp, 1);
        
        barh(subvolImp,'stacked');
        h = gca;
        h.YTick = 1:1+2*obj.numSubvolumes;
        v = arrayfun(@(x)sprintf('v%d', x), 1:obj.numSubvolumes, 'uni', 0);
        s = {'s1', 's2'};
        c = cellfun(@(x) {[x s{1}], [x s{2}]}, v, 'uni', 0);
        c = horzcat(c{:});
        h.YTickLabel = {'v0', c{:}};
        title('Subvolume importance')
    end
end


end

