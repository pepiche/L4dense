function cdf = bbinocdf(x, n, a, b, mode)
%BBINOCDF Beta-binomial cdf from 0 to x.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if nargin < 5
    mode = 'lower';
end

switch mode
    case 'lower'
        cdf = arrayfun(@(y)sum(Math.Prob.bbinopdf(0:min(y, n), n, a, b)), x);
    case 'upper'
        cdf = arrayfun(@(y)sum(Math.Prob.bbinopdf((y+1):n, n, a, b)), x);
end

end

