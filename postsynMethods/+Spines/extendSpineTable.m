function spineTable = extendSpineTable(config,spineTable)
% Description:
%   Collect all dendrite agglos, their corresponding lengths, spines. spine density

outDir = config.outDir;                                                                                     
outfile = fullfile(outDir,'spineTableExt.mat');

m = load(config.param);
p = m.p;

% load synapses
Util.log('Loading synapses...')
m=load(config.synapseFile);
synapses = m.synapses;

Util.log('Now finding input synapses for dendrites')
presynAgglos = {};
postsynAgglos = spineTable{:,1};

[ ~, ~, post2syn, ~ ] = ...
            L4.Synapses.synapsesAggloMapping( synapses, presynAgglos, postsynAgglos );
inputSynapses = post2syn;

% load synapse relabels
m=load(fullfile(outDir,'synapseRelabels.mat'));
synapseRelabels = m.synapseRelabels;

% extend dendrite table by adding input synapses and their labels                                                               
inputSynapseLabels = cellfun(@(x) synapseRelabels(x),inputSynapses,'uni',0);

spineTable = [spineTable table(inputSynapses,inputSynapseLabels)];

Util.log('Now saving results of spineTable')
info = Util.runInfo(false);
save(outfile,'spineTable','info');

end
