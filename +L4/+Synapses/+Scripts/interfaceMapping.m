% mapping of all detected synem interfaces after postprocessing
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


%% get synapse agglomerates

p = Gaba.getSegParameters('ex145_ROI2017');
axons = L4.Axons.getLargeAxons(p, true, true);
[synapses, isSpineSyn, heuristics, debug] = ...
    L4.Synapses.createSynapseAgglomerates(p, [], axons);


%% create initial synapse interface mapping

[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
synIdxPP = debug.synIdxPostProc;
synIdxPPC = debug.synIdxCenter;
maxId = max(graph.edges(:));

% pre/post in center
tmp = graph.synScores(synIdxPPC, 1) >= graph.synScores(synIdxPPC, 2);
synPPC = graph.edges(synIdxPPC, :);
prePPC = synPPC([tmp, ~tmp]);
postPPC = synPPC([~tmp, tmp]);

% pre post outside
idx = synIdxPP & ~synIdxPPC;
tmp = graph.synScores(idx, 1) >= graph.synScores(idx, 2);
synPPC = graph.edges(idx, :);
prePP = synPPC([tmp, ~tmp]);
postPP = synPPC([~tmp, tmp]);

synComps = cat(1, {prePP}, {prePPC}, {postPP}, {postPPC});
zeroComp = setdiff(0:maxId, cell2mat(synComps))';
components = cat(1, {zeroComp}, synComps);
WK.makeWKMapping(components, 'SynInterfaces_v3');