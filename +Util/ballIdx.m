function [ lidx, ballSize ] = ballIdx( r, targetCubeSize, voxelSize)
%BALLIDX Get relative linear indices of a 3d ball of radius r.
% INPUT r: Double ball radius.
%       targetCubeSize: [1x3] int specifying the size of the target cube in
%           which the linear indices are valid.
%       voxelSize: (Optional) [3x1] double specifying the voxel size in each
%           dimension.
%           (Default: [1, 1, 1])
% OUTPUT lidx: [Nx1] int of linear indices w.r.t. a cube of targetCube size
%           containing the relative linear indices of a ball of radius r
%           around a reference point.
%        ballSize: [1x3] int containing the size of the ball in voxels.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('voxelSize','var') || isempty(voxelSize)
    voxelSize = ones(3,1);
end

%construct ball
siz = ceil(r./voxelSize);
[x,y,z] = meshgrid(-siz(1):siz(1),-siz(2):siz(2),-siz(3):siz(3));
h = (voxelSize(1).*x).^2 + (voxelSize(2).*y).^2 + (voxelSize(3).*z).^2 <= r^2;
ballSize = size(h);

%convert ball to linear coordinates in targetCubeSize
[x,y,z] = ind2sub(ballSize,find(h));
lidx = sub2ind(targetCubeSize,x,y,z);
lidx = lidx - lidx(ceil(length(lidx)/2)); %subtract center of ball
end