% script for testing/requerying the ending cases e3a (end attachment to an
% agglo but not to the ending of that agglo)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

%% inspect random e3a cases

p = Gaba.getSegParameters('ex145_ROI2017');
caseDest = load(fullfile(p.agglo.saveFolder, 'attachedEndings_5.0.mat'));
e3aIdx = caseDest.endingCaseDistinctions == 5;
e3aFlightIdx = caseDest.flightsOfEndingCases(e3aIdx);
e3aFlightIdx = cell2mat(e3aFlightIdx);

% I guess this should be fulfilled
assert(all(e3aFlightIdx > 0));
Util.log('Found %d e3a endings.', length(e3aFlightIdx));

% get the tracings
m = load(fullfile(p.agglo.saveFolder, 'caseDistinctions_5.0.mat'));
flightPaths = m.flightPaths;
e3aNodes = flightPaths.ff.nodes(e3aFlightIdx);
e3aNodes = e3aNodes(:);

% random examples to nml
numSamples = 20;
% ridx = randperm(length(e3aNodes), numSamples);
skel = L4.Util.getSkel();
skel.parameters.experiment.name = '2012-09-28_ex145_07x2_20170829_queries';
sampleFiles = flightPaths.ff.filenames(e3aFlightIdx(ridx));
sampleStartNodes = flightPaths.ff.startNode(e3aFlightIdx(ridx));
sampleFiles = cellfun(@(x)fullfile('/gaba', x), sampleFiles, 'uni', 0);
for i = 1:numSamples
    % load flight path nml
    skel = skel.mergeSkels(sampleFiles{i});
    skel.names{i} = sprintf('E3aTracing_%03d', ridx(i));
    
    % mark start node
    idx = find(ismember(skel.nodes{i}(:,1:3), sampleStartNodes{i}, 'rows'));
    if isempty(idx)
        warning('Sample %i start node could not be determined.', i);
    else
        skel.branchpoints = cat(1, skel.branchpoints, ...
            skel.nodesNumDataAll{i}(idx,1));
        [skel.nodesAsStruct{i}(idx).comment] = deal('startNode');
    end
end
skel.write('E3aExamples.nml')