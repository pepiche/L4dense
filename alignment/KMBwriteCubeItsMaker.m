
function KMBwriteCubeItsMaker(C,xyzCs,magIt,thismontage,writeB,c8d,useWriteB)

for i=it([[0;0;0] c8d-1])
    %     if i(1)<46||i(2)<36||i(1)>142||i(2)>164
    %         continue;
    %      end
    %
    xyzcounts=(xyzCs-1).*c8d+i;
    xyzstrs=cell(3,1);
    for dimIt=1:3
        xyz='xyz';
        xyzstrs{dimIt}=sprintf(strcat(xyz(dimIt),'%0.4d'),xyzcounts(dimIt));
    end
    magStr=strcat('mag',num2str(magIt));
    %     if C.boergensFileOrder
    %         thisdir=fullfile(C.cubedir,magStr,xyzstrs{3},xyzstrs{2},xyzstrs{1});
    %     else
    thisdir=fullfile(C.cubedir,magStr,xyzstrs{1},xyzstrs{2},xyzstrs{3});
    %    end
    filename=strcat(thisdir,filesep,filesep,C.cubefn,'_',magStr,'_',xyzstrs{1},'_',xyzstrs{2},'_',xyzstrs{3},'.raw');
    
    lowI=i*C.cubesize+1;
    highI=(i+1)*C.cubesize;
    tempcube=thismontage{floor(log2(magIt)+1)}(lowI(1):highI(1),lowI(2):highI(2),lowI(3):highI(3));
    flag=true;
    if useWriteB
        flag=writeB(i(1)+1,i(2)+1,i(3)+1);
    end
    if (magIt~=3 && magIt~=5)
        flag=ceil(C.procs*C.slPth/(C.cubesize*magIt))>i(3);
    end
    if flag
        if C.makedir
            mkdir(thisdir);
        end
        fid1 = fopen(filename, 'w+');
        %         if C.invertCubes
        %             fwrite(fid1,255-cast(tempcube,'uint8'));
        %         else
        fwrite(fid1,cast(tempcube,'uint8'));
        %         end
        fclose(fid1);
        
        
    end
    
    
end



