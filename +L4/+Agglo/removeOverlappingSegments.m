function agglos = removeOverlappingSegments( agglos, deleteEmpty )
%REMOVEOVERLAPPINGSEGMENTS Remove overlapping segments from agglos.
% A segment is assumed to be overlapping if it is in more than one agglo.
% Note that also within agglos the segment ids must be unique or they will
% be removed.
% INPUT agglos: [Nx1] cell
%           Cell array of unique segment ids or cell array of agglos (i.e.
%           each cell is a cell of segments lists, e.g. when specifying
%           axons and dendrites separately).
%       deleteEmpty: (Optional) logical
%           Delete agglos that are empty after the segment deletion.
%           (Default: true)
% OUTPUT agglos: [Nx1] cell
%           The input agglos with all segment ids that occur in multiple
%           agglos removed.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

toCell = false;
if iscell(agglos{1})
    l = cellfun(@length, agglos);
    agglos = vertcat(agglos{:});
    toCell = true;
end

agglos = agglos(:);

maxSegIds = max(cellfun(@max, agglos));
count = accumarray(cell2mat(agglos), 1);
toDelSegIds = count >= 2;
toDelLUT = false(maxSegIds, 1);
toDelLUT(toDelSegIds) = true;
agglos = cellfun(@(x)x(~toDelLUT(x)), agglos, 'uni', 0);

if ~exist('deleteEmpty', 'var') || deleteEmpty
    toDel = cellfun(@isempty, agglos);
    agglos(toDel) = [];
elseif any(cellfun(@isempty, agglos))
    warning('There are empty agglos.');
end

if toCell
    l = l - accumarray(repelem((1:length(l))', l), toDel);
    agglos = mat2cell(agglos, l, 1);
end

end

