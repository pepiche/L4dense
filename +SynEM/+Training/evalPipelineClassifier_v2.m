function result = evalPipelineClassifier_v2( runFolder, classifierFile, ...
    saveRes, learners )
%EVALPIPELINECLASSIFIER_V2 Evaluate a classifier from a pipeline run on the
% test set v2.
% INPUT runFolder: string
%           Path to trainingPipeline run save folder.
%       classifierFile: string
%           Name of the classifier file in runFolder/classifier/.
%       saveRes: (Optional) logical
%           Flag to save the output. The output file is name
%           runFolder/result_classifierFile.mat
%           (Default: false)
%       learners: (Optional) [Nx1] int
%           Linear indices of the weak learners to use for predictions.
%           (Default: all weak learners)
% OUTPUT result: struct
%           Struct containing the classifier performance results.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

info = Util.runInfo(); %#ok<NASGU>

if ~exist('learners', 'var') || isempty(learners)
    learners = [];
end

m = load(fullfile(runFolder, 'TrainingPipelineInputs.mat'));
options = m.options;

m = load(fullfile(runFolder, 'classifier', classifierFile));
classifier = m.classifier;

% training performance
[X, y] = SynEM.Util.getTrainingDataFrom(fullfile(runFolder, 'train'), ...
    options.labelType, false, options.discardUndecided);
[~, scoresTrain] = classifier.predict(X, learners);
[rpTrain, thres_train, aucTrain] = SynEM.Eval.interfaceRP(y, scoresTrain);

% validation performance
[X_val, y_val] = SynEM.Util.getTrainingDataFrom( ...
    fullfile(runFolder, 'val'), options.labelType, false, ...
    options.discardUndecided);
[~, scoresVal] = classifier.predict(X_val, learners);
[rpVal, thres_val, aucVal] = SynEM.Eval.interfaceRP(y_val, scoresVal);

% load synem test set
tS_synem = SynEM.TestSet.loadInterfaceGT( ...
    '/gaba/u/bstaffle/data/SynEM/data', true, false, 'v3');

% test set performance (SynEMv2 test set)
[tS_v2, labels] = SynEM.TestSetv2.getTestSet( ...
    '/gaba/u/bstaffle/data/SynEMv2/TestSet', true, false);

% get target labels
y = cell(3, 1);
y{1} = double(tS_synem.group);
y{2} = tS_v2.interfaceLabels{1};
y{3} = tS_v2.interfaceLabels{2};

% make target labels unique; note that within tS_v2 they are already unique
minId = max(y{1});
for i = 2:length(y)
    y{i}(y{i} > 0) = y{i}(y{i} > 0) + minId;
end

% load features
toKeep = cell(3, 1);
m = load(fullfile(runFolder, 'test', 'ex145_segNewBig_Cube67.mat'), 'X');
toKeep{1} = tS_synem.toKeep;
X_cube67 = m.X;
m = load(fullfile(runFolder, 'test', 'ex145_ROI2017_Cube1082.mat'), 'X');
toKeep{2} = tS_v2.toKeep{1}(labels{1}.voxelCount > 150);
X_cube1082 = m.X;
m = load(fullfile(runFolder, 'test', 'ex145_ROI2017_Cube1579.mat'), 'X');
toKeep{3} = tS_v2.toKeep{2}(labels{2}.voxelCount > 150);
X_cube1579 = m.X;

% inidivudal test set evaluation
result_67 = classifierPerformance(classifier, X_cube67, y{1}, ...
    toKeep{1}, 0, learners);
result_1082 = classifierPerformance(classifier, X_cube1082, y{2}, ...
    toKeep{2}, tS_v2.numMissedSyn(1), learners);
result_1579 = classifierPerformance(classifier, X_cube1579, y{3}, ...
    toKeep{3}, tS_v2.numMissedSyn(2), learners);

% combined test set evaluation (requires to make cube67 ids unique from
% others first - within the ROI2017 cubes this is already done in the
% loading routine)
result = classifierPerformance( classifier, ...
    {X_cube67, X_cube1082, X_cube1579}, ...
    vertcat(y{:}), vertcat(toKeep{:}), sum(tS_v2.numMissedSyn), learners);
       
% combine results
result.rpTrain = rpTrain;
result.thres_train = thres_train;
result.aucTrain = aucTrain;
result.rpVal = rpVal;
result.thres_val = thres_val;
result.aucVal = aucVal;
result.testCube_67 = result_67;
result.testCube_1082 = result_1082;
result.testCube_1579 = result_1579;

if exist('saveRes', 'var') && ~isempty(saveRes) && saveRes
    save(fullfile(runFolder, 'classifier', ['result_' classifierFile]), ...
        'result', 'info');
end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function result = classifierPerformance(classifier, X, y, toKeep, ...
    numMissed, learners)
scoresTest = zeros(0, 2);
if iscell(X)
    % multiple test sets at once
    for i = 1:length(X)
        [~, tmp] = classifier.predict(X{i}, learners);
        scoresTest = cat(1, scoresTest, reshape(tmp, [], 2));
    end
else
    [~, scoresTest] = classifier.predict(X, learners);
    scoresTest = reshape(scoresTest, [], 2);
end

[rpTest, thres_test, aucTest] = SynEM.Eval.interfaceRP(y, ...
    max(scoresTest(toKeep, :), [], 2), [], numMissed);
result.scoresTest = scoresTest(toKeep, :);
result.rpTest = rpTest;
result.thres_test = thres_test;
result.aucTest = aucTest;
end

