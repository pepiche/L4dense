% detection of exits at the soma
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>


%% load data

p = Gaba.getSegParameters('ex145_ROI2017');

% get somas from whole cell gt for now
somaAgglos = load(fullfile(p.agglo.saveFolder,'center_somas.mat'));
somaAgglos = somaAgglos.result(:,2);

[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
point = segmentMeta.point;
borderSize = borderMeta.borderSize;
clear segmentMeta borderMeta


%% exit detection

distT = 1e3; % nm
probT = 0.98;
exits = L4.Soma.exitDetection(somaAgglos(1), point, ...
    graph.edges, graph.prob, probT, borderSize, [], distT, ...
    p.raw.voxelSize);

%% example to nml

skel = L4.Util.getSkel();
skel = skel.setDescription( ...
    sprintf('Exit detection test with distT %.2f um.', distT./1000));
skel = skel.addTree([], point(somaAgglos{1}, :));
skel = skel.addTree([], exits{1}.points);
skel.write('ExitDetection_SomaAgglo1.nml')