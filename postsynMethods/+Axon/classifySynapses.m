function synapseLabels = classifySynapses(config,synapses,shAgglos)
% find whether a bouton is spine-targeting or not

outDir = config.outDir;

shIds = vertcat(shAgglos{:});

synapsesPost = vertcat(synapses{:,3});

% labels synapses targeting-spines
m1 = max(shIds);
m2 = max(cellfun(@max,synapsesPost));
m = max(m1,m2);
shLUT = false(m,1);
shLUT(shIds) = true;

Util.log('Now calculating synapse labels for spine-targets')
tic;
synapseLabels = cellfun(@(x) any(shLUT(x)),synapsesPost);
toc;
info = Util.runInfo(false);
save(fullfile(outDir,'synapseLabels.mat'),'synapseLabels','info');
end
