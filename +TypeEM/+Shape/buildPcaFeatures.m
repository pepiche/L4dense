function pcaFeat = buildPcaFeatures(pcaVar)
    % FEATPCA Calculates PCA-based features
    %
    % Based on ideas from
    %   Manuel Berning and Thomas Kipf
    
    % largest PCA
    pcaMax = pcaVar(1);
    
    % relative contributions
    pcaFracs = pcaVar ./ sum(pcaVar);
    
    % ratios between PCAs
    pcaRatios = [ ...
        pcaVar(2) / pcaVar(1), ...
        pcaVar(3) / pcaVar(1), ...
        pcaVar(3) / pcaVar(2)];
        
    pcaFeat = struct;
    pcaFeat.max = pcaMax;
    pcaFeat.fracs = pcaFracs;
    pcaFeat.ratios = pcaRatios;
end

