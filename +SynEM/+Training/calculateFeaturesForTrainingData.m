function job = calculateFeaturesForTrainingData( dataFolder, ...
    saveFolder, featureMap, ignoreBorder, cluster, discardUndecided, ...
    shaftTraining )
%CALCULATEFEATURESFORTRAININGDATA Calculate the specified feature map for
%all training cube in dataFolder.
% INPUT dataFolder: string
%           Path to training data folder.
%       saveFolder: string
%           Path to folder where features are saved.
%       featureMap: Interface.FeautreMap object
%       ignoreBorder: (Optional) logical
%           Flat to set the feature map border to zero during calculation
%           (compatibility with old synapse detection data).
%           (Default: false)
%       cluster: (Optional) parallel.cluster object
%           Cluster object used for calculation.
%           (Default: getCluster('cpu') if this function exists)
%       discardUndecided: (Optional) logical
%           Flag indicating that interfaces annotated with undecided are to
%           be discarded.
%           (Default: false)
%       shaftTraining: (Optional) flag
%           Flag to train on shaft synapses only.
%           (Default: false)
% OUTPUT job: job object
%           Job object for feature calculation on workers.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('ignoreBorder','var') || isempty(ignoreBorder)
    ignoreBorder = false;
end
if ~exist('discardUndecided','var') || isempty(discardUndecided)
    discardUndecided = false;
end
if ~exist('shaftTraining','var') || isempty(shaftTraining)
    shaftTraining = false;
end
subvols = find(ismember([40, 80, 160], featureMap.subvolsSize));

s = what(dataFolder);
inputCell = cell(length(s.mat),1);
for i = 1:length(s.mat)
    inputCell{i} = {[SynEM.Util.addFilesep(dataFolder), s.mat{i}], ...
        SynEM.Util.addFilesep(saveFolder), featureMap, ignoreBorder, ...
        subvols, discardUndecided, shaftTraining};
end


try
    if ~exist('cluster', 'var') || isempty(cluster)
        cluster = getCluster('cpu');
    end
    job = startJob(cluster, @jobWrapper, inputCell,0);
catch
    warning(['No cluster object found. ' ...
        ' Calculation is done sequentially.']);
    cellfun(@(x)jobWrapper(x{:}), inputCell);
    job = [];
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function jobWrapper(file, saveFolder, fm, useZeroBorder, ...
    subvols, discardUndecided, shaftTraining)

Util.log('Processing file %s', file);

if strcmp(fm.device, 'gpu')
    selectEmptyGPUDev();
end

%load data
m = load(file);
data = m.data;
raw = single(data.raw);
interfaceLabels = m.interfaceLabels;
undecidedList = m.undecidedList;
metadata = m.metadata;

% add bbox to LoadWKData features
idx = find(cellfun(@(x)isa(x, 'SynEM.Feature.LoadWkData'), ...
    fm.featTexture));
for i = 1:length(idx)
    fm.featTexture{idx(i)}.bbox = metadata.bboxBig;
end

toDel = false(length(interfaceLabels), 1);

% shaft synapse training
if shaftTraining
    Util.log('Training on shaft synapses only.');
    tL = m.typeLabels;
    toDelNonShaftSyns = tL.idx(~ismember(tL.type, {'shaft', 'soma'}));
    toDel(toDelNonShaftSyns) = true;
end

% discard undecided interfaces
if discardUndecided
    Util.log('Discarding undecided interfaces.');
    toDel = toDel | undecidedList;
end

% discard interfaces at border
if isfield(m, 'borderInt')
    Util.log('Discarding interfaces at border.');
    isAtBorder = m.borderInt.isAtBorder;
    toDel = toDel | isAtBorder;
end

% delete toDel interfaces
data.interfaceSurfaceList(toDel) = [];
data.subsegmentsList = cellfun(@(x)x(~toDel,:), ...
    data.subsegmentsList, 'UniformOutput', false);
interfaceLabels(toDel) = [];
undecidedList(toDel) = [];

%apply area threshold and select subvols (required for labels)
areaT = cellfun(@length, data.interfaceSurfaceList) > fm.areaT;
interfaces.surface = data.interfaceSurfaceList(areaT);
interfaces.subseg = cellfun(@(x)x(areaT,:),data.subsegmentsList, ...
    'UniformOutput',false);
interfaces.subseg = interfaces.subseg(subvols);
interfaceLabels = interfaceLabels(areaT);
undecideList = undecidedList(areaT);
clear m

%calculate outputs
X = fm.calculate(interfaces, raw, useZeroBorder);
y = [interfaceLabels == 1; interfaceLabels == 2];
undecided = [undecideList; undecideList];

%save results
if ~exist(saveFolder,'dir')
    mkdir(saveFolder)
end
[~,name] = fileparts(file);
Util.log('Saving result to %s.', [saveFolder, 'Features_', name, '.mat']);
m = matfile([saveFolder, 'Features_', name, '.mat'],'Writable',true);
m.X = X;
m.classLabels = y;
m.featureMap = fm;
m.undecided = undecided;

end
