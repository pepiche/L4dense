function spineDensityMarching(config)
% Description:
% marching sphere along dendrite agglo to find attached spine density

outDir = config.outDir;

m = load(config.param);
p = m.p;
points = Seg.Global.getSegToPointMap(p); 

% load dendrite agglos
Util.log('Loading dendrite agglos');
m=load(config.dendriteFile);
indBigDends = m.indBigDends;
dendrites = Superagglos.getSegIds(m.dendrites);

% spine attachment
Util.log('Collecting spine attachments for indBigDends')
m=load(config.spineFile);
dendAgglos = cellfun(@(x) double(x),m.dendAgglos,'uni',0);
shAgglos = cellfun(@(x) double(x),m.shAgglos,'uni',0);
attached = m.attached;
edges = m.edges;

% keep attached spines
idxKeep = attached~=0;
attached = attached(idxKeep);
shAgglos = shAgglos(idxKeep);
edges = edges(idxKeep);

% find in dendrites the spine attachment segId
spineStartSeg = cell(length(shAgglos),1);

idxInside = ~cellfun(@(x) any(x(:)),edges); % this spine head was part of dendrite
spineStartSeg(idxInside) = num2cell(cellfun(@(x) x(1),shAgglos(idxInside))); % take first sh seg

spineStartSeg(~idxInside) = num2cell(cellfun(@(x) x(find(x,1,'last')),...
    edges(~idxInside))); % find last non-zero entry

% find coms of spineStart segIds
spineStartCoMs = cellfun(@(x) points(x),spineStartSeg);

% assert that spine-head-attachment was run on same version of dendrites
assert(size(dendAgglos,1)==size(dendrites,1));

% find per dendrite spineStarts
[seg,coms]= arrayfun(@(x) spineStartsInThisDendrite(attached==x,...
    spineStartSeg,spineStartCoMs),(1:length(dendAgglos))','uni',0);

dendritesExt = dendAgglos(indBigDends);
seg = seg(indBigDends);
coms = coms(indBigDends);


end
function [seg,coms] = spineStartsInThisDendrite(ind,spineStartSeg,spineStartCoMs)
    seg = spineStartSeg(ind);
    coms = spineStartCoMs(ind);
end