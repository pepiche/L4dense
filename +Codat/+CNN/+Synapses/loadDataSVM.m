function [ raw, target, targetWeights ] = loadDataSVM( path, synWeight, ...
    outputNL, psdOnly )
%LOADDATASVM Load synaptic junctions, vesicle cloud, mito training data.
% INPUT path: string
%           Path to matfile.
%       synWeight: (Optional) double
%           Weights for PSD labels.
%           (Default: 1)
%       outputNL: (Optional) string
%           String to choose target formatting for output non-linearity.
%           Options are 'tanh', 'sigmoid', 'softmax'
%           (Default: 'tanh')
%       psdOnly: (Optional) local
%           Flag indicating to output only the PSD target.
%           (Default: false)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('outputNL', 'var') || isempty(outputNL)
    outputNL = 'tanh';
end
if ~exist('psdOnly', 'var') || isempty(psdOnly)
    psdOnly = false;
end

m = load(path);
raw = (single(m.raw) - 122)./22;
seg = m.seg;

switch outputNL
    case 'tanh'
        target = zeros([size(seg), 3], 'single');
        for i = 1:3
            target(:,:,:,i) = single(seg == i) - single(seg ~= i);
        end
        targetWeights = ones(size(target), 'single');
        if exist('synWeight', 'var') && ~isempty(synWeight)
            targetWeights(:,:,:,1) = synWeight.*single(seg == 1) + ...
                single(seg ~= 1);
        end
        if psdOnly
            target = target(:,:,:,1);
            targetWeights = targetWeights(:,:,:,1);
        end
    case 'sigmoid'
        target = zeros([size(seg), 3], 'single');
        for i = 1:3
            target(:,:,:,i) = single(seg == i);
        end
        targetWeights = ones(size(target), 'single');
        if exist('synWeight', 'var') && ~isempty(synWeight)
            targetWeights(:,:,:,1) = synWeight.*single(seg == 1) + ...
                single(seg ~= 1);
        end
        if psdOnly
            target = target(:,:,:,1);
            targetWeights = targetWeights(:,:,:,1);
        end
    case 'softmax'
        target = zeros([size(seg), 4], 'single');
        for i = 0:3
            target(:,:,:,i + 1) = single(seg == i);
        end
        targetWeights = ones(size(target), 'single');
        if exist('synWeight', 'var') && ~isempty(synWeight)
            targetWeights(:,:,:,2) = synWeight.*single(seg == 1) + ...
                single(seg ~= 1);
        end
        if psdOnly
            target = target(:,:,:,1:2);
            target(:,:,:,1) = 1 - target(:,:,:,2);
            targetWeights = targetWeights(:,:,:,1:2);
        end
end




end
