function result = evalPipelineClassifier( runFolder, classifierFile, saveRes )
%EVALPIPELINECLASSIFIER Evaluate a classifier from a pipeline run.
% INPUT runFolder: string
%           Path to trainingPipeline run save folder.
%       classifierFile: string
%           Name of the classifier file in runFolder/classifier/.
%       saveRes: (Optional) logical
%           Flag to save the output. The output file is name
%           runFolder/result_classifierFile.mat
%           (Default: false)
% OUTPUT result: struct
%           Struct containing the classifier performance results.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

m = load(fullfile(runFolder, 'TrainingPipelineInputs.mat'));
options = m.options;

m = load(fullfile(runFolder, 'classifier', classifierFile));
classifier = m.classifier;

% training performance
[X, y] = SynEM.Util.getTrainingDataFrom(fullfile(runFolder, 'train'), ...
    options.labelType, false, options.discardUndecided);
[~, scoresTrain] = classifier.predict(X);
[rpTrain, thres_train, aucTrain] = SynEM.Eval.interfaceRP(y, scoresTrain);

% validation performance
[X_val, y_val] = SynEM.Util.getTrainingDataFrom( ...
    fullfile(runFolder, 'val'), options.labelType, false, ...
    options.discardUndecided);
[~, scoresVal] = classifier.predict(X_val);
[rpVal, thres_val, aucVal] = SynEM.Eval.interfaceRP(y_val, scoresVal);

% test performance
m = load(fullfile(runFolder, 'classifier', 'Cube67_X.mat'), 'X_test');
X_test = m.X_test;
result = SynEM.Training.evaluateClassifierOnTestSet(X_test, ...
    classifier, classifier.options.fm, '/u/bstaffle/data/SynEM/data', []);
result.rpTrain = rpTrain;
result.thres_train = thres_train;
result.aucTrain = aucTrain;
result.rpVal = rpVal;
result.thres_val = thres_val;
result.aucVal = aucVal;

if exist('saveRes', 'var') && saveRes
    save(fullfile(runFolder, 'classifier', ['result_' classifierFile]), ...
        '-struct', 'result');
end

end

