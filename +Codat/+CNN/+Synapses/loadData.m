function [ raw, target, targetWeights ] = loadData( path, weight, maskR, outputNL )
%LOADDATA Data for tanh output layer where area around psd is masked and
% weighted to balance classes.
% INPUT path: string
%           Path to training data file.
%       weight: (Optional) positive double
%           Relative weight for synaptic class (weights will be normalized
%           to sum to 1).
%       maskR: (Optional) double
%           Radius of the ball around the PSD labels for masking.
%           (Default: 6)
%       outputNL: (Optional) string
%           Specify output non-linearity to adapt target range.
%           'tanh': Target range [-1, 1] (Default)
%           'sigmoid': Target range [0, 1]
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

m = load(path);
raw = (single(m.raw) - 122)./22;
target = single(m.target);

%mask area around psd
targetMask = false(size(target));
if ~exist('maskR','var') || isempty(maskR)
    maskR = 6;
end
if maskR > 0
    for i = 1:size(target,3)
        targetMask(:,:,i) = imdilate(target(:,:,i),ball(maskR,[1 1 2.5]));
    end
end
targetMask(target > 0) = false;

%target weights
if ~exist('weight','var') || isempty(weight)
    weight = numel(target)/sum(target(:));
end
targetWeights = ones(size(target),'single');
targetWeights(target == 1) = weight;
targetWeights(targetMask) = 0;
if ~exist('outputNL','var') || isempty(outputNL) || strcmp(outputNL,'tanh')
    target(target == 0) = -1;
end

end

function h = ball( radius, factor )
%BALL
[x,y,z] = meshgrid(-ceil(radius/factor(1)):ceil(radius/factor(1)), ...
                   -ceil(radius/factor(2)):ceil(radius/factor(2)), ...
                   -ceil(radius/factor(3)):ceil(radius/factor(3)));
h = (factor(1).*x).^2 + (factor(2).*y).^2 + (factor(3).*z).^2 <= radius^2;
end
