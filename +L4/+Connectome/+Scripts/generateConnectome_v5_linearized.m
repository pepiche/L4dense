%% script to generate the connectome for a segmentation v5
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
%         Alessandro Motta <alessandro.motta@brain.mpg.de>

%% Configuration

rootDir = Gaba.getSegParameters('ex145_ROI2017');
rootDir = rootDir.saveFolder;

axonFile = fullfile( ...
    rootDir, 'aggloState', 'axons_19_a_linearized.mat');
dendriteFile = fullfile( ...
    rootDir, 'aggloState', ...
    'dendrites_wholeCells_03_v2_classified.mat');
synFile = fullfile( ...
    rootDir, 'connectomeState', ...
    'SynapseAgglos_v3_ax_spine_clustered_classified.mat');

info = Util.runInfo();


%% Complete configuration

p = Gaba.getSegParameters('ex145_ROI2017');

[~, axName] = fileparts(axonFile);
axName = strrep(axName, '_', '-');

[~, dendName] = fileparts(dendriteFile);
dendName = strrep(dendName, '_', '-');

outFile = fullfile(p.connectome.saveFolder, sprintf( ...
    'connectome_%s_%s_spine-syn-clust.mat', axName, dendName));
outFileSmall = fullfile(p.connectome.saveFolder, sprintf( ...
    'connectome_%s_%s_spine-syn-clust_small.mat', axName, dendName));


%% Loading agglos & synapses

% axons
Util.log('Loading axons.');
p.agglo.axonAggloFile = axonFile;
[axons, ~, axonParentIds] = L4.Axons.getLargeAxons(p, true, true);
assert(~any(cellfun(@isempty, axons)));

% dendrites
Util.log('Loading dendrites and target classes.');
m = load(dendriteFile);

targetClass = m.targetClass;
dendrites = m.dendAgglos;
clear m;

dendriteParentIds = find(targetClass ~= 'Ignore');
dendrites = dendrites(dendriteParentIds);
targetClass = targetClass(dendriteParentIds);

% target classes
idxSoma = targetClass == 'Somata';
idxWC = targetClass == 'WholeCell';
idxAD = targetClass == 'ApicalDendrite';
idxSD = targetClass == 'SmoothDendrite';
idxAIS = targetClass == 'AxonInitialSegment';
idxOther = targetClass == 'OtherDendrite';

% synapses
Util.log('Loading synapses.');
mSyn = load(synFile);
synapses = mSyn.synapses;


%% calculate full connectome

[ syn2Agglo, pre2syn, post2syn, connectome ] = ...
    L4.Synapses.synapsesAggloMapping( synapses, axons, dendrites );
connectomeFull = L4.Connectome.pairwise2Full(connectome, ...
    [length(axons), length(dendrites)]);


%% connectome synapse information

Util.log('Adding synapse meta information.');

% load border meta (must be wrt to edges in graph)
[graph, segmentMeta, borderMeta] = Seg.IO.loadGraph(p, false);
dend = load(p.svg.borderMetaFile, 'borderArea');
borderMeta.borderArea = nan(size(borderMeta.borderSize));
borderMeta.borderArea(~isnan(borderMeta.borderSize)) = dend.borderArea;

connectomeMeta = L4.Connectome.connectomeBorderMeta(connectome, ...
    synapses, borderMeta);


%% generate contactome

Util.log('Calculating contactome.')
contactome = L4.Agglo.findEdgesBetweenAgglos2(axons, dendrites, ...
    graph.edges);
contactomeMeta = L4.Connectome.contactomeBorderMeta(contactome, ...
    borderMeta);


%% target class connectome

Util.log('Target class mapping.');
denClasses = {'soma', 'whole cells', 'apical dendrites', ...
    'smooth dendrite', 'AIS', 'other'};
classConnectome = zeros(length(axons), length(denClasses));
classConnectome(:,1) = sum(connectomeFull(:, idxSoma), 2);
classConnectome(:,2) = sum(connectomeFull(:, idxWC), 2);
classConnectome(:,3) = sum(connectomeFull(:, idxAD), 2);
classConnectome(:,4) = sum(connectomeFull(:, idxSD), 2);
classConnectome(:,5) = sum(connectomeFull(:, idxAIS), 2);
classConnectome(:,6) = sum(connectomeFull(:, idxOther), 2);


%% axon statistics
% taken from connectEM.Connectome.augmentForMoritz

Util.log('Adding axon meta information.');
axonMeta = table;
axonMeta.id = reshape(1:numel(axons), [], 1);
axonMeta.parentId = axonParentIds;
axonMeta.synCount = accumarray( ...
    connectome.edges(:, 1), ...
    cellfun(@numel, connectome.synIdx), ...
   [numel(axons), 1], @sum, 0);
axonMeta.spineSynCount = accumarray( ...
    connectome.edges(:, 1), cellfun(@(ids) ...
    sum(mSyn.isSpineSyn(ids)), connectome.synIdx), ...
   [numel(axons), 1], @sum, 0);


%% dendrite statistics
% added by amotta

Util.log('Adding dendrite meta information.');
denMeta = table;
denMeta.id = reshape(1:numel(dendrites), [], 1);
denMeta.parentId = dendriteParentIds;
denMeta.targetClass = reshape(targetClass, [], 1);
denMeta.synCount = accumarray( ...
    connectome.edges(:, 2), ...
    cellfun(@numel, connectome.synIdx), ...
   [numel(dendrites), 1], @sum, 0);
denMeta.spineSynCount = accumarray( ...
    connectome.edges(:, 2), cellfun(@(ids) ...
    sum(mSyn.isSpineSyn(ids)), connectome.synIdx), ...
   [numel(dendrites), 1], @sum, 0);


%% save results

save(outFile, 'connectome', 'contactome', 'connectomeMeta', ...
    'contactomeMeta', 'axons', 'dendrites', 'classConnectome', ...
    'denClasses', 'axonMeta', 'denMeta', 'info');
Util.protect(outFile);

save(outFileSmall, 'connectome', 'connectomeMeta', 'classConnectome', ...
    'denClasses', 'axonMeta', 'denMeta', 'info');
Util.protect(outFileSmall);
