% run config using the svm predictions and all paper features
% and only the presynaptic process
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% data and save folder
dataFolder = ['/gaba/u/bstaffle/data/SynEM/' ...
    'SynapseDetectionTrainingData_2_typeLabels'];
saveFolder = ['/tmpscratch/bstaffle/data/SynEM/Classifier/' ...
    'runShaft/cnn17_4_synem_preOnly_trShaft'];

% feature map
m = load(['/gaba/u/bstaffle/data/CNN_Training/SVM_CNN/' ...
    'cnn17_SVM_4_large/cnn17_SVM_4_large.mat'], 'cnet');
cnet = m.cnet;
fm = SynEM.getFeatureMap('PaperAndCNN', cnet);
fm.setPreOnly(true);

% training options
options.useGPU = true;
options.shaftSynapseTraining = true;
options.trainPredClassifier = false;

% cluster
cluster = getCluster('gpu');

% start training
SynEM.Training.trainingPipeline(dataFolder, saveFolder, fm, [], [], ...
    options, cluster);
