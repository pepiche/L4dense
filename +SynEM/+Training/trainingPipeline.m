function trainingPipeline( dataFolder, saveFolder, fm, pSegNewBig, ...
    pROI2017, options, cluster )
%TRAININGPIPELINE Training pipeline for SynEM classifiers.
% INPUT dataFolder: string
%           Path to training data folder.
%       saveFolder: string
%           Path to folder where features are saved.
%       featureMap: Interface.FeatureMap object
%       pSegNewBig, pROI2017: (Optional) struct
%           Segmentation parameter struct used to load the information for
%           cube 67 (test cube in segNewBig) and cube 1082, 1579 (test
%           cubes in ROI2017).
%           (Default: Gaba.getSegParameters for the corresponding
%           segmentation).
%       options: (Optional) Struct
%           Struct with further options
%           'labelType': string
%               See 'type' in SynEM.Util.getTrainingDataFrom.
%               (Default: 'direction')
%           'discardUndecided': logical
%               Discard interfaces marked as undecided during training.
%               see also SynEM.Training.calculateFeaturesForTrainingData
%               (Default: false)
%           'useGPU': logical
%               Flag indicating that the training feature calculation
%               should be done on GPU.
%           'ensembleArgs: [2Nx1] cell
%               Cell array of name value pairs for
%               SynEM.Classifier.BoostedEnsemble.train.
%           'skipFeatCalc': logical
%               Skip the feature calculation and only use the
%               features found in the 'test' and 'val' subfolders.
%               (Default: false)
%           'className': string
%               Filename for the classifier output file (without .mat
%               ending).
%               (Default: LogitBoost)
%           'trainPredClassifier': logical
%               Flag indicating that also a classifier only on the relevant
%               features should be trained.
%               (Default: true)
%           'overwrite': logical
%               Overwrite an existing training run in the output folder.
%               Existing checkes whether the TrainingPipelineInputs.mat
%               file exists in the output folder.
%               (Default: false)
%           'loadInputParameters': logical
%               Flag to load the TrainingPipelineInputs.mat from a previous
%               run. If true the only the saveFolder has to be specified.
%               Everything except the options are then replaced by the
%               content of 'TrainingPipelineInputs.mat' from the
%               saveFolder.
%               (Default: false)
%           'shaftSynapseTraining': logical
%               Flag to train only on shaft synapses. Requires typeLabels
%               in the training cubes.
%               (see e.g. SynEM.TrainingData.addTypeAnnotationsToCubes)
%               (Default: false)
%       cluster: (Optional) parallel.cluster object
%           The cluster object used for parallel computation.
%           (Default: getCluster('cpu') or local computation if the former
%           does not exist)
% see also SynEM.Training.calculateFeaturesForTrainingData
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

%store run info
info = Util.runInfo(false);

% seg parameter structs
if ~exist('pSegNewBig', 'var') || isempty(pSegNewBig)
    pSegNewBig = Gaba.getSegParameters('ex145_segNewBig');
end
if ~exist('pROI2017', 'var') || isempty(pROI2017)
    pROI2017 = Gaba.getSegParameters('ex145_ROI2017');
end

%options
if ~exist('options', 'var') || isempty(options)
    options = struct;
end
optDef = defaultOptions();
options = Util.setUserOptions(optDef, options);

%load previous input parameters if specified
inputsFile = fullfile(saveFolder, 'TrainingPipelineInputs.mat');
if options.loadInputParameters
    m = load(inputsFile);
    dataFolder = m.dataFolder;
    fm = m.fm;
    fm.setSelectedFeat(); %set to all features
    pSegNewBig = m.pSegNewBig;
    pROI2017 = m.pROI2017;
end

dataFolder = SynEM.Util.addFilesep(dataFolder);
Util.log('Loading data from %s.', dataFolder);
saveFolder = SynEM.Util.addFilesep(saveFolder);
Util.log('Results are stored in %s.', saveFolder);

if ~exist(saveFolder,'dir')
    mkdir(saveFolder);
end

%store all inputs for reproducability
if ~exist(inputsFile, 'file') || options.overwrite
    Util.log('Pipeline run parameters are stored in %s.', ...
        fullfile(saveFolder, 'TrainingPipelineInputs.mat'));
    Util.save(fullfile(saveFolder, 'TrainingPipelineInputs.mat'), ...
        dataFolder, saveFolder, fm, pSegNewBig, pROI2017, info, options);
else
    error('Training run already exists in folder %s.', saveFolder);
end

% feature calculation
if ~options.skipFeatCalc
    Util.log('Calculating features.');

    if options.useGPU
        fm.useDevice('gpu');
    end

    if exist('cluster', 'var') && ~isempty(cluster)
        job = SynEM.Training.calculateFeaturesForTrainingData( ...
            dataFolder, saveFolder, fm, true, cluster, false, ...
            options.shaftSynapseTraining);
        jobTest = SynEM.Training.calculateFeaturesForTestData( ...
            saveFolder, pSegNewBig, pROI2017, fm, cluster);
    else
        job = SynEM.Training.calculateFeaturesForTrainingData( ...
            dataFolder, saveFolder, fm, true, [], false, ...
            options.shaftSynapseTraining);
        jobTest = SynEM.Training.calculateFeaturesForTestData(...
            saveFolder, pSegNewBig, pROI2017, fm);
    end

    if ~isempty(job) % if calculation was done on cluster
        Util.log('Waiting for job %d results.', job.Id);
        wait(job);
        errIdx = Cluster.getIdxOfTasksWithError(job);
        if ~isempty(errIdx)
            error(['The feature calculation job with id %d has errors.' ...
                ' Aborting training pipeline ...'], job.Id);
        end
    end
end
SynEM.Util.groupTrainingData(saveFolder);

% classifier training
Util.log('Training classifier.');
[X, y] = SynEM.Util.getTrainingDataFrom([saveFolder, 'train'], ...
    options.labelType, false, options.discardUndecided);

if ~isempty(options.ensembleArgs)
    classifier = SynEM.Classifier.BoostedEnsemble.train(X, y, ...
        options.ensembleArgs{:});
else
    classifier = SynEM.Classifier.BoostedEnsemble.train(X, y);
end
classifier.options.fm = fm;

% classifier saving
Util.log('Saving classifier to %s.', [saveFolder, 'classifier']);
if ~exist([saveFolder, 'classifier'], 'dir')
    mkdir([saveFolder, 'classifier']);
end
Util.save(fullfile(saveFolder, 'classifier', ...
    [options.className 'Full.mat']), ...
    classifier);
resubL = classifier.resubLoss();
classifier = compact(classifier);
classifier = classifier.calculatePredVar();
Util.save(fullfile(saveFolder, 'classifier', ...
    [options.className '.mat']), ...
    classifier);

% classifier evaluation
if exist('jobTest', 'var')
    arrayfun(@wait, jobTest);
end
Util.log('Evaluating validation set performance.');
result = SynEM.Training.evalPipelineClassifier_v2(saveFolder, ...
    options.className, false);
result.resubL = resubL;
outfile = fullfile(saveFolder, 'classifier', ...
    sprintf('result_%s', options.className));
Util.save(outfile, result);
Util.log('Classifier performance saved to %s.', outfile);

% training prediction classifier
if options.trainPredClassifier
    Util.log('Training prediction classifier.');
    imp = classifier.ens.predictorImportance();
    idx = fm.setSelectedFeat(imp > 0);
    if ~isempty(options.ensembleArgs)
        classifierPred = SynEM.Classifier.BoostedEnsemble.train( ...
            X(:,idx), y, options.ensembleArgs{:});
    else
        classifierPred = SynEM.Classifier.BoostedEnsemble.train( ...
            X(:,idx), y);
    end
    classifierPred.options.fm = fm;
    classifierPred.options.imp = imp;

    classifierPred = classifierPred.compact();

    savePath = fullfile(saveFolder, 'classifier', ...
        [options.className 'Pred.mat']);
    Util.log('Saving prediction classifier to %s.', savePath);
    Util.save(savePath, classifierPred);
    fm.setSelectedFeat(true(fm.numFeatures, 1));
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function options = defaultOptions(options)
options.labelType = 'direction';
options.discardUndecided = false;
options.useGPU = false;
options.ensembleArgs = [];
options.overwrite = false;
options.loadInputParameters = false;
options.skipFeatCalc = false;
options.className = 'LogitBoost';
options.trainPredClassifier = true;
options.shaftSynapseTraining = false;
end
