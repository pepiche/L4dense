% run config using the svm predictions and all paper features
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

% data and save folder
dataFolder = ['/gaba/u/bstaffle/data/SynEM/' ...
    'SynapseDetectionTrainingData_2_typeLabels'];
saveFolder = ['/tmpscratch/bstaffle/data/SynEM/Classifier/' ...
    'runShaft/cnn17_4_synem_trShaft'];

% feature map
m = load(['/gaba/u/bstaffle/data/CNN_Training/SVM_CNN/' ...
    'cnn17_SVM_4_large/cnn17_SVM_4_large.mat'], 'cnet');
cnet = m.cnet;
fm = SynEM.getFeatureMap('PaperAndCNN', cnet);


% training options
options.useGPU = true;
options.shaftSynapseTraining = true;

% cluster
cluster = getCluster('gpu');

% start training
SynEM.Training.trainingPipeline(dataFolder, saveFolder, fm, [], [], ...
    options, cluster);
