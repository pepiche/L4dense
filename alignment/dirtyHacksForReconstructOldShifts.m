%% Windows
C.origin='D:\Drosophila\PartData';

C.originRapid='D:\Drosophila\PartData';
ij=ImageM('C:\Program Files (x86)\ImageJ\ij.jar');
C.matdir=['D:\Drosophila\mat\' C.cubefn '\'];
C.cubedir=['D:\Drosophila\cubes\' C.cubefn '\'];

mkdir(C.cubedir);

%% Linux
C.origin='/zdata/kevin';
C.matdir=['/zdata/kevin/mat/' C.cubefn '/'];
mkdir(C.matdir);
C.cubedir=fullfile('/zdata/kevin/cubes',C.cubefn);

%% GBM
C.origin='H:\';
%ij=ImageM('C:\Program Files (x86)\ImageJ\ij.jar');
C.matdir=['F:\Drosophila\mat\' C.cubefn '\'];
C.cubedir=['F:\Drosophila\cubes\' C.cubefn '\'];
C.originRapid='F:\Drosophila';
mkdir(C.cubedir);
mkdir(C.matdir);



%% C
C.knumberstr='C80_11';
C.datestr='2012-06-26';
C.knumberstrshort='C80_11';
C.slPth=8;
C.M=9;
C.N=13;
C.xcorrmax=512;


C.xcorr_size=[1024 192;192 1024;C.xcorrmax C.xcorrmax];
C.cubesize=128;
C.cubefn = strcat(C.datestr,'_',C.knumberstr);
C.maxMagMultXY=[8*21 8*24];
C.maxMagMultXYHigh=[1 1];
C.maxMagHigh=8;
C.invertCubes=false;
C.writeCrop=0;
C.cacheSize=128;
C.readRaw=false;
C.pixelSize=[12 12 24];
C.allweightsReset=false;
C.boergensFileOrder=false;
C.adaptContrast=1.9;
%% all fns
all_fns=KMBmakeFileList(C,C.origin,'.tif');
% all_fns(1:3,:,:)=cell(3,13,7321);
% all_fns(:,1:3,:)=cell(9,3,7321);
% all_fns(8:end,:,:)=cell(2,13,7321);
% all_fns(:,10:end,:)=cell(9,4,7321);


        C.dims=size(KMBreadTif(all_fns{5,5,690*8})');


C.P=size(all_fns,3)-1;
C.procs=C.P/C.slPth;
%% Start jobs
jm = findResource('scheduler', 'configuration', 'FermatCPU');
job = createJob(jm, 'configuration', 'FermatCPU');
%for currentThread=1:C.procs
for currentThread=560:700 
zzI=(currentThread-1)*C.slPth+1:currentThread*C.slPth+1;

    inputargs = {C,currentThread,all_fns(:,:,zzI)};
    createTask(job, @KMBmeasureShifts, 0, inputargs, 'configuration', 'FermatCPU');
end
submit(job);
parfor ii=560:700
    KMBmeasureShifts(C,ii,all_fns(:,:,(ii-1)*C.slPth+1:ii*C.slPth+1))
end

%KMBparforHelper(true,@(ii)KMBmeasureShifts(C,ii,all_fns(:,:,(currentThread-1)*C.slPth+1:currentThread*C.slPth+1)),C.procs);
%currentThread=573;zzI=(currentThread-1)*C.slPth+1:currentThread*C.slPth+1;KMBmeasureShifts(C,currentThread,all_fns(:,:,zzI));


ttemp=KMBmakeLS(C);
C.minis=[Inf Inf];
C.maxis=[-Inf -Inf];
it=1;
mynew=[0 0];
for count = 1:3816
    this_slicecount = count;
    for imgcount = 1:C.M*C.N
        thisimg = (this_slicecount-1)*C.M*C.N+imgcount;
        xyIts = [ mod(imgcount-1,C.M) floor((imgcount-1)/C.M)]+1;
        thisshifts = ttemp.shifts(thisimg,:);
        xycoords = C.dims.*(xyIts-1)+fix(thisshifts)+1;
        xycoords=ttemp.R(thisimg,2:3);
       % mynew(it,:)=xycoords;
        it=it+1;
        if size(at(all_fns,[xyIts';count]),1)>0
            C.minis=min([C.minis;xycoords]);
            C.maxis=max([C.maxis;xycoords+C.dims-1]);
        end
        
    end
end
ttemp.shiftsN=[ttemp.shifts(:,1)-C.minis(1) ttemp.shifts(:,2)-C.minis(2)]+1;
delete(strcat(C.matdir,'*Made.mat'));

multA=C.cubesize*C.maxMagMultXY; %defines the size of chunks processed from one subsubthread
multB=C.cubesize*C.maxMagMultXY.*C.maxMagMultXYHigh;  %same thing for high writer
C.montagewh=ceil((C.maxis-C.minis+1)./(multB)).*multB;
C.outA=[1 1;1 1;1 ceil(C.P/(C.cubesize))];
C.inA=[[1;1], C.montagewh'./multA'];
C.outB=[1 1;1 1;1 ceil(C.P/(C.cubesize*C.maxMagHigh))];
C.inB=[[1;1], C.montagewh'./multB'];
for magItI=0:log2(C.maxMagHigh)
    magIt=2^magItI;
    if magIt==1
        Lmontagewh=C.montagewh;
        montagedepth= C.outA(3,2)*C.cubesize;
    else
        Lmontagewh= C.montagewh/magIt;
        montagedepth= C.outB(3,2)*C.cubesize*C.maxMagHigh/magIt;
    end
    
    KNOSSOS_writeKconfFile(strcat(C.cubedir,filesep,'mag',num2str(magIt),filesep),strcat(C.cubefn,'_mag',num2str(magIt)),[Lmontagewh,montagedepth],C.pixelSize*magIt,magIt);
end

ix1=it(C.outA);

%KMBparforHelper(false,@(thII)KMBwriteCubes3(all_fns,thII,C,ttemp),size(ix1,2));
for i=1:size(ix1,2)
    KMBwriteCubesRapid(all_fns,i,C,ttemp);
end

ix2=it(C.outB);

%KMBparforHelper(false,@(thII)KMBwriteCubesHigh(thII,C),size(ix2,2));
KMBparforHelper(false,@(thII)KMBwriteCubesHighRapid(thII,C),size(ix2,2));
