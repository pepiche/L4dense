function skel = aggloHighProbEdges2Nml( agglo, segCom, edges, prob, ...
    probT, skel, borderSize, bsT )
%AGGLOHIGHPROBEDGES2NML Write in agglo with all high-propability edges to
% nml for inspection.
% INPUT agglo: [Nx1] cell
%           List of segment ids that correspond to one agglomerate.
%       segCom: [Nx3] double
%           The center of mass list for segments.
%       edges: [Nx2] int
%           Global edge list.
%       prob: [Nx1] double
%           Global neurite continuity probability list.
%       probT: double
%           Minimal threshold on continuity probability to keep an edge.
%       skel: (Optional) skeleton object
%           Skeleton object to which the agglos are added.
%           (Default: new skeleton object is created)
%       borderSize: (Optional) [Nx1] double
%           Border size for each edge. If not supplied then no thresholding
%           on border size will be done.
%       bsT: (Optional) double
%           Minimal border size to keep an edge.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('skel', 'var') || isempty(skel)
    skel = L4.Util.getSkel();
end

toKeep = prob >= probT;

if exist('borderSize', 'var') && exist('bsT', 'var')
    toKeep = toKeep & (borderSize >= bsT);
end

edges = edges(toKeep, :);

for i = 1:length(agglo)
    aggloEdgeIdx = all(ismember(edges, agglo{i}), 2);
    [~, ~, mapping] = Util.renumber(agglo{i});
    thisEdges = mapping(edges(aggloEdgeIdx, :));
    skel = skel.addTree(sprintf('Agglo_%02d', i), segCom(agglo{i}, :), ...
        thisEdges);
end

end

